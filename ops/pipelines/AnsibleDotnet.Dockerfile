# Image with Ansible, Dotnet SDK and Node 12

FROM willhallonline/ansible:2.9-ubuntu-20.04
RUN apt-get update
RUN apt -y install wget rsync curl dirmngr apt-transport-https lsb-release ca-certificates

# Install Dotnet SDK
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y tzdata
RUN wget -q https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN dpkg -i packages-microsoft-prod.deb
RUN apt -y -f install software-properties-common
RUN add-apt-repository universe
RUN apt-get update
RUN apt-get -y install dotnet-sdk-3.1

# Install Node
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash
RUN apt-get -y install nodejs
