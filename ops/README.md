This folder contains [Ansible](https://www.ansible.com/) assets responsible for provisioning hosts and deploying this application.

## Setup
1. Procure access to Ubuntu 20.04 host(s) which will be used to host this application.
2. Setup DNS records to point to these host(s).

## Usage
From the root of this repository, run one of the following commands:

### Provision
Ansible playbook for provision hosts in ops/inventory.yml file.
This prepares the hosts to receive deployments by doing the following:
  - Install Nginx
  - Generate a SSL certificate from [Let's Encrypt](https://letsencrypt.org/) and configure Nginx to use it
  - Install .Net Core
  - Install Supervisor (will run/manage the ASP.NET app)
  - Install PostgreSQL
  - Setup a cron job to automatically backup the PostgreSQL database, compress it, and upload it to S3.
  - Setup UFW (firewall) to lock everything down except inbound SSH and web traffic
  - Create a deploy user, directory for deployments and configure Nginx to serve from this directory

#### Stage
Provide values to overwrite:
`ansible-playbook -l stage -i ./ops/inventory.yml ./ops/provision.yml -e db_password=super!secret -e smtp_password=super!secret`

#### Prod
Provide values to overwrite:
`ansible-playbook -l production -i ./ops/inventory.yml ./ops/provision.yml -e db_password=super!secret -e smtp_password=super!secret -e s3_key=S3KEYIE7C64DHJQ -e s3_secret=S3SecretCoGocK6Y2Q1o98+qSUkWn7aS1MK5rzv`

### Deploy
GitLab CI config have a trigger for executing the script. `gitlab-ci.yml` and `./ops/deploy.yml`

## Pipeline dockerfiles
There are Dockerfiles with hosted images in GitLab used in the pipeline run in GitLab CI.

### Image with Ansible, Dotnet and Node for building and deploying the app
`docker build . -f .\ops\pipelines\AnsibleDotnet.Dockerfile -t registry.gitlab.com/autohub/autohub-web/ansible-dotnet:v1.0`
### Image with Dotnet and SonarCloud for static code analyze
`docker build . -f .\ops\pipelines\SonarDotnet.Dockerfile -t registry.gitlab.com/autohub/autohub-web/sonar-dotnet:v1.0`

## Notes
 - The deploy.yml and provision.yml playbooks were written against and tested on Ubuntu 20.04.
 - The [Ansible Best Practices](http://docs.ansible.com/ansible/playbooks_best_practices.html) document demonstrates using /groups_vars/... for application environment variables (i.e. production / staging) and /group_vars/all for global variables.  However, we are using inventory group variables, all contained within the inventory file (inventory.yml) to define environment and global variables.  Because of this, all the variables are in a single location and easily managed.
