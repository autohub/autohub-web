# AutoHub - Virtual vehicle history manager

[![Production](https://img.shields.io/website?label=Production&url=https%3A%2F%2Fautohub.bg)](https://autohub.bg)
[![Staging](https://img.shields.io/website?label=Staging&url=https%3A%2F%2Fpi.kardzali.ddns.bulsat.com/)](https://pi.kardzali.ddns.bulsat.com/)
[![pipeline status](https://gitlab.com/autohub/autohub-web/badges/master/pipeline.svg)](https://gitlab.com/autohub/autohub-web/commits/master)
[![coverage report](https://gitlab.com/autohub/autohub-web/badges/master/coverage.svg)](https://gitlab.com/autohub/autohub-web/commits/master)
[![API Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=ah.api&metric=alert_status)](https://sonarcloud.io/dashboard?id=ah.api)
[![Client Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=ah.client.react&metric=alert_status)](https://sonarcloud.io/dashboard?id=ah.client.react)
[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-908a85?logo=gitpod)](https://gitpod.io#https://gitlab.com/autohub/autohub-web/)


## Overview of Stack
### Server
   - ASP.NET Core 3.1
   - PostgreSQL 12
   - Entity Framework Core w/ EF Migrations
   - JSON Web Token (JWT) authorization
   - Docker used for development PostgreSQL database and MailCatcher server
   - Hangfire for scheduling jobs
   - RazorLight as a template engine for emails
   - Sentry backend handler for catching errors
   - Health endpoint at `/api/health`

### Client
   - React
   - TypeScript
   - Webpack for asset bundling and HMR (Hot Module Replacement)
   - CSS Modules
   - Fetch API for REST requests
   - Semantic UI React
   - Nivo components
   - Jest sonar for SonarCloud analysis
   - I18Next for Localization
   - Sentry frontend handler for catching errors

### Testing
   - xUnit for .NET Core
   - Coverlet for code coverage
   - SonarCloud check
   - Enzyme for React
   - MailCatcher for development email delivery

### DevOps
   - Ansible playbook for provisioning (Nginx reverse proxy, SSL via Let's Encrypt, PostgreSQL backups to S3)
   - Ansible playbook for deployment
   - Usage of Dockerfiles for Ansible Deploy env and for Sonar Dotnet env

## Setup

[![Open in Gitpod](https://gitpod.io/button/open-in-gitpod.svg)](https://gitpod.io#https://gitlab.com/autohub/autohub-web/)

1. Install the following:
   - [.NET Core 3.1](https://www.microsoft.com/net/core)
   - [Node.js >= v8](https://nodejs.org/en/download/)
   - [Ansible >= 2.6](http://docs.ansible.com/ansible/intro_installation.html)
   - [Docker](https://docs.docker.com/engine/installation/)
2. Generate dev certificate `dotnet dev-certs https`. If Win or Mac add `--trust`
2.1. If Unix machine export the cert and run `certutil -d sql:$HOME/.pki/nssdb -A -t "P,," -n localhost -i localhost`
3. Run `npm install && npm start`
4. Open browser and navigate to [https://localhost:5001](https://localhost:5001).

## Scripts

### `npm install`

When first cloning the repo or adding new dependencies, run this command.  This will:

- Install Node dependencies from package.json
- Install .NET Core dependencies from api/api.csproj and api.test.unit/api.test.unit.csproj (using dotnet restore)

### `npm start`

To start the app for development, run this command.  This will:

- Run `docker-compose up` to ensure the PostgreSQL and MailCatcher Docker images are up and running
- Run dotnet watch run which will build the app (if changed), watch for changes and start the web server on http://localhost:5000
- Run Webpack dev middleware with HMR via [ASP.NET JavaScriptServices](https://github.com/aspnet/JavaScriptServices)

### `npm run migrate`

After making changes to Entity Framework models in `api/Models/`, run this command to generate and run a migration on the database.  A timestamp will be used for the migration name.

### `npm test`

This will run the xUnit tests in api.test.unit/, api.test.integration and the Mocha/Enzyme tests in client-react.test/.

## CI/CD

Gitlab config for build, test and deploy the app.
Ansible for provision the app. Check `/ops/README.md` for more info.
Ansible for deploy the app to environment. Triggered by CI. See `/ops/README.md` for more.

## Development Email Delivery

This project includes a [MailCatcher](https://mailcatcher.me/) Docker image so that when email is sent during development (i.e. new user registration), it can be viewed
in the MailCacher web interface at [http://localhost:1080/](http://localhost:1080/).

## Visual Studio Code config

This project has [Visual Studio Code](https://code.visualstudio.com/) tasks and debugger launch config located in .vscode/.

### Tasks

- **Command+Shift+B** - Runs the "build" task which builds the api/ project
- **Command+Shift+T** - Runs the "test" task which runs the xUnit tests in api.test.unit/ and Mocha/Enzyme tests in client-react.test/.

### Debug Launcher

With the following debugger launch configs, you can set breakpoints in api/ or the the Mocha tests in client-react.test/ and have full debugging support.

- **Debug api/ (server)** - Runs the vscode debugger (breakpoints) on the api/ .NET Core app
- **Debug client-react.test/ (Mocha tests)** - Runs the vscode debugger on the client-react.test/ Mocha tests
