import { demoData } from "./demoData";
import {
    isExpiredByMilage,
    isExpiredByDate,
} from "../client-react/services/RemindableVehicleHistory";
import moment = require("moment");

describe("RemindableVehicleHistory", () => {
    describe("isExpiredByMilage", () => {
        it("should return false when isRemindableByMilage is false", () => {
            const vh = { ...demoData[0] };
            vh.isRemindableByMilage = false;
            const result = isExpiredByMilage(vh);
            expect(result).toBeFalsy();
        });
        it("should return true when expiresOnMilage is lower then currentMilage", () => {
            const vh = { ...demoData[0] };
            vh.isRemindableByMilage = true;
            vh.expiresOnMilage = 169999;
            const result = isExpiredByMilage(vh);
            expect(result).toBeTruthy();
        });
        it("should return false when expiresOnMilage is bigger then currentMilage", () => {
            const vh = { ...demoData[0] };
            vh.expiresOnMilage = 270001;
            const result = isExpiredByMilage(vh);
            expect(result).toBeFalsy();
        });
        it("should return false when expiresOnMilage is equal to currentMilage", () => {
            const vh = { ...demoData[0] };
            vh.isRemindableByMilage = true;
            vh.expiresOnMilage = 270000;
            const result = isExpiredByMilage(vh);
            expect(result).toBeFalsy();
        });
    });
    describe("isExpiredByDate", () => {
        it("sould return false when isRemindableByDate is false", () => {
            const vh = { ...demoData[0] };
            vh.isRemindableByDate = false;
            const result = isExpiredByDate(vh);
            expect(result).toBeFalsy();
        });
        it("sould return true if expiresOn is before today", () => {
            const vh = { ...demoData[0] };
            vh.isRemindableByDate = true;
            vh.expiresOn = moment.utc().local().subtract(1, "day");
            const result = isExpiredByDate(vh);
            expect(result).toBeTruthy();
        });
        it("sould return false if expiresOn is today", () => {
            const vh = { ...demoData[0] };
            vh.expiresOn = moment.utc().local().subtract(5, "hours");
            const result = isExpiredByDate(vh);
            expect(result).toBeFalsy();
        });
        it("sould return false if expiresOn is after today", () => {
            const vh = { ...demoData[0] };
            vh.expiresOn = moment.utc().local().add(5, "hours");
            const result = isExpiredByDate(vh);
            expect(result).toBeFalsy();
        });
    });
});
