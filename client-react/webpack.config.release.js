var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

var path = require('path');

var config = {
    mode: 'production',
    module: {
        rules: [
            // Use react-hot for HMR and then ts-loader to transpile TS (pass path to tsconfig because it is not in root (cwd) path)
            { test: /\.ts(x?)$/, loaders: ['ts-loader'] },
            {
                test: /\.css$/i,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
            {
                test: /\.(jpg|jpeg|png|woff|woff2|eot|ttf|svg)$/,
                loader: 'url-loader?limit=100000',
                options: {
                    esModule: false,
                },
            }
        ]
    },
    externals: {
        'react': 'React',
        'react-dom': 'ReactDOM'
    },
    devtool: '',
    optimization: {
        minimize: true,
        minimizer: [
            new TerserJSPlugin({}),
            new OptimizeCSSAssetsPlugin({
                cssProcessorOptions: { discardComments: { removeAll: true } },
                canPrint: true
            })
        ],
    },
    plugins: [
        // Use only when cdn is false
        new webpack.ProvidePlugin({
            "React": "react",
        }),
        new HtmlWebpackPlugin({
            release: true,
            template: path.join(__dirname, 'index.ejs'),
            useCdn: true,
            minify: true
        }),
        new MiniCssExtractPlugin({ filename: "[name].css" }),
        new webpack.DefinePlugin({
            'process.env.ASPNETCORE_ENVIRONMENT': JSON.stringify(process.env.ASPNETCORE_ENVIRONMENT),
            'process.env.APP_VERSION': JSON.stringify(process.env.CI_COMMIT_TAG ||  process.env.CI_COMMIT_BRANCH)
        }),
    ]
};

module.exports = config;
