// Polyfills
import 'whatwg-fetch';
import './polyfills/object-assign';
import './polyfills/array-find';
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { BrowserRouter as Router } from 'react-router-dom';
import Routes from './components/Routes';
import * as moment from 'moment';
import * as Sentry from '@sentry/browser';
import { hotjar } from 'react-hotjar';
// import MessengerCustomerChat from 'react-messenger-customer-chat';
import detector from "i18next-browser-languagedetector";
import translationBG from './locales/bg/translation.json';
import translationEN from './locales/en/translation.json';
import i18n from "i18next";
import { initReactI18next } from "react-i18next";

// Styles
import '../node_modules/semantic-ui-css/semantic.min.css';
import './styles/global.css';

var isDevEnv = process.env.ASPNETCORE_ENVIRONMENT === 'Development';
if (!isDevEnv) {
    const dsn = "https://d1d6ff7aad6947a8a37bd1e1160434e4@sentry.io/1841121";
    const hotjarId = 1596029;
    const hotjarSnippetVersion = 6;

    Sentry.init({ dsn: dsn });
    hotjar.initialize(hotjarId, hotjarSnippetVersion);
}

const resources = {
    en: {
        translation: translationEN
    },
    bg: {
        translation: translationBG
    }
};

i18n
    .use(detector)
    .use(initReactI18next)
    .init({
        resources,
        fallbackLng: ["en", "bg"],
        interpolation: {
            escapeValue: false
        }
    });


ReactDOM.render([
    <Router>
        <Routes />
    </Router>,
    // <MessengerCustomerChat
    //     pageId="101525074675897"
    //     appId="556710851574675"
    //     htmlRef={window.location.pathname}
    //     themeColor="#1b2a31"
    //     loggedInGreeting="Здравейте! Как бихме могли да ви помогнем?"
    //     loggedOutGreeting="Здравейте! Как бихме могли да ви помогнем?"
    // />
],
    document.getElementById("app")
);

// Allow Hot Module Reloading
declare var module: any;
if (module.hot) {
    module.hot.accept();
}

moment.locale(i18n.language);
