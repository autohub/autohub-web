import * as lscache from 'lscache';

export default class Auth {
    static ONE_WEEK_IN_MINUTES: number = 10080;

    static getToken() {
        const user = lscache.get("user");
        return !!user && user.token;
    }

    static getUser() {
        return lscache.get("user")
    }

    static removeUser(): void {
        lscache.remove("user");
    }

    static setUser(user: any) {
        lscache.set("user", user, this.ONE_WEEK_IN_MINUTES);
    }
}
