import VehicleHistoryService from '../services/VehicleHistory';
import { IRemindableVehicleHistory } from '../typings/RemindableVehicleHistories';

let vehicleHistoryService = new VehicleHistoryService();

export const updateRenew = (vh: IRemindableVehicleHistory, history: any) => {
    vehicleHistoryService.updateRenew(vh).then(() => {
        history.push('/vehicles/history/:type/add/:id'
            .replace(":id", vh.vehicleId.toString())
            .replace(":type", vh.type.toString()));
    });
}
