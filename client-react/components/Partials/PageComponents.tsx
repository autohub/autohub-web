import * as React from "react";
import { Header, Button, Icon, Divider, Loader } from 'semantic-ui-react'
import Helmet from 'react-helmet';
import { withRouter, RouteComponentProps } from "react-router";
import { useTranslation } from "react-i18next";

export const PageHeader = (props: any) => {
    return <div style={{ display: 'inline' }}>
        <Header as='h1'>{props.headerText}</Header>
        <Button icon onClick={() => props.history.goBack()}>
            <Icon name="arrow left" />
        </Button>
    </div>;
};

export const FormActionsButtons = ({ isLoading }: any, props: any) => {
    const { t } = useTranslation();

    return <div>
        <Divider hidden />
        <Button.Group>
            <Button loading={isLoading} positive size='large'>
                <Icon name='check'></Icon>
                {t("FormSubmit")}
            </Button>
            <Button size='large' onClick={() => props.history.goBack()}>
                <Icon name='remove'></Icon>
                {t("FormCancel")}
            </Button>
        </Button.Group>
    </div>
}

export const TitleComponent = ({ title }: any) => {
    const { t } = useTranslation();
    title = t(title);
    const defaultTitle = t("Welcome-Message");
    let suffix = 'AutoHub.bg';
    return (
        <Helmet>
            <title>{title ? title + ' - ' + suffix : defaultTitle}</title>
        </Helmet>
    );
};

export const withTitle = ({ component: Component, title }: any) => {
    const Title = (props: any) => {
        return (
            <React.Fragment>
                <TitleComponent title={title} />
                <Component {...props} />
            </React.Fragment>
        );
    };
    return Title
};

export const ScrollToTop = withRouter(
    class ScrollToTopWithoutRouter extends React.Component<RouteComponentProps<any>, any> {
        componentDidUpdate(prevProps: Readonly<RouteComponentProps<any>>) {
            if (this.props.location !== prevProps.location) {
                window.scrollTo(0, 0)
            }
        }

        render(): JSX.Element {
            return null;
        }
    }
);

export const CenteredLoader = () => {
    const { t } = useTranslation();

    return <Loader active inline='centered' size='large' style={{ marginTop: '4em' }}>
        {t("Loading")}
    </Loader>
}
