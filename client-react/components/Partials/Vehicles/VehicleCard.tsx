import * as React from "react";
import { RoutePaths } from '../../Routes';
import { Icon, Button, Label, Dropdown, Segment, Header } from 'semantic-ui-react'
import { Link } from "react-router-dom";
import { CreateVehicleHistoryModal } from "../VehicleHistory/CreateVehicleHistoryModal";
import { IVehicle } from "../../../typings/Vehicle";
import { useTranslation } from "react-i18next";

export const VehicleCard = (props: any) => {
    let vehicle = props.vehicle as IVehicle;
    const { t } = useTranslation();
    const cardHeader = props.vehicle.make + ' ' + props.vehicle.model;
    const cardMeta = t("Milage") + ': ' + vehicle.milage + ' ' + t("Milage-Km");

    return <Segment href="" link>
        <Link to={RoutePaths.VehicleView.replace(":id", vehicle.id.toString())}>
            <Header>
                {cardHeader}
                <Header.Subheader>{cardMeta}</Header.Subheader>
            </Header>
        </Link>
        <Segment.Group horizontal compact>
            <Segment>
                <Label circular>{vehicle.totalVehicleHistories}</Label>
                {vehicle.totalVehicleHistories === 1 ? t("Entry") : t("Entries")}
            </Segment>
            {vehicle.remindableVehicleHistories.length > 0 &&
                <Segment>
                    <Label color='orange' circular>{vehicle.remindableVehicleHistories.length}</Label>
                    {vehicle.remindableVehicleHistories.length === 1 ? t("Scheduled") : t("Scheduled-pl")}
                </Segment>
            }
        </Segment.Group>
        <VehicleCardButtons {...props} />
    </Segment>
}

export const VehicleCardButtons = (props: any) => {
    const { t } = useTranslation();

    return <Button.Group fluid>
        <CreateVehicleHistoryModal vehicleId={props.vehicle.id} {...props} color='green' />
        <Button onClick={() =>
            props.history.push({
                pathname: RoutePaths.VehicleHistories,
                state: { vehicleId: props.vehicle.id.toString() }
            })
        }>
            <Icon name="history" /><span>{t("Nav-History")}</span>
        </Button>
        <Dropdown button
            compact
            trigger={<Icon size='large' name="ellipsis horizontal" />}
            icon={null}
            style={{ textAlign: 'center' }}
            pointing='top right'>
            <Dropdown.Menu>
                <Dropdown.Item as={Link} to={RoutePaths.VehicleView.replace(":id", props.vehicle.id.toString())}>
                    <Icon name="eye" />{t("Details")}
                </Dropdown.Item>
                <Dropdown.Item as={Link} to={RoutePaths.VehicleEdit.replace(":id", props.vehicle.id.toString())}>
                    <Icon name="edit" />{t("Edit")}
                </Dropdown.Item>
                <Dropdown.Divider />
                <Dropdown.Item onClick={() => props.delete(props.vehicle)} className='link'>
                    <Icon name="delete" />{t("Delete")}
                </Dropdown.Item>
            </Dropdown.Menu>
        </Dropdown>
    </Button.Group>
}

