import 'object-assign';
import * as React from 'react';
import { Link } from 'react-router-dom';
import { Icon, Accordion, List, Label, Segment, Header } from 'semantic-ui-react'
import { RoutePaths } from '../../Routes';
import { mapRemindableVehicleHistories } from '../../../services/RemindableVehicleHistory';
import { IRemindableVehicleHistory } from './../../../typings/RemindableVehicleHistories';
import { IconedHeader } from '../Messages';
import { IVehicle } from '../../../typings/Vehicle';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

export const ExpirationVehicleHistoriesAccordion = (props: any) => {
    const [activeIndex, setActiveIndex] = useState(0);
    const { t } = useTranslation();

    const handleClick = (e: any, titleProps: any) => {
        const { index } = titleProps
        const newIndex = activeIndex === index ? -1 : index

        setActiveIndex(newIndex);
    }

    const menuItems = [
        {
            icon: 'fire',
            type: "Fueling",
            text: t("Services-Fueling")
        },
        {
            icon: 'dollar',
            type: "Tax",
            text: t("Services-Taxes")
        },
        {
            icon: 'wrench',
            type: "Repair",
            text: t("Services-Repairs")
        },
    ];

    const { history } = props;
    const vehicles = props.vehicles as Array<IVehicle>;
    const isDashboardHeader = props.isDashboardHeader;
    const remindablesGroup = mapRemindableVehicleHistories(vehicles);

    return (<div>
        {isDashboardHeader &&
            <IconedHeader
                iconName='history'
                header={t("Dashboard-VehicleHistories")}
                content={t("Dashboard-VehicleHistoriesDesc")}
            />
        }
        {!isDashboardHeader &&
            <Segment attached='top'>
                <Header as='h3'>
                    <Icon name='history' />
                    <Header.Content>
                        {t("Dashboard-VehicleHistories")}
                    </Header.Content>
                </Header>
            </Segment>
        }
        <Segment attached>
            <List relaxed>
                {menuItems.map((i: any) =>
                    <List.Item>
                        <Icon name={i.icon} />
                        <List.Content>
                            <List.Header as='a' onClick={() =>
                                history.push({
                                    pathname: RoutePaths.VehicleHistories,
                                    state: { type: i.type, vehicleId: vehicles.length === 1 ? vehicles[0].id : '' }
                                })
                            }>
                                {i.text}
                            </List.Header>
                        </List.Content>
                    </List.Item>
                )}
            </List>
        </Segment>
        <Accordion styled style={{ borderTopLeftRadius: 0, borderTopRightRadius: 0 }}>
            <Accordion.Title active={activeIndex === 1} index={1} onClick={handleClick}>
                <Icon name='dropdown' />
                <Icon color='red' name='minus circle' />
                {remindablesGroup.Expired.length}
                {remindablesGroup.Expired.length === 1 ? t("VehicleHistory-ExpiredSingle") : t("VehicleHistory-ExpiredPlural")}
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 1}>
                <VehicleHistoriesList vehicleHistories={remindablesGroup.Expired} />
            </Accordion.Content>
            <Accordion.Title active={activeIndex === 2} index={2} onClick={handleClick}>
                <Icon name='dropdown' />
                <Icon color='orange' name='warning sign' />
                {remindablesGroup.Expiring.length}
                {remindablesGroup.Expiring.length === 1 ? t("VehicleHistory-ExpiringSingle") : t("VehicleHistory-ExpiringPlural")}
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 2}>
                <VehicleHistoriesList vehicleHistories={remindablesGroup.Expiring} />
            </Accordion.Content>
        </Accordion>
    </div >)
}

export const VehicleHistoriesList = (props: any) => {
    const { t } = useTranslation();

    return <List divided relaxed>
        {props.vehicleHistories.sort().map((vh: IRemindableVehicleHistory) =>
            <List.Item key={vh.id}>
                <List.Header as='a'>
                    <Link to={
                        RoutePaths.VehicleHistoryView
                            .replace(":id", vh.id.toString())
                            .replace(":type", vh.type.toLowerCase())
                    }>
                        {t(vh.type)} - {t(vh.category)}
                    </Link>
                </List.Header>
                <List.Description>
                    {vh.isRemindableByDate &&
                        <Label className='expirationLabel' horizontal>{vh.expirationInfoDate}</Label>
                    }
                    {vh.isRemindableByMilage &&
                        <Label className='expirationLabel' horizontal>{vh.expirationInfoMilage}</Label>
                    }
                </List.Description>
            </List.Item>
        )}
    </List>
}

