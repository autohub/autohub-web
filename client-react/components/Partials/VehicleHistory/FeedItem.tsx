import * as React from 'react';
import { RoutePaths } from '../../Routes';
import { Icon, Button, Feed, Header, Dropdown, Label } from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import * as moment from 'moment'
import { mapExpirationInfo } from '../../../services/RemindableVehicleHistory';
import { IRemindableVehicleHistory } from '../../../typings/RemindableVehicleHistories';
import { updateRenew } from '../../Helpers';
import { VehicleHistoryType, FuelType, RepairVehicleHistoryCategory, TaxVehicleHistoryCategory } from '../../../typings/Enums/VehicleHistories';
import { useTranslation } from 'react-i18next';
import { GetTranslatedEnumTypes } from '../../../helpers/EnumHelpers';

export const FeedItem = (props: any) => {
    const { t } = useTranslation();
    const getVehicleHistoryDetails = () => {
        let details = {
            editUrl: null as string,
            viewUrl: null as string,
            title: null as string,
            icon: null as JSX.Element,
            expirationInfo: {} as { [key: string]: string }
        };
        const vehicleHistoryTypes = GetTranslatedEnumTypes(VehicleHistoryType);

        switch (props.vehicleHistory.type as keyof typeof VehicleHistoryType) {
            
            case "Tax":
                details.editUrl = RoutePaths.TaxVehicleHistoryEdit;
                details.viewUrl = RoutePaths.TaxVehicleHistoryView;
                details.icon = <Icon circular name='dollar sign' />;
                const taxCategoryTypes = GetTranslatedEnumTypes(TaxVehicleHistoryCategory);
                const taxType = vehicleHistoryTypes.get(props.vehicleHistory.type);
                const taxCategory = taxCategoryTypes.get(props.vehicleHistory.category);
                details.title = taxType + " - " + taxCategory;
                break;
            case "Repair":
                details.editUrl = RoutePaths.RepairVehicleHistoryEdit;
                details.viewUrl = RoutePaths.RepairVehicleHistoryView;
                details.icon = <Icon circular name='wrench' />;
                const repairCategoryTypes = GetTranslatedEnumTypes(RepairVehicleHistoryCategory);
                const repairType = vehicleHistoryTypes.get(props.vehicleHistory.type);
                const repairCategory = repairCategoryTypes.get(props.vehicleHistory.category);
                details.title = repairType + " - " + repairCategory;
                break;
            case "Fueling":
                details.editUrl = RoutePaths.FuelingVehicleHistoryEdit;
                details.viewUrl = RoutePaths.FuelingVehicleHistoryView;
                details.icon = <Icon circular name='fire' />;
                const fuelCategoryTypes = GetTranslatedEnumTypes(FuelType);
                const fuelType = vehicleHistoryTypes.get(props.vehicleHistory.type);
                const fuelCategory = fuelCategoryTypes.get(props.vehicleHistory.category);
                details.title = fuelType + " - " + fuelCategory;
                break;
        }

        return details;
    }

    let vehicleHistory = props.vehicleHistory;
    const vehicleHistoryDetails = getVehicleHistoryDetails();
    var vhMapper = mapExpirationInfo(vehicleHistory);

    return <Feed.Event>
        <Feed.Label>
            {vehicleHistoryDetails.icon}
        </Feed.Label>
        <Feed.Content>
            <Feed.Date>
                {
                    moment.utc(vehicleHistory.createdOn).local().fromNow()
                    + t("On") + vehicleHistory.currentMilage + t("Milage-Km")
                }
            </Feed.Date>
            <Feed.Summary>
                <Header as='h3'>
                    <Link to={vehicleHistoryDetails.viewUrl.replace(":id", vehicleHistory.id.toString())}>
                        {vehicleHistoryDetails.title}
                    </Link>
                </Header>
            </Feed.Summary>
            {(vehicleHistory.isRemindableByDate || vehicleHistory.isRemindableByMilage) &&
                <ExpirationDateMessage vehicleHistory={vhMapper} />
            }
            <Feed.Meta>
                <Feed.Like>
                    <Icon name='money bill alternate outline' />
                    {vehicleHistory.totalPrice} {t("Currency-Lev")}
                </Feed.Like>
                <Dropdown
                    trigger={<Button size='mini' icon='ellipsis horizontal' />}
                    icon={null}
                    pointing='top right'>
                    <Dropdown.Menu>
                        {!vehicleHistory.completedOn && (vehicleHistory.isRemindableByDate || vehicleHistory.isRemindableByMilage) &&
                            <Dropdown.Item onClick={() => updateRenew(vehicleHistory as IRemindableVehicleHistory, props.history)}>
                                <Icon name="refresh" />{t("Renew")}
                            </Dropdown.Item>
                        }
                        <Dropdown.Item as={Link} to={vehicleHistoryDetails.viewUrl.replace(":id", vehicleHistory.id.toString())}>
                            <Icon name="eye" />{t("Details")}
                        </Dropdown.Item>
                        <Dropdown.Item as={Link} to={vehicleHistoryDetails.editUrl.replace(":id", vehicleHistory.id.toString())}>
                            <Icon name="edit" />{t("Edit")}
                        </Dropdown.Item>
                        <Dropdown.Item className='link' onClick={() => props.delete(vehicleHistory)}>
                            <Icon name="delete" />{t("Delete")}
                        </Dropdown.Item>
                    </Dropdown.Menu>
                </Dropdown>
            </Feed.Meta>
        </Feed.Content>
    </Feed.Event>
}

export const ExpirationDateMessage = (props: any) => {
    const { t } = useTranslation();

    const vh = props.vehicleHistory as IRemindableVehicleHistory;
    if (vh.completedOn) {
        const date = moment.utc(vh.completedOn).local().calendar();
        return <Feed.Extra>
            <Label horizontal>{t("Renewed")} {date.toString().toLowerCase()}</Label>
        </Feed.Extra>;
    }
    else {
        return <Feed.Extra>
            <span>
                {vh.isRemindableByDate &&
                    <Label color='red' horizontal>{vh.expirationInfoDate}</Label>
                }
                {vh.isRemindableByMilage &&
                    <Label color='red' horizontal>{vh.expirationInfoMilage}</Label>
                }
            </span>
        </Feed.Extra>
    }
}
