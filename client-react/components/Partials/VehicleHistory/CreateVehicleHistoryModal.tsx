import * as React from "react";
import { RoutePaths } from '../../Routes';
import { Message, Icon, Modal, Button, Step } from 'semantic-ui-react'
import { useTranslation } from "react-i18next";

export const CreateVehicleHistoryModal = (props: any) => {
    const { t } = useTranslation();

    return <Modal
        trigger={<Button color={props.color ? props.color : ''}><Icon name='plus' /><span>{t("Add")}</span></Button>}
        size={'tiny'}
        centered={false} closeIcon
    >
        <Message
            size='large'
            attached='top'
            header={t("AddHistory")}
            content={t("ChooseHistoryType")}
            style={{ marginLeft: '0px', marginRight: '0px' }}
        />
        <Step.Group fluid vertical attached='bottom'>
            <Step link onClick={() => props.history.push({
                pathname: RoutePaths.FuelingVehicleHistoryAdd,
                state: { vehicleId: props.vehicleId.toString() }
            })}>
                <Icon name='fire' />
                <Step.Content>
                    <Step.Title>{t("Fueling")}</Step.Title>
                    <Step.Description>{t("Services-FuelingDesc")}</Step.Description>
                </Step.Content>
            </Step>

            <Step link onClick={() => props.history.push({
                pathname: RoutePaths.RepairVehicleHistoryAdd,
                state: { vehicleId: props.vehicleId.toString() }
            })}>
                <Icon name='wrench' />
                <Step.Content>
                    <Step.Title>{t("Repair")}</Step.Title>
                    <Step.Description>{t("Services-RepairsDesc")}</Step.Description>
                </Step.Content>
            </Step>
            <Step link onClick={() => props.history.push({
                pathname: RoutePaths.TaxVehicleHistoryAdd,
                state: { vehicleId: props.vehicleId.toString() }
            })}>
                <Icon name='dollar sign' />
                <Step.Content>
                    <Step.Title>{t("Tax")}</Step.Title>
                    <Step.Description>{t("Services-TaxesDesc")}</Step.Description>
                </Step.Content>
            </Step>
        </Step.Group>
    </Modal>
}                                 