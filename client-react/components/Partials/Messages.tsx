import * as React from "react";
import { Message, Icon, Header, Segment } from 'semantic-ui-react'
import { useTranslation } from "react-i18next";

interface StateErrorsProps {
    errors: { [key: string]: string }
}

export const DetailedErrorMessage = (props: StateErrorsProps) => {
    const { t } = useTranslation();

    if (props.errors.general) {
        return <Message
            icon='warning sign'
            header={t("SomethingWentWrong")}
            content={props.errors.general}
            error attached
        />
    }
    else if (props.errors
        && !props.errors.general
        && Object.keys(props.errors).length !== 0
        && props.errors.constructor === Object) {
        return <Message
            icon='warning sign'
            header={t("SomethingWentWrong")}
            attached error role="alert"
            list={Object.keys(props.errors).map(key => props.errors[key])}
        />
    }
    else {
        return (null);
    }
}

export const IconedHeader = (props: any) => {
    return <Message icon attached='top'>
        <Icon name={props.iconName} size='large' className='mainIconColor' />
        <Message.Content>
            <Message.Header>{props.header}</Message.Header>
            {props.content}
        </Message.Content>
    </Message>
};

export const SuccessAttachedMessage = (props: any) => {
    return <Message
        positive
        icon='checkmark'
        attached
        header={props.header}
        content={props.content}
    />
}

export const SegmentMessage = (props: any) => {
    return <Segment placeholder style={{ textAlign: 'center' }}>
        <Header icon>
            <Icon name='info circle' color='blue' />
            {props.title}
        </Header>
    </Segment>
}
