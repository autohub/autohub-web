import * as React from 'react';
import { Route } from 'react-router-dom';
import { RoutePaths } from '../Routes';
import AuthService from '../../services/Auth';
import AccountService from '../../services/Account';
import {
	Container,
	Icon,
	Button,
	Image,
	Menu,
	Sidebar,
	Responsive,
	Dropdown
} from "semantic-ui-react";
import Headroom from 'react-headroom';
import i18next from 'i18next';
import moment from 'moment';
import { useState } from 'react';
import { useTranslation } from 'react-i18next';

let authService = new AuthService();
let accountService = new AccountService();
let logo = require('../../images/logo.png');

const NavBarChildren = ({ children }: any) => (
	<Container style={{ marginTop: "3em" }}>{children}</Container>
);

export const NavBar = ({ ...props }: any) => {
	const [visible, setVisible] = useState(false);
	const { t } = useTranslation();

	const leftItems: any = [
		{ as: "a", content: t("Nav-Home"), key: RoutePaths.Home, icon: 'home' },
		{ as: "a", content: t("Nav-About"), key: RoutePaths.AboutPage, icon: 'exclamation circle' },
		{ as: "a", content: t("Nav-Service"), key: RoutePaths.FeaturesPage, icon: 'tasks' }
	];

	const leftItemsLogged: any = [
		{ as: "a", content: t("Nav-Dashboard"), key: RoutePaths.Dashboard, name: 'dashboard', icon: 'home' },
		{ as: "a", content: t("Nav-Vehicles"), key: RoutePaths.Vehicles, name: 'garage', icon: 'car' },
		{ as: "a", content: t("Nav-History"), key: RoutePaths.VehicleHistories, name: 'history', icon: 'history' },
		{ as: "a", content: t("Nav-Reports"), key: RoutePaths.Statistics, name: 'statistics', icon: 'chart bar' }
	];

	const handlePusher = () => {
		if (visible) setVisible(false);
	};

	const handleLogoClick = () =>
		AuthService.isSignedIn() ? props.history.push(RoutePaths.Dashboard) : props.history.push(RoutePaths.Home);

	const handleToggle = () => setVisible(!visible);
	const { children }: any = props;
	const menuItems = AuthService.isSignedIn() ? leftItemsLogged : leftItems;

	return (
		<div>
			<Responsive {...Responsive.onlyMobile}>
				<NavBarMobile
					leftItems={menuItems}
					onPusherClick={handlePusher}
					onToggle={handleToggle}
					visible={visible}
					handleLogoClick={handleLogoClick}
					{...props}
				>
					<NavBarChildren>{children}</NavBarChildren>
				</NavBarMobile>
			</Responsive>
			<Responsive minWidth={Responsive.onlyTablet.minWidth}>
				<NavBarDesktop leftItems={menuItems} {...props} handleLogoClick={handleLogoClick} />
				<NavBarChildren>{children}</NavBarChildren>
			</Responsive>
		</div>
	);
}

const handleChangeLng = (value: string) => {
	i18next.changeLanguage(value);
	moment.locale(value);
	if(AuthService.isSignedIn())
		accountService.updateLang(value);
}

const NavBarDesktop = ({ ...props }: any) => {
	const [lang, setLang] = useState(i18next.language);

	const stateOptions = i18next.languages.map(lang => ({
		key: lang,
		text: lang,
		value: lang,
		flag: lang == "en" ? "uk" : lang
	}));

	const handleChange = (e: any, { value }: any) => {
		setLang(value);
		handleChangeLng(value);
	}

	const { leftItems, handleLogoClick }: any = props;
	return (
		<Headroom>
			<Menu size='massive' inverted>
				<Container>
					<Menu.Item as='a' header link onClick={() => handleLogoClick()}>
						<Image size="mini" src={logo} />
					</Menu.Item>
					{leftItems.map((item: any) => <Menu.Item onClick={() => props.history.push(item.key)} link="true" {...item} />)}
					<Menu.Menu position="right">
						<Menu.Item>
							<UserProfile {...props} />
						</Menu.Item>
						<Menu.Item>
							<Dropdown button labeled className='icon' icon='world'
								fluid
								selection
								onChange={handleChange}
								options={stateOptions}
								placeholder='Language'
								value={lang}
							/>
						</Menu.Item>
					</Menu.Menu>
				</Container>
			</Menu>
		</Headroom>
	)
}

const NavBarMobile = ({ children, leftItems, onPusherClick, onToggle, rightItems, visible, ...props }: any) => {
	const [lng, setLng] = useState(i18next.language);
	const stateOptions = i18next.languages.map(lang => ({
		key: lang,
		text: lang,
		value: lang
	}));

	const handleChange = (e: any, { value }: any) => {
		setLng(value)
		handleChangeLng(value);
	}

	return <div>
		<Sidebar
			as={Menu}
			animation="overlay"
			icon="labeled"
			inverted
			items={leftItems}
			vertical
			visible={visible}
			style={{ maxWidth: '95px' }}
		>
			{leftItems.map((item: any) =>
				<Menu.Item
					onClick={() => props.history.push(item.key) || onPusherClick()}
					link="true"
					{...item}
				/>
			)}
		</Sidebar>
		<Sidebar.Pusher
			dimmed={visible}
			onClick={onPusherClick}
			style={{ minHeight: "100vh", display: "inline" }}
		>
			<Headroom>
				<Menu inverted>
					<Menu.Item as='a' header link onClick={() => props.handleLogoClick()}>
						<Image size="mini" src={logo} />
					</Menu.Item>
					<Menu.Item onClick={onToggle}>
						<Icon name={visible ? 'close' : 'sidebar'} />
					</Menu.Item>
					<Menu.Menu position="right">
						<Menu.Item style={{ padding: "0.4em" }}>
							<UserProfile isMobile={true} {...props} />
						</Menu.Item>
						<Menu.Item style={{ padding: "0.4em" }}>
							<Dropdown button
								fluid
								selection
								onChange={handleChange}
								options={stateOptions}
								value={lng}
							/>
						</Menu.Item>
					</Menu.Menu>
				</Menu>
			</Headroom>
			{children}
		</Sidebar.Pusher>
	</div>
};

const UserProfile = ({ ...props }: any) => {
	const [user] = useState(authService.getUser());
	const { t } = useTranslation();

	const signOut = (): void => {
		authService.signOut();
		props.history.push(RoutePaths.Login, { signedOut: true });
	}

	const options: any = [
		{
			key: 'user',
			text: <span>{t("ProfileDropdownLoggedInAs")} <br /><strong>{!!user && user.userEmail}</strong></span>,
			disabled: true,
		},
		{ 
			key: 'dashboard', 
			selected: false, 
			text: t("Nav-Dashboard"), 
			icon: 'grid layout', 
			onClick: () => props.history.push(RoutePaths.Dashboard) 
		},
		{ 
			key: 'useraccount', 
			text: t("ProfileSettings"), 
			icon: 'user', 
			onClick: () => props.history.push(RoutePaths.AccountEdit) 
		},
		{ 
			key: 'settings', 
			text: t("ProfileChangePassword"), 
			icon: 'key', 
			onClick: () => props.history.push(RoutePaths.AccountPasswordChange) 
		},
		{ 
			key: 'sign-out', 
			text: t("ProfileLogout"), 
			icon: 'sign out', 
			onClick: () => signOut() 
		}
	]

	const mobileProfileDropdown = <Dropdown button icon='user' text={t("ProfileLoggedMobileDropdown")} labeled className='icon' pointing='top right'>
		<Dropdown.Menu>
			{options.map((x: any) =>
				<Dropdown.Item text={x.text} icon={x.icon} disabled={x.disabled} onClick={x.onClick} />
			)}
		</Dropdown.Menu>
	</Dropdown>

	const desktopProfileDropdown = <Dropdown button labeled className='icon' icon='user' pointing='top right'
		options={options}
		text={!!user && user.username}
	/>

	const { isMobile } = props;

	return (
		<Route render={() => (
			AuthService.isSignedIn() ? (
				isMobile ? mobileProfileDropdown : desktopProfileDropdown
			) : (
					<Button icon labeled color='orange' onClick={() => props.history.push(RoutePaths.Login)}>
						<Icon name='user'></Icon> {!isMobile ? t("Dashboard-DesktopActionButton") : t("Dashboard-MobileActionButton")}
					</Button>
				)
		)} />
	);
}


