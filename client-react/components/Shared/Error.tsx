import * as React from "react";
import { Link, RouteComponentProps } from 'react-router-dom';

export const ErrorPage = (props: any) => {

    const getErrorCode = () => {
        return props.match.params.code;
    }

    const getErrorMessage = () => {
        let message = null;
        switch (props.match.params.code) {
            case 'email-confirm':
                message = 'The email confirmation link you used is invalid or expired.'
                break;
            default:
                message = 'An unknown error has occured.'
        }

        return message;
    }

    let code = getErrorCode();
    return <div>
        <h1>Error</h1>
        <p>{getErrorMessage()}</p>
        {code &&
            <p>Code: {code}</p>
        }

    </div>;
}

