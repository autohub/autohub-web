import * as React from "react";
import {
    Container,
    Grid,
    Header,
    List,
    Segment,
    Button,
    Icon,
    Label,
} from 'semantic-ui-react'
import { Link } from 'react-router-dom';
import { RoutePaths } from "../Routes";
import { useTranslation } from "react-i18next";

const Footer = () => {
    const { t } = useTranslation();
    const appVersion = process.env.APP_VERSION || "InDevelopment";
    return (
        <div>
            <Segment inverted vertical id='footer'>
                <Container>
                    <Grid divided inverted stackable>
                        <Grid.Row>
                            <Grid.Column width={3}>
                                <Header inverted as='h4' content={t("Footer-FollowUs")} />
                                <List link inverted>
                                    <a href="https://fb.me/autohub.bg" target="_blank"><Icon inverted color='grey' name='facebook square' size='big' /></a>
                                    <a href="https://m.me/autohub.bg" target="_blank"><Icon inverted color='grey' name='facebook messenger' size='big' /></a>
                                </List>
                            </Grid.Column>
                            <Grid.Column width={3}>
                                <Header inverted as='h4' content={t("Footer-AboutUs")} />
                                <List link inverted>
                                    <List.Item as='a'><Link to={RoutePaths.AboutPage}>{t("Footer-MenuAboutUs")}</Link></List.Item>
                                    <List.Item as='a'><Link to={RoutePaths.FeaturesPage}>{t("Footer-MenuServices")}</Link></List.Item>
                                    <List.Item as='a'><Link to={RoutePaths.FaqPage}>{t("Footer-MenuFaq")}</Link></List.Item>
                                    <List.Item as='a'><Link to={RoutePaths.PrivacyPolicyPage}>{t("Footer-MenuPrivacyPolicy")}</Link></List.Item>
                                    <List.Item as='a'><Link to={RoutePaths.TermsNConditionsPage}>{t("Footer-MenuTermsNConditions")}</Link></List.Item>
                                </List>
                            </Grid.Column>
                            <Grid.Column width={6}>
                                <Header as='h4' inverted>{t("Footer-Feedback")}</Header>
                                <p>{t("Footer-FeedbackDesc")}</p>
                                <Link to={RoutePaths.FeedbackPage}>
                                    <Button color='green' as='a' inverted size='small'>
                                        {t("Footer-FeedbackButton")}
                                    </Button>
                                </Link>
                            </Grid.Column>
                            <Grid.Column width={4}>
                                <Header inverted as='h4' content={t("Footer-AboutTheApp")} />
                                <List link inverted>
                                    <List.Item>{t("Footer-AboutTheAppVersion")}: {appVersion}</List.Item>
                                    <List.Item>{t("Footer-AboutTheAppDev")}: <Label size='small' color='green'>{t("Footer-AboutTheAppDevStatus")}</Label></List.Item>
                                    <List.Item>Copyright © {new Date().getFullYear()} AutoHub.bg</List.Item>
                                </List>
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Container>
            </Segment>
        </div>
    )
}

export default Footer
