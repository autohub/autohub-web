import * as React from "react";
import { Label, Grid, Form, Radio, Divider, Input } from 'semantic-ui-react';
import AccountService, { IAccount, UserType } from '../../../services/Account'
import { PageHeader, FormActionsButtons, CenteredLoader } from "../../Partials/PageComponents";
import { IconedHeader, DetailedErrorMessage, SuccessAttachedMessage } from "../../Partials/Messages";
import { toLowerCaseErrors } from "../../../helpers/Utils";
import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

let accountService = new AccountService();

export const AccountEdit = (props: any) => {
    const [account, setAccount] = useState(null as IAccount);
    const [isBusinessProfile, setIsBusinessProfile] = useState(false);
    const [changeCompleted, setChangeCompleted] = useState(false);
    const [errors, setErrors] = useState({} as { [key: string]: string });
    const [isLoading, setIsLoading] = useState(true);
    const [isSubmitLoading, setSubmitIsLoading] = useState(false);
    const { t } = useTranslation();

    useEffect(() => {

        accountService.fetch().then((response) => {
            setAccount(response.content);
            return (response);
        }).then((response) => {
            setIsBusinessProfile(response.content.userType === "Business");
            setIsLoading(false);
        });
    }, [])

    const handleSubmit = () => {
        setSubmitIsLoading(true);
        event.preventDefault();
        account.userType = isBusinessProfile ? UserType.Business : UserType.Personal
        saveAccount(account);
        setSubmitIsLoading(false);
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        let accountUpdates = {
            [name]: value
        }
        setAccount({...Object.assign(account, accountUpdates)} as IAccount);
    }

    const saveAccount = (account: IAccount) => {
        setErrors({} as { [key: string]: string });
        setChangeCompleted(false);
        accountService.update(account).then((response) => {
            if (!response.is_error) {
                setChangeCompleted(true);
            } else {
                setErrors(toLowerCaseErrors(response.error_content));
            }
        });
    }

    const toggle = () => setIsBusinessProfile(!isBusinessProfile);

    if (isLoading) {
        return <CenteredLoader />;
    }
    else {
        return <div>
            <PageHeader headerText={t("ProfileEditPageHeader")} {...props} />
            <Divider hidden />
            <Grid stackable columns={2}>
                <Grid.Column>
                    <IconedHeader
                        iconName='user outline'
                        header={t("ProfileEditHeader")}
                        content={t("ProfileEditSubheader")}
                    />
                    <DetailedErrorMessage errors={errors} />
                    {changeCompleted &&
                        <SuccessAttachedMessage
                            header={t("Success")}
                            content={t("ProfileUpdate")}
                        />
                    }
                    <Form className="segment attached" size='large' onSubmit={handleSubmit}>
                        <Form.Field>
                            <label htmlFor="radioGroup">{t("ProfileType")}</label>
                            <Radio
                                toggle
                                label={isBusinessProfile == true ? t("ProfileTypeBusiness") : t("ProfileTypePersonal")}
                                name='radioGroup'
                                value="business"
                                onChange={toggle}
                                checked={!!isBusinessProfile}
                            />
                        </Form.Field>
                        <Form.Field required error={!!errors.name}>
                            <label htmlFor="name">{isBusinessProfile == true ? t("ProfileBusinessName") : t("ProfilePersonalName")} </label>
                            <Input
                                name='name'
                                value={account.name}
                                onChange={handleChange}
                            />
                            {errors.name &&
                                <Label basic color='red' pointing>{errors.name}</Label>
                            }
                        </Form.Field>
                        <FormActionsButtons isLoading={isSubmitLoading} {...props} />
                    </Form>
                </Grid.Column>
            </Grid>
        </div>;
    }
}
