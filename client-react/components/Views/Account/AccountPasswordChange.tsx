import * as React from "react";
import { Label, Grid, Message, Form, Segment, Divider } from 'semantic-ui-react';
import AccountService from '../../../services/Account';
import { PageHeader, FormActionsButtons } from "../../Partials/PageComponents";
import { IconedHeader, DetailedErrorMessage } from "../../Partials/Messages";
import { toLowerCaseErrors } from "../../../helpers/Utils";
import { useRef, useState } from "react";
import { useTranslation } from "react-i18next";
let accountService = new AccountService();

export const AccountPasswordChange = (props: any) => {
    const oldPassword = useRef(null);
    const newPassword = useRef(null);
    const newPasswordConfirmation = useRef(null);

    const [passwordChanged, setPasswordChanged] = useState(false);
    const [errors, setErrors] = useState({} as { [key: string]: string });
    const [isSubmitLoading, setSubmitIsLoading] = useState(false);

    const { t } = useTranslation();

    const handleSubmit = () => {
        setSubmitIsLoading(true);
        event.preventDefault();
        setErrors({});

        accountService.passwordChange(
            oldPassword.current.value,
            newPassword.current.value,
            newPasswordConfirmation.current.value
        ).then(response => {
            setSubmitIsLoading(false);
            if (!response.is_error) {
                setPasswordChanged(true);
            }
            else {
                setErrors(toLowerCaseErrors(response.error_content));
            }
        });
    }

    if (passwordChanged) {
        return <ResetPasswordRequestSend />
    } else {
        return <div>
            <PageHeader headerText={t("ProfilePasswordChangeHeader")} {...props} />
            <Divider hidden />
            <Grid stackable columns={2}>
                <Grid.Column>
                    <IconedHeader
                        iconName='key'
                        header={t("ProfilePasswordChangeSubheader")}
                        content={t("ProfilePasswordChangeHeaderContent")}
                    />
                    <DetailedErrorMessage errors={errors} />
                    <Form className="segment attached" size='large' onSubmit={handleSubmit}>
                        <Form.Field required error={!!errors.oldPassword}>
                            <label htmlFor="inputPassword">{t("ProfileCurrentPassword")}</label>
                            <input
                                placeholder={t("ProfileCurrentPasswordPlaceholder")}
                                type='password'
                                ref={oldPassword}
                            />
                            {errors.oldPassword &&
                                <Label basic color='red' pointing>{errors.oldPassword}</Label>
                            }
                        </Form.Field>
                        <Form.Field required error={!!errors.newPassword}>
                            <label htmlFor="inputPassword">{t("ProfileNewPassword")}</label>
                            <input
                                placeholder={t("ProfileNewPasswordPlaceholder")}
                                type='password'
                                ref={newPassword}
                            />
                            {errors.newPassword &&
                                <Label basic color='red' pointing>{errors.newPassword}</Label>
                            }
                        </Form.Field>
                        <Form.Field required error={!!errors.newPasswordConfirmation}>
                            <label htmlFor="inputPassword">{t("ProfileRepeatPassword")}</label>
                            <input
                                placeholder={t("ProfileRepeatPasswordPlaceholder")}
                                type='password'
                                ref={newPasswordConfirmation}
                            />
                            {errors.newPasswordConfirmation &&
                                <Label basic color='red' pointing>{errors.newPasswordConfirmation}</Label>
                            }
                        </Form.Field>
                        <FormActionsButtons isLoading={isSubmitLoading} {...props} />
                    </Form>
                </Grid.Column>
            </Grid>
        </div>
    };
}

export const ResetPasswordRequestSend = () => {
    const { t } = useTranslation();

    return <div className="auth">
        <Grid stackable centered columns={2}>
            <Grid.Column>
                <Message
                    positive
                    icon='checkmark'
                    attached
                    header={t("ProfilePasswordChangedMessage")}
                    content={t("ProfilePasswordChangedContent")}
                />
                <Segment attached>
                    <p>{t("ProfilePasswordChangedHint")}</p>
                </Segment>
            </Grid.Column>
        </Grid>
    </div>
}
