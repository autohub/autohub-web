import * as React from "react";
import StatisticsService, {
    IVehiclesStatistics,
    IGraphReportRequest,
    IGraphReportResponse,
    IGraphReportTotalVehicleHistoriesCosts,
    IGraphReportMilageGrowth,
    IGraphReportTotalVehicleHistoriesByMonthsCosts,
    IGraphReportFuelConsumptions,
    IGraphReportFuelPrices
} from '../../../services/Statistics';
import {
    Grid,
    Divider,
    Segment,
    Statistic,
    Form,
    Button,
    Icon
} from 'semantic-ui-react';
import { PageHeader, CenteredLoader } from "../../Partials/PageComponents";
import {
    convertMilageByMonthData,
    convertVehicleHistoriesByTypeForMonth,
    convertFuelingByTypeForMonth,
    convertFuelingPricesByTypeForMonth
} from "../../../helpers/StatisticsHelpers";
import { IconedHeader, SegmentMessage } from "../../Partials/Messages";
import { FuelingPricesLine, TotalMilageLine, VehicleHistoriesPie, ExpensesPerMonthBar, FuelingConsumptionLine } from "./NivoComponents";
import { VehiclesToDropdownOptions, MapToDropdownOptions } from "../../../helpers/Options";
import { ReportGraphTypes, GetGraphReportTypes } from "../../../typings/Enums/Reports";
import * as moment from 'moment';
import DatePicker from 'react-datepicker';
import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

let statisticsService = new StatisticsService();

export const Statistics = (props: any) => {
    const [vehiclesStats, setVehiclesStats] = useState(null as IVehiclesStatistics);
    const [isLoading, setIsLoading] = useState(true);
    const [isGraphLoading, setIsGraphLoading] = useState(true);
    const [isSubmitLoading, setSubmitIsLoading] = useState(false);
    const [graphReportRequest, setGraphReportRequest] = useState({
        graphType: "TotalVehicleHistoriesCosts",
        vehicleId: '',
        from: moment.utc().local().subtract(1, 'month'),
        to: moment.utc().local()
    } as IGraphReportRequest);
    const [graphReportResponse, setGraphReportResponse] = useState({
        totalFuelingCosts: 0,
        totalRepairCosts: 0,
        totalTaxesCosts: 0
    } as IGraphReportResponse);

    const { t } = useTranslation();

    useEffect(() => {
        showAll();
    }, [])

    const showAll = () =>
        statisticsService.fetch()
            .then((response) => {
                setVehiclesStats(response.content);
                setIsLoading(false);
                generateReport();
            });

    const generateReport = () =>
        statisticsService.generateGraphReport(graphReportRequest).then((response) => {
            setGraphReportResponse(response.content);
            setSubmitIsLoading(false);
            setIsGraphLoading(false);
        });

    const handleGraphSubmit = () => {
        setSubmitIsLoading(true);
        event.preventDefault();
        setIsGraphLoading(true);
        generateReport();
    }

    const handleGraphReportRequestDropdown = (event: any, data: any) => {
        let graphReportRequestUpdates = {
            [data.name]: data.value
        }
        setGraphReportRequest({ ...Object.assign(graphReportRequest, graphReportRequestUpdates) as IGraphReportRequest });
    }

    const setFromDate = (date: any) => {
        let graphReportRequestUpdates = {
            from: date
        }
        setGraphReportRequest({ ...Object.assign(graphReportRequest, graphReportRequestUpdates) as IGraphReportRequest });
    }

    const setToDate = (date: any) => {
        let graphReportRequestUpdates = {
            to: date
        }
        setGraphReportRequest({ ...Object.assign(graphReportRequest, graphReportRequestUpdates) as IGraphReportRequest });
    }

    const graphReportTypes = GetGraphReportTypes();
    const graphOpts = MapToDropdownOptions(graphReportTypes);

    return isLoading ? <CenteredLoader /> :
        <div>
            <PageHeader headerText={t("Reports")} {...props} />
            <Divider hidden />
            <Grid stackable columns={2}>
                <Grid.Column width={11}>
                    <IconedHeader
                        iconName='chart pie'
                        header={t("Charts")}
                        content={t("ChartsDesc")}
                    />
                    <Segment attached>
                        <Form onSubmit={handleGraphSubmit}>
                            <Form.Group>
                                <Form.Field className='field-aligned' width={5}>
                                    <label>{t("ChartType")}</label>
                                    <Form.Select
                                        value={graphReportRequest.graphType}
                                        options={graphOpts}
                                        name='graphType'
                                        placeholder={t("ChartTypePlaceHolder")}
                                        onChange={handleGraphReportRequestDropdown}
                                        fluid selection labeled
                                    />
                                </Form.Field>
                                <Form.Field className='field-aligned' width={5}>
                                    <label>{t("ChartVehicle")}</label>
                                    <Form.Select
                                        value={graphReportRequest.vehicleId}
                                        name='vehicleId'
                                        options={VehiclesToDropdownOptions(vehiclesStats.vehicles)}
                                        placeholder={t("AllVehiclesPlaceholder")}
                                        onChange={handleGraphReportRequestDropdown}
                                        fluid selection labeled clearable
                                    />
                                </Form.Field>
                                <Form.Field className='field-aligned' width={3}>
                                    <label>{t("ChartFromDate")}</label>
                                    <DatePicker
                                        selected={graphReportRequest.from}
                                        onChange={date => setFromDate(date)}
                                        placeholderText={t("ChartFromPlaceholder")}
                                        name="from"
                                        peekNextMonth
                                        showMonthDropdown
                                        showYearDropdown
                                        dropdownMode="select"
                                        className='datepicker'
                                    />
                                </Form.Field>
                                <Form.Field width={3}>
                                    <label>{t("ChartToDate")}</label>
                                    <DatePicker
                                        selected={graphReportRequest.to}
                                        onChange={date => setToDate(date)}
                                        placeholderText={t("ChartToPlaceholder")}
                                        name="to"
                                        maxDate={moment.utc().local()}
                                        peekNextMonth
                                        showMonthDropdown
                                        showYearDropdown
                                        dropdownMode="select"
                                        className='datepicker'
                                    />
                                </Form.Field>
                            </Form.Group>
                            <div>
                                <Divider hidden />
                                <Button loading={isSubmitLoading}  positive>
                                    <Icon name='check'></Icon>{t("ChartGenerate")}
                                </Button>
                            </div>
                        </Form>
                    </Segment>
                    <Segment attached='bottom' style={{ height: "400px" }}>
                        {isGraphLoading && <CenteredLoader />}
                        {!isGraphLoading
                            && graphReportRequest.graphType === ReportGraphTypes.TotalVehicleHistoriesCosts
                            && graphReportResponse.hasOwnProperty("totalTaxesCosts")
                            && <TotalVehicleHistoriesCostsGraphView data={graphReportResponse} />
                        }
                        {!isGraphLoading
                            && graphReportRequest.graphType === ReportGraphTypes.MilageGrowth
                            && graphReportResponse.hasOwnProperty("milageByMonthData")
                            && <VehiclesMilageGrowthGraphView data={graphReportResponse} />
                        }
                        {!isGraphLoading
                            && graphReportRequest.graphType === ReportGraphTypes.TotalVehicleHistoriesCostsByMonth
                            && graphReportResponse.hasOwnProperty("vehicleHistoriesByMonthsData")
                            && <TotalVehicleHistoriesByMonthCostsGraphView data={graphReportResponse} />
                        }
                        {!isGraphLoading
                            && graphReportRequest.graphType === ReportGraphTypes.FuelConsumptions
                            && graphReportResponse.hasOwnProperty("fuelingsByTypeForMonthData")
                            && <FuelConsumptionsGraphView data={graphReportResponse} />
                        }
                        {!isGraphLoading
                            && graphReportRequest.graphType === ReportGraphTypes.FuelPrices
                            && graphReportResponse.hasOwnProperty("fuelingPricesByTypeForMonthData")
                            && <FuelPricesGraphView data={graphReportResponse} />
                        }
                    </Segment>
                </Grid.Column>
                <Grid.Column width={5}>
                    <SidebarStatisticsOverall stats={vehiclesStats} />
                </Grid.Column>
            </Grid>
        </div>
}

const NoDataSegment = () => {
    const { t } = useTranslation();
    return <SegmentMessage title={t("NoPeriodDataMessage")} />;
}

const TotalVehicleHistoriesCostsGraphView = ({ data }: any) => {
    const { t } = useTranslation();
    const graphData = data as IGraphReportTotalVehicleHistoriesCosts;
    const overAllTaxesData = [
        {
            "id": t("Taxes"),
            "label": t("Taxes"),
            "value": graphData.totalTaxesCosts,
            "color": "hsl(157, 70%, 50%)"
        },
        {
            "id": t("Repairs"),
            "label": t("Repairs"),
            "value": graphData.totalRepairCosts,
            "color": "hsl(107, 70%, 50%)"
        },
        {
            "id": t("Fuelings"),
            "label": t("Fuelings"),
            "value": graphData.totalFuelingCosts,
            "color": "hsl(94, 70%, 50%)"
        }
    ];

    const isEmptyData = graphData.totalFuelingCosts == 0 && graphData.totalRepairCosts == 0 && graphData.totalTaxesCosts == 0;

    return isEmptyData ? <NoDataSegment /> : <VehicleHistoriesPie data={overAllTaxesData} />
}

const VehiclesMilageGrowthGraphView = ({ data }: any) => {
    const graphData = data as IGraphReportMilageGrowth;
    const overAllMilageData = [{
        "data": convertMilageByMonthData(graphData.milageByMonthData)
    }];

    return graphData.milageByMonthData.length == 0
        ? <NoDataSegment />
        : <TotalMilageLine data={overAllMilageData} />
}


const TotalVehicleHistoriesByMonthCostsGraphView = ({ data }: any) => {
    const graphData = data as IGraphReportTotalVehicleHistoriesByMonthsCosts;
    const expensesByTypeAndMonth = convertVehicleHistoriesByTypeForMonth(graphData.vehicleHistoriesByMonthsData);
    return graphData.vehicleHistoriesByMonthsData.length == 0
        ? <NoDataSegment />
        : <ExpensesPerMonthBar data={expensesByTypeAndMonth} />
}

const FuelConsumptionsGraphView = ({ data }: any) => {
    const graphData = data as IGraphReportFuelConsumptions;
    const fuelingConsumptionsData = convertFuelingByTypeForMonth(graphData.fuelingsByTypeForMonthData);

    return graphData.fuelingsByTypeForMonthData.length == 0
        ? <NoDataSegment />
        : <FuelingConsumptionLine data={fuelingConsumptionsData} />
}

const FuelPricesGraphView = ({ data }: any) => {
    const graphData = data as IGraphReportFuelPrices;
    const fuelingPricesData = convertFuelingPricesByTypeForMonth(graphData.fuelingPricesByTypeForMonthData);

    return graphData.fuelingPricesByTypeForMonthData.length == 0
        ? <NoDataSegment />
        : <FuelingPricesLine data={fuelingPricesData} />
}

const SidebarStatisticsOverall = (props: any) => {
    const stats = props.stats as IVehiclesStatistics;
    const { t } = useTranslation();

    return <div>
        <IconedHeader
            iconName='chart bar'
            header={t("StatisticsHeader")}
            content={t("StatisticsSubheader")}
        />
        <Segment attached='bottom'>
            <Statistic.Group size='mini' horizontal>
                <Statistic>
                    <Statistic.Value>{stats.vehicles.length}</Statistic.Value>
                    <Statistic.Label>{t("AllVehicles")}</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{stats.totalVehiclesDistance} {t("Milage-Km")}</Statistic.Value>
                    <Statistic.Label>{t("StatisticsOverallDistance")}</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{stats.totalVehicleHistories}</Statistic.Value>
                    <Statistic.Label>{t("VehicleHistories")}</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{stats.totalVehicleHistoriesCost} {t("Currency-Lev")}</Statistic.Value>
                    <Statistic.Label>{t("TotalExpenses")}</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{stats.totalFuelingVehicleHistories} {t("Currency-Lev")}</Statistic.Value>
                    <Statistic.Label>{t("TotalFuelings")}</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{stats.totalRepairVehicleHistories} {t("Currency-Lev")}</Statistic.Value>
                    <Statistic.Label>{t("TotalRepairs")}</Statistic.Label>
                </Statistic>
                <Statistic>
                    <Statistic.Value>{stats.totalTaxVehicleHistories} {t("Currency-Lev")}</Statistic.Value>
                    <Statistic.Label>{t("TotalTaxes")}</Statistic.Label>
                </Statistic>
            </Statistic.Group>
        </Segment>
    </div>
}
