import * as React from 'react';
import { ResponsiveLine } from '@nivo/line'
import { ResponsiveBar } from '@nivo/bar';
import { ResponsivePie } from '@nivo/pie'
import { LegendProps } from '@nivo/legends';
import { useTranslation } from 'react-i18next';

const commonLineProperties = {
    margin: { top: 50, right: 20, bottom: 30, left: 50 },
    colors: { datum: 'color' },
    enablePointLabel: true,
    pointSize: 10,
    pointBorderWidth: 2,
    pointBorderColor: { from: 'serieColor' },
    pointLabel: "yFormatted",
    pointLabelYOffset: -12,
    useMesh: true,
    axisTop: null as any,
    axisRight: null as any,
    pointColor: { from: 'color' }
}

export const GetCommonLineLegends = () => {
    const legends: LegendProps[] = [
        {
            anchor: 'top-left',
            direction: 'row',
            justify: false,
            translateX: 0,
            translateY: -50,
            itemsSpacing: 10,
            itemDirection: 'left-to-right',
            itemWidth: 80,
            itemHeight: 20,
            itemOpacity: 0.75,
            symbolSize: 15,
            symbolShape: 'circle',
            symbolBorderColor: 'rgba(0, 0, 0, .5)',
            effects: [
                {
                    on: 'hover',
                    style: {
                        itemBackground: 'rgba(0, 0, 0, .03)',
                        itemOpacity: 1
                    }
                }
            ]
        }
    ]

    return legends;
}

export const CustomTooltip = ({ slice, text }: any) => {
    return (
        <div style={{
            background: 'white',
            padding: '4px 6px',
            borderRadius: '3px',
            boxShadow: '0px 1px 1px 0px rgba(92,91,92,.5)'
        }}>
            {slice.points.map((point: { id: any; serieColor: any; serieId: any; data: { xFormatted: any, yFormatted: any; }; }) => (
                <div key={point.id} >
                    <span style={{
                        display: 'inline-block',
                        width: '10px',
                        height: '10px',
                        backgroundColor: point.serieColor,
                        marginRight: '3px'
                    }}></span>
                    {text.replace("%yFormatted%", point.data.yFormatted.toString())
                        .replace("%serieId%", point.serieId.toString())
                        .replace("%xFormatted%", point.data.xFormatted.toString())
                    }
                </div>
            ))}
        </div>
    );
}

export const FuelingPricesLine = ({ data }: any) => {
    const { t } = useTranslation();

    return <ResponsiveLine
        {...commonLineProperties}
        data={data}
        xScale={{
            type: 'time',
            format: '%Y-%m-%d',
            precision: 'day',
        }}
        xFormat="time:%Y-%m-%d"
        yScale={{
            type: 'linear',
            stacked: false,
            min: 'auto',
            max: 'auto'
        }}
        axisBottom={{
            format: '%b %d',
            tickValues: 'every 12 month',
            legend: t("TimeScaleByDate"),
            legendPosition: 'middle',
            legendOffset: 20,
        }}
        axisLeft={{
            orient: 'left',
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: t("PricePerLiterInLev"),
            legendOffset: 15,
            legendPosition: 'middle',
        }}
        legends={GetCommonLineLegends()}
        enableSlices={"x"}
        sliceTooltip={({ slice }: any) => <CustomTooltip slice={slice} text={t("PriceLiterTooltip")} />}
    />
}

export const FuelingConsumptionLine = ({ data }: any) => {
    const { t } = useTranslation();

    return <ResponsiveLine
        {...commonLineProperties}
        margin={{ top: 50, right: 20, bottom: 70, left: 50 }}
        data={data}
        xScale={{ type: 'point' }}
        yScale={{
            type: 'linear',
            stacked: false,
            min: 'auto',
            max: 'auto'
        }}
        axisBottom={{
            orient: 'bottom',
            tickSize: 5,
            tickPadding: 5,
            tickRotation: -90,
            legend: t("TimeScaleByMonth"),
            legendPosition: 'middle',
            legendOffset: 65,
        }}
        axisLeft={{
            orient: 'left',
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: t("AverageFuelConsumptionPerLiter"),
            legendOffset: 15,
            legendPosition: 'middle',
        }}
        legends={GetCommonLineLegends()}
        pointLabel="y"
        enableSlices={"x"}
        sliceTooltip={({ slice }: any) => <CustomTooltip slice={slice} text={t("FuelConsumptionTooltip")} />}
    />
}

//TODO: Test this thang after BE translations
export const ExpensesPerMonthBar = ({ data }: any) => {
    const { t } = useTranslation();

    return <ResponsiveBar
        data={data}
        keys={[t("Repair"), t("Fueling"), t("Tax")]}
        indexBy="month"
        margin={{ top: 50, right: 20, bottom: 70, left: 50 }}
        padding={0.3}
        colors={{ scheme: 'nivo' }}
        axisTop={null}
        axisRight={null}
        axisBottom={{
            tickSize: 5,
            tickPadding: 5,
            tickRotation: -90,
            legend: t("TimeScaleByMonth"),
            legendPosition: 'middle',
            legendOffset: 65
        }}
        axisLeft={{
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: t("PriceInLev"),
            legendPosition: 'middle',
            legendOffset: 15
        }}
        labelSkipWidth={12}
        labelSkipHeight={12}
        labelTextColor={{ from: 'color', modifiers: [['darker', 1.6]] }}
        legends={[
            {
                dataFrom: 'keys',
                anchor: 'top-left',
                direction: 'row',
                justify: false,
                translateX: 0,
                translateY: -50,
                itemsSpacing: 10,
                itemWidth: 100,
                itemHeight: 20,
                itemDirection: 'left-to-right',
                itemOpacity: 0.85,
                symbolSize: 20,
                effects: [
                    {
                        on: 'hover',
                        style: {
                            itemOpacity: 1
                        }
                    }
                ]
            }
        ]}
        animate={true}
        motionStiffness={90}
        motionDamping={15}
    />
}

export const TotalMilageLine = ({ data }: any) => {
    const { t } = useTranslation();

    return <ResponsiveLine
        data={data}
        margin={{ top: 15, right: 20, bottom: 50, left: 70 }}
        xScale={{ type: 'point' }}
        yScale={{ type: 'linear', stacked: true, min: 'auto', max: 'auto' }}
        axisTop={null}
        axisRight={null}
        axisBottom={{
            orient: 'top',
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: t("TimeScaleByMonth"),
            legendOffset: 40,
            legendPosition: 'middle'
        }}
        axisLeft={{
            orient: 'left',
            tickSize: 5,
            tickPadding: 5,
            tickRotation: 0,
            legend: t("MilageInKm"),
            legendOffset: 15,
            legendPosition: 'middle'
        }}
        pointSize={10}
        pointColor={{ from: 'color' }}
        pointBorderWidth={2}
        pointBorderColor={{ from: 'serieColor' }}
        pointLabel="y"
        pointLabelYOffset={-12}
        useMesh={true}
        enableSlices={"x"}
        sliceTooltip={({ slice }: any) => {
            return (
                <div style={{
                    background: 'white',
                    padding: '4px 6px',
                    borderRadius: '3px',
                    boxShadow: '0px 1px 1px 0px rgba(92,91,92,.5)'
                }}>
                    {slice.points.map((point: { index: any; serieColor: any; data: { xFormatted: any, yFormatted: any; }; }) => (
                        <div key={point.index} >
                            <span style={{
                                display: 'inline-block',
                                width: '10px',
                                height: '10px',
                                backgroundColor: point.serieColor,
                                marginRight: '3px'
                            }}></span>
                            <span>{point.data.yFormatted} {t("MilageFor")} {point.data.xFormatted}</span>
                        </div>
                    ))}
                </div>
            )
        }}
    />
}

export const VehicleHistoriesPie = ({ data }: any) => (
    <ResponsivePie
        data={data}
        margin={{ top: 40, right: 80, bottom: 80, left: 80 }}
        innerRadius={0.5}
        padAngle={4}
        cornerRadius={3}
        colors={{ scheme: 'nivo' }}
        borderWidth={0}
        borderColor={{ from: 'color', modifiers: [['darker', 0.2]] }}
        radialLabelsSkipAngle={10}
        radialLabelsTextXOffset={6}
        radialLabelsTextColor="#333333"
        radialLabelsLinkOffset={0}
        radialLabelsLinkDiagonalLength={16}
        radialLabelsLinkHorizontalLength={24}
        radialLabelsLinkStrokeWidth={1}
        radialLabelsLinkColor={{ from: 'color' }}
        slicesLabelsSkipAngle={10}
        slicesLabelsTextColor="#333333"
        animate={true}
        motionStiffness={90}
        motionDamping={15}
        legends={[
            {
                anchor: 'bottom',
                direction: 'row',
                translateY: 56,
                itemWidth: 140,
                itemHeight: 18,
                itemTextColor: '#999',
                symbolSize: 18,
                symbolShape: 'circle',
                effects: [
                    {
                        on: 'hover',
                        style: {
                            itemTextColor: '#000'
                        }
                    }
                ]
            }
        ]}
    />
)