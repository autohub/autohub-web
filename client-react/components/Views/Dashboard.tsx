import * as React from "react";
import { RoutePaths } from '../Routes';
import { List, Grid, Icon, Divider, Segment, Statistic, Button, Message } from 'semantic-ui-react';
import AuthService from '../../services/Auth';
import { PageHeader, CenteredLoader } from "../Partials/PageComponents";
import { ExpirationVehicleHistoriesAccordion } from "../Partials/VehicleHistory/ExpirationVehicleHistoriesAccordion";
import { IconedHeader } from "../Partials/Messages";
import StatisticsService, { IVehiclesStatistics } from "../../services/Statistics";
import VehiclesService from "../../services/Vehicles";
import { IVehicle } from "../../typings/Vehicle";
import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

let authService = new AuthService();
let statisticsService = new StatisticsService();
let vehiclesService = new VehiclesService();

export const Dashboard = (props: any) => {
    const [vehicles, setVehicles] = useState([] as Array<IVehicle>);
    const [stats, setStats] = useState(null as IVehiclesStatistics);
    const [isLoading, setIsLoading] = useState(true);
    const { t } = useTranslation();

    useEffect(() => {
        vehiclesService.fetchAll().then((response) => {
            setVehicles(response.content);
        }).then(() => {
            statisticsService.fetch().then((response) => {
                setStats(response.content);
                setIsLoading(false);
            });
        });
    }, [])

    if (isLoading) {
        return <CenteredLoader />;
    }
    else {
        return <div>
            <PageHeader headerText={t("Nav-Dashboard")} {...props} />
            <Divider hidden />
            <Grid stackable columns={3}>
                <Grid.Row>
                    <Grid.Column>
                        <MessageBox vehicles={vehicles} />
                        <Vehicles {...props} />
                        <VehicleHistory vehicles={vehicles} {...props} />
                    </Grid.Column>
                    <Grid.Column>
                        <VehiclesStatistics stats={stats} {...props} />
                    </Grid.Column>
                    <Grid.Column>
                        <Profile {...props} />
                    </Grid.Column>
                </Grid.Row>
            </Grid>
        </div>
    }
}

export const MessageBox = (props: any) => {
    const vehicles = props.vehicles as Array<IVehicle>;
    return <div>
        {vehicles.length === 0 && <WelcomeMessage />}
        {vehicles.length > 0 && vehicles.every(e => e.totalVehicleHistories === 0) &&
            <NoVehicleHistoryMessage />
        }
    </div>
}

export const NoVehicleHistoryMessage = () => {
    const { t } = useTranslation();
    return <Message
        className='dash-col'
        color='teal'
        icon='info circle'
        header={t("Dashboard-NoVehicleHistoryMessage")}
        content={t("Dashboard-AddVehicleHistories")}
    />
}

export const WelcomeMessage = () => {
    const { t } = useTranslation();

    return <Message
        className='dash-col'
        color='teal'
        icon='info circle'
        header={t("Dashboard-WelcomeMessage")}
        content={t("Dashboard-NoVehiclesMessage")}
    />
}

export const Profile = (props: any) => {
    const { t } = useTranslation();

    const signOut = () => {
        authService.signOut();
        props.history.push(RoutePaths.Login, { signedOut: true });
    }

    return <div>
        <IconedHeader
            iconName='user'
            header={t("Dashboard-ProfileTitle")}
            content={t("Dashboard-Profile")}
        />
        <Segment attached='bottom'>
            <List relaxed>
                <List.Item>
                    <Icon name='user' />
                    <List.Content>
                        <List.Header as='a' onClick={() => props.history.push(RoutePaths.AccountEdit)}>
                            {t("Dashboard-ProfileSettings")}
                        </List.Header>
                    </List.Content>
                </List.Item>
                <List.Item>
                    <Icon name='key' />
                    <List.Content>
                        <List.Header as='a' onClick={() => props.history.push(RoutePaths.AccountPasswordChange)}>
                            {t("Dashboard-ProfileChangePassword")}
                        </List.Header>
                    </List.Content>
                </List.Item>
                <List.Item>
                    <Icon name='shutdown' />
                    <List.Content>
                        <List.Header as='a' onClick={() => signOut()}>
                            {t("Dashboard-ProfileLogout")}
                        </List.Header>
                    </List.Content>
                </List.Item>
            </List>
        </Segment>
    </div>
}

export const Vehicles = (props: any) => {
    const { t } = useTranslation();

    const menu = {
        icon: 'car',
        title: t("Dashboard-Vehicles"),
        content: t("Dashboard-VehiclesDesc"),
        items: [
            {
                icon: 'plus',
                route: RoutePaths.VehicleAdd,
                text: t("Dashboard-VehiclesAdd")
            },
            {
                icon: 'grid layout',
                route: RoutePaths.Vehicles,
                text: t("Dashboard-VehiclesList")
            },
        ]
    }

    return <div className='dash-col'>
        <IconedHeader
            iconName={menu.icon}
            header={menu.title}
            content={menu.content}
        />
        <Segment attached='bottom'>
            <List relaxed>
                {menu.items.map((i: any) =>
                    <List.Item>
                        <Icon name={i.icon} />
                        <List.Content>
                            <List.Header as='a' onClick={() => props.history.push(i.route)}>
                                {i.text}
                            </List.Header>
                        </List.Content>
                    </List.Item>
                )}
            </List>
        </Segment>
    </div>
}

export const VehicleHistory = (props: any) => {
    const { vehicles, history } = props;

    return <div>
        <ExpirationVehicleHistoriesAccordion
            vehicles={vehicles}
            history={history}
            isDashboardHeader='true'
        />
    </div>
}

export const VehiclesStatistics = (props: any) => {
    if (props.stats) {
        const stats = props.stats as IVehiclesStatistics;
        const { t } = useTranslation();

        return <div className='dash-col'>
            <IconedHeader
                iconName='chart bar'
                header={t("Dashboard-Statistics")}
                content={t("Dashboard-StatisticsDesc")}
            />
            <Segment attached='bottom'>
                <Statistic.Group size='mini' horizontal>
                    <Statistic>
                        <Statistic.Value>{stats.vehicles.length}</Statistic.Value>
                        <Statistic.Label>{t("Dashboard-StatisticsVehicles")}</Statistic.Label>
                    </Statistic>
                    <Statistic>
                        <Statistic.Value>{stats.totalVehicleHistories}</Statistic.Value>
                        <Statistic.Label>{t("Dashboard-StatisticsVehicleHistories")}</Statistic.Label>
                    </Statistic>
                    <Statistic>
                        <Statistic.Value>{stats.totalVehicleHistoriesCost} {t("Currency-Lev")}</Statistic.Value>
                        <Statistic.Label>{t("Dashboard-StatisticsTotals")}</Statistic.Label>
                    </Statistic>
                    <Button compact onClick={() => props.history.push(RoutePaths.Statistics)}>
                        {t("Dashboard-StatisticsMore")}
                    </Button>
                </Statistic.Group>
            </Segment>
        </div>
    }
    else {
        return (null);
    }
}
