import * as React from 'react';
import VehicleHistoryService from '../../../services/VehicleHistory'
import VehicleService from '../../../services/Vehicles'
import { RoutePaths } from '../../Routes';
import { Label, Divider, Form, TextArea, Input, Grid, Message } from 'semantic-ui-react'
import { PageHeader, FormActionsButtons, CenteredLoader } from '../../Partials/PageComponents';
import { IconedHeader, DetailedErrorMessage } from '../../Partials/Messages';
import { VehiclesToDropdownOptions, MapToDropdownOptions } from '../../../helpers/Options';
import { toLowerCaseErrors } from '../../../helpers/Utils';
import { IFuelingVehicleHistory } from '../../../typings/VehicleHistory';
import DatePicker from 'react-datepicker'
import * as moment from 'moment'
import { IVehicle } from '../../../typings/Vehicle';
import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { FuelType } from '../../../typings/Enums/VehicleHistories';
import { GetTranslatedEnumTypes } from '../../../helpers/EnumHelpers';

let vehicleHistoryService = new VehicleHistoryService();
let vehicleService = new VehicleService();

export const FuelingVehicleHistoryAdd = (props: any) => {
    const [vehicleHistory, setVehicleHistory] = useState({
        place: undefined,
        vehicleId: '',
        totalPrice: undefined,
        category: undefined,
        type: "Fueling",
        createdOn: moment.utc().local(),
        note: '',
        currentMilage: undefined,
        isPartial: false,
        isMissedPrevious: false,
        liters: undefined,
        pricePerLiter: undefined
    } as IFuelingVehicleHistory);
    const [vehicles, setVehicles] = useState([] as Array<IVehicle>);
    const [errors, setErrors] = useState({} as { [key: string]: string });
    const [isLoading, setIsLoading] = useState(true);
    const [isSubmitLoading, setSubmitIsLoading] = useState(false);
    const { t } = useTranslation();

    useEffect(() => {
        if (props.match.path == RoutePaths.FuelingVehicleHistoryEdit) {
            const type = props.location.pathname.split('/')[3];
            const vehicleId = props.match.params.id;
            vehicleHistoryService.fetch(vehicleId, type).then((response) => {
                setVehicleHistory(response.content.vehicleHistory as IFuelingVehicleHistory);
                setVehicles(response.content.vehicles);
                setIsLoading(false);
            });
        }
        else {
            let { vehicleId = '' } = props.location.state || {};
            vehicleService.fetchAll().then((response) => {
                setVehicles(response.content);
                setVehicleHistory({ ...Object.assign(vehicleHistory, { vehicleId }) });
                setIsLoading(false);
            });
        }
    }, [])

    const saveVehicleHistory = (vehicleHistory: IFuelingVehicleHistory) => {
        setErrors({} as { [key: string]: string });
        vehicleHistoryService.saveFueling(vehicleHistory).then((response) => {
            if (!response.is_error) {
                props.history.push(RoutePaths.VehicleHistories.replace(":id", vehicleHistory.vehicleId));
            } else {
                setErrors(toLowerCaseErrors(response.error_content));
                window.scrollTo(0, 0);
            }
        });
    }

    const handleSubmit = () => {
        setSubmitIsLoading(true);
        event.preventDefault();
        saveVehicleHistory(vehicleHistory);
        setSubmitIsLoading(false);
    }

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        let vehicleHistoryUpdates = {
            [name]: value
        }
        setVehicleHistory({ ...Object.assign(vehicleHistory, vehicleHistoryUpdates) as IFuelingVehicleHistory });
    }

    const handleDropDownChange = (event: any, data: any) => {
        let vehicleHistoryUpdates = {
            [data.name]: data.value
        }
        setVehicleHistory({ ...Object.assign(vehicleHistory, vehicleHistoryUpdates) as IFuelingVehicleHistory });
    }

    const handleChangeDate = (name: any, date: any) => {
        let vehicleHistoryUpdates = {
            [name]: date
        }
        setVehicleHistory({ ...Object.assign(vehicleHistory, vehicleHistoryUpdates) as IFuelingVehicleHistory });
    }

    const handleVehicleChange = (e: any, { value }: any) =>
        setVehicleHistory({ ...Object.assign(vehicleHistory, { vehicleId: value }) });

    const toggleIsPartial = () =>
        setVehicleHistory({ ...Object.assign(vehicleHistory, { isPartial: !vehicleHistory.isPartial }) });

    const toggleIsMissed = () =>
        setVehicleHistory({ ...Object.assign(vehicleHistory, { isMissedPrevious: !vehicleHistory.isMissedPrevious }) });

    const createPageTitle = () =>
        props.match.path == RoutePaths.FuelingVehicleHistoryEdit ? t("FuelingEditTitle") : t("FuelingAddTitle");

    const selectedVehicle = vehicles.find(v => v.id === vehicleHistory.vehicleId);
    const fuelTypesOpts = MapToDropdownOptions(GetTranslatedEnumTypes(FuelType));

    return isLoading ? <CenteredLoader /> :
        <div>
            <PageHeader headerText={createPageTitle()} {...props} />
            <Divider hidden />
            <Grid stackable columns={2}>
                <Grid.Column>
                    <IconedHeader
                        iconName='fire'
                        header={t("Fueling")}
                        content={t("RequiredFieldsNotice")}
                    />
                    <DetailedErrorMessage errors={errors} />
                    <Form className='attached fluid segment' onSubmit={handleSubmit} size='large'>
                        <Form.Group widths='equal'>
                            <Form.Field required error={!!errors.vehicleId}>
                                <Form.Select
                                    required
                                    label={t("Vehicle")}
                                    attached
                                    value={vehicleHistory.vehicleId}
                                    options={VehiclesToDropdownOptions(vehicles)}
                                    placeholder={t("ChooseVehicle")}
                                    onChange={handleVehicleChange}
                                    size="large"
                                    selection
                                    labeled
                                />
                                {errors.vehicleId &&
                                    <Label basic color='red' pointing>{errors.vehicleId}</Label>
                                }
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths='equal'>
                            <Form.Field required error={!!errors.category}>
                                <Form.Select
                                    required
                                    defaultValue={vehicleHistory.category}
                                    name="category"
                                    label={t("FuelType")}
                                    onChange={handleDropDownChange}
                                    options={fuelTypesOpts}
                                    placeholder={t("ChooseFuelType")}
                                    size="large"
                                    selection
                                />
                                {errors.category &&
                                    <Label basic color='red' pointing>{errors.category}</Label>
                                }
                            </Form.Field>
                            <Form.Field required error={!!errors.totalPrice}>
                                <label>{t("Price")}</label>
                                <Input label={{ basic: true, content: t("Currency-Lev") }}
                                    labelPosition='right'
                                    name='totalPrice'
                                    placeholder={t("EnterPrice")}
                                    value={vehicleHistory.totalPrice}
                                    onChange={handleInputChange}
                                />
                                {errors.totalPrice &&
                                    <Label basic color='red' pointing>{errors.totalPrice}</Label>
                                }
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths='equal'>
                             <Form.Field required error={!!errors.currentMilage}>
                                <label>{t("CurrentMilage")}</label>
                                <Input
                                    name='currentMilage'
                                    placeholder={t("EnterMilage")}
                                    label={{ basic: true, content: t("Milage-Km") }}
                                    labelPosition='right'
                                    value={vehicleHistory.currentMilage}
                                    onChange={handleInputChange}
                                />
                                {selectedVehicle && <span>{t("Previous")}: {selectedVehicle.milage} {t("Milage-Km")}</span>}
                                {errors.currentMilage &&
                                    <Label basic color='red' pointing>{errors.currentMilage}</Label>
                                }
                            </Form.Field>
                            <Form.Field required error={!!errors.createdOn}>
                                <label>{t("DateCreated")}</label>
                                <DatePicker
                                    selected={moment.utc(vehicleHistory.createdOn).local()}
                                    onChange={handleChangeDate.bind(this, 'createdOn')}
                                    placeholderText={t("EnterDateCreated")}
                                    name="createdOn"
                                    peekNextMonth
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    className='datepicker'
                                />
                                {errors.createdOn &&
                                    <Label basic color='red' pointing>{errors.createdOn}</Label>
                                }
                            </Form.Field>
                        </Form.Group>
                        <Form.Group>
                            <Form.Field className='field-aligned' required error={!!errors.pricePerLiter}>
                                <label>{t("PricePerLiter")}</label>
                                <Input
                                    name='pricePerLiter'
                                    placeholder={t("EnterPricePerLiter")}
                                    label={{ basic: true, content: t("Currency-Lev") }}
                                    labelPosition='right'
                                    value={vehicleHistory.pricePerLiter}
                                    onChange={handleInputChange}
                                />
                                {errors.pricePerLiter &&
                                    <Label basic color='red' pointing>{errors.pricePerLiter}</Label>
                                }
                            </Form.Field>
                            <Form.Field className='field-aligned' required error={!!errors.liters}>
                                <label>{t("Liters")}</label>
                                <Input
                                    name='liters'
                                    placeholder={t("EnterLiters")}
                                    label={{ basic: true, content: t("Quantity-L") }}
                                    labelPosition='right'
                                    value={vehicleHistory.liters}
                                    onChange={handleInputChange}
                                />
                                {errors.liters &&
                                    <Label basic color='red' pointing>{errors.liters}</Label>
                                }
                            </Form.Field>
                        </Form.Group>
                        <Form.Group>
                            <Form.Field className='field-aligned'>
                                <label>{t("FilledTank")}</label>
                                <Form.Checkbox toggle
                                    value='1'
                                    onChange={toggleIsPartial}
                                    name='isPartial'
                                    label={vehicleHistory.isPartial == false ? t("Yes") : t("No")}
                                    checked={!vehicleHistory.isPartial}
                                />
                            </Form.Field>
                            <Form.Field className='field-aligned'>
                                <label>{t("MissedFueling")}</label>
                                <Form.Checkbox toggle
                                    value='1'
                                    onChange={toggleIsMissed}
                                    name='isMissed'
                                    label={vehicleHistory.isMissedPrevious == true ? t("Yes") : t("No")}
                                    checked={!!vehicleHistory.isMissedPrevious}
                                />
                            </Form.Field>
                        </Form.Group>
                        <Form.Field>
                            <label>{t("Note")}</label>
                            <Form.Input
                                control={TextArea}
                                name='note'
                                rows={1}
                                placeholder={t("EnterNote")}
                                size="large"
                                value={vehicleHistory.note}
                                onChange={handleInputChange}
                            />
                        </Form.Field>
                        <FormActionsButtons isLoading={isSubmitLoading} {...props} />
                    </Form>
                </Grid.Column>
                <Grid.Column>
                    <Message>
                        <Message.Header>{t("Important!")}</Message.Header>
                        <Message.List items={[t("FuelConsumptionInfo")]} />
                    </Message>
                </Grid.Column>
            </Grid>
        </div>
}
