import 'object-assign';
import * as React from 'react';
import { Link } from 'react-router-dom';
import VehicleHistoryService from '../../../services/VehicleHistory';
import { Label, Segment, Divider, Header, Grid, Button, Icon } from 'semantic-ui-react';
import * as moment from 'moment';
import { PageHeader, CenteredLoader } from '../../Partials/PageComponents';
import { mapExpirationInfo } from '../../../services/RemindableVehicleHistory';
import { IRemindableVehicleHistory } from '../../../typings/RemindableVehicleHistories';
import { IVehicleHistory, IFuelingVehicleHistory, ITaxVehicleHistory, IRepairVehicleHistory } from '../../../typings/VehicleHistory';
import { updateRenew } from '../../Helpers';
import { VehicleHistoryType, FuelType, TaxVehicleHistoryCategory, RepairVehicleHistoryCategory } from '../../../typings/Enums/VehicleHistories';
import { CreateVehicleHistoryModal } from '../../Partials/VehicleHistory/CreateVehicleHistoryModal';
import { IVehicle } from '../../../typings/Vehicle';
import { RoutePaths } from '../../Routes';
import { GetTranslatedEnumTypes } from '../../../helpers/EnumHelpers';
import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';

let vehicleHistoryService = new VehicleHistoryService();

export const VehicleHistoryView = (props: any) => {
    const [vehicleHistory, setVehicleHistory] = useState(null as IVehicleHistory);
    const [vehicles, setVehicles] = useState([] as Array<IVehicle>);
    const [isLoading, setIsLoading] = useState(true);
    const { t } = useTranslation();

    useEffect(() => {
        const type = props.location.pathname.split('/')[3];
        const vehicleId = props.match.params.id;
        vehicleHistoryService.fetch(vehicleId, type).then((response) => {
            setVehicleHistory(response.content.vehicleHistory as IRemindableVehicleHistory);
            setVehicles(response.content.vehicles);
            setIsLoading(false);
        });
    }, [])

    const vehicle = vehicles.find(x => x.id == vehicleHistory.vehicleId);
    let types: Map<string, any> = new Map<string, any>();
    types.set("Tax", <TaxVehicleHistoryView vehicleHistory={vehicleHistory} vehicle={vehicle} {...props} />);
    types.set("Repair", <RepairVehicleHistoryView vehicleHistory={vehicleHistory} vehicle={vehicle} {...props} />);
    types.set("Fueling", <FuelingVehicleHistoryView vehicleHistory={vehicleHistory} vehicle={vehicle} />);

    return isLoading ? <CenteredLoader /> : <div>
        <PageHeader headerText={t("Vehicles-History-Details")} {...props} />
        <CreateVehicleHistoryModal vehicleId={vehicleHistory.vehicleId} {...props} color='green' />
        <Divider hidden />
        <Grid stackable columns={2}>
            <Grid.Column>
                {types.get(vehicleHistory.type)}
            </Grid.Column>
        </Grid>
    </div >
}

const SegmentLabel = ({ isPositive }: any) => {
    const { t } = useTranslation();
    return <Label style={{ marginLeft: '0px' }} basic color={isPositive ? 'green' : 'red'}>
        {isPositive ? t("Yes") : t("No")}
    </Label>
}

const SegmentHeader = ({ type, category, iconName }: any) =>
    <Segment>
        <Header as='h3'>
            <Icon name={iconName} className='mainIconColor' />
            <Header.Content>
                {type} - {category}
            </Header.Content>
        </Header>
    </Segment>

const SegmentCommon = ({ vehicle, vehicleHistory }: any) => {
    const { t } = useTranslation();

    return <Segment.Group horizontal attached>
        <Segment>
            <Header as='h4'>
                <Link to={RoutePaths.VehicleView.replace(":id", vehicle.id.toString())}>{vehicle.title}</Link>
                <Header.Subheader>{t("Vehicle")}</Header.Subheader>
            </Header>
        </Segment>
        <Segment>
            <Header as='h4'>{vehicleHistory.currentMilage} {t("Milage-Km")}
                <Header.Subheader>{t("OnMilage")}</Header.Subheader>
            </Header>
        </Segment>
        <Segment>
            <Header as='h4'>{vehicleHistory.totalPrice} {t("Currency-Lev")}
                <Header.Subheader>{t("Price")}</Header.Subheader>
            </Header>
        </Segment>
    </Segment.Group>
}

const SegmentMeta = ({ vehicleHistory }: any) => {
    const { t } = useTranslation();

    return <React.Fragment>
        {!!vehicleHistory.note &&
            <Segment>
                <Header as='h4'>{vehicleHistory.note}
                    <Header.Subheader>{t("Note")}</Header.Subheader>
                </Header>
            </Segment>
        }
        <Segment attached='bottom'>
            <span>{t("Added")}: {moment.utc(vehicleHistory.createdOn).local().fromNow()}</span>
            {!!vehicleHistory.updatedOn &&
                <span> / {t("Changed")}: {moment.utc(vehicleHistory.updatedOn).local().fromNow()} </span>
            }
        </Segment>
    </React.Fragment>
}

const FuelingVehicleHistoryView = ({ vehicleHistory, vehicle }: any) => {
    const vh = vehicleHistory as IFuelingVehicleHistory;
    const vehicleHistoryTypes = GetTranslatedEnumTypes(VehicleHistoryType);
    const taxCategoryTypes = GetTranslatedEnumTypes(FuelType);
    const type = vehicleHistoryTypes.get(vh.type);
    const category = taxCategoryTypes.get(vh.category);
    const iconName = 'fire';
    const { t } = useTranslation();

    return (
        <Segment.Group>
            <SegmentHeader type={type} category={category} iconName={iconName} />
            <SegmentCommon vehicle={vehicle} vehicleHistory={vh} />
            <Segment.Group horizontal attached>
                <Segment>
                    <Header as='h4'>{vh.pricePerLiter} {t("Currency-Lev")}
                        <Header.Subheader>{t("PricePerLiter")}</Header.Subheader>
                    </Header>
                </Segment>
                <Segment>
                    <Header as='h4'>{vh.liters} {t("Quantity-L")}
                        <Header.Subheader>{t("Liters")}</Header.Subheader>
                    </Header>
                </Segment>
            </Segment.Group>
            <Segment.Group horizontal attached>
                <Segment>
                    <Header as='h4'>
                        <SegmentLabel isPositive={vh.isMissedPrevious} />
                        <Header.Subheader>{t("MissedFueling")}</Header.Subheader>
                    </Header>
                </Segment>
                <Segment>
                    <Header as='h4'>
                        <SegmentLabel isPositive={!vh.isPartial} />
                        <Header.Subheader>{t("FilledTank")}</Header.Subheader>
                    </Header>
                </Segment>
            </Segment.Group>
            <SegmentMeta vehicleHistory={vh} />
        </Segment.Group>
    );
}

const TaxVehicleHistoryView = ({ vehicleHistory, vehicle, history }: any) => {
    const vh = mapExpirationInfo(vehicleHistory) as ITaxVehicleHistory;
    const vehicleHistoryTypes = GetTranslatedEnumTypes(VehicleHistoryType);
    const taxCategoryTypes = GetTranslatedEnumTypes(TaxVehicleHistoryCategory);
    const type = vehicleHistoryTypes.get(vh.type);
    const category = taxCategoryTypes.get(vh.category);
    const iconName = 'dollar sign';

    return (
        <Segment.Group>
            <SegmentHeader type={type} category={category} iconName={iconName} />
            {(vh.isRemindableByDate || vh.isRemindableByMilage) &&
                <ExpirationDateMessage vehicleHistory={vh} history={history} />
            }
            <SegmentCommon vehicle={vehicle} vehicleHistory={vh} />
            <SegmentMeta vehicleHistory={vh} />
        </Segment.Group>
    );
}

const RepairVehicleHistoryView = ({ vehicleHistory, vehicle, history }: any) => {
    const vh = mapExpirationInfo(vehicleHistory) as IRepairVehicleHistory;
    const vehicleHistoryTypes = GetTranslatedEnumTypes(VehicleHistoryType);
    const taxCategoryTypes = GetTranslatedEnumTypes(RepairVehicleHistoryCategory);
    const type = vehicleHistoryTypes.get(vh.type);
    const category = taxCategoryTypes.get(vh.category);
    const iconName = 'wrench';

    return (
        <Segment.Group>
            <SegmentHeader type={type} category={category} iconName={iconName} />
            {(vh.isRemindableByDate || vh.isRemindableByMilage) &&
                <ExpirationDateMessage vehicleHistory={vh} history={history} />
            }
            <SegmentCommon vehicle={vehicle} vehicleHistory={vh} />
            <SegmentMeta vehicleHistory={vh} />
        </Segment.Group>
    );
}

export const ExpirationDateMessage = (props: any) => {
    const vh = props.vehicleHistory as IRemindableVehicleHistory;
    const { t } = useTranslation();

    if (vh.completedOn) {
        const date = moment.utc(vh.completedOn).local().calendar();
        return <Segment>
            <Header as='h4'>{t("Renewed")}
                <Label horizontal>{date.toString().toLowerCase()}</Label>
            </Header>
        </Segment>;
    }
    else {
        return <Segment>
            <Header as='h4'>
                <span style={{ float: 'left' }}>
                    {t("ScheduledFor")}
                    {vh.isRemindableByDate &&
                        <Label color='red' horizontal>{vh.expirationInfoDate}</Label>
                    }
                    {vh.isRemindableByMilage &&
                        <Label color='red' horizontal>{vh.expirationInfoMilage}</Label>
                    }
                </span>
                {!vh.completedOn &&
                    <Button size='small' style={{ float: 'right' }} compact onClick={() => updateRenew(vh as IRemindableVehicleHistory, props.history)}>
                        <Icon name="refresh" />  {t("Renew")}
                    </Button>
                }
                <div style={{ clear: 'both' }}></div>
            </Header>
        </Segment>
    }
}
