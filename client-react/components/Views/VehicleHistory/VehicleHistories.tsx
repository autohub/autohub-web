import * as React from "react";
import VehicleHistoryService from '../../../services/VehicleHistory';
import {
    Divider,
    Segment,
    Feed,
    Grid,
    Pagination,
    Icon,
    Form
} from 'semantic-ui-react'
import { PageHeader, CenteredLoader } from "../../Partials/PageComponents";
import { CreateVehicleHistoryModal } from "../../Partials/VehicleHistory/CreateVehicleHistoryModal";
import { SegmentMessage } from "../../Partials/Messages";
import { FeedItem } from "../../Partials/VehicleHistory/FeedItem";
import { IVehicleHistory, IPagedListVehicleHistoriesRequest } from "../../../typings/VehicleHistory";
import { IVehicle } from "../../../typings/Vehicle";
import { FuelType, VehicleHistoryType, TaxVehicleHistoryCategory, RepairVehicleHistoryCategory } from "../../../typings/Enums/VehicleHistories";
import { VehiclesToDropdownOptions, MapToDropdownOptions } from "../../../helpers/Options";
import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { GetTranslatedEnumTypes } from "../../../helpers/EnumHelpers";

let vehicleHistoryService = new VehicleHistoryService();
export interface IVehicleHistoryFilter {
    vehicleId: string,
    type: string,
    category: string
}

export const VehicleHistories = (props: any) => {
    const [vehicleHistories, setVehicleHistories] = useState([] as Array<IVehicleHistory>);
    const [vehicles, setVehicles] = useState([] as Array<IVehicle>);
    const [isLoadingItems, setIsLoadingItems] = useState(true);
    const [activePage, setActivePage] = useState(1);
    const [pagesCount, setPagesCount] = useState(1);
    const [perPage] = useState(10);
    const [filter, setFilter] = useState({
        vehicleId: props.location.state?.vehicleId || '',
        type: props.location.state?.type || '',
        category: ''
    } as IVehicleHistoryFilter)
    const { t } = useTranslation();

    useEffect(() => {
        setActivePage(() => 1);
        setIsLoadingItems(true);
    }, [filter])

    useEffect(() => {
        setIsLoadingItems(true);
    }, [activePage])

    useEffect(() => {
        if (isLoadingItems) {
            loadPageItems();
        }
    }, [isLoadingItems])

    const loadPageItems = () => {
        const req = {
            perPage,
            activePage,
            vehicleId: filter.vehicleId,
            type: filter.type,
            category: filter.category
        } as IPagedListVehicleHistoriesRequest;

        vehicleHistoryService
            .fetchAll(req)
            .then((response) => {
                setVehicles(response.content.vehicles);
                setVehicleHistories(response.content.vehicleHistories);
                setPagesCount(response.content.pagesCount);
                setIsLoadingItems(false);
            });
    }

    const handlePaginationChange = (e: any, { activePage }: any) => {
        setActivePage(activePage);
        window.scrollTo(0, 0);
    }

    const handleFilterChange = (e: any, { value, name }: any) => {
        let filterUpdate = {
            [name]: value
        }
        var res = { ...Object.assign(filter, filterUpdate) as IVehicleHistoryFilter };
        res.category = res.type ? res.category : '';
        setFilter(res);
    }

    const deleteFunc = (vehicleHistory: IVehicleHistory) => {
        vehicleHistoryService.delete(vehicleHistory.id, vehicleHistory.type).then(() => {
            let updateVehicleHistories = vehicleHistories;
            updateVehicleHistories.splice(updateVehicleHistories.indexOf(vehicleHistory), 1);
            setVehicleHistories([...updateVehicleHistories]);
        });
    }

    const getCategoryTypes = () => {
        let types: Map<string, any> = new Map<string, any>();
        types.set("Tax", TaxVehicleHistoryCategory);
        types.set("Repair", RepairVehicleHistoryCategory);
        types.set("Fueling", FuelType);

        return types;
    }

    const types = getCategoryTypes();
    const categoryOpts = MapToDropdownOptions(GetTranslatedEnumTypes(types.get(filter.type)));
    const typesOpts = MapToDropdownOptions(GetTranslatedEnumTypes(VehicleHistoryType));

    return <div>
        <PageHeader headerText={t("Vehicles-History")} {...props} />
        <CreateVehicleHistoryModal vehicleId={filter.vehicleId} {...props} color='green' />
        <Divider hidden />
        <Segment>
            <Form>
                <Form.Group widths='equal'>
                    <Form.Field>
                        <label>{t("FilterByVehicle")}</label>
                        <Form.Select
                            clearable
                            value={filter.vehicleId}
                            options={VehiclesToDropdownOptions(vehicles)}
                            placeholder={t("AllVehiclesPlaceholder")}
                            onChange={handleFilterChange}
                            name="vehicleId"
                            fluid
                            selection
                            labeled
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>{t("FilterByType")}</label>
                        <Form.Select
                            clearable
                            value={filter.type}
                            options={typesOpts}
                            placeholder={t("AllTypesPlaceholder")}
                            onChange={handleFilterChange}
                            name='type'
                            fluid
                            selection
                            labeled
                        />
                    </Form.Field>
                    <Form.Field>
                        <label>{t("FilterByCategory")}</label>
                        <Form.Select
                            clearable
                            value={filter.category}
                            options={categoryOpts}
                            placeholder={t("AllCategoriesPlaceholder")}
                            onChange={handleFilterChange}
                            name='category'
                            fluid
                            selection
                            labeled
                        />
                    </Form.Field>
                </Form.Group>
            </Form>
        </Segment>
        <Grid stackable columns={2}>
            <Grid.Column>
                {isLoadingItems &&
                    <CenteredLoader />
                }
                {!isLoadingItems &&
                    <React.Fragment>
                        {vehicleHistories && vehicleHistories.length > 0 &&
                            <React.Fragment>
                                <Segment>
                                    <Feed>
                                        {vehicleHistories.map((vehicleHistory) =>
                                            <FeedItem
                                                vehicleHistory={vehicleHistory}
                                                delete={deleteFunc}
                                                {...props}
                                            />
                                        )}
                                    </Feed>
                                </Segment>
                                <Divider hidden />
                                {pagesCount > 1 &&
                                    <Pagination
                                        onPageChange={handlePaginationChange}
                                        activePage={activePage}
                                        totalPages={pagesCount}
                                        size='small'
                                        name='activePage'
                                        siblingRange={0}
                                        boundaryRange={0}
                                        ellipsisItem={{ content: <Icon name='ellipsis horizontal' />, icon: true }}
                                        firstItem={{ content: <Icon name='angle double left' />, icon: true }}
                                        lastItem={{ content: <Icon name='angle double right' />, icon: true }}
                                        prevItem={{ content: <Icon name='angle left' />, icon: true }}
                                        nextItem={{ content: <Icon name='angle right' />, icon: true }}
                                    />
                                }
                            </React.Fragment>
                        }
                        {Object.keys(vehicleHistories).length === 0 &&
                            <SegmentMessage title={t("NoVehicleHistories")} />
                        }
                    </React.Fragment>
                }
            </Grid.Column>
        </Grid>
    </div>
}
