import * as React from 'react';
import VehicleHistoryService from '../../../services/VehicleHistory'
import VehicleService from '../../../services/Vehicles'
import { RoutePaths } from '../../Routes';
import { Label, Divider, Form, TextArea, Input, Grid } from 'semantic-ui-react'
import DatePicker from 'react-datepicker'
import * as moment from 'moment'
import { PageHeader, FormActionsButtons, CenteredLoader } from '../../Partials/PageComponents';
import { IconedHeader, DetailedErrorMessage } from '../../Partials/Messages';
import { GetNotificationDaysRangeOptions, GetNotificationMilageRangeOptions, VehiclesToDropdownOptions, MapToDropdownOptions } from '../../../helpers/Options';
import { toLowerCaseErrors } from '../../../helpers/Utils';
import { IRepairVehicleHistory } from '../../../typings/VehicleHistory';
import { IVehicle } from '../../../typings/Vehicle';
import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { GetTranslatedEnumTypes } from '../../../helpers/EnumHelpers';
import { RepairVehicleHistoryCategory } from '../../../typings/Enums/VehicleHistories';

let vehicleHistoryService = new VehicleHistoryService();
let vehicleService = new VehicleService();

export const RepairVehicleHistoryAdd = (props: any) => {
    const [vehicleHistory, setVehicleHistory] = useState({
        vehicleId: '',
        type: "Repair",
        category: undefined,
        totalPrice: undefined,
        place: undefined,
        createdOn: moment.utc().local(),
        note: '',
        currentMilage: undefined,
        expiresOn: undefined,
        daysRange: undefined,
        isRemindableByDate: false,
        isRemindableByMilage: false,
        expiresOnMilage: undefined,
        milageRange: undefined
    } as IRepairVehicleHistory);
    const [vehicles, setVehicles] = useState([] as Array<IVehicle>);
    const [errors, setErrors] = useState({} as { [key: string]: string });
    const [isLoading, setIsLoading] = useState(true);
    const [isSubmitLoading, setSubmitIsLoading] = useState(false);
    const { t } = useTranslation();

    useEffect(() => {
        if (props.match.path == RoutePaths.RepairVehicleHistoryEdit) {
            const type = props.location.pathname.split('/')[3];
            const vehicleId = props.match.params.id;
            vehicleHistoryService.fetch(vehicleId, type).then((response) => {
                setVehicleHistory(response.content.vehicleHistory as IRepairVehicleHistory);
                setVehicles(response.content.vehicles);
                setIsLoading(false);
            });
        }
        else {
            let { vehicleId = '' } = props.location.state || {};
            vehicleService.fetchAll().then((response) => {
                setVehicles(response.content);
                setVehicleHistory({ ...Object.assign(vehicleHistory, { vehicleId }) });
                setIsLoading(false);
            });
        }
    }, [])

    const saveVehicleHistory = (vehicleHistory: IRepairVehicleHistory) => {
        setErrors({} as { [key: string]: string });
        vehicleHistoryService.saveRepair(vehicleHistory).then((response) => {
            if (!response.is_error) {
                props.history.push(RoutePaths.VehicleHistories.replace(":id", vehicleHistory.vehicleId));
            } else {
                setErrors(toLowerCaseErrors(response.error_content));
                window.scrollTo(0, 0);
            }
        });
    }

    const handleSubmit = () => {
        setSubmitIsLoading(true);
        event.preventDefault();
        saveVehicleHistory(vehicleHistory);
        setSubmitIsLoading(false);
    }

    const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        let vehicleHistoryUpdates = {
            [name]: value
        }
        setVehicleHistory({ ...Object.assign(vehicleHistory, vehicleHistoryUpdates) as IRepairVehicleHistory });
    }

    const handleDropDownChange = (event: any, data: any) => {
        let vehicleHistoryUpdates = {
            [data.name]: data.value
        }
        setVehicleHistory({ ...Object.assign(vehicleHistory, vehicleHistoryUpdates) as IRepairVehicleHistory });
    }

    const handleChangeDate = (name: any, date: any) => {
        let vehicleHistoryUpdates = {
            [name]: date
        }
        setVehicleHistory({ ...Object.assign(vehicleHistory, vehicleHistoryUpdates) as IRepairVehicleHistory });
    }

    const handleVehicleChange = (e: any, { value }: any) =>
        setVehicleHistory({ ...Object.assign(vehicleHistory, { vehicleId: value }) });

    const toggleDateReminder = () =>
        setVehicleHistory({ ...Object.assign(vehicleHistory, { isRemindableByDate: !vehicleHistory.isRemindableByDate }) });

    const toggleMilageReminder = () =>
        setVehicleHistory({ ...Object.assign(vehicleHistory, { isRemindableByMilage: !vehicleHistory.isRemindableByMilage }) });

    const createPageTitle = () =>
        props.match.path == RoutePaths.RepairVehicleHistoryEdit ? t("RepairEditTitle") : t("RepairAddTitle");

    const selectedVehicle = vehicles.find(v => v.id === vehicleHistory.vehicleId);
    const categoryOpts = MapToDropdownOptions(GetTranslatedEnumTypes(RepairVehicleHistoryCategory));

    return isLoading ? <CenteredLoader /> :
        <div>
            <PageHeader headerText={createPageTitle()} {...props} />
            <Divider hidden />
            <Grid stackable columns={2}>
                <Grid.Column>
                    <IconedHeader
                        iconName='wrench'
                        header={t("Repair")}
                        content={t("RequiredFieldsNotice")}
                    />
                    <DetailedErrorMessage errors={errors} />
                    <Form className='attached fluid segment' size='large' onSubmit={handleSubmit}>
                        <Form.Group widths='equal'>
                            <Form.Field required error={!!errors.vehicleId}>
                                <Form.Select
                                    required
                                    label={t("Vehicle")}
                                    attached
                                    value={vehicleHistory.vehicleId}
                                    options={VehiclesToDropdownOptions(vehicles)}
                                    placeholder={t("ChooseVehicle")}
                                    onChange={handleVehicleChange}
                                    size="large"
                                    selection
                                    labeled
                                />
                                {errors.vehicleId &&
                                    <Label basic color='red' pointing>{errors.vehicleId}</Label>
                                }
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths='equal'>
                            <Form.Field required error={!!errors.category}>
                                <Form.Select
                                    required
                                    defaultValue={vehicleHistory.category}
                                    name="category"
                                    label={t("Category")}
                                    onChange={handleDropDownChange.bind(this)}
                                    options={categoryOpts}
                                    placeholder={t("ChooseCategory")}
                                    size="large"
                                    selection
                                />
                                {errors.category &&
                                    <Label basic color='red' pointing>{errors.category}</Label>
                                }
                            </Form.Field>
                            <Form.Field required error={!!errors.totalPrice}>
                                <label>{t("Price")}</label>
                                <Input
                                    label={{ basic: true, content: t("Currency-Lev") }}
                                    labelPosition='right'
                                    name='totalPrice'
                                    placeholder={t("EnterPrice")}
                                    value={vehicleHistory.totalPrice}
                                    onChange={handleInputChange}
                                />
                                {errors.totalPrice &&
                                    <Label basic color='red' pointing>{errors.totalPrice}</Label>
                                }
                            </Form.Field>
                        </Form.Group>
                        <Form.Group widths='equal'>
                            <Form.Field required error={!!errors.currentMilage}>
                                <label>{t("CurrentMilage")}</label>
                                <Input
                                    name='currentMilage'
                                    placeholder={t("EnterMilage")}
                                    label={{ basic: true, content: t("Milage-Km") }}
                                    labelPosition='right'
                                    value={vehicleHistory.currentMilage}
                                    onChange={handleInputChange}
                                />
                                {selectedVehicle && <span>{t("Previous")}: {selectedVehicle.milage} {t("Milage-Km")}</span>}
                                {errors.currentMilage &&
                                    <Label basic color='red' pointing>{errors.currentMilage}</Label>
                                }
                            </Form.Field>
                            <Form.Field required error={!!errors.createdOn}>
                                <label>{t("DateCreated")}</label>
                                <DatePicker
                                    selected={moment.utc(vehicleHistory.createdOn).local()}
                                    onChange={handleChangeDate.bind(this, 'createdOn')}
                                    placeholderText={t("EnterDateCreated")}
                                    name="createdOn"
                                    peekNextMonth
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                    className='datepicker'
                                />
                                {errors.createdOn &&
                                    <Label basic color='red' pointing>{errors.createdOn}</Label>
                                }
                            </Form.Field>
                        </Form.Group>
                        <Divider />
                        <Form.Group>
                            <Form.Field width={4} className='field-aligned'>
                                <label>{t("ReminderByDate")}</label>
                                <Form.Checkbox toggle
                                    value='1'
                                    onChange={toggleDateReminder}
                                    name='isRemindableByDate'
                                    label={vehicleHistory && vehicleHistory.isRemindableByDate ? t("Yes") : t("No")}
                                    checked={vehicleHistory && !!vehicleHistory.isRemindableByDate}
                                />
                            </Form.Field>
                            <Form.Field width={8} className='field-aligned'>
                                <label>{t("ReminderToDate")}</label>
                                <DatePicker
                                    selected={vehicleHistory && vehicleHistory.expiresOn ? moment.utc(vehicleHistory.expiresOn).local() : null}
                                    onChange={handleChangeDate.bind(this, 'expiresOn')}
                                    placeholderText={t("EnterReminderToDate")}
                                    name="expiresOn"
                                    peekNextMonth
                                    showMonthDropdown
                                    showYearDropdown
                                    dropdownMode="select"
                                />
                            </Form.Field>
                            <Form.Field width={8} className='field-aligned' error={!!errors.daysRange}>
                                <Form.Select
                                    style={{ minWidth: '10em' }}
                                    defaultValue={vehicleHistory && vehicleHistory.daysRange ? vehicleHistory.daysRange.toString() : ""}
                                    name="daysRange"
                                    label={t("DaysRange")}
                                    onChange={handleDropDownChange.bind(this)}
                                    options={GetNotificationDaysRangeOptions()}
                                    placeholder={t("ChooseDaysRange")}
                                    size="large"
                                    selection
                                />
                                {errors.daysRange &&
                                    <Label basic color='red' pointing>{errors.daysRange}</Label>
                                }
                            </Form.Field>
                        </Form.Group>
                        <Divider />
                        <Form.Group>
                            <Form.Field width={4} className='field-aligned'>
                                <label>{t("ReminderByMilage")}</label>
                                <Form.Checkbox toggle
                                    value='1'
                                    onChange={toggleMilageReminder}
                                    name='isRemindableByMilage'
                                    label={vehicleHistory && vehicleHistory.isRemindableByMilage ? t("Yes") : t("No")}
                                    checked={vehicleHistory && !!vehicleHistory.isRemindableByMilage}
                                />
                            </Form.Field>
                            <Form.Field width={8} className='field-aligned' error={!!errors.expiresOnMilage}>
                                <label>{t("ReminderToMilage")}</label>
                                <Input
                                    label={{ basic: true, content: t("Milage-Km") }}
                                    labelPosition='right'
                                    name='expiresOnMilage'
                                    placeholder={t("EnterReminderToMilage")}
                                    value={vehicleHistory.expiresOnMilage}
                                    onChange={handleInputChange}
                                />
                                {errors.expiresOnMilage &&
                                    <Label basic color='red' pointing>{errors.expiresOnMilage}</Label>
                                }
                            </Form.Field>
                            <Form.Field width={8} className='field-aligned' error={!!errors.milageRange}>
                                <Form.Select
                                    style={{ minWidth: '10em' }}
                                    defaultValue={vehicleHistory.milageRange ? vehicleHistory.milageRange.toString() : ""}
                                    name="milageRange"
                                    label={t("MilageRange")}
                                    onChange={handleDropDownChange.bind(this)}
                                    options={GetNotificationMilageRangeOptions()}
                                    placeholder={t("EnterMilageRange")}
                                    size="large"
                                    selection
                                />
                                {errors.milageRange &&
                                    <Label basic color='red' pointing>{errors.milageRange}</Label>
                                }
                            </Form.Field>
                        </Form.Group>
                        <Divider />
                        <Form.Group widths='equal'>
                            <Form.Field>
                                <label>{t("Note")}</label>
                                <Form.Input
                                    control={TextArea}
                                    name='note'
                                    rows={1}
                                    placeholder={t("EnterNote")}
                                    size="large"
                                    value={vehicleHistory.note}
                                    onChange={handleInputChange}
                                />
                            </Form.Field>
                        </Form.Group>
                        <FormActionsButtons isLoading={isSubmitLoading} {...props} />
                    </Form>
                </Grid.Column>
            </Grid>
        </div>
}
