import * as React from "react";
import { Link } from 'react-router-dom';
import { RoutePaths } from './../../Routes';
import AuthService from '../../../services/Auth';
import { Button, Label, Form, Grid, Segment, Divider, Radio, Container } from 'semantic-ui-react'
import { IconedHeader, DetailedErrorMessage, SuccessAttachedMessage } from "../../Partials/Messages";
import { toLowerCaseErrors } from "../../../helpers/Utils";
import { useRef, useState } from "react";
import { useTranslation } from "react-i18next";

let authService = new AuthService();

export const Register = (props: any) => {
    const email = useRef(null);
    const password = useRef(null);
    const name = useRef(null);
    let emailRef = null;
    const [isBusinessProfile, setIsBusinessProfile] = useState(false);
    const [registerComplete, setRegisterComplete] = useState(false);
    const [errors, setErrors] = useState({} as { [key: string]: string });
    const [isLoading, setIsLoading] = useState(false);
    const { t } = useTranslation();

    const toggle = () => setIsBusinessProfile(!isBusinessProfile);

    const handleSubmit = () => {
        event.preventDefault();
        setErrors({});
        setIsLoading(true);
        const userType = isBusinessProfile ? "Business" : "Personal";
        emailRef = email.current.value;
        authService.register(
            email.current.value,
            password.current.value,
            name.current.value,
            userType)
            .then(response => {
                setIsLoading(false);
                if (!response.is_error) {
                    setRegisterComplete(true);
                } else {
                    setErrors(toLowerCaseErrors(response.error_content));
                }
            });
    }

    if (registerComplete) {
        return <RegisterComplete email={emailRef} />
    } else {
        return <div className="auth">
            <Grid stackable centered columns={2}>
                <Grid.Column>
                    <IconedHeader
                        iconName='user outline'
                        header={t("Register-Header")}
                        content={t("Register-Subheader")}
                    />
                    <DetailedErrorMessage errors={errors} />
                    <Form className="segment attached" size='large' onSubmit={handleSubmit}>
                        <Form.Field>
                            <label htmlFor="inputName">{t("ProfileType")}</label>
                            <Radio
                                toggle
                                label={isBusinessProfile === true ? t("ProfileTypeBusiness") : t("ProfileTypePersonal")}
                                name='radioGroup'
                                value="business"
                                onChange={toggle}
                            />
                        </Form.Field>
                        <Form.Field required error={!!errors.name}>
                            <label htmlFor="inputName">{isBusinessProfile === true ? t("ProfileBusinessName") : t("ProfilePersonalName")}</label>
                            <input
                                type="text"
                                id="inputName"
                                ref={name}
                                placeholder={t("Register-NamePlaceholder")}
                            />
                            {errors.name &&
                                <Label basic color='red' pointing>{errors.name}</Label>
                            }
                        </Form.Field>
                        <Form.Field required error={!!errors.username}>
                            <label htmlFor="inputEmail">{t("Email")}</label>
                            <input
                                type="email"
                                id="inputEmail"
                                ref={email}
                                placeholder={t("EmailPlaceholder")}
                            />
                            {errors.username &&
                                <Label basic color='red' pointing>{errors.username}</Label>
                            }
                        </Form.Field>
                        <Form.Field required error={!!errors.password}>
                            <label htmlFor="inputPassword">{t("Password")}</label>
                            <input
                                id="inputPassword"
                                placeholder={t("PasswordPlaceholder")}
                                type='password'
                                name="inputPassword"
                                ref={password}
                            />
                            {errors.password &&
                                <Label basic color='red' pointing>{errors.password}</Label>
                            }
                        </Form.Field>
                        <Divider hidden />
                        <Button loading={isLoading} color='green' inverted fluid size='large' type="submit">
                            {t("RegisterSubmit")}
                        </Button>
                    </Form>
                    <Segment attached='bottom'>
                        <Link to={RoutePaths.Login}>
                            {t("RegisterToLogin")}
                        </Link>
                    </Segment>
                </Grid.Column>
            </Grid>
        </div>;
    }
}

interface RegisterCompleteProps {
    email: string;
}

export const RegisterComplete = (props: any) => {
    const { email } = props as RegisterCompleteProps;
    const { t } = useTranslation();

    return <div className="auth">
        <Container>
            <Grid stackable centered columns={2}>
                <Grid.Column>
                    <SuccessAttachedMessage
                        header={t("RegisterCompleteHeader")}
                        content={t("RegisterCompleteSubheader")}
                    />
                    <Segment attached>
                        <p>{t("RegisterCompleteContent", { email })}</p>
                    </Segment>
                    <Segment attached='bottom'>
                        <Link role="button" to={RoutePaths.Login}>
                            {t("RegisterPostCompleteToLogin")}
                        </Link>
                    </Segment>
                </Grid.Column>
            </Grid>
        </Container>
    </div>;
}
