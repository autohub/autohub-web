import * as React from "react";
import { Link } from 'react-router-dom';
import { RoutePaths } from '../../Routes';
import { Label, Grid, Button, Form, Segment, Container } from 'semantic-ui-react';
import AuthService from '../../../services/Auth';
import { IconedHeader, DetailedErrorMessage, SuccessAttachedMessage } from "../../Partials/Messages";
import { toLowerCaseErrors } from "../../../helpers/Utils";
import { useState } from "react";
import { useTranslation } from "react-i18next";
let authService = new AuthService();

export const PasswordResetRequest = () => {
    const [email, setEmail] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [resetPasswordRequestSend, setResetPasswordRequestSend] = useState(false);
    const [errors, setErrors] = useState({} as { [key: string]: string });
    const { t } = useTranslation();

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        setEmail(value);
    }

    const handleSubmit = () => {
        setErrors({});
        setIsLoading(true);
        event.preventDefault();

        authService.passwordResetRequest(email).then(response => {
            if (!response.is_error) {
                setResetPasswordRequestSend(true);
                setIsLoading(false);
            } else {
                setErrors(toLowerCaseErrors(response.error_content));
                setIsLoading(false);
            }
        });
    }

    if (resetPasswordRequestSend) {
        return <ResetPasswordRequestSend email={email} />
    } else {
        return <div className="auth">
            <Grid stackable centered columns={2}>
                <Grid.Column>
                    <IconedHeader
                        iconName='key'
                        header={t("ResetPasswordRequest-Header")}
                        content={t("ResetPassword-Subheader")}
                    />
                    <DetailedErrorMessage errors={errors} />
                    <Form className="segment attached" size='large' onSubmit={handleSubmit}>
                        <Form.Field required error={errors.email && true}>
                            <label htmlFor="inputEmail">{t("ResetPassword-Email")}</label>
                            <input
                                placeholder={t("ResetPassword-EmailPlaceholder")}
                                type="email"
                                id="inputEmail"
                                onChange={handleChange}
                                value={email}
                            />
                            {errors.email && <Label basic color='red' pointing>{errors.email}</Label>}
                        </Form.Field>
                        <Button color='green' loading={isLoading} inverted fluid size='large' type="submit">{t("ResetPassword-Submit")}</Button>
                    </Form>
                    <Segment attached='bottom'>
                        <Link to={RoutePaths.Login}>{t("ResetPassword-ToLogin")}</Link>
                    </Segment>
                </Grid.Column>
            </Grid>
        </div>
    }
}

interface ResetPasswordRequestSendProps {
    email: string;
}

export const ResetPasswordRequestSend = (props: any) => {
    const { email } = props as ResetPasswordRequestSendProps;
    const { t } = useTranslation();

    return <div className="auth">
        <Container>
            <Grid stackable centered columns={2}>
                <Grid.Column>
                    <SuccessAttachedMessage
                        header={t("ResetPassword-SendHeader")}
                        content={t("ResetPassword-SendSubheader")}
                    />
                    <Segment attached='bottom'>
                        <p>
                            {t("ResetPassword-SendContent", { email })}
                        </p>
                    </Segment>
                </Grid.Column>
            </Grid>
        </Container>
    </div>;
}
