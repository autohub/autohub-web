import { useState } from "react";
import { GoogleLogin } from 'react-google-login';
import { RoutePaths } from './../../Routes';
import { toLowerCaseErrors } from "../../../helpers/Utils";
import AuthService from '../../../services/Auth';
import { Icon } from "semantic-ui-react";
import GoogleLoginPropsExtended from './../../../customTypings/@types/react-google-login/extensions.d.';
import * as React from "react";
import { useTranslation } from "react-i18next";
let authService = new AuthService();

export const GoogleLoginButton = (props: any) => {

    const [state, setState] = useState({
        errors: {} as { [key: string]: string }
    });

    const { t } = useTranslation();

    const { history } = props;

    const onAuthCallback = (data: any) => {
        authService.signInGoogle(data).then(response => {
            if (!response.is_error) {
                history.push(RoutePaths.Dashboard);
            } else {
                setState({ errors: toLowerCaseErrors(response.error_content) });
            }
        });
    }

    const onAuthFailure = (data: any) => {
        console.error(data);
    }

    const loginProps = {
        clientId: "112144662994-umverfcepn8i9q26qqq7eovuldj0j3ca.apps.googleusercontent.com",
        className: "google-login-button",
        autoLoad: false,
        icon: false,
        cookiePolicy: 'single_host_origin',
        onSuccess: onAuthCallback,
        onFailure: onAuthFailure,
    } as GoogleLoginPropsExtended.IconedProps

    return <GoogleLogin {...loginProps}> 
        <Icon name='google' />
        {t("Auth-Google")}
    </GoogleLogin>
}