import * as React from "react";
import { Label, Grid, Button, Form } from 'semantic-ui-react';
import AuthService from '../../../services/Auth';
import { IconedHeader, DetailedErrorMessage } from "../../Partials/Messages";
import { toLowerCaseErrors } from "../../../helpers/Utils";
import { useRef, useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

let authService = new AuthService();

export const PasswordResetConfirmation = (props: any) => {
    const newPassword = useRef(null);
    const newPasswordConfirmation = useRef(null);
    const [token, setToken] = useState('' as string);
    const [errors, setErrors] = useState({} as { [key: string]: string });
    const { t } = useTranslation();

    useEffect(() => {
        const search = props.location.search;
        const params = new URLSearchParams(search);
        setToken(params.get('token'));

        if (props.match.params.id == null || props.match.params.id == '') {
            setErrors({ general: t("ResetPassword-UserIdError") });
        }
    }, [])

    const handleSubmit = () => {
        event.preventDefault();
        setErrors({});

        authService.passwordReset(
            props.match.params.id,
            token,
            newPassword.current.value,
            newPasswordConfirmation.current.value
        ).then(response => {
            if (!response.is_error) {
                //TODO: Hide query params
                props.history.push('/login?password-changed=1');
            } else {
                setErrors(toLowerCaseErrors(response.error_content));
            }
        });
    }

    return <div className="auth">
        <Grid stackable centered columns={2}>
            <Grid.Column>
                <IconedHeader
                    iconName='key'
                    header={t("ResetPassword-ConfirmationHeader")}
                    content={t("ResetPassword-ConfirmationSubheader")}
                />
                <DetailedErrorMessage errors={errors} />
                <Form className="segment attached" size='large' onSubmit={handleSubmit}>
                    <Form.Field required error={!!errors.newPassword}>
                        <label htmlFor="inputPassword">{t("ResetPassword-ConfirmationNewPassword")}</label>
                        <input
                            id="inputPassword"
                            placeholder={t("ResetPassword-ConfirmationPlaceholder")}
                            type='password'
                            name="inputPassword"
                            ref={newPassword}
                        />
                        {errors.newPassword &&
                            <Label basic color='red' pointing>{errors.newPassword}</Label>
                        }
                    </Form.Field>
                    <Form.Field required error={!!errors.newPasswordConfirmation}>
                        <label htmlFor="inputPassword">{t("ResetPassword-ConfirmationPasswordRepeat")}</label>
                        <input
                            id="inputPassword"
                            placeholder={t("ResetPassword-ConfirmationPasswordRepeatPlaceholder")}
                            type='password'
                            name="inputPasswordConfirmation"
                            ref={newPasswordConfirmation}
                        />
                        {errors.newPasswordConfirmation &&
                            <Label basic color='red' pointing>{errors.newPasswordConfirmation}</Label>
                        }
                    </Form.Field>
                    <Button inverted color='green' fluid size='large' type="submit">
                        {t("ResetPassword-ConfirmationSubmit")}
                    </Button>
                </Form>
            </Grid.Column>
        </Grid>
    </div>
}