import { useState } from "react";
import FacebookLogin from 'react-facebook-login';
import { RoutePaths } from './../../Routes';
import { toLowerCaseErrors } from "../../../helpers/Utils";
import AuthService from '../../../services/Auth';
import { Icon } from "semantic-ui-react";
import * as React from "react";
import { useTranslation } from "react-i18next";
let authService = new AuthService();

export const FacebookLoginButton = (props: any) => {

    const [state, setState] = useState({
        errors: {} as { [key: string]: string }
    });

    const { t } = useTranslation();

    const { history } = props;

    const responseFacebook = (data: any) => {
        authService.signInFacebook(data).then(response => {
            if (!response.is_error) {
                history.push(RoutePaths.Dashboard);
            } else {
                setState({ errors: toLowerCaseErrors(response.error_content) });
            }
        });
    }

    return <FacebookLogin
        appId="556710851574675"
        autoLoad={false}
        textButton={t("Auth-Facebook")}
        size="small"
        disableMobileRedirect={true}
        fields="name,email,picture"
        callback={responseFacebook}
        icon={<Icon name='facebook'/>}
        onFailure={(error: any) => console.error(error)}
    />
}