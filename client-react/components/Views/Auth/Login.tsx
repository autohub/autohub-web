import * as React from "react";
import { Link } from 'react-router-dom';
import { RoutePaths } from './../../Routes';
import AuthService from '../../../services/Auth';
import { Button, Form, Grid, Message, Segment, Divider, Label } from 'semantic-ui-react';
import { IconedHeader, DetailedErrorMessage } from "../../Partials/Messages";
import { toLowerCaseErrors } from "../../../helpers/Utils";
import { FacebookLoginButton } from "./FacebookLoginButton";
import { GoogleLoginButton } from "./GoogleLoginButton";
import { useState, useRef } from "react";
import { useTranslation } from "react-i18next";

let authService = new AuthService();

export const Login = (props: any) => {
    const username = useRef(null);
    const password = useRef(null);
    const [isSubmitLoading, setSubmitIsLoading] = useState(false);
    const [errors, setErrors] = useState({} as { [key: string]: string });
    const [initialLoad, setInitialLoad] = useState(true);

    const { t } = useTranslation();

    const handleSubmit = () => {
        setSubmitIsLoading(true);
        event.preventDefault();
        setErrors({});
        setInitialLoad(false);
        authService.signIn(username.current.value, password.current.value).then(response => {
            setSubmitIsLoading(false);
            if (!response.is_error) {
                props.history.push(RoutePaths.Dashboard);
            } else {
                setErrors(toLowerCaseErrors(response.error_content));
            }
        });
    }

    const getInitialLoadContent = () => {
        const search = props.location.search;
        const params = new URLSearchParams(search);
        let initialLoadContent = null;

        if (initialLoad) {
            if (params.get('confirmed')) {
                initialLoadContent = <Message icon='checkmark' header={t("Auth-EmailConfirmed")} success attached />
            }

            if (params.get('expired')) {
                initialLoadContent = <Message icon='info circle' header={t("Auth-SessionExpired")} content={t("Auth-SessionExpiredContent")} info attached />
            }

            if (params.get('password-changed')) {
                initialLoadContent = <Message icon='checkmark' header={t("Auth-PasswordChanged")} content={t("Auth-PasswordChangedContent")} success attached />
            }

            if (props.history.location.state && props.history.location.state.signedOut) {
                initialLoadContent = <Message icon='info circle' header={t("Auth-Logout")} info attached />
            }
        }

        return initialLoadContent;
    }

    return <div className="auth">
        <Grid stackable centered columns={2}>
            <Grid.Column>
                <IconedHeader
                    iconName='user outline'
                    header={t("Auth-LoginHeader")}
                    content={t("Auth-LoginDesc")}
                />
                {getInitialLoadContent()}
                <DetailedErrorMessage errors={errors} />
                <Form className="segment attached" size='large' onSubmit={handleSubmit}>
                    <Form.Field required error={!!errors.username}>
                        <label htmlFor="inputEmail">{t("Auth-LoginEmail")}</label>
                        <input
                            placeholder={t("Auth-LoginEmailPlaceholder")}
                            type="email"
                            id="inputEmail"
                            ref={username}
                        />
                        {errors.username &&
                            <Label basic color='red' pointing>{errors.username}</Label>
                        }
                    </Form.Field>
                    <Form.Field required error={!!errors.password}>
                        <label htmlFor="inputPassword">{t("Auth-Password")}</label>
                        <input
                            placeholder={t("Auth-PasswordPlaceholder")}
                            type='password'
                            name="inputPassword"
                            ref={password}
                        />
                        {errors.password &&
                            <Label basic color='red' pointing>{errors.password}</Label>
                        }
                    </Form.Field>
                    <Divider hidden />
                    <Button
                        loading={isSubmitLoading}
                        color='green'
                        fluid inverted
                        size='large'
                        type="submit">
                            {t("Auth-LoginButton")}
                    </Button>
                    <Divider horizontal>{t("Auth-Or")}</Divider>
                    <Grid stackable columns={2}>
                        <Grid.Row>
                            <Grid.Column>
                                <FacebookLoginButton {...props} />
                            </Grid.Column>
                            <Grid.Column>
                                <GoogleLoginButton {...props} />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                    <Divider horizontal>{t("Auth-Or")}</Divider>
                    <Button
                        color='green'
                        fluid inverted
                        size='large'
                        onClick={() => props.history.push(RoutePaths.Register)}>
                        {t("Auth-RegisterButton")}
                    </Button>
                </Form>
                <Segment attached='bottom'>
                    <Link to={RoutePaths.PasswordResetRequest}>{t("Auth-ForgotPasswordButton")}</Link>
                </Segment>
            </Grid.Column>
        </Grid>
    </div>
}
