import 'object-assign';
import * as React from 'react';
import VehicleService from '../../../services/Vehicles'
import { RoutePaths } from '../../Routes';
import { Input, Divider, Form, Label, Grid } from 'semantic-ui-react'
import DatePicker from 'react-datepicker'
import * as moment from 'moment'
import "react-datepicker/dist/react-datepicker.css";
import { PageHeader, FormActionsButtons, CenteredLoader } from '../../Partials/PageComponents';
import { IconedHeader, DetailedErrorMessage } from '../../Partials/Messages';
import { MapToDropdownOptions } from '../../../helpers/Options';
import { toLowerCaseErrors } from '../../../helpers/Utils';
import { IVehicle } from '../../../typings/Vehicle';
import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { VehicleFuelType } from '../../../typings/Enums/Vehicles';
import { GetTranslatedEnumTypes } from '../../../helpers/EnumHelpers';

let vehicleService = new VehicleService();

export const VehicleAdd = (props: any) => {
    const [vehicle, setVehicle] = useState(null as IVehicle);
    const [errors, setErrors] = useState({} as { [key: string]: string });
    const [isLoading, setIsLoading] = useState(true);
    const [isSubmitLoading, setSubmitIsLoading] = useState(false);
    const { t } = useTranslation();

    useEffect(() => {
        if (props.match.path == RoutePaths.VehicleEdit) {
            vehicleService.fetch(props.match.params.id).then((response) => {
                setVehicle(response.content);
                setIsLoading(false);
            });
        }
        else {
            let newVehicle: IVehicle = {
                make: '',
                model: '',
                title: '',
                productionDate: undefined,
                milage: undefined,
                engineSize: undefined,
                horsePower: undefined,
                fuelType: undefined
            };
            setVehicle(newVehicle);
            setIsLoading(false);
        }
    }, [])

    const handleSubmit = () => {
        setSubmitIsLoading(true);
        event.preventDefault();
        saveVehicle(vehicle);
        setSubmitIsLoading(false);
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        let vehicleUpdates = {
            [name]: value
        }
        setVehicle({ ...Object.assign(vehicle, vehicleUpdates) } as IVehicle);
    }

    const handleChangeDate = (date: any) => {
        let vehicleUpdates = {
            productionDate: date
        }
        setVehicle({ ...Object.assign(vehicle, vehicleUpdates) } as IVehicle);
    }

    const saveVehicle = (vehicle: IVehicle) => {
        setErrors({} as { [key: string]: string });
        vehicleService.save(vehicle).then((response) => {
            if (!response.is_error) {
                props.history.push(RoutePaths.Vehicles);
            } else {
                setErrors(toLowerCaseErrors(response.error_content));
            }
        });
    }

    const handleDropDownChange = (event: any, data: any) => {
        let vehicleUpdates = {
            [data.name]: data.value
        }
        setVehicle({ ...Object.assign(vehicle, vehicleUpdates) } as IVehicle);
    }

    const makePageTitle = () => {
        return props.match.path == RoutePaths.VehicleEdit
            ? t("EditVehicle")
            : t("AddVehicle");
    }

    const vehicleFuelTypesOpts = MapToDropdownOptions(GetTranslatedEnumTypes(VehicleFuelType));

    return isLoading ? <CenteredLoader /> : <div>
        <PageHeader headerText={makePageTitle()} {...props} />
        <Divider hidden />
        <Grid stackable columns={2}>
            <Grid.Column>
                <IconedHeader
                    iconName='car'
                    header={t("AddVehicleSubheader")}
                    content={t("RequiredFieldsNotice")}
                />
                <DetailedErrorMessage errors={errors} />
                <Form className='attached fluid segment' onSubmit={handleSubmit} size='large'>
                    <Form.Group widths='equal'>
                        <Form.Field required error={!!errors.make}>
                            <label>{t("Make")}</label>
                            <Input
                                fluid name='make'
                                placeholder={t("EnterMake")}
                                value={vehicle.make}
                                onChange={handleChange}
                            />
                            {errors.make &&
                                <Label basic color='red' pointing>{errors.make}</Label>
                            }
                        </Form.Field>
                        <Form.Field required error={!!errors.model}>
                            <label>{t("Model")}</label>
                            <Input
                                fluid
                                name='model'
                                placeholder={t("EnterModel")}
                                value={vehicle.model}
                                onChange={handleChange}
                            />
                            {errors.model &&
                                <Label basic color='red' pointing>{errors.model}</Label>
                            }
                        </Form.Field>
                    </Form.Group>
                    <Form.Group widths='equal'>
                        <Form.Field required error={!!errors.milage}>
                            <label>{t("CurrentMilage")}</label>
                            <Input
                                name='milage'
                                value={vehicle.milage}
                                onChange={handleChange}
                                fluid
                                label={{ basic: true, content: t("Milage-Km") }}
                                labelPosition='right'
                                placeholder={t("EnterMilage")}
                            />
                            {errors.milage &&
                                <Label basic color='red' pointing>{errors.milage}</Label>
                            }
                        </Form.Field>
                        <Form.Field required error={!!errors.fuelType}>
                            <Form.Select
                                required
                                defaultValue={vehicle.fuelType}
                                name="fuelType"
                                label={t("FuelType")}
                                onChange={handleDropDownChange.bind(this)}
                                options={vehicleFuelTypesOpts}
                                placeholder={t("ChooseFuelType")}
                                size="large"
                                selection
                            />
                            {errors.fuelType &&
                                <Label basic color='red' pointing>{errors.fuelType}</Label>
                            }
                        </Form.Field>
                    </Form.Group>
                    <Form.Group widths='equal'>
                        <Form.Field>
                            <label>{t("EngineSize")}</label>
                            <Input
                                name='engineSize'
                                value={vehicle.engineSize}
                                onChange={handleChange}
                                label={{ basic: true, content: t("Quantity-cm3") }}
                                labelPosition='right'
                                fluid
                                placeholder={t("EnterEngineSize")}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>{t("Power")}</label>
                            <Input
                                name='horsePower'
                                value={vehicle.horsePower}
                                onChange={handleChange}
                                fluid
                                label={{ basic: true, content: t("Power-Hp") }}
                                labelPosition='right'
                                placeholder={t("EnterPower")}
                            />
                        </Form.Field>
                        <Form.Field>
                            <label>{t("DateManufactured")}</label>
                            <DatePicker
                                selected={vehicle.productionDate ? moment.utc(vehicle.productionDate).local() : null}
                                onChange={handleChangeDate.bind(this)}
                                placeholderText={t("EnterDateManufactured")}
                                name='productionDate'
                                peekNextMonth
                                showMonthDropdown
                                showYearDropdown
                                dropdownMode="select"
                            />
                        </Form.Field>
                    </Form.Group>
                    <FormActionsButtons isLoading={isSubmitLoading} {...props} />
                </Form>
            </Grid.Column>
        </Grid>
    </div>
}
