import * as React from 'react';
import * as moment from 'moment'
import VehicleService from '../../../services/Vehicles'
import { Header, Divider, Segment, Grid, Icon, Label, Popup } from 'semantic-ui-react'
import { PageHeader, CenteredLoader } from '../../Partials/PageComponents';
import { ExpirationVehicleHistoriesAccordion }
    from '../../Partials/VehicleHistory/ExpirationVehicleHistoriesAccordion';
import { IVehicle } from '../../../typings/Vehicle';
import { VehicleFuelType } from '../../../typings/Enums/Vehicles';
import { CreateVehicleHistoryModal } from '../../Partials/VehicleHistory/CreateVehicleHistoryModal';
import { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { GetTranslatedEnumTypes } from '../../../helpers/EnumHelpers';

let vehicleService = new VehicleService();

export const VehicleView = (props: any) => {
    const [vehicle, setVehicle] = useState(null as IVehicle);
    const [isLoading, setIsLoading] = useState(true);
    const { t } = useTranslation();

    useEffect(() => {
        vehicleService.fetch(props.match.params.id)
            .then((response) => {
                setVehicle(response.content);
                setIsLoading(false);
            });
    }, [])

    const vehicleFuelTypes = GetTranslatedEnumTypes(VehicleFuelType);

    return isLoading ? <CenteredLoader /> :
        (<React.Fragment>
            <PageHeader headerText={t("Vehicles-Details")} {...props} />
            <CreateVehicleHistoryModal vehicleId={vehicle.id} {...props} color='green' />
            <Divider hidden />
            <Grid stackable columns={2}>
                <Grid.Column>
                    <Segment.Group>
                        <Segment>
                            <Header as='h3'>
                                <Icon name='car' className='mainIconColor' />
                                <Header.Content>
                                    {vehicle.title}
                                    {vehicle.lastFuelConsumption &&
                                        <Popup
                                            content={t("LastFuelConsumption")}
                                            position='right center'
                                            trigger={<Label basic>{vehicle.lastFuelConsumption}{t("FuelPerMilage")}</Label>}
                                            inverted
                                        />
                                    }
                                    {vehicle.averageFuelConsumption && vehicle.averageFuelConsumption !== vehicle.lastFuelConsumption &&
                                        <Popup
                                            content={t("AverageFuelConsumption")}
                                            position='right center'
                                            trigger={<Label basic>{vehicle.averageFuelConsumption}{t("FuelPerMilage")}</Label>}
                                            inverted
                                        />
                                    }
                                </Header.Content>
                            </Header>
                        </Segment>
                        <Segment.Group horizontal>
                            <Segment>
                                <Header as='h4'>{vehicle.milage}
                                    <Header.Subheader>{t("Milage")}</Header.Subheader>
                                </Header>
                            </Segment>
                            <Segment>
                                <Header as='h4'>{vehicleFuelTypes.get(vehicle.fuelType)}
                                    <Header.Subheader>{t("FuelType")}</Header.Subheader>
                                </Header>
                            </Segment>
                        </Segment.Group>
                        {(vehicle.engineSize || vehicle.horsePower || vehicle.productionDate) &&
                            <Segment.Group color='red' horizontal>
                                {vehicle.engineSize &&
                                    <Segment>
                                        <Header as='h5'>{vehicle.engineSize} {t("Quantity-cm3")}
                                                <Header.Subheader>{t("EngineSize")}</Header.Subheader>
                                        </Header>
                                    </Segment>
                                }
                                {vehicle.horsePower &&
                                    <Segment>
                                        <Header as='h5'>{vehicle.horsePower} {t("Power-Hp")}
                                                <Header.Subheader>{t("Power")}</Header.Subheader>
                                        </Header>
                                    </Segment>
                                }
                                {vehicle.productionDate &&
                                    <Segment>
                                        <Header as='h5'>{moment.utc(vehicle.productionDate).calendar()}
                                            <Header.Subheader>{t("DateManufactured")}</Header.Subheader>
                                        </Header>
                                    </Segment>
                                }
                            </Segment.Group>
                        }
                        <Segment attached='bottom'>
                            <span>{t("Added-m")}: {moment.utc(vehicle.createdOn).local().fromNow()}</span>
                            {!!vehicle.updatedOn &&
                                <span> / {t("Changed-m")}: {moment.utc(vehicle.updatedOn).local().fromNow()} </span>
                            }
                        </Segment>
                    </Segment.Group>
                </Grid.Column>
                <Grid.Column>
                    {vehicle && !isLoading &&
                        <ExpirationVehicleHistoriesAccordion
                            vehicles={[vehicle]}
                            {...props}
                        />
                    }
                </Grid.Column>
            </Grid>
        </React.Fragment>)
}
