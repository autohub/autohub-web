import * as React from "react";
import { RoutePaths } from '../../Routes';
import VehicleService from '../../../services/Vehicles';
import {
    Grid,
    Icon,
    Button,
    Divider
} from 'semantic-ui-react';
import { PageHeader, CenteredLoader } from "../../Partials/PageComponents";
import { VehicleCard } from '../../Partials/Vehicles/VehicleCard';
import { SegmentMessage } from "../../Partials/Messages";
import { IVehicle } from "../../../typings/Vehicle";
import { useState, useEffect } from "react";
import { useTranslation } from "react-i18next";

let vehicleService = new VehicleService();

export const Vehicles = (props: any) => {
    const [vehicles, setVehicles] = useState([] as Array<IVehicle>);
    const [isLoading, setIsLoading] = useState(true);
    const { t } = useTranslation();

    useEffect(() => {
        vehicleService.fetchAll().then((response) => {
            setVehicles(response.content);
            setIsLoading(false);
        });
    }, [])

    const deleteFunc = (vehicle: IVehicle) => {
        vehicleService.delete(vehicle.id).then(() => {
            let updateVehicles = vehicles;
            updateVehicles.splice(updateVehicles.indexOf(vehicle), 1);
            setVehicles([...updateVehicles]);
        });
    }

    return isLoading ? <CenteredLoader /> :
        <div>
            <PageHeader headerText={t("Dashboard-Vehicles")} {...props} />
            <Button icon
                color='green'
                labelPosition='left'
                onClick={() => props.history.push(RoutePaths.VehicleAdd)}>
                <Icon name='plus' /> {t("Dashboard-VehiclesAdd")}
                </Button>
            <Divider hidden />
            <Grid stackable columns={3}>
                <Grid.Column width={16}>
                    <Grid stackable columns={3}>
                        {vehicles.map((vehicle) =>
                            <Grid.Column >
                                <VehicleCard vehicle={vehicle} delete={deleteFunc} {...props} />
                            </Grid.Column>
                        )}
                        {Object.keys(vehicles).length === 0 &&
                            <Grid.Column><SegmentMessage title={t("NoVehicleAvailable")} /></Grid.Column>
                        }
                    </Grid>
                </Grid.Column>
            </Grid>
        </div>
}
