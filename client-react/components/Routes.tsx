import * as React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import { Register } from './Views/Auth/Register';
import { Login } from './Views/Auth/Login';
import { PasswordResetRequest } from './Views/Auth/PasswordResetRequest';
import { PasswordResetConfirmation } from './Views/Auth/PasswordResetConfirmation';
import { AccountEdit } from './Views/Account/AccountEdit';
import { AccountPasswordChange } from './Views/Account/AccountPasswordChange';
import { ErrorPage } from './Shared/Error';
import { Dashboard } from './Views/Dashboard';
import { Vehicles } from './Views/Vehicles/Vehicles';
import { Home } from './StaticPages/Home';
import { VehicleAdd } from './Views/Vehicles/VehicleAdd';
import { VehicleView } from './Views/Vehicles/VehicleView';
import { VehicleHistories } from './Views/VehicleHistory/VehicleHistories';
import { FuelingVehicleHistoryAdd } from './Views/VehicleHistory/FuelingVehicleHistoryAdd';
import { RepairVehicleHistoryAdd } from './Views/VehicleHistory/RepairVehicleHistoryAdd';
import { TaxVehicleHistoryAdd } from './Views/VehicleHistory/TaxVehicleHistoryAdd';
import { VehicleHistoryView } from './Views/VehicleHistory/VehicleHistoryView';
import { Statistics } from './Views/Statistics/Statistics';
import { NavBar } from './Shared/NavBar';
import Footer from './Shared/Footer';
import { About } from './StaticPages/About';
import { Faq } from './StaticPages/Faq';
import { Feedback } from './StaticPages/Feedback';
import { PrivacyPolicy } from './StaticPages/PrivacyPolicy';
import { TermsNConditions } from './StaticPages/TermsNConditions';
import { Features } from './StaticPages/Features';
import AuthService from '../services/Auth';
import { Container } from 'semantic-ui-react';
import Analytics from 'react-router-ga';
import { withTitle, ScrollToTop } from './Partials/PageComponents';

export class RoutePaths {
    public static Home: string = "/";
    public static AboutPage: string = "/about";
    public static FaqPage: string = "/faq";
    public static PrivacyPolicyPage: string = "/privacy-policy";
    public static TermsNConditionsPage: string = "/terms-and-conditions";
    public static FeaturesPage: string = "/features";
    public static FeedbackPage: string = "/feedback";
    public static Login: string = "/login";
    public static Register: string = "/register";
    public static PasswordResetRequest: string = "/account/password/request";
    public static PasswordReset: string = "/account/:id/password/reset/";
    public static Dashboard: string = "/dashboard";
    public static AccountEdit: string = "/account/edit";
    public static AccountPasswordChange: string = "/account/password/change";
    public static Vehicles: string = "/vehicles";
    public static VehicleAdd: string = "/vehicle/new";
    public static VehicleEdit: string = "/vehicles/edit/:id";
    public static VehicleView: string = "/vehicles/details/:id";
    public static VehicleHistories: string = "/vehicles/history/list";
    public static VehicleHistoryAdd: string = "/vehicles/history/new";
    public static FuelingVehicleHistoryAdd: string = "/vehicles/history/fueling/add";
    public static RepairVehicleHistoryAdd: string = "/vehicles/history/repair/add";
    public static TaxVehicleHistoryAdd: string = "/vehicles/history/tax/add/";
    public static FuelingVehicleHistoryEdit: string = "/vehicles/history/fueling/edit/:id";
    public static RepairVehicleHistoryEdit: string = "/vehicles/history/repair/edit/:id";
    public static TaxVehicleHistoryEdit: string = "/vehicles/history/tax/edit/:id";
    public static FuelingVehicleHistoryView: string = "/vehicles/history/fueling/view/:id";
    public static TaxVehicleHistoryView: string = "/vehicles/history/tax/view/:id";
    public static RepairVehicleHistoryView: string = "/vehicles/history/repair/view/:id";
    public static VehicleHistoryView: string = "/vehicles/history/:type/view/:id";
    public static Statistics: string = "/statistics/";
}

const HomeComponent = withTitle({ component: Home });
const PasswordResetRequestComponent = withTitle({ component: PasswordResetRequest, title: "Page-ResetPassword" });
const PasswordResetConfirmationComponent = withTitle({ component: PasswordResetConfirmation, title: "Page-ResetPasswordConfirmation" });
const AboutComponent = withTitle({ component: About, title: "Page-About" });
const FaqComponent = withTitle({ component: Faq, title: "Page-Faq" });
const PrivacyPolicyComponent = withTitle({ component: PrivacyPolicy, title: "Page-PrivacyPolicy" });
const TermsNConditionsComponent = withTitle({ component: TermsNConditions, title: "Page-TermsNConditions" });
const FeaturesComponent = withTitle({ component: Features, title: "Page-Services" });
const FeedbackComponent = withTitle({ component: Feedback, title: "Page-Feedback" });
const LoginComponent = withTitle({ component: Login, title: "Auth-Login" });
const RegisterComponent = withTitle({ component: Register, title: "Auth-Register" });
const DashboardComponent = withTitle({ component: Dashboard, title: "Dashboard" });
const StatisticsComponent = withTitle({ component: Statistics, title: "Statistics" });
const AccountEditComponent = withTitle({ component: AccountEdit, title: "ProfileEdit" });
const AccountPasswordChangeComponent = withTitle({ component: AccountPasswordChange, title: "ProfileChangePassword" });
const VehiclesComponent = withTitle({ component: Vehicles, title: "Vehicles" });
const VehicleAddComponent = withTitle({ component: VehicleAdd, title: "Vehicles-Add" });
const VehicleViewComponent = withTitle({ component: VehicleView, title: "Vehicles-Details" });
const VehicleHistoriesComponent = withTitle({ component: VehicleHistories, title: "Vehicles-History" });
const FuelingVehicleHistoriesComponent = withTitle({ component: FuelingVehicleHistoryAdd, title: "Vehicles-History-Add" });
const RepairVehicleHistoriesComponent = withTitle({ component: RepairVehicleHistoryAdd, title: "Vehicles-History-Add" });
const TaxVehicleHistoriesComponent = withTitle({ component: TaxVehicleHistoryAdd, title: "Vehicles-History-Add" });
const VehicleHistoryViewComponent = withTitle({ component: VehicleHistoryView, title: "Vehicles-History-Details" });

var isDevEnv = process.env.ASPNETCORE_ENVIRONMENT === 'Development';

export default class Routes extends React.Component<any, any> {
    render() {
        //TODO: To configuration
        const gaId = "UA-153654233-1";
        return <Analytics id={isDevEnv ? "" : gaId} debug={isDevEnv ? true : false}>
            <ScrollToTop />
            <Switch>
                <DefaultLayout exact path={RoutePaths.Home} component={HomeComponent} />
                <DefaultLayout exact path={RoutePaths.Login} component={LoginComponent} />
                <DefaultLayout path={RoutePaths.Register} component={RegisterComponent} />
                <DefaultLayout exact path={RoutePaths.PasswordResetRequest} component={PasswordResetRequestComponent} />
                <DefaultLayout path={RoutePaths.PasswordReset} component={PasswordResetConfirmationComponent} />
                <DefaultLayout exact path={RoutePaths.AboutPage} component={AboutComponent} />
                <DefaultLayout exact path={RoutePaths.FaqPage} component={FaqComponent} />
                <DefaultLayout exact path={RoutePaths.PrivacyPolicyPage} component={PrivacyPolicyComponent} />
                <DefaultLayout exact path={RoutePaths.TermsNConditionsPage} component={TermsNConditionsComponent} />
                <DefaultLayout exact path={RoutePaths.FeaturesPage} component={FeaturesComponent} />
                <DefaultLayout exact path={RoutePaths.FeedbackPage} component={FeedbackComponent} />
                <PrivateLayout exact path={RoutePaths.Dashboard} component={DashboardComponent} />
                <PrivateLayout exact path={RoutePaths.AccountEdit} component={AccountEditComponent} />
                <PrivateLayout exact path={RoutePaths.AccountPasswordChange} component={AccountPasswordChangeComponent} />
                <PrivateLayout exact path={RoutePaths.Vehicles} component={VehiclesComponent} />
                <PrivateLayout path={RoutePaths.VehicleAdd} component={VehicleAddComponent} />
                <PrivateLayout path={RoutePaths.VehicleEdit} component={VehicleAddComponent} />
                <PrivateLayout path={RoutePaths.VehicleView} component={VehicleViewComponent} />
                <PrivateLayout path={RoutePaths.VehicleHistories} component={VehicleHistoriesComponent} />
                <PrivateLayout path={RoutePaths.FuelingVehicleHistoryAdd} component={FuelingVehicleHistoriesComponent} />
                <PrivateLayout path={RoutePaths.RepairVehicleHistoryAdd} component={RepairVehicleHistoriesComponent} />
                <PrivateLayout path={RoutePaths.TaxVehicleHistoryAdd} component={TaxVehicleHistoriesComponent} />
                <PrivateLayout path={RoutePaths.FuelingVehicleHistoryEdit} component={FuelingVehicleHistoriesComponent} />
                <PrivateLayout path={RoutePaths.RepairVehicleHistoryEdit} component={RepairVehicleHistoriesComponent} />
                <PrivateLayout path={RoutePaths.TaxVehicleHistoryEdit} component={TaxVehicleHistoriesComponent} />
                <PrivateLayout path={RoutePaths.FuelingVehicleHistoryView} component={VehicleHistoryViewComponent} />
                <PrivateLayout path={RoutePaths.TaxVehicleHistoryView} component={VehicleHistoryViewComponent} />
                <PrivateLayout path={RoutePaths.RepairVehicleHistoryView} component={VehicleHistoryViewComponent} />
                <PrivateLayout path={RoutePaths.VehicleHistoryView} component={VehicleHistoryViewComponent} />
                <PrivateLayout path={RoutePaths.Statistics} component={StatisticsComponent} />
                <Route path='/error/:code?' component={ErrorPage} />
            </Switch>
        </Analytics>
    }
}

const isAuthPage = (props: any) => {
    return props.location.pathname == RoutePaths.Login
        || props.location.pathname == RoutePaths.Register
        || props.location.pathname == RoutePaths.PasswordResetRequest
}

const PrivateLayout = ({ component: Component, ...rest }: { component: any, path: string, exact?: boolean }) => (
    <Route {...rest} render={props => (
        AuthService.isSignedIn() ? (
            <div>
                <NavBar {...props}></NavBar>
                {/* TODO: Wrap child in navbar for pushed click to close the menu  */}
                <Container>
                    <Component {...props} />
                </Container>
                <Footer />
            </div>
        ) : (
                <Redirect to={{
                    pathname: RoutePaths.Login,
                    state: { from: props.location }
                }} />
            )
    )} />
);

const DefaultLayout = ({ component: Component, ...rest }: { component: any, path: string, exact?: boolean }) => (
    <Route {...rest} render={props => (
        AuthService.isSignedIn() && isAuthPage(props) ? (
            <Redirect to={{
                pathname: RoutePaths.Dashboard,
                state: { from: props.location }
            }} />
        ) : (
                <div>
                    <NavBar {...props}></NavBar>
                    <Container>
                        <Component {...props} />
                    </Container>
                    <Footer />
                </div>
            )
    )} />
);
