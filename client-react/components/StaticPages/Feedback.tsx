import * as React from "react";
import { IFeedbackMessage } from '../../services/Feedback'
import { Label, Form, Container, Input, Divider, Grid, Segment, TextArea } from 'semantic-ui-react';
import FeedbackService from '../../services/Feedback';
import { PageHeader, FormActionsButtons } from "../Partials/PageComponents";
import { IconedHeader, DetailedErrorMessage, SuccessAttachedMessage } from "../Partials/Messages";
import { toLowerCaseErrors } from "../../helpers/Utils";
import { useState } from "react";
import { useTranslation } from "react-i18next";

let feedbackService = new FeedbackService();

export const Feedback = ({ ...props }: any) => {
    const [feedbackMessage, setFeedbackMessage] = useState({
        message: '',
        name: '',
        email: ''
    } as IFeedbackMessage);
    const [feedbackMessageSend, setFeedbackMessageSend] = useState(false);
    const [errors, setErrors] = useState({} as { [key: string]: string });
    const { t } = useTranslation();

    const handleSubmit = () => {
        event.preventDefault();
        saveMessage(feedbackMessage);
    }

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        let messageUpdates = {
            [name]: value
        }
        Object.assign(feedbackMessage, messageUpdates) as IFeedbackMessage;
        setFeedbackMessage({ ...feedbackMessage });
    }

    const saveMessage = (message: IFeedbackMessage) => {
        setErrors({} as { [key: string]: string });
        feedbackService.post(message).then((response) => {
            if (!response.is_error) {
                setFeedbackMessageSend(true);
            } else {
                setErrors(toLowerCaseErrors(response.error_content));
            }
        });
    }

    if (feedbackMessageSend) {
        return <FeedBackMessageSend message={feedbackMessage} />
    } else {
        return <Container>
            <PageHeader headerText={t("Page-Feedback")} {...props} />
            <Divider hidden />
            <Grid stackable columns={2}>
                <Grid.Column>
                    <IconedHeader
                        iconName='write square'
                        header={t("Feedback-HeaderTitle")}
                        content={t("Feedback-HeaderDesc")}
                    />
                    <DetailedErrorMessage errors={errors} />
                    <Form className='attached fluid segment' onSubmit={handleSubmit} size='large'>
                        <Form.Field required error={!!errors.name}>
                            <label>{t("Feedback-FormName")}</label>
                            <Input
                                fluid
                                name='name'
                                placeholder={t("Feedback-FormNamePlaceholder")}
                                value={feedbackMessage.name}
                                onChange={handleChange}
                            />
                            {errors.name &&
                                <Label basic color='red' pointing>{errors.name}</Label>
                            }
                        </Form.Field>
                        <Form.Field required error={!!errors.email}>
                            <label>{t("Feedback-FormEmail")}</label>
                            <Input
                                name='email'
                                value={feedbackMessage.email}
                                onChange={handleChange}
                                fluid
                                placeholder={t("Feedback-FormEmailPlaceholder")}
                            />
                            {errors.email &&
                                <Label basic color='red' pointing>{errors.email}</Label>
                            }
                        </Form.Field>
                        <Form.Field>
                            <Form.Input
                                label={t("Feedback-FormMessage")}
                                placeholder={t("Feedback-FormMessagePlaceholder")}
                                control={TextArea}
                                name='message'
                                value={feedbackMessage.message}
                                onChange={handleChange}
                                required
                                error={!!errors.message}
                            />
                            {errors.message &&
                                <Label basic color='red' pointing>{errors.message}</Label>
                            }
                        </Form.Field>
                        <FormActionsButtons {...props} />
                    </Form>
                </Grid.Column>
            </Grid>
        </Container>
    }
}

interface ResetPasswordRequestSendProps {
    message: IFeedbackMessage;
}

export const FeedBackMessageSend = (messageProps: ResetPasswordRequestSendProps) => {
    const { t } = useTranslation();
    return <div>
        <Divider hidden></Divider>
        <Grid centered columns={2}>
            <Grid.Column>
                <SuccessAttachedMessage
                    header={t("Feedback-FormMessageSendHeader")}
                    content={t("Feedback-FormMessageSendContent")}
                />
                <Segment attached='bottom'>
                    <p>
                        {t("Feedback-FormMessageSendInfo", { name: messageProps.message.name, email: messageProps.message.email })}
                    </p>
                </Segment>
            </Grid.Column>
        </Grid>
    </div>;
}
