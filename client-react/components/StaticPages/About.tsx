import * as React from "react";
import { RoutePaths } from '../Routes';
import { Header, Container, Button, Divider, Segment } from 'semantic-ui-react';
import { PageHeader } from "../Partials/PageComponents";
import { useTranslation } from "react-i18next";

export const About = ({ ...props }: any) => {
    const { t } = useTranslation();

    return <Container>
        <PageHeader headerText={t("Nav-About")} {...props} />
        <Divider hidden />
        <Segment>
            <Header textAlign='left' as='h2'>
                {t("About-Title")}
            </Header>
            <p style={{ fontSize: '1.33em', textAlign: 'left' }}>
                {t("About-Desc")}
            </p>
            <Header textAlign='left' as='h2'>
                {t("About-ServicesTitle")}
            </Header>
            <p style={{ fontSize: '1.33em', textAlign: 'left' }}>
                {t("About-ServicesDesc")}
            </p>
            <Button as='a' color='green' inverted size='large' onClick={() => props.history.push(RoutePaths.FeaturesPage)}>
                {t("About-ServicesActionButton")}
            </Button>
            <Header textAlign='left' as='h2'>{t("About-ProblemsTitle")}</Header>
            <Header as='h3'>{t("About-Problems1")}</Header>
            <p style={{ fontSize: '1.33em' }}>
                {t("About-Problems1-Desc")}
            </p>
            <Header as='h3'>{t("About-Problems2")}</Header>
            <p style={{ fontSize: '1.33em' }}>
                {t("About-Problems2-Desc")}
            </p>
            <Header as='h3'>{t("About-Problems3")}</Header>
            <p style={{ fontSize: '1.33em' }}>
                {t("About-Problems3-Desc")}
            </p>
            <Header as='h3'>{t("About-Problems4")}</Header>
            <p style={{ fontSize: '1.33em' }}>
                {t("About-Problems4-Desc")}
            </p>
            <Button as='a' color='orange' inverted size='large' onClick={() => props.history.push(RoutePaths.VehicleAdd)}>
                {t("About-ActionButton")}
            </Button>
        </Segment>
    </Container>
}
