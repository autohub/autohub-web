import * as React from "react";
import { Container, Icon, Divider, Accordion } from 'semantic-ui-react';
import { PageHeader } from "../Partials/PageComponents";
import { useTranslation } from "react-i18next";
import { useState } from "react";

export const Faq = ({ ...props }: any) => {
    const { t } = useTranslation();
    return <Container>
        <PageHeader headerText={t("Page-Faq")} {...props} />
        <Divider hidden />
        <AccordionFaq />
    </Container>
}

export const AccordionFaq = () => {
    const { t } = useTranslation();
    const [activeIndex, setActiveIndex] = useState(-1);

    const handleClick = (e: any, titleProps: any) => {
        const { index } = titleProps;
        const newIndex = activeIndex === index ? -1 : index;

        setActiveIndex(newIndex);
    }

    return (
        <Accordion fluid styled>
            <Accordion.Title active={activeIndex === 0} index={0} onClick={handleClick}>
                <Icon name='dropdown' />
                {t("Faq-Question1")}
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 0}>
                <p>{t("Faq-Answer1")}</p>
            </Accordion.Content>
            <Accordion.Title active={activeIndex === 1} index={1} onClick={handleClick}>
                <Icon name='dropdown' />
                {t("Faq-Question2")}
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 1}>
                <p>{t("Faq-Answer2")}</p>
            </Accordion.Content>
            <Accordion.Title active={activeIndex === 2} index={2} onClick={handleClick}>
                <Icon name='dropdown' />
                {t("Faq-Question3")}
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 2}>
                <p>{t("Faq-Answer3")}</p>
            </Accordion.Content>
            <Accordion.Title active={activeIndex === 3} index={3} onClick={handleClick}>
                <Icon name='dropdown' />
                {t("Faq-Question4")}
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 3}>
                <p>{t("Faq-Answer4")}</p>
            </Accordion.Content>
            <Accordion.Title active={activeIndex === 4} index={4} onClick={handleClick}>
                <Icon name='dropdown' />
                {t("Faq-Question5")}
            </Accordion.Title>
            <Accordion.Content active={activeIndex === 4}>
                <p>{t("Faq-Answer5")}</p>
            </Accordion.Content>
        </Accordion>
    )
}
