import * as React from "react";
import { Container, Divider, Header, Segment, Grid, Icon } from 'semantic-ui-react';
import { PageHeader } from "../Partials/PageComponents";
import { useTranslation } from "react-i18next";

export const Features = ({ ...props }: any) => {
    const { t } = useTranslation();

    return <Container>
        <PageHeader headerText={t("Page-Services")} {...props} />
        <Divider hidden />
        <Segment>
            <Header textAlign='left' as='h2'>{t("About-ServicesTitle")}</Header>
            <p style={{ fontSize: '1.33em', textAlign: 'left' }}>
                {t("Services-Title")}
            </p>
            <Divider hidden />
            <Segment style={{ padding: '0em', borderBottom: 'none' }} vertical>
                <Grid celled='internally' columns='equal' stackable>
                    <ServicesList />
                </Grid>
            </Segment>
        </Segment>
    </Container>
}

export const ServicesList = () => {
    const { t } = useTranslation();

    return <React.Fragment>
        <Grid.Row textAlign='center' className='mainFontColor'>
            <Grid.Column style={{ paddingBottom: '3em', paddingTop: '3em' }}>
                <Header icon as='h3' style={{ fontSize: '2em' }}>
                    <Icon name='fire' />
                    {t("Services-Fueling")}
                    <Header.Subheader>{t("Services-FuelingDesc")}</Header.Subheader>
                </Header>
            </Grid.Column>
            <Grid.Column style={{ paddingBottom: '3em', paddingTop: '3em' }}>
                <Header icon as='h3' style={{ fontSize: '2em' }}>
                    <Icon name='wrench' />
                    {t("Services-Repairs")}
                    <Header.Subheader>{t("Services-RepairsDesc")}</Header.Subheader>
                </Header>
            </Grid.Column>
            <Grid.Column style={{ paddingBottom: '3em', paddingTop: '3em' }}>
                <Header icon as='h3' style={{ fontSize: '2em' }}>
                    <Icon name='dollar' />
                    {t("Services-Taxes")}                    
                    <Header.Subheader>{t("Services-TaxesDesc")}</Header.Subheader>
                </Header>
            </Grid.Column>
        </Grid.Row>
        <Grid.Row textAlign='center' className='mainFontColor'>
            <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
                <Header icon as='h3' style={{ fontSize: '2em' }}>
                    <Icon name='bell outline' />
                    {t("Services-Reminders")}
                    <Header.Subheader>{t("Services-RemindersDesc")}</Header.Subheader>
                </Header>
            </Grid.Column>
            <Grid.Column style={{ paddingBottom: '5em', paddingTop: '5em' }}>
                <Header icon as='h3' style={{ fontSize: '2em' }}>
                    <Icon name='shipping fast' />
                    {t("Services-Fleet")}
                    <Header.Subheader>{t("Services-FleetDesc")}</Header.Subheader>
                </Header>
            </Grid.Column>
            <Grid.Column icon style={{ paddingBottom: '5em', paddingTop: '5em' }}>
                <Header icon as='h3' style={{ fontSize: '2em' }}>
                    <Icon name='chart bar' />
                    {t("Services-Statistics")}
                    <Header.Subheader>{t("Services-StatisticsDesc")}</Header.Subheader>
                </Header>
            </Grid.Column>
        </Grid.Row>
    </React.Fragment>
}
