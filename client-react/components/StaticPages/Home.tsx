import * as React from "react";
import { Link, RouteComponentProps } from 'react-router-dom';
import { RoutePaths } from '../Routes';
import AuthService from '../../services/Auth';
import { Divider, Button } from 'semantic-ui-react';
import {
    Container,
    Grid,
    Header,
    Image,
    Segment,
} from 'semantic-ui-react'
import { useTranslation } from "react-i18next";
import { ServicesList } from "./Features";

let authService = new AuthService();

export class Home extends React.Component<RouteComponentProps<any>, any> {
    signOut() {
        authService.signOut();
        this.props.history.push(RoutePaths.Login, { signedOut: true });
    }
    render() {
        return <div>
            <HomepageLayout />
        </div>;
    }
}

const HomepageLayout = () => {
    const { t } = useTranslation();
    return <div>
        <Container textAlign='center' >
            <Image size="massive" src={require('../../images/intro.jpeg')} style={{ display: 'inline-block' }} />
            <Header
                as='h1'
                content={t("Welcome-Message")}
                className='homeHeader'
            />
            <Header
                as='h2'
                content={t("Welcome-Desc")}
                className='homeSubheader'
            />
            <Divider horizontal inverted>
                <Link to={RoutePaths.Login}>
                    <Button link animated='fade' color='green' inverted size='huge'>
                        <Button.Content visible>
                            {t("Welcome-ActionButton")}
                        </Button.Content>
                        <Button.Content hidden>
                            {t("Welcome-ActionButtonToggle")}
                        </Button.Content>
                    </Button>
                </Link>
            </Divider>
        </Container>
        <Segment className='sectionHeader' vertical textAlign='center'>
            <Header as='h2'>{t("Home-ServicesHeader")}
                <Header.Subheader>
                    {t("Home-ServicesSubheader")}
                </Header.Subheader>
            </Header>
        </Segment>
        <Segment style={{ padding: '0em' }} vertical>
            <Grid celled='internally' columns='equal' stackable>
                <ServicesList />
            </Grid>
        </Segment>
        <Segment className='sectionHeader' vertical>
            <Container text>
                <Divider as='h2' className='header' horizontal>
                    {t("Home-ProblemsHeader")}
                </Divider>
                <p style={{ fontSize: '1.33em' }}>
                    {t("Home-ProblemsSubheader")}
                </p>
                <Header as='h3'>{t("Home-Problems1Header")}</Header>
                <p style={{ fontSize: '1.33em' }}>
                    {t("Home-Problems1Desc")}
                </p>
                <Header as='h3'>{t("Home-Problems2Header")}</Header>
                <p style={{ fontSize: '1.33em' }}>
                    {t("Home-Problems2Desc")}
                </p>
                <Link to={RoutePaths.AboutPage}>
                    <Button as='a' color='green' inverted size='large'>{t("Home-ProblemsMoreButton")}</Button>
                </Link>
            </Container>
        </Segment>
    </div>
}
export default HomepageLayout