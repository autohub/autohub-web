import { GoogleLoginProps } from 'react-google-login';

declare namespace GoogleLoginPropsExtended {
    interface IconedProps extends GoogleLoginProps {
        icon?: boolean
    }
}

export default GoogleLoginPropsExtended;