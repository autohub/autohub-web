var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var releaseConfig = require('./webpack.config.release');
var isProductionEnvironment = process.env.ASPNETCORE_ENVIRONMENT === 'Production';
var path = require('path');
var merge = require('extendify')({ isDeep: true, arrays: 'replace' });

var config = {
    mode: "development",
    entry: {
        main: path.join(__dirname, 'boot.tsx')
    },
    output: {
        path: path.join(__dirname, '../api/', 'wwwroot'),
        filename: '[name].js',
        publicPath: '/'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.styl', '.css']
    },
    module: {
        rules: [
            { test: /\.ts(x?)$/, loaders: ['ts-loader'], exclude: /node_modules/ },
            { test: /\.css/, loader: 'style-loader!css-loader' },
            {
                test: /\.(jpg|jpeg|png|woff|woff2|eot|ttf|svg)$/,
                loader: 'url-loader?limit=100000',
                options: {
                    esModule: false,
                },
            }
        ]
    },
    devtool: 'inline-source-map',
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, 'index.ejs'),
            inject: true
        }),
        new webpack.WatchIgnorePlugin([
            /\.js$/,
            /\.d\.ts$/
        ]),
        new webpack.DefinePlugin({
            'process.env.ASPNETCORE_ENVIRONMENT': JSON.stringify(process.env.ASPNETCORE_ENVIRONMENT),
            'process.env.APP_VERSION': JSON.stringify(process.env.CI_COMMIT_TAG)
        })
    ]
};

if (isProductionEnvironment) {
    config = merge(config, releaseConfig);
}

module.exports = config;
