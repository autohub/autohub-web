import RestUtilities from './RestUtilities';
import { IVehicle } from '../typings/Vehicle';
import { Moment } from 'moment';

export default class Statistics {
    fetch() {
        return RestUtilities.get<IVehiclesStatistics>('/api/statistics');
    }

    generateGraphReport(data: IGraphReportRequest) {
        return RestUtilities.post<IGraphReportResponse>('/api/reports', data);
    }
}

export interface IVehiclesStatistics {
    vehicles: Array<IVehicle>;
    totalVehiclesDistance: number;
    totalVehicleHistories: number;
    totalVehicleHistoriesCost: number;
    totalFuelingVehicleHistories: number;
    totalRepairVehicleHistories: number;
    totalTaxVehicleHistories: number;
}

export interface IGraphReportRequest {
    graphType: string;
    vehicleId: string;
    from: Moment;
    to: Moment;
}

export interface IGraphReportResponse { }

export interface IGraphReportTotalVehicleHistoriesCosts extends IGraphReportResponse {
    totalFuelingCosts: number,
    totalRepairCosts: number,
    totalTaxesCosts: number
}

export interface IGraphReportTotalVehicleHistoriesByMonthsCosts extends IGraphReportResponse {
    vehicleHistoriesByMonthsData: any[]
}

export interface IGraphReportMilageGrowth extends IGraphReportResponse {
    milageByMonthData?: any[];
}


export interface IGraphReportFuelConsumptions extends IGraphReportResponse {
    fuelingsByTypeForMonthData?: any[];
}

export interface IGraphReportFuelPrices extends IGraphReportResponse {
    fuelingPricesByTypeForMonthData?: any[];
}

