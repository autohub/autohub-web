import RestUtilities from './RestUtilities';
import { IVehicle } from './../typings/Vehicle';

export default class Vehicles {
    fetchAll() {
        return RestUtilities.get<Array<IVehicle>>('/api/vehicles');
    }

    fetch(id: string) {
        return RestUtilities.get<IVehicle>(`/api/vehicles/${id}`);
    }

    search(query: string) {
        return RestUtilities.get<Array<IVehicle>>(`/api/vehicles/search/?q=${query}`);
    }

    update(vehicle: IVehicle) {
        return RestUtilities.put<IVehicle>('/api/vehicles/', vehicle);
    }

    create(vehicle: IVehicle) {
        return RestUtilities.post<IVehicle>('/api/vehicles', vehicle);
    }

    save(vehicle: IVehicle) {
        if (vehicle.id) {
            return this.update(vehicle);
        } else {
            return this.create(vehicle);
        }
    }

    delete(id: string) {
        return RestUtilities.delete(`/api/vehicles/${id}`);
    }
}
