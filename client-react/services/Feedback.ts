import RestUtilities from './RestUtilities';

export interface IFeedbackMessage {
    message: string,
    name: string,  
    email: string
}

export default class Feedback {
    post(message: IFeedbackMessage) {
        return RestUtilities.post<IFeedbackMessage>('/api/contacts', message);
    }
}

