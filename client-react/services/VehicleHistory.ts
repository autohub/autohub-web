import RestUtilities from './RestUtilities';
import { IRemindableVehicleHistory } from './../typings/RemindableVehicleHistories';
import { 
    IVehicleHistory, 
    ITaxVehicleHistory, 
    IRepairVehicleHistory, 
    IFuelingVehicleHistory, 
    IPagedListVehicleHistoriesResponse, 
    IDetailsVehicleHistoryResponse,
    IPagedListVehicleHistoriesRequest
} from '../typings/VehicleHistory';

export default class VehicleHistories {
    fetchAll(req: IPagedListVehicleHistoriesRequest) {
        return RestUtilities.get<IPagedListVehicleHistoriesResponse>(
            `/api/vehicles/history?vehicleId=${req.vehicleId}&type=${req.type}&category=${req.category}&page=${req.activePage}&perPage=${req.perPage}`);
    }

    fetch(vehicleId: string, type: string) {
        return RestUtilities.get<IDetailsVehicleHistoryResponse>(`/api/vehicles/history/${type}/${vehicleId}`);
    }

    search(query: string) {
        return RestUtilities.get<Array<IVehicleHistory>>(`/api/vehicles/search/?q=${query}`);
    }

    updateRenew(vehicleHistory: IRemindableVehicleHistory) {  
        return RestUtilities.put<IRemindableVehicleHistory>(`/api/vehicles/history/${vehicleHistory.id}/renew`, {})
    }

    updateTax(vehicleHistory: ITaxVehicleHistory) {
        return RestUtilities.put<ITaxVehicleHistory>('/api/vehicles/history/tax/', vehicleHistory);
    }

    updateRepair(vehicleHistory: IRepairVehicleHistory) {
        return RestUtilities.put<IRepairVehicleHistory>('/api/vehicles/history/repair/', vehicleHistory);
    }

    updateFueling(vehicleHistory: IFuelingVehicleHistory) {
        return RestUtilities.put<IFuelingVehicleHistory>('/api/vehicles/history/fueling/', vehicleHistory);
    }

    createTax(vehicleHistory: ITaxVehicleHistory) {
        return RestUtilities.post<ITaxVehicleHistory>('/api/vehicles/history/tax', vehicleHistory);
    }

    createRepair(vehicleHistory: IRepairVehicleHistory) {
        return RestUtilities.post<IRepairVehicleHistory>('/api/vehicles/history/repair', vehicleHistory);
    }

    createFueling(vehicleHistory: IFuelingVehicleHistory) {
        return RestUtilities.post<IFuelingVehicleHistory>('/api/vehicles/history/fueling', vehicleHistory);
    }

    saveTax(vehicleHistory: ITaxVehicleHistory) {
        if (vehicleHistory.id) {
            return this.updateTax(vehicleHistory);
        } else {
            return this.createTax(vehicleHistory);
        }
    }

    saveRepair(vehicleHistory: IRepairVehicleHistory) {
        if (vehicleHistory.id) {
            return this.updateRepair(vehicleHistory);
        } else {
            return this.createRepair(vehicleHistory);
        }
    }

    saveFueling(vehicleHistory: IFuelingVehicleHistory) {
        if (vehicleHistory.id) {
            return this.updateFueling(vehicleHistory);
        } else {
            return this.createFueling(vehicleHistory);
        }
    }

    delete(id: string, type: string) {
        return RestUtilities.delete(`/api/vehicles/history/${type.toLowerCase()}/${id}`);
    }
}
