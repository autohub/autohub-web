import {
    IRemindableVehicleHistory,
    IRemindersGroup,
} from "./../typings/RemindableVehicleHistories";
import * as moment from "moment";
import { IVehicle } from "../typings/Vehicle";
import { useTranslation } from "react-i18next";

export const mapRemindableVehicleHistories = (vehicles: Array<IVehicle>) => {
    let group: IRemindersGroup = {
        Expiring: Array<IRemindableVehicleHistory>(),
        Expired: Array<IRemindableVehicleHistory>(),
    };

    vehicles.forEach((v) => {
        if (v.remindableVehicleHistories.length > 0) {
            const expiring = mapExpiring(v.remindableVehicleHistories);
            const expired = mapExpired(v.remindableVehicleHistories);

            group.Expiring = group.Expiring.concat(expiring);
            group.Expired = group.Expired.concat(expired);
        }
    });

    return group;
};

export const mapExpirationInfo = (vehicleHistory: IRemindableVehicleHistory) => {
    const { t } = useTranslation();

    if (vehicleHistory.isRemindableByDate) {
        vehicleHistory.expirationInfoDate = moment
            .utc()
            .to(moment.utc(vehicleHistory.expiresOn).local());
    }
    if (vehicleHistory.isRemindableByMilage) {
        var diff =
            vehicleHistory.expiresOnMilage -
            vehicleHistory.currentVehicleMilage;
        vehicleHistory.expirationInfoMilage =
            (diff > 0 ? t("VehicleHistory-ExpiringAfter") : t("VehicleHistory-ExpiringBefore")) + " " + Math.abs(diff) + " " + t("Milage-Km");
    }

    return vehicleHistory;
};

const mapExpiring = (vehicleHistories: IRemindableVehicleHistory[]) =>
    vehicleHistories
        .filter(
            (vh) =>
                !isExpiredByDate(vh) &&
                !isExpiredByMilage(vh) &&
                (isRemindingByDate(vh) || isRemindingByMilage(vh)) &&
                !vh.completedOn
        )
        .map((vh) => mapExpirationInfo(vh));

const mapExpired = (vehicleHistories: IRemindableVehicleHistory[]) =>
    vehicleHistories
        .filter((vh) => isExpired(vh) && !vh.completedOn)
        .map((vh) => mapExpirationInfo(vh));

const isExpired = (vh: IRemindableVehicleHistory) =>
    isExpiredByDate(vh) || isExpiredByMilage(vh);

const isRemindingByMilage = (vehicleHistory: IRemindableVehicleHistory) =>
    vehicleHistory.isRemindableByMilage &&
    !!vehicleHistory.expiresOnMilage &&
    vehicleHistory.milageRange > 0 &&
    isInMilageRange(vehicleHistory) &&
    !isExpiredByMilage(vehicleHistory);

const isInMilageRange = (vh: IRemindableVehicleHistory) =>
    vh.expiresOnMilage - vh.currentVehicleMilage < vh.milageRange;

const isRemindingByDate = (vehicleHistory: IRemindableVehicleHistory) =>
    vehicleHistory.isRemindableByDate &&
    !!vehicleHistory.expiresOn &&
    vehicleHistory.daysRange > 0 &&
    isInDateRange(vehicleHistory) &&
    !isExpiredByDate(vehicleHistory);

const isInDateRange = (vehicleHistory: IRemindableVehicleHistory) => {
    const now = moment.utc().local();
    const end = moment.utc(vehicleHistory.expiresOn).local();
    const diff = end.diff(now, "days");
    const result = vehicleHistory.daysRange - diff > 0;
    return result;
};

export const isExpiredByDate = (vh: IRemindableVehicleHistory) => {
    if (!vh.isRemindableByDate) return;
    var now = moment.utc().local();
    var end = moment.utc(vh.expiresOn).local();
    const result = end.diff(now, "days") < 0;
    return result;
};

export const isExpiredByMilage = (vh: IRemindableVehicleHistory) =>
    vh.isRemindableByMilage
        ? vh.currentVehicleMilage > vh.expiresOnMilage
        : false;
