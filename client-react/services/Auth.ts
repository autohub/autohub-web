import RestUtilities from './RestUtilities';
import AuthStore from '../stores/Auth';

interface IAuthResponse {
    token: string;
    username: string;
    userEmail: string;
}

interface IUser {
    username: string,
    password: string
}

interface IRegisterUser extends IUser {
    name: string,
    usertype: string
}

export default class Auth {
    static isSignedIn(): boolean {
        return !!AuthStore.getToken();
    }

    getUser(): IAuthResponse {
        return AuthStore.getUser();
    }

    signIn(email: string, password: string) {
        let user: IUser = {
            username: email,
            password: password
        };
        return RestUtilities.post<IAuthResponse>('/api/auth/login', user)
            .then((response) => {
                if (!response.is_error) {
                    AuthStore.setUser(response.content);
                }
                return response;
            });
    }

    signInFacebook(data: any) {
        return RestUtilities.post<IAuthResponse>('/api/externalauth/facebook', data)
            .then((response) => {
                if (!response.is_error) {
                    AuthStore.setUser(response.content);
                }
                return response;
            });
    }

    signInGoogle(data: any) {
        var idToken = data.tokenId;
        return RestUtilities.post<IAuthResponse>('/api/externalauth/google', { "idToken": idToken })
            .then((response) => {
                if (!response.is_error) {
                    AuthStore.setUser(response.content);
                }
                return response;
            });
    }

    register(email: string, password: string, name: string, usertype: string) {
        let user: IRegisterUser = {
            username: email,
            password: password,
            usertype: usertype,
            name: name
        };
        return RestUtilities.post<IAuthResponse>('/api/auth/register', user)
            .then((response) => {
                return response;
            });
    }

    confirm(token: string): Promise<boolean> {
        return RestUtilities.post('/api/auth/confirm', { token: token })
            .then((response) => {
                return true;
            }).catch((err) => {
                console.log(err);
                return false;
            });
    }

    passwordResetRequest(email: string) {
        return RestUtilities.post<IAuthResponse>('/api/auth/password/request', { email: email })
            .then((response) => {
                return response;
            })
    }

    passwordReset(userId: string, token: string, newPassword: string, newPasswordConfirmation: string) {
        return RestUtilities.post<IAuthResponse>('/api/auth/password/reset', {
            userId: userId,
            token: token,
            newPassword: newPassword,
            newPasswordConfirmation: newPasswordConfirmation
        }).then((response) => {
            return response;
        })
    }

    signOut(): void {
        AuthStore.removeUser();
    }
}
