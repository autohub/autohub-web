import RestUtilities from './RestUtilities';

export enum UserType {
    Personal = "Personal",
    Business = "Business"
};

export interface IAccount {
    id: string,
    name: string,
    userType: string
}

export default class Account {
    fetch() {
        return RestUtilities.get<IAccount>(`/api/account`);
    }

    update(account: IAccount) {
        return RestUtilities.put<IAccount>(`/api/account`, account);
    }

    updateLang(language: string) {
        return RestUtilities.put('/api/account/language', { language });
    }

    passwordChange(oldPassword: string, newPassword: string, newPasswordConfirmation: string) {
        return RestUtilities.put<IAccount>('/api/account/password', {
            oldPassword: oldPassword,
            newPassword: newPassword,
            newPasswordConfirmation: newPasswordConfirmation
        })
    }

    delete() {
        return RestUtilities.delete(`/api/account`);
    }
}

