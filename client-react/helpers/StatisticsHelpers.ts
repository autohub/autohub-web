import { useTranslation } from "react-i18next";

export const convertMilageByMonthData = (vehiclesMilageData: any[]) => {
    const res: any[] = [];
    for (const key in vehiclesMilageData) {
        const el = vehiclesMilageData[key];
        res.push({ x: el.yearMonth, y: el.totalMilage })
    }
    return res.sort(dynamicSort("x"));
}

export const dynamicSort = (property: string) => {
    var sortOrder = 1;
    if (property[0] === "-") {
        sortOrder = -1;
        property = property.substr(1);
    }
    return function (a: any, b: any) {
        var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
        return result * sortOrder;
    }
}

export const convertVehicleHistoriesByTypeForMonth = (vehicleHistoriesStats: any[]) => {
    const { t } = useTranslation();
    
    const result: any[] = []
    for (const key in vehicleHistoriesStats) {
        const el = vehicleHistoriesStats[key];
        result.push({
            "month": el.yearMonth,
            "Repair": el.totalPriceRepairs,
            "Fueling": el.totalPriceFueling,
            "Tax": el.totalPriceTaxes
        });
    }

    return result.sort(dynamicSort("month"));
}

export const convertFuelingByTypeForMonth = (vehicleHistoriesStats: any[]) => {
    const result: any[] = [];
    for (const key in vehicleHistoriesStats) {
        const element = vehicleHistoriesStats[key];
        result.push({
            id: element.type,
            color:  "hsl(" + getRandomInt(1, 239) + ", 70%, 50%)",
            data: element.consumptionsPerMonth.map((e: { yearMonth: any; consumption: any; }) => (
                { x: e.yearMonth, y: e.consumption }
            ))
        });
    }
    
    return result;
}

export const convertFuelingPricesByTypeForMonth = (vehicleHistoriesStats: any[]) => {
    const result: any[] = [];
    for (const key in vehicleHistoriesStats) {
        const element = vehicleHistoriesStats[key];
        result.push({
            id: element.type,
            color:  "hsl(" + getRandomInt(1, 239) + ", 70%, 50%)",
            data: element.fuelingPricesPerMonth.map((e: { date: any; price: any; }) => (
                { x: e.date, y: e.price }
            ))
        });
    }

    return result;
}

export const getRandomInt = (min: number, max: number) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
