export const toLowerFirstChar = (s: string) => s.charAt(0).toLowerCase() + s.slice(1);

export const toLowerCaseErrors = (responseErrors: any) => {
    var errors = {} as { [key: string]: string };
    Object.keys(responseErrors).map(key => (errors[toLowerFirstChar(key)] = responseErrors[key]));
    return errors;
}