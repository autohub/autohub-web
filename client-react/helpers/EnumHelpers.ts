import { useTranslation } from "react-i18next";

export const GetEnumValueByKey = <T>(type: T, key: string) =>
    type[key as keyof typeof type];

export const GetTranslatedEnumTypes = <T>(type: T): Map<string, string> => {
    const { t } = useTranslation();
    let types: Map<string, string> = new Map<string, string>();

    for (var n in type) {
        types.set(n.toString(), t(n.toString()));
    };

    return types;
}