import { IVehicle } from '../typings/Vehicle';

export interface IDropDownOptions {
    key: any,
    text: string,
    value: any
}

export const EnumToDropdownOptions = <T>(type: T) => {
    const options: { id: string; text: string, value: string }[] = [];
    for (var n in type) {
        options.push({
            id: n,
            text: (type as any)[n],
            value: n
        });
    };
    return options;
}

export const MapToDropdownOptions = (map: Map<string, string>) => {
    let opts = Array<IDropDownOptions>();

    for (let [key, value] of map.entries()) {
        opts.push({ key: key, text: value, value: key } as IDropDownOptions);
    }
    return opts;
}

export const VehiclesToDropdownOptions = (vehicles: Array<IVehicle>) =>
    vehicles.map(x => ({
        key: x.id,
        text: x.title,
        value: x.id
    }));

export const GetNotificationDaysRangeOptions = () => {
    const days = [1, 3, 5, 10, 15, 30, 40]
    let daysRange: { id: string; text: string, value: string }[] = [];
    for (var n in days) {
        daysRange.push({
            id: days[n].toString(),
            text: days[n].toString(),
            value: days[n].toString()
        });
    };
    return daysRange;
}

export const GetNotificationMilageRangeOptions = () => {
    const milage = [100, 500, 1000, 3000, 5000, 10000]
    let milageRange: { id: string; text: string, value: string }[] = [];
    for (var n in milage) {
        milageRange.push({
            id: milage[n].toString(),
            text: milage[n].toString(),
            value: milage[n].toString()
        });
    };
    return milageRange;
}
