import { Moment } from "moment";
import { IRemindableVehicleHistory } from './RemindableVehicleHistories';
import { IVehicle } from "./Vehicle";
import { FuelType, TaxVehicleHistoryCategory, RepairVehicleHistoryCategory } from "./Enums/VehicleHistories";

export interface IPagedListVehicleHistoriesResponse {
    vehicleHistories: Array<IVehicleHistory>,
    vehicles: Array<IVehicle>,
    pagesCount: number
}

export interface IPagedListVehicleHistoriesRequest {
    activePage: number, 
    perPage: number,
    vehicleId?: string, 
    type?: string,
    category?: string 
}

export interface IDetailsVehicleHistoryResponse {
    vehicles: Array<IVehicle>,
    vehicleHistory: IVehicleHistory
}
export interface IVehicleHistory {
    id?: string;
    vehicleId: string;
    category?: string;
    totalPrice: number;
    place: string;
    note: string;
    type: string;
    currentMilage?: number;
    createdOn?: Moment;
    updatedOn?: Moment;
}

export interface ITaxVehicleHistory extends IRemindableVehicleHistory {
    category: TaxVehicleHistoryCategory;
}

export interface IRepairVehicleHistory extends IRemindableVehicleHistory {
    category: RepairVehicleHistoryCategory;
}

export interface IFuelingVehicleHistory extends IVehicleHistory {
    category: FuelType;
    liters: number;
    pricePerLiter?: number;
    isPartial?: boolean;
    isMissedPrevious?: boolean;
}
