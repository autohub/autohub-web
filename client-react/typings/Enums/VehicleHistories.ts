export enum VehicleHistoryType {
    Tax = "Tax",
    Repair = "Repair",
    Fueling = "Fueling",
}

export enum TaxVehicleHistoryCategory {
    Fine = "Fine",
    Duty = "Duty",
    Parking = "Parking",
    Payment = "Payment",
    RoadTax = "RoadTax",
    Registration = "Registration",
    AdditionalInsurance = "AdditionalInsurance",
    Insurance = "Insurance",
    TechnicalCheck = "TechnicalCheck",
    Vignette = "Vignette",
}

export enum RepairVehicleHistoryCategory {
    ExhaustSystem = "ExhaustSystem",
    TransmissionSystem = "TransmissionSystem",
    CoolingSystem = "CoolingSystem",
    BreakingSystem = "BreakingSystem",
    SteeringSystem = "SteeringSystem",
    FuelingSystem = "FuelingSystem",
    HeatingSystem = "HeatingSystem",
    SuspensionSystem = "SuspensionSystem",
    AirConditionerSystem = "AirConditionerSystem",
    Clutch = "Clutch",
    Fluids = "Fluids",
    OilChange = "OilChange",
    BodyAndChassis = "BodyAndChassis",
    TyresAndRims = "TyresAndRims",
    Engine = "Engine",
    Straps = "Straps",
    Wipers = "Wipers",
    Lights = "Lights",
    GlassesAndMirrors = "GlassesAndMirrors",
    Candles = "Candles",
    ExpensesForLabor = "ExpensesForLabor",
    Battery = "Battery",
    FuelFilter = "FuelFilter",
    InnerFilter = "InnerFilter",
    AirFilter = "AirFilter",
    BreakingPads = "BreakingPads",
    Inspection = "Inspection"
}

export enum FuelType {
    Gasoline = "Gasoline",
    Diesel = "Diesel",
    Gas = "Gas",
    Methane = "Methane",
    Ethanol = "Ethanol"
}
