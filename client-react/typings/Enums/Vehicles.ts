export enum VehicleFuelType {
    Gasoline = "Gasoline",
    Diesel = "Diesel",
    GasGasoline = "GasGasoline",
    MethaneGasoline = "MethaneGasoline",
    Electricity = "Electricity",
    Ethanol = "Ethanol"
}