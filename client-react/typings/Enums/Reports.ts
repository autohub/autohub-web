import { useTranslation } from "react-i18next";

export enum ReportGraphTypes {
    MilageGrowth = "MilageGrowth",
    TotalVehicleHistoriesCosts = "TotalVehicleHistoriesCosts",
    TotalVehicleHistoriesCostsByMonth = "TotalVehicleHistoriesCostsByMonth",
    FuelPrices = "FuelPrices",
    FuelConsumptions = "FuelConsumptions"
}

export const GetGraphReportTypes = () : Map<string, string> => {
    const { t } = useTranslation();
    let types: Map<string, string> = new Map<string, any>();

    for (var n in ReportGraphTypes) {
        types.set(n.toString(), t(n.toString()));
    };

    return types;
}