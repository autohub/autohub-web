import { IVehicleHistory } from '../typings/VehicleHistory';
import { Moment } from 'moment';

export interface IRemindableVehicleHistory extends IVehicleHistory {
    isRemindableByDate?: boolean;
    isRemindableByMilage?: boolean;
    expiresOn?: Moment;
    expiresOnMilage?: number;
    daysRange?: number;
    milageRange?: number;
    expirationInfoMilage?: string;
    expirationInfoDate?: string;
    currentVehicleMilage?: number;
    completedOn?: Moment;
}

export interface IRemindersGroup {
    Expiring: Array<IRemindableVehicleHistory>;
    Expired: Array<IRemindableVehicleHistory>;
}
