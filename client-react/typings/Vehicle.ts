import { Moment } from "moment";
import { IRemindableVehicleHistory } from "./RemindableVehicleHistories";
import { VehicleFuelType } from "./Enums/Vehicles";

export interface IVehicle {
    id?: string,
    make: string;   
    model: string;
    title: string;
    productionDate?: Moment;
    fuelType: VehicleFuelType;
    createdOn?: Moment;
    updatedOn?: Moment;
    milage?: number;
    engineSize?: number;
    horsePower?: number;
    remindableVehicleHistories?: Array<IRemindableVehicleHistory>;
    totalVehicleHistories?: number;
    lastFuelConsumption?: number;
    averageFuelConsumption?: number;
}