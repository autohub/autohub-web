using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using api.Domain.Models.Responses.Auth;
using api.test.integration.Helpers;

namespace api.test.integration.Fixtures
{
    public class AuthenticationFixture
    {
        private static HttpClient client;
        private string token;

        public string GetAuthToken(HttpClient testClient)
        {
            client = testClient;
            client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("en"));
            return string.IsNullOrEmpty(token)
                ? CreateTokenAsync().Result
                : token;
        }

        public async Task<string> CreateTokenAsync()
        {
            await RegisterUserAsync();
            token = await GetTokenAsync();
            return token;
        }

        private async Task RegisterUserAsync()
        {
            var content = new Dictionary<string, string>()
            {
                {"username", Constants.Username},
                {"password", Constants.Password},
                {"name", "Georgi Marokov"},
                {"userType", "Personal"},
            };

            await client.PostAsync(
                "/api/auth/register",
                Utilities.CreateJsonHttpContent(content));
        }

        private Task<string> GetTokenAsync()
        {
            var content = new Dictionary<string, string>()
            {
                {"username", Constants.Username},
                {"password", Constants.Password}
            };

            var response = client.PostAsync(
                "/api/auth/login",
                Utilities.CreateJsonHttpContent(content));

            var responseModel =  response
                .Result
                .Content
                .ReadAsAsync<LoginUserResponse>()
                .Result;

            return Task.FromResult(responseModel.Token);
        }
    }
}
