using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore;
using Hangfire;
using Hangfire.MemoryStorage;
using api.test.integration.Mocks;
using Microsoft.AspNetCore.TestHost;
using api.Data;
using Microsoft.AspNetCore.Identity;
using api.Infrastructure.Apis.Facebook;
using api.Infrastructure.Email;

namespace api.test.integration.Fixtures
{
    public class CustomWebApplicationFactory<TStartup>
        : WebApplicationFactory<TStartup> where TStartup : class
    {
        protected override IWebHostBuilder CreateWebHostBuilder() =>
            WebHost.CreateDefaultBuilder()
            .UseStartup<TStartup>();

        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            base.ConfigureWebHost(builder);

            builder.UseEnvironment("Testing");

            builder.ConfigureServices(services =>
            {
                var serviceProvider = new ServiceCollection()
                    .AddEntityFrameworkInMemoryDatabase()
                    .BuildServiceProvider();

                // Add a database context (ApplicationDbContext) using an in-memory database for testing.
                services.AddDbContext<DefaultDbContext>(options =>
                {
                    options.UseInMemoryDatabase("InMemoryDbForTesting");
                    options.UseInternalServiceProvider(serviceProvider);
                });

                var sp = services.BuildServiceProvider();

                using var scope = sp.CreateScope();
                var scopedServices = scope.ServiceProvider;
                var db = scopedServices.GetRequiredService<DefaultDbContext>();
                var logger = scopedServices
                    .GetRequiredService<ILogger<CustomWebApplicationFactory<TStartup>>>();

                db.Database.EnsureCreated();
            });

            // Overwrite base Startup services
            var inMemoryStorage = GlobalConfiguration.Configuration.UseMemoryStorage();
            builder.ConfigureTestServices(s =>
            {
                s.Configure<IdentityOptions>(options => options.SignIn.RequireConfirmedEmail = false);
                s.AddTransient<IEmailSender, EmailSenderMock>();
                s.AddHttpClient<IFacebookClient, FacebookClientMock>();
                s.AddHangfire(x => x.UseStorage(inMemoryStorage));
            });
        }
    }
}
