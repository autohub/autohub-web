using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using api.Domain.Models.Responses.VehicleHistory;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace api.test.integration.Helpers
{
    public static class VehicleHistoriesFactory
    {
        public static async Task<string> GetCreatedVehicleHistoryId(HttpClient client)
        {
            var content = new Dictionary<string, string>()
            {
                {"vehicleId", await VehiclesFactory.GetVehicleId(client)},
                {"type", "Fueling"},
                {"currentMilage", "12232001"},
                {"totalPrice", "12"},
                {"category", "Gasoline"},
                {"liters", "121"},
                {"pricePerLiter", "2"},
            };

            var response = await client.PostAsync(
                "/api/vehicles/history/fueling",
                Utilities.CreateJsonHttpContent(content));

            return await ExtractVehicleHistoryId(response);
        }

        public static async Task<string> GetCreatedTaxVehicleHistoryId(HttpClient client)
        {
            var content = new Dictionary<string, string>()
            {
                {"vehicleId", await VehiclesFactory.GetCreatedVehicleId(client)},
                {"type", "Tax"},
                {"category", "Fine"},
                {"currentMilage", (Constants.InitMilage + 100).ToString()},
                {"totalPrice", "100"},
                {"isRemindable", "false"},
                {"expiresOn", "2020-01-25T22:00:00.000Z"},
                {"daysRange", "3"},
                {"isRemindableByDate", "true"},
                {"isRemindableByMilage", "true"},
                {"expiresOnMilage", "131"},
                {"milageRange", "5000"},
                {"note", ""},
            };

            var response = await client.PostAsync(
                "/api/vehicles/history/tax",
                Utilities.CreateJsonHttpContent(content));

            return await ExtractVehicleHistoryId(response);
        }

        public static async Task<string> GetCreatedRepairVehicleHistoryId(HttpClient client)
        {
            var content = new Dictionary<string, string>()
            {
                {"vehicleId", await VehiclesFactory.GetCreatedVehicleId(client)},
                {"type", "Repair"},
                {"category", "ExhaustSystem"},
                {"currentMilage", (Constants.InitMilage + 100).ToString()},
                {"totalPrice", "100"},
                {"isRemindable", "false"},
                {"expiresOn", "2020-01-25T22:00:00.000Z"},
                {"daysRange", "3"},
                {"isRemindableByDate", "true"},
                {"isRemindableByMilage", "true"},
                {"expiresOnMilage", "1000"},
                {"milageRange", "5000"},
                {"note", ""},
            };

            var response = await client.PostAsync(
                "/api/vehicles/history/repair",
                Utilities.CreateJsonHttpContent(content));

            return await ExtractVehicleHistoryId(response);
        }

        private static async Task<string> ExtractVehicleHistoryId(HttpResponseMessage response)
        {
            var responseContent = await response.Content.ReadAsStringAsync();
            var jsonContent = (JObject)JsonConvert.DeserializeObject(responseContent);
            var vehicleHistoryId = jsonContent.GetValue("id");
            return vehicleHistoryId.ToString();
        }

        public static async Task<string> GetVehicleHistoryId(HttpClient client)
        {
            var vehicleId = await VehiclesFactory.GetVehicleId(client);

            var responseVehiclesHistory = await client.GetAsync(
                $"api/vehicles/history?vehicleId={vehicleId}");
            var listVehiclesHistory = await responseVehiclesHistory.Content
                .ReadAsAsync<PagedListVehicleHistoriesResponse>();
            var vehicleHistyroyId = listVehiclesHistory.VehicleHistories.ToList()[0].Id.ToString();

            return vehicleHistyroyId;
        }
    }
}
