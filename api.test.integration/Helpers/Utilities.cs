using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;

namespace api.test.integration.Helpers
{
  public static class Utilities
    {
        public static StringContent CreateJsonHttpContent(Dictionary<string, string> content) =>
            new StringContent(
                JsonConvert.SerializeObject(content),
                Encoding.UTF8,
                "application/json"
            );
    }
}
