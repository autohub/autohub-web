using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using api.Domain.Enums;
using api.Domain.Models.Responses.Vehicle;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace api.test.integration.Helpers
{
    public static class VehiclesFactory
    {
        public static async Task<string> GetCreatedVehicleId(HttpClient client)
        {
            const long milage = Constants.InitMilage;
            const FuelType fuelType = FuelType.Gasoline;

            var content = new Dictionary<string, string>()
            {
                {"make", "Testla"},
                {"model", "TModels"},
                {"milage", milage.ToString()},
                {"ProductionDate", "12/12/91"},
                {"engineSize", "2000"},
                {"horsePower", "121"},
                {"fuelType", fuelType.ToString()}
            };

            var response = await client.PostAsync(
                "/api/vehicles/",
                Utilities.CreateJsonHttpContent(content));

            var responseContent = response.Content.ReadAsStringAsync().Result;
            var jsonContent = (JObject)JsonConvert.DeserializeObject(responseContent);
            var vehicleId = jsonContent.GetValue("id");

            return vehicleId.ToString();
        }

        public static async Task<string> GetVehicleId(HttpClient client)
        {
            var responseVehicles = await client.GetAsync("/api/vehicles/");
            var listVehicles = await responseVehicles.Content.ReadAsAsync<List<ListVehicleResponse>>();
            var vehicleId = listVehicles[0].Id.ToString();

            return vehicleId;
        }
    }
}
