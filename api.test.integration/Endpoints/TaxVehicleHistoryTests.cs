using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using api.Domain.Models.Responses.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.test.integration.Fixtures;
using api.test.integration.Helpers;
using FluentAssertions;
using Xunit;

namespace api.test.integration.Endpoints
{
    [TestCaseOrderer("api.test.integration.Helpers.PriorityOrderer", "api.test.integration")]
    public class TaxVehicleHistoryTests : BaseAuthTest, IClassFixture<AuthenticationFixture>
    {
        public TaxVehicleHistoryTests(CustomWebApplicationFactory<Startup> factory, AuthenticationFixture authFixture)
            : base(factory, authFixture) { }

        [Fact, TestPriority(0)]
        public async Task Post_Returns_CreatedAtRouteResult()
        {
            //Arrange
            const long milage = Constants.InitMilage + 10;
            var content = new Dictionary<string, string>()
            {
                {"vehicleId", await VehiclesFactory.GetCreatedVehicleId(client)},
                {"type", "Tax"},
                {"currentMilage", milage.ToString()},
                {"totalPrice", "12"},
                {"category", "Insurance"},
                {"IsRemindableByDate", "false"},
                {"IsRemindableByMilage", "false"}
            };

            //Act
            var response = await client.PostAsync(
                "/api/vehicles/history/tax",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Fact, TestPriority(0)]
        public async Task Post_WithExpireByDate_Returns_CreatedAtRouteResult()
        {
            //Arrange
            const long milage = Constants.InitMilage + 10;
            var content = new Dictionary<string, string>()
            {
                {"vehicleId", await VehiclesFactory.GetCreatedVehicleId(client)},
                {"type", "Tax"},
                {"currentMilage", milage.ToString()},
                {"totalPrice", "12"},
                {"category", "Insurance"},
                {"isRemindableByDate", "true"},
                {"IsRemindableByMilage", "false"},
                {"expiresOn", "2020-01-25T22:00:00.000Z"},
                {"daysRange", "1"}
            };

            //Act
            var response = await client.PostAsync(
                "/api/vehicles/history/tax",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Fact, TestPriority(0)]
        public async Task Post_WithExpireByMilage_Returns_CreatedAtRouteResult()
        {
            //Arrange
            const long milage = Constants.InitMilage + 10;
            var content = new Dictionary<string, string>()
            {
                {"vehicleId", await VehiclesFactory.GetCreatedVehicleId(client)},
                {"type", "Tax"},
                {"currentMilage", milage.ToString()},
                {"totalPrice", "12"},
                {"category", "Insurance"},
                {"isRemindableByMilage", "true"},
                {"IsRemindableByDate", "false"},
                {"expiresOnMilage", "131"},
                {"milageRange", "5000"}
            };

            //Act
            var response = await client.PostAsync(
                "/api/vehicles/history/tax",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Fact, TestPriority(0)]
        public async Task Post_WithExpireByMilageAndDate_Returns_CreatedAtRouteResult()
        {
            //Arrange
            const long milage = Constants.InitMilage + 10;
            var content = new Dictionary<string, string>()
            {
                {"vehicleId", await VehiclesFactory.GetCreatedVehicleId(client)},
                {"type", "Tax"},
                {"currentMilage", milage.ToString()},
                {"totalPrice", "12"},
                {"category", "Insurance"},
                {"isRemindableByMilage", "true"},
                {"expiresOnMilage", "131"},
                {"milageRange", "5000"},
                {"isRemindableByDate", "true"},
                {"expiresOn", "2020-01-25T22:00:00.000Z"},
                {"daysRange", "1"}
            };

            //Act
            var response = await client.PostAsync(
                "/api/vehicles/history/tax",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Fact, TestPriority(2)]
        public async Task Get_Returns_OkObjectResult()
        {
            //Arrange
            var id = await VehicleHistoriesFactory.GetCreatedTaxVehicleHistoryId(client);

            //Act
            var response = await client.GetAsync($"/api/vehicles/history/tax/{id}");
            var responseContent = await response.Content.ReadAsAsync<DetailsTaxVehicleHistoryResponse>();

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            responseContent.VehicleHistory.Id.Should().NotBeEmpty();
            responseContent.VehicleHistory.CurrentMilage.Should().NotBe(0);
            responseContent.VehicleHistory.TotalPrice.Should().BeGreaterThan(0);
        }

        [Fact, TestPriority(2)]
        public async Task Get_Returns_OkObjectResult_WithExpireInfo()
        {
            //Arrange
            var vehicleId = await VehiclesFactory.GetCreatedVehicleId(client);
            const long milage = Constants.InitMilage + 10;
            var content = new Dictionary<string, string>()
            {
                {"vehicleId", vehicleId},
                {"type", "Tax"},
                {"currentMilage", milage.ToString()},
                {"totalPrice", "12"},
                {"category", "Insurance"},
                {"isRemindableByMilage", "true"},
                {"expiresOnMilage", (milage + 100).ToString()},
                {"milageRange", "5000"},
                {"isRemindableByDate", "true"},
                {"expiresOn", "2020-01-25T22:00:00.000Z"},
                {"daysRange", "1"}
            };

            var vehicleHistoryResponse = await client.PostAsync(
                "/api/vehicles/history/tax",
                Utilities.CreateJsonHttpContent(content));
            var vehicleHistoryResponseContent = await vehicleHistoryResponse.Content
                .ReadAsAsync<TaxVehicleHistoryModel>();

            //Act
            var response = await client.GetAsync($"/api/vehicles/history/tax/{vehicleHistoryResponseContent.Id}");
            var responseContent = await response.Content.ReadAsAsync<DetailsTaxVehicleHistoryResponse>();

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            responseContent.VehicleHistory.IsRemindableByDate.Should().BeTrue();
            responseContent.VehicleHistory.ExpiresOn.Should().NotBeNull();
            responseContent.VehicleHistory.DaysRange.Should().NotBe(0);
            responseContent.VehicleHistory.IsRemindableByMilage.Should().BeTrue();
            responseContent.VehicleHistory.MilageRange.Should().NotBe(0);
            responseContent.VehicleHistory.ExpiresOnMilage.Should().NotBe(0);
        }

        [Fact, TestPriority(2)]
        public async Task Put_AcceptedResult()
        {
            //Arrange
            var id = await VehicleHistoriesFactory.GetCreatedTaxVehicleHistoryId(client);
            const long milage = Constants.InitMilage + 100;
            var vehicleId = await VehiclesFactory.GetVehicleId(client);
            var content = new Dictionary<string, string>()
            {
                {"id", id},
                {"vehicleId", vehicleId},
                {"type", "Tax"},
                {"category", "Insurance"},
                {"currentMilage", milage.ToString()},
                {"totalPrice", "12"},
                {"IsRemindableByDate", "false"},
                {"IsRemindableByMilage", "false"},
            };

            //Act
            var response = await client.PutAsync(
                "/api/vehicles/history/tax/",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Accepted);
        }

        [Fact, TestPriority(2)]
        public async Task Put_WithExpireByMilageAndDate_Returns_AcceptedResult()
        {
            //Arrange
            var id = await VehicleHistoriesFactory.GetCreatedTaxVehicleHistoryId(client);
            const long milage = Constants.InitMilage + 10000;
            var content = new Dictionary<string, string>()
            {
                {"id", id},
                {"vehicleId", await VehiclesFactory.GetVehicleId(client)},
                {"type", "Tax"},
                {"category", "Insurance"},
                {"currentMilage", milage.ToString()},
                {"totalPrice", "12"},
                {"isRemindableByMilage", "true"},
                {"expiresOnMilage", (milage + 100).ToString()},
                {"milageRange", "5000"},
                {"isRemindableByDate", "true"},
                {"expiresOn", "2020-01-25T22:00:00.000Z"},
                {"daysRange", "1"}
            };

            //Act
            var response = await client.PutAsync(
                "/api/vehicles/history/tax",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Accepted);
        }

        [Fact, TestPriority(2)]
        public async Task Put_WithExpireByDate_Returns_AcceptedResult()
        {
            //Arrange
            var id = await VehicleHistoriesFactory.GetCreatedTaxVehicleHistoryId(client);
            const long milage = Constants.InitMilage + 1000;
            var content = new Dictionary<string, string>()
            {
                {"id", id},
                {"vehicleId", await VehiclesFactory.GetVehicleId(client)},
                {"type", "Tax"},
                {"category", "Insurance"},
                {"currentMilage", milage.ToString()},
                {"totalPrice", "12"},
                {"isRemindableByDate", "true"},
                {"IsRemindableByMilage", "false"},
                {"expiresOn", "2020-01-25T22:00:00.000Z"},
                {"daysRange", "1"}
            };

            //Act
            var response = await client.PutAsync(
                "/api/vehicles/history/tax",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Accepted);
        }

        [Fact, TestPriority(2)]
        public async Task Put_WithExpireByMilage_Returns_AcceptedResult()
        {
            //Arrange
            var id = await VehicleHistoriesFactory.GetCreatedTaxVehicleHistoryId(client);
            const long milage = Constants.InitMilage + 10000;
            var content = new Dictionary<string, string>()
            {
                {"id", id},
                {"vehicleId", await VehiclesFactory.GetVehicleId(client)},
                {"type", "Tax"},
                {"category", "Insurance"},
                {"currentMilage", milage.ToString()},
                {"totalPrice", "12"},
                {"isRemindableByMilage", "true"},
                {"IsRemindableByDate", "false"},
                {"expiresOnMilage", (milage + 100).ToString()},
                {"milageRange", "5000"}
            };

            //Act
            var response = await client.PutAsync(
                "/api/vehicles/history/tax",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Accepted);
        }

        [Fact, TestPriority(3)]
        public async Task Delete_OkResult()
        {
            //Arrange
            var id = await VehicleHistoriesFactory.GetCreatedTaxVehicleHistoryId(client);

            //Arrange & Act
            var response = await client.DeleteAsync($"/api/vehicles/history/tax/{id}");

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Delete_UnauthorizedResult_ForNotOwningVehicleHistory()
        {
            //Arrange
            var id = new Guid().ToString();

            //Act
            var response = await client.DeleteAsync($"/api/vehicles/history/tax/{id}");

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task Get_UnauthorizedResult_ForNotOwningVehicleHistory()
        {
            //Arrange
            var id = new Guid().ToString();

            // Arrange & Act
            var response = await client.GetAsync($"/api/vehicles/history/tax/{id}");

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Theory, TestPriority(4)]
        [MemberData(nameof(PostVehicleHistoryData))]
        public async Task Post_BadRequestObjectResult_WhenModelStateIsInvalid(
            string type, string currentMilage, string totalPrice, string category)
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"vehicleId", await VehiclesFactory.GetVehicleId(client)},
                {"type", type},
                {"currentMilage", currentMilage},
                {"totalPrice", totalPrice},
                {"category", category},
            };

            //Act
            var response = await client.PostAsync(
                "/api/vehicles/history/tax",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Theory]
        [MemberData(nameof(UpdateVehicleHistoryData))]
        public async Task Put_BadRequestObjectResult_WhenModelStateIsInvalid(
            string type, string currentMilage, string totalPrice, string category)
        {
            //Arrange
            var vehicleHistoryId = await VehicleHistoriesFactory.GetCreatedTaxVehicleHistoryId(client);
            var content = new Dictionary<string, string>()
            {
                {"id", vehicleHistoryId},
                {"vehicleId", await VehiclesFactory.GetVehicleId(client)},
                {"type", type},
                {"currentMilage", currentMilage},
                {"totalPrice", totalPrice},
                {"category", category},
            };

            //Act
            var response = await client.PutAsync(
                "/api/vehicles/history/tax/",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        public static IEnumerable<object[]> PostVehicleHistoryData => GetFalseVehicleHistoryData;

        public static IEnumerable<object[]> UpdateVehicleHistoryData => GetFalseVehicleHistoryData;

        public static IEnumerable<object[]> GetFalseVehicleHistoryData =>
            new List<object[]>
            {
                new object[] { "type", "currentMilage", "totalPrice", "category" },
                new object[] { "", "123333", "123", "Insurance" },
                new object[] { "tax", "", "123", "Insurance" },
                new object[] { "tax", "1234", "", "Insurance" },
                new object[] { "tax", "1234", "123", "" },
                new object[] { "", "", "", "" },
                new object[] { " ", " ", " ", " " },
            };
    }
}
