using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using api.test.integration.Fixtures;
using api.test.integration.Helpers;
using FluentAssertions;
using Xunit;

namespace api.test.integration.Endpoints
{
    public class ContactTests : BaseTest
    {
        public ContactTests(CustomWebApplicationFactory<Startup> factory) : base(factory) { }

        [Fact]
        public async Task Post_OkResult()
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"email", "test@georgi.test"},
                {"name", "georgi"},
                {"message", "Georgi Marokov"},
            };

            //Act
            var response = await client.PostAsync(
                "/api/contacts",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Theory]
        [MemberData(nameof(ContactData))]
        public async Task Post_BadRequestObjectResult_WhenModelStateIsInvalid(
            string email, string name, string message)
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"email", email},
                {"name", name},
                {"message", message}
            };

            //Act
            var response = await client.PostAsync(
                "/api/contacts",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        public static IEnumerable<object[]> ContactData =>
           new List<object[]>
            {
                new object[] { "tester", "1234", "" },
                new object[] { "", "1234", "ValidName" },
                new object[] { "tester@test.com", "", "" },
                new object[] { "tester@", "valid1234", "testValid" },
                new object[] { "", "", ""},
                new object[] { "  ", "  ", " "}
            };
    }
}
