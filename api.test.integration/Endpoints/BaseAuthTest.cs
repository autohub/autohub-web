using System.Net.Http.Headers;
using api.test.integration.Fixtures;

namespace api.test.integration.Endpoints
{
    public class BaseAuthTest : BaseTest
    {
        protected BaseAuthTest(CustomWebApplicationFactory<Startup> factory, AuthenticationFixture authFixture)
            : base(factory)
        {
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
               "Bearer", authFixture.GetAuthToken(client));
        }
    }
}
