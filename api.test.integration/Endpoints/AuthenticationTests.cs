using Xunit;
using System.Net.Http;
using System.Threading.Tasks;
using System.Net;
using FluentAssertions;
using System.Collections.Generic;
using api.test.integration.Helpers;
using api.Common;
using api.Domain.Models.Responses.Auth;
using api.test.integration.Fixtures;

namespace api.test.integration.Endpoints
{
    [TestCaseOrderer("api.test.integration.Helpers.PriorityOrderer", "api.test.integration")]
    public class AuthenticationTests : BaseTest
    {
        public AuthenticationTests(CustomWebApplicationFactory<Startup> factory)
            : base(factory) { }

        [Fact, TestPriority(0)]
        public async Task Post_Register_OkResult()
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"username", Constants.Username},
                {"password", Constants.Password},
                {"name", "Georgi Marokov"},
                {"userType", "Personal"},
            };

            //Act
            var response = await client.PostAsync(
                "/api/auth/register",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Theory]
        [MemberData(nameof(RegisterData))]
        public async Task Post_Register_BadRequestObjectResult_WhenModelStateIsInvalid(
            string username, string password, string name, string userType)
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"username", username},
                {"password", password},
                {"name", name},
                {"userType", userType},
            };

            //Act
            var response = await client.PostAsync(
                "/api/auth/register",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact, TestPriority(1)]
        public async Task Post_Login_OkObjectResult_WithToken()
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"username", Constants.Username},
                {"password", Constants.Password},
                {"name", "Georgi Marokov"},
                {"userType", "Personal"},
            };

            //Act
            var response = await client.PostAsync(
                "/api/auth/login",
                Utilities.CreateJsonHttpContent(content));

            var responseModel = response.Content.ReadAsAsync<LoginUserResponse>().Result;

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            responseModel.Token.Should().NotBeNullOrEmpty();
            responseModel.UserEmail.Should().NotBeNullOrEmpty();
            responseModel.Username.Should().NotBeNullOrEmpty();
        }

        [Theory]
        [MemberData(nameof(LoginData))]
        public async Task Post_Login_BadRequestObjectResult_WhenFalseCombinationsPassed(
            string username, string password, string expected)
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"username", username},
                {"password", password},
                {"name", "teseter"},
                {"userType", "Personal"},
            };

            //Act
            var response = await client.PostAsync(
                "/api/auth/login",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            var responseModel = response.Content.ReadAsStringAsync().Result;
            responseModel.Should().Contain(expected);
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Theory]
        [InlineData("", "")]
        [InlineData(" ", " ")]
        public async Task Post_Login_BadRequestObjectResult_WhenModelIsNotValid(
            string username, string password)
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"username", username},
                {"password", password}
            };

            //Act
            var response = await client.PostAsync(
                "/api/auth/login",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            var responseModel = response.Content.ReadAsStringAsync().Result;
            responseModel.Should().Contain(ErrorMessages.RequiredField.Substring(3));
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact, TestPriority(2)]
        public async Task Post_ResetPasswordRequest_OkResult()
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"email", Constants.Username},
            };

            //Act
            var response = await client.PostAsync(
                "/api/auth/password/request",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Theory]
        [MemberData(nameof(ResetPasswordRequestData))]
        public async Task Post_ResetPasswordRequest_BadRequestObjectResult_WhenFalseCombinationsPassed(
            string email)
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"email", email},
            };

            //Act
            var response = await client.PostAsync(
                "/api/auth/password/request",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Theory]
        [MemberData(nameof(ResetPasswordData))]
        public async Task Post_ResetPassword_BadRequestObjectResult_WhenFalseCombinationsPassed(
            string userId, string token, string newPassword, string newPasswordConfirmation)
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"userId", userId},
                {"token", token},
                {"newPassword", newPassword},
                {"newPasswordConfirmation", newPasswordConfirmation},
            };

            //Act
            var response = await client.PostAsync(
                "/api/auth/password/reset",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Theory]
        [MemberData(nameof(ConfirmData))]
        public async Task Post_Confirm_BadRequestObjectResult_WhenFalseCombinationsPassed(
            string userId, string token)
        {
            //Arrange & Act
            var response = await client.GetAsync(
                $"/api/auth/confirm?userId={userId}&token={token}");

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.Redirect);
        }

        public static IEnumerable<object[]> ResetPasswordData =>
            new List<object[]>
            {
                new object[] { "invalduid", "invalidtoken", "newPassword", "newPasswordConfirmation" },
                new object[] { "", "invalidtoken", "newPassword", "newPasswordConfirmation" },
                new object[] { "invalduid", "", "newPassword", "newPasswordConfirmation" },
                new object[] { "invalduid", "invalidtoken", "", "newPasswordConfirmation" },
                new object[] { "invalduid", "invalidtoken", "newPassword", "" },
                new object[] { "", "", "", "" },
                new object[] { " ", " ", " ", " " },
            };

        public static IEnumerable<object[]> ConfirmData =>
            new List<object[]>
            {
                new object[] { Constants.Username, "token" },
                new object[] { "invalduid", "invalidtoken" },
                new object[] { "", "invalidtoken" },
                new object[] { "invalduid", "" },
                new object[] { "", "" },
                new object[] { " ", " " },
            };

        public static IEnumerable<object[]> LoginData =>
            new List<object[]>
            {
                new object[] { "tester", "1234", ErrorMessages.LoginWrongEmailOrPassword },
                new object[] { "tester@user.com", "1234512", ErrorMessages.LoginWrongEmailOrPassword },
                new object[] { Constants.Username, "1234", ErrorMessages.LoginWrongEmailOrPassword },
                new object[] { "tester", Constants.Password, ErrorMessages.LoginWrongEmailOrPassword },
            };

        public static IEnumerable<object[]> RegisterData =>
            new List<object[]>
            {
                new object[] { "tester", "1234", "", "NotAtype" },
                new object[] { "", "1234", "ValidName", "Business" },
                new object[] { "tester@test.com", "", "", "Personal" },
                new object[] { "tester@", "valid1234", "testValid", "Personal" },
                new object[] { "", "", "", "" },
                new object[] { "  ", "  ", " ", "  " },
            };

        public static IEnumerable<object[]> ResetPasswordRequestData =>
            new List<object[]>
            {
                new object[] { "tester" },
                new object[] { "tester@" },
                new object[] { "tester@gmail.com" },
                new object[] { "  " },
                new object[] { "" },
            };
    }
}
