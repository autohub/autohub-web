using System.Net.Http;
using System.Net.Http.Headers;
using api.test.integration.Fixtures;
using Microsoft.AspNetCore.Mvc.Testing;
using Xunit;

namespace api.test.integration.Endpoints
{
    public abstract class BaseTest : IClassFixture<CustomWebApplicationFactory<Startup>>
    {
        protected HttpClient client;
        protected CustomWebApplicationFactory<Startup> _factory;

        protected BaseTest(CustomWebApplicationFactory<Startup> factory)
        {
            _factory = factory;
            client = _factory.CreateClient(
                new WebApplicationFactoryClientOptions
                {
                    AllowAutoRedirect = false,
                }
            );
            client.DefaultRequestHeaders.AcceptLanguage.Add(new StringWithQualityHeaderValue("en"));
        }
    }
}
