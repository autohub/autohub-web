using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using api.Domain.Enums;
using api.Domain.Models.Responses.VehicleHistory;
using api.test.integration.Fixtures;
using api.test.integration.Helpers;
using FluentAssertions;
using Xunit;

namespace api.test.integration.Endpoints
{
    [TestCaseOrderer("api.test.integration.Helpers.PriorityOrderer", "api.test.integration")]
    public class FuelingVehicleHistoryTests : BaseAuthTest, IClassFixture<AuthenticationFixture>
    {
        public FuelingVehicleHistoryTests(CustomWebApplicationFactory<Startup> factory, AuthenticationFixture authFixture)
            : base(factory, authFixture) { }

        [Fact, TestPriority(0)]
        public async Task Post_CreatedAtRouteResult()
        {
            //Arrange
            const long milage = Constants.InitMilage + 10;
            var content = new Dictionary<string, string>()
            {
                {"vehicleId", await VehiclesFactory.GetCreatedVehicleId(client)},
                {"type", "Fueling"},
                {"currentMilage", milage.ToString()},
                {"totalPrice", "12"},
                {"category", "Gasoline"},
                {"liters", "121"},
                {"pricePerLiter", "2"},
            };

            //Act
            var response = await client.PostAsync(
                "/api/vehicles/history/fueling",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Fact, TestPriority(1)]
        public async Task Post_BadRequestObjectResult_WhenNotSameFuelType()
        {
            //Arrange
            const long milage = Constants.InitMilage + 10;
            const FuelType fuelType = FuelType.Diesel;

            var content = new Dictionary<string, string>()
            {
                {"vehicleId", await VehiclesFactory.GetCreatedVehicleId(client)},
                {"type", "Fueling"},
                {"currentMilage", milage.ToString()},
                {"totalPrice", "12"},
                {"category", fuelType.ToString()},
                {"liters", "121"},
                {"pricePerLiter", "2"},
            };

            //Act
            var response = await client.PostAsync(
                "/api/vehicles/history/fueling",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact, TestPriority(1)]
        public async Task Get_OkObjectResult()
        {
            //Arrange
            var id = await VehicleHistoriesFactory.GetVehicleHistoryId(client);

            //Act
            var response = await client.GetAsync($"/api/vehicles/history/fueling/{id}");
            var responseContent = await response.Content.ReadAsAsync<DetailsFuelingVehicleHistoryResponse>();

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            responseContent.VehicleHistory.Id.Should().NotBeEmpty();
            responseContent.VehicleHistory.CurrentMilage.Should().NotBe(0);
            responseContent.VehicleHistory.TotalPrice.Should().NotBe(0);
            responseContent.VehicleHistory.Liters.Should().NotBe(0);
        }

        [Fact, TestPriority(2)]
        public async Task Put_AcceptedResult()
        {
            //Arrange
            var id = await VehicleHistoriesFactory.GetVehicleHistoryId(client);
            var vehicleId = await VehiclesFactory.GetVehicleId(client);
            var content = new Dictionary<string, string>()
            {
                {"id", id},
                {"vehicleId", vehicleId},
                {"type", "Fueling"},
                {"currentMilage", "12232001"},
                {"totalPrice", "12"},
                {"category", "Gasoline"},
                {"liters", "121"},
                {"pricePerLiter", "2"},
            };

            //Act
            var response = await client.PutAsync(
                "/api/vehicles/history/fueling/",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Accepted);
        }

        [Fact, TestPriority(6)]
        public async Task Delete_OkResult()
        {
            //Arrange
            var id = await VehicleHistoriesFactory.GetVehicleHistoryId(client);

            //Arrange & Act
            var response = await client.DeleteAsync($"/api/vehicles/history/fueling/{id}");

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact, TestPriority(6)]
        public async Task Delete_UnauthorizedResult_ForNotOwningVehicleHistory()
        {
            //Arrange
            var id = new Guid().ToString();

            //Act
            var response = await client.DeleteAsync($"/api/vehicles/history/fueling/{id}");

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task Get_UnauthorizedResult_ForNotOwningVehicleHistory()
        {
            //Arrange
            var id = new Guid().ToString();

            // Arrange & Act
            var response = await client.GetAsync($"/api/vehicles/history/fueling/{id}");

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Theory, TestPriority(4)]
        [MemberData(nameof(PostVehicleHistoryData))]
        public async Task Post_BadRequestObjectResult_WhenModelStateIsInvalid(
            string type, string currentMilage, string totalPrice, string category,
            string liters, string pricePerLiter)
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"vehicleId", await VehiclesFactory.GetVehicleId(client)},
                {"type", type},
                {"currentMilage", currentMilage},
                {"totalPrice", totalPrice},
                {"category", category},
                {"liters", liters},
                {"pricePerLiter", pricePerLiter},
            };

            //Act
            var response = await client.PostAsync(
                "/api/vehicles/history/fueling",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Theory]
        [MemberData(nameof(UpdateVehicleHistoryData))]
        public async Task Put_BadRequestObjectResult_WhenModelStateIsInvalid(
            string type, string currentMilage, string totalPrice, string category,
            string liters, string pricePerLiter)
        {
            //Arrange
            var vehicleHistoryId = await VehicleHistoriesFactory.GetVehicleHistoryId(client);
            var content = new Dictionary<string, string>()
            {
                {"id", vehicleHistoryId},
                {"vehicleId", await VehiclesFactory.GetVehicleId(client)},
                {"type", type},
                {"currentMilage", currentMilage},
                {"totalPrice", totalPrice},
                {"category", category},
                {"liters", liters},
                {"pricePerLiter", pricePerLiter},
            };

            //Act
            var response = await client.PutAsync(
                "/api/vehicles/history/fueling/",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        public static IEnumerable<object[]> PostVehicleHistoryData => GetFalseVehicleHistoryData;

        public static IEnumerable<object[]> UpdateVehicleHistoryData => GetFalseVehicleHistoryData;

        public static IEnumerable<object[]> GetFalseVehicleHistoryData =>
            new List<object[]>
            {
                new object[] { "type", "currentMilage", "totalPrice", "category", "liters", "pricePerLiter" },
                new object[] { "", "123333", "123", "Diesel", "20", "12" },
                new object[] { "Fueling", "", "123", "Diesel", "20", "12" },
                new object[] { "Fueling", "1234", "", "Diesel", "20", "12" },
                new object[] { "Fueling", "1234", "123", "", "20", "12" },
                new object[] { "Fueling", "1234", "123", "Diesel", "", "12" },
                new object[] { "Fueling", "1234", "123", "Diesel", "20", "" },
                new object[] { "", "", "", "", "", "" },
                new object[] { " ", " ", " ", " ", " ", " " },
            };
    }
}
