using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using api.Application.Statistics.Queries;
using api.test.integration.Fixtures;
using FluentAssertions;
using Xunit;

namespace api.test.integration.Endpoints
{
    [TestCaseOrderer("api.test.integration.Helpers.PriorityOrderer", "api.test.integration")]
    public class StatisticsTests : BaseAuthTest, IClassFixture<AuthenticationFixture>
    {
        public StatisticsTests(CustomWebApplicationFactory<Startup> factory, AuthenticationFixture authFixture)
            : base(factory, authFixture) { }

        [Fact]
        public async Task Get_OkObjectResult()
        {
            //Arrange & Act
            var response = await client.GetAsync("/api/statistics");
            var responseContent = await response.Content.ReadAsAsync<StatisticsResponse>();

            //Assert                                                                                                        
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            responseContent.Should().BeOfType<StatisticsResponse>();
        }
    }
}