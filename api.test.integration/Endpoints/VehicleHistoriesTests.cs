using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using api.Domain.Models.Responses.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.test.integration.Fixtures;
using api.test.integration.Helpers;
using FluentAssertions;
using Xunit;

namespace api.test.integration.Endpoints
{
    [TestCaseOrderer("api.test.integration.Helpers.PriorityOrderer", "api.test.integration")]
    public class VehicleHistoriesTests : BaseAuthTest, IClassFixture<AuthenticationFixture>
    {
        public VehicleHistoriesTests(CustomWebApplicationFactory<Startup> factory, AuthenticationFixture authFixture)
            : base(factory, authFixture) { }

        [Fact]
        public async Task Get_OkObjectResult()
        {
            //Arrange
            var vehicleId = await VehiclesFactory.GetCreatedVehicleId(client);
            var vehicleHistoryId = await VehicleHistoriesFactory.GetCreatedVehicleHistoryId(client);

            //Act
            var response = await client.GetAsync($"/api/vehicles/history?vehicleId={vehicleId}");
            var responseContent = await response.Content.ReadAsAsync<PagedListVehicleHistoriesResponse>();

            //Assert                                                                                                        
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            responseContent.VehicleHistories.Should().BeOfType<List<GenericVehicleHistory>>();
            responseContent.VehicleHistories.Should().HaveCountGreaterThan(0);
            responseContent.VehicleHistories.ToList()[0].Id.Should().Equals(vehicleHistoryId);
        }
    }
}