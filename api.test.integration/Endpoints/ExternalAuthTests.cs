using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using api.test.integration.Fixtures;
using api.test.integration.Helpers;
using FluentAssertions;
using Xunit;

namespace api.test.integration.Endpoints
{
    public class ExternalAuthTests : BaseTest
    {
        public ExternalAuthTests(CustomWebApplicationFactory<Startup> factory) : base(factory) { }

        [Fact]
        public async Task FacebookLogin_Returns_OkResult_WithToken()
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                { "name", "Georgi Marokov" },
                { "email", "georgi.marokov@gmail.com" },
                { "id", "3025402804156318" },
                { "accessToken", "EAAH6U1iMZB5MBAATde" },
                { "userID", "3025402804156318" },
                { "expiresIn", "6357" },
                { "signedRequest", "6jV1DsI-KNHD4Ut6RRyA8hEqWgDFAKv-cGZs7N1Ja7E" },
                { "data_access_expiration_time", "1587215642" },
            };

            //Act
            var response = await client.PostAsync(
                "/api/externalauth/facebook",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task GoogleLogin_Returns_BadRequest()
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                { "idToken", Guid.NewGuid().ToString() },
            };

            //Act
            var response = await client.PostAsync(
                "/api/externalauth/google",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }
    }
}
