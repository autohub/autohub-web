using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using api.Domain.Models.Responses.Vehicle;
using api.Common;
using api.test.integration.Fixtures;
using api.test.integration.Helpers;
using FluentAssertions;
using Xunit;
using api.Domain;

namespace api.test.integration.Endpoints
{
    [TestCaseOrderer("api.test.integration.Helpers.PriorityOrderer", "api.test.integration")]
    public class VehicleTests : BaseAuthTest, IClassFixture<AuthenticationFixture>
    {
        public VehicleTests(CustomWebApplicationFactory<Startup> factory, AuthenticationFixture authFixture)
            : base(factory, authFixture) { }

        [Fact, TestPriority(0)]
        public async Task Post_CreatedAtRouteResult()
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"make", "Testla"},
                {"model", "TModel"},
                {"milage", Constants.InitMilage.ToString()},
                {"ProductionDate", "12/12/91"},
                {"engineSize", "2000"},
                {"horsePower", "121"},
                {"fuelType", "Gasoline"}
            };

            //Act
            var response = await client.PostAsync(
                "/api/vehicles/",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Created);
        }

        [Fact, TestPriority(1)]
        public async Task Get_AllVehicles_OkObjectResult()
        {
            //Arrange & Act
            var response = await client.GetAsync("/api/vehicles/");
            var responseContent = await response.Content.ReadAsAsync<List<ListVehicleResponse>>();

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            responseContent.Should().BeOfType<List<ListVehicleResponse>>();
            responseContent.Should().HaveCountGreaterThan(0);
        }

        [Fact, TestPriority(2)]
        public async Task Get_OkObjectResult()
        {
            //Arrange
            var vehicleId = await VehiclesFactory.GetVehicleId(client);

            //Act
            var response = await client.GetAsync($"/api/vehicles/{vehicleId}");
            var responseContent = await response.Content.ReadAsAsync<DetailsVehicleResponse>();

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
            responseContent.Id.Should().NotBeEmpty();
            responseContent.Make.Should().NotBeNullOrEmpty();
            responseContent.Model.Should().NotBeNullOrEmpty();
            responseContent.Milage.Should().HaveValue();
        }

        [Fact, TestPriority(2)]
        public async Task Put_AcceptedResult()
        {
            //Arrange
            var vehicleId = await VehiclesFactory.GetVehicleId(client);
            const long milage = Constants.InitMilage + 100;
            var content = new Dictionary<string, string>()
            {
                {"id", vehicleId},
                {"make", "Testla"},
                {"model", "TModel"},
                {"milage", milage.ToString()},
                {"ProductionDate", "12/12/91"},
                {"engineSize", "2000"},
                {"horsePower", "121"},
                {"fuelType", "Diesel"}
            };

            //Act
            var response = await client.PutAsync(
                "/api/vehicles",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.Accepted);
        }

        [Fact, TestPriority(2)]
        public async Task Put_BadRequestObjectResult_WhenMilageIsLower()
        {
            //Arrange
            var vehicleId = await VehiclesFactory.GetVehicleId(client);
            const long milage = Constants.InitMilage - 10;
            var content = new Dictionary<string, string>()
            {
                {"id", vehicleId},
                {"make", "Testla"},
                {"model", "TModel"},
                {"milage", milage.ToString()},
                {"ProductionDate", "12/12/91"},
                {"engineSize", "2000"},
                {"horsePower", "121"},
                {"fuelType", "Diesel"}
            };

            //Act
            var response = await client.PutAsync(
                "/api/vehicles",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Fact, TestPriority(3)]
        public async Task Delete_OkResult()
        {
            //Arrange
            var vehicleId = await VehiclesFactory.GetVehicleId(client);

            //Arrange & Act
            var response = await client.DeleteAsync($"/api/vehicles/{vehicleId}");

            //Assert
            response.EnsureSuccessStatusCode();
            response.StatusCode.Should().Be(HttpStatusCode.OK);
        }

        [Fact]
        public async Task Delete_UnauthorizedResult_ForNotOwningVehicle()
        {
            //Arrange
            var vehicleId = new Guid().ToString();

            //Act
            var response = await client.DeleteAsync($"/api/vehicles/{vehicleId}");

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Fact]
        public async Task Get_UnauthorizedResult_ForNotOwningVehicle()
        {
            //Arrange
            var vehicleId = new Guid().ToString();

            // Arrange & Act
            var response = await client.GetAsync($"/api/vehicles/{vehicleId}");

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.Unauthorized);
        }

        [Theory]
        [MemberData(nameof(PostVehicleData))]
        public async Task Post_BadRequestObjectResult_WhenModelStateIsInvalid(
            string make, string model, string milage, string ProductionDate, string engineSize,
            string horsePower, string fuelType)
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"make", make},
                {"model", model},
                {"milage", milage},
                {"ProductionDate", ProductionDate},
                {"engineSize", engineSize},
                {"horsePower", horsePower},
                {"fuelType", fuelType}
            };

            //Act
            var response = await client.PostAsync(
                "/api/vehicles/",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        [Theory]
        [MemberData(nameof(UpdateVehicleData))]
        public async Task Put_BadRequestObjectResult_WhenModelStateIsInvalid(
            string id, string make, string model, string milage,
            string ProductionDate, string engineSize, string horsePower, string fuelType)
        {
            //Arrange
            var content = new Dictionary<string, string>()
            {
                {"id", id},
                {"make", make},
                {"model", model},
                {"milage", milage},
                {"ProductionDate", ProductionDate},
                {"engineSize", engineSize},
                {"horsePower", horsePower},
                {"fuelType", fuelType}
            };

            //Act
            var response = await client.PutAsync(
                "/api/vehicles/",
                Utilities.CreateJsonHttpContent(content));

            //Assert
            response.StatusCode.Should().Be(HttpStatusCode.BadRequest);
        }

        public static IEnumerable<object[]> PostVehicleData =>
            new List<object[]>
            {
                new object[] { "make", "model", "milage", "prodDate", "engineSize", "horsePower", "fuelType" },
                new object[] { "", "", "123333", "12/12/18", "2000", "122", "Gas" },
                new object[] { "  ", "  ", "123333", "12/12/18", "2000", "122", "Gasoline" },
                new object[] { "validMake", "validModel", "123333", "12/12/18", long.MaxValue, long.MaxValue, "Diesel" },
                new object[] { "validMake", "validModel", "123333", "12/12/18", long.MaxValue, long.MaxValue, "" },
                new object[] {
                    RandomGenerator.RandomString(ModelConstants.MaxStringLength + 2),
                    RandomGenerator.RandomString(ModelConstants.MaxStringLength + 2),
                    "123333", "12/12/18", 102, 14, "Diesel" },
                new object[] { " ", " ", " ", " ", " ", " ", " " },
                new object[] { "", "", "", "", "", "", "" },
            };

        public static IEnumerable<object[]> UpdateVehicleData =>
         new List<object[]>
            {
                new object[] { "id", "make", "model", "milage", "prodDate", "engineSize", "horsePower", "fuelType" },
                new object[] { " ", "make", "model", "milage", "prodDate", "engineSize", "horsePower", "fuelType" },
                new object[] { "id", "", "", "123333", "12/12/18", "2000", "122", "Gas" },
                new object[] { "id", "  ", "  ", "123333", "12/12/18", "2000", "122", "Gasoline" },
                new object[] { "valid", "validMake", "validModel", "123333", "12/12/18", long.MaxValue, long.MaxValue, "Diesel" },
                new object[] { "validId", "validMake", "validModel", "123333", "12/12/18", long.MaxValue, long.MaxValue, "" },
                new object[] {
                    RandomGenerator.RandomString(ModelConstants.MaxStringLength + 2),
                    RandomGenerator.RandomString(ModelConstants.MaxStringLength + 2),
                    RandomGenerator.RandomString(ModelConstants.MaxStringLength + 2),
                    "123333", "12/12/18", 102, 14, "Diesel" },
                new object[] {" ", " ", " ", " ", " ", " ", " ", " " },
                new object[] {"", "", "", "", "", "", "", "" },
            };
    }
}
