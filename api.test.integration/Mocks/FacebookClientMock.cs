using System;
using System.Net.Http;
using System.Threading.Tasks;
using api.Infrastructure.Apis.Facebook;
using api.Services.Configs;
using api.Services.Users;
using Microsoft.Extensions.Options;

namespace api.test.integration.Mocks
{
    public class FacebookClientMock : IFacebookClient
    {
        public FacebookClientMock(
            HttpClient httpClient,
            IOptions<AuthClientOptions> options,
            IConfigurationManager configurationManager)
        {
            httpClient.BaseAddress = new Uri("https://test.com");
            httpClient.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory");
            _ = configurationManager;
            _ = options.Value;
            _ = httpClient;
        }

        public Task<FacebookAppAccessToken> GetAppAccessToken() =>
            Task.FromResult(new FacebookAppAccessToken()
            {
                AccessToken = string.Empty,
                TokenType = string.Empty
            });

        public Task<FacebookUserModel> GetUserData(string accessToken) =>
            Task.FromResult(new FacebookUserModel()
            {
                Id = string.Empty,
                Email = "georgi.marokov@gmail.com",
                Name = "Georgi Marokov",
                FirstName = "Georgi",
                LastName = "Marokov"
            });

        public Task<FacebookUserAccessTokenValidation> GetUserTokenValidation(string userAccessToken, string appAccessToken) =>
            Task.FromResult(new FacebookUserAccessTokenValidation()
            {
                IsValid = true,
            });
    }
}
