using System.Threading.Tasks;
using api.Domain.Models;
using api.Infrastructure.Email;

namespace api.test.integration.Mocks
{
    public class EmailSenderMock : IEmailSender
    {
        public Task SendEmailAsync(EmailModel model)
        {
            return Task.CompletedTask;
        }

        public Task SendTemplatedEmail<T>(T model) where T : EmailModel
        {
            return Task.CompletedTask;
        }
    }
}