using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Models.Requests.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Services.VehicleHistories;
using api.Services.Vehicles;
using api.test.unit.Helpers;
using Moq;
using Xunit;

namespace api.test.unit.Services
{
    public class TaxVehicleHistoryServiceTests : BaseServiceTests
    {
        private readonly TaxVehicleHistoryService _service;
        private readonly Mock<IVehicleService> _vehicleServiceMock;

        public TaxVehicleHistoryServiceTests()
        {
            _vehicleServiceMock = new Mock<IVehicleService>();

            _service = new TaxVehicleHistoryService(
                _data.Object,
                _mapper,
                _vehicleServiceMock.Object
            );
        }

        [Fact]
        public async Task GetTaxVehicleHistory_Returns_DetailsTaxVehicleHistoryResponse()
        {
            //Arrange
            var id = Guid.NewGuid();
            var vehicleHistory = new TaxVehicleHistory()
            {
                Id = id
            };

            _data.Setup(x => x.TaxVehicleHistories.All())
                .Returns(new List<TaxVehicleHistory>() { vehicleHistory }
                    .AsQueryable());

            //Act
            var result = await _service.GetTaxVehicleHistoryAsync(id);

            //Assert
            Assert.IsType<TaxVehicleHistoryModel>(result);
        }

        [Fact]
        public async Task GetTaxVehicleHistory_Returns_Null()
        {
            //Arrange
            _data.Setup(x => x.TaxVehicleHistories.All())
                .Returns(new List<TaxVehicleHistory>() { new TaxVehicleHistory() }
                    .AsQueryable());

            //Act
            var result = await _service.GetTaxVehicleHistoryAsync(Guid.NewGuid());

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task UpdateTaxVehicleHistory_Throws()
        {
            //Arrange
            var vehicleHistory = new UpsertTaxVehicleHistoryRequest();

            _data.Setup(x => x.TaxVehicleHistories.All())
                .Returns(new List<TaxVehicleHistory>()
                    .AsQueryable());

            //Act & Assert
            await Assert.ThrowsAnyAsync<Exception>(async () =>
                await _service.UpdateTaxVehicleHistoryAsync(vehicleHistory));
        }

        [Fact]
        public async Task UpdateTaxVehicleHistory_NotThrows()
        {
            //Arrange
            var id = Guid.NewGuid();
            var taxVehicleHistory = new UpsertTaxVehicleHistoryRequest() { Id = id };
            var vehicleHistory = new TaxVehicleHistory() { Id = id };

            _data.Setup(x => x.TaxVehicleHistories.GetById(It.IsAny<Guid>()))
                .Returns(vehicleHistory);

            _vehicleServiceMock.Setup(x => x.UpdateMilageAsync(It.IsAny<Guid>(), It.IsAny<long>()))
               .Returns(Task.CompletedTask);

            //Act & Assert
            await _service.UpdateTaxVehicleHistoryAsync(taxVehicleHistory);
        }

        [Theory]
        [MemberData(nameof(ValidUpdateVehicleHistoriesWithExpireInfo))]
        public async Task UpdateTaxVehicleHistory_WithValidRemindableVehicleHistory(
            UpsertTaxVehicleHistoryRequest vehicleHistory)
        {
            //Arrange
            var id = Guid.NewGuid();
            var expectedVehicleHistory = new TaxVehicleHistory() { Id = id };

            _data.Setup(x => x.TaxVehicleHistories.GetById(It.IsAny<Guid>()))
                .Returns(expectedVehicleHistory);

            _vehicleServiceMock.Setup(x => x.UpdateMilageAsync(It.IsAny<Guid>(), It.IsAny<long>()))
               .Returns(Task.CompletedTask);

            //Act & Assert
            await _service.UpdateTaxVehicleHistoryAsync(vehicleHistory);
        }

        [Fact]
        public async Task CreateTaxVehicleHistory_NotThrow()
        {
            //Arrange
            var vehicleHistory = new UpsertTaxVehicleHistoryRequest();
            _data.Setup(x => x.TaxVehicleHistories.All())
                .Returns(new List<TaxVehicleHistory>()
                    .AsQueryable());

            //Act
            var result = await _service.CreateTaxVehicleHistoryAsync(vehicleHistory);

            //Assert
            Assert.True(result == Guid.Empty);
        }

        [Theory]
        [MemberData(nameof(ValidVehicleHistories))]
        public async Task CreateTaxVehicleHistory_WithValidRemindableVehicleHistory(
            UpsertTaxVehicleHistoryRequest vehicleHistory)
        {
            //Arrange
            _data.Setup(x => x.TaxVehicleHistories.Add(It.IsAny<TaxVehicleHistory>()))
                .Verifiable();

            //Act
            var result = await _service.CreateTaxVehicleHistoryAsync(vehicleHistory);

            //Assert
            Assert.True(result == Guid.Empty);
        }

        [Fact]
        public async Task DeleteTaxVehicleHistory_NotThrow()
        {
            //Arrange
            var vehicleHistory = new UpsertTaxVehicleHistoryRequest();

            _data.Setup(x => x.TaxVehicleHistories.All())
                .Returns(new List<TaxVehicleHistory>()
                    .AsQueryable());

            //Act & Assert
            await _service.DeleteTaxVehicleHistoryAsync(It.IsAny<Guid>());
        }

        public static IEnumerable<object[]> ValidVehicleHistories =>
           VehicleHistoryFactory.GetValidTaxRemindableVehicleHistories();

        public static IEnumerable<object[]> ValidUpdateVehicleHistoriesWithExpireInfo =>
           VehicleHistoryFactory.GetValidUpdateTaxRemindableVehicleHistories();
    }
}
