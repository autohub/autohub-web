using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using api.Domain.Entities;
using api.Domain.Enums;
using api.Domain.Models;
using api.Domain.Models.VehicleHistory;
using api.Domain.Models.Vehicles;
using api.Infrastructure.Email;
using api.Infrastructure.Hangfire;
using api.Services.Configs;
using api.Services.Users;
using api.Services.VehicleHistories;
using api.test.unit.Fixtures;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace api.test.unit.Services
{
    public class CronServiceTests : BaseServiceTests, IClassFixture<TestFixture>
    {
        private readonly CronService _service;
        private readonly Mock<IUserService> _userServiceMock = new Mock<IUserService>();
        private readonly Mock<IEmailSender> _emailSenderMock = new Mock<IEmailSender>();
        private readonly Mock<IRemindableVehicleHistoryService> _reminderServiceMock = new Mock<IRemindableVehicleHistoryService>();
        private readonly Mock<IConfigurationManager> _configurationMock = new Mock<IConfigurationManager>();
        private readonly Mock<ILogger<CronService>> _loggerMock = new Mock<ILogger<CronService>>();

        public CronServiceTests(TestFixture fixture)
        {
            _service = new CronService(
                _emailSenderMock.Object,
                _userServiceMock.Object,
                _loggerMock.Object,
                _reminderServiceMock.Object,
                _configurationMock.Object,
                fixture.Localizer);
        }

        [Fact]
        public async Task EmailUsersWithIncomingVehicleService_Should_UpdateOccurredVehicleHistories()
        {
            //Arrange
            var data = new List<RemindableVehicleHistoryModel>()
            {
                new RemindableVehicleHistoryModel()
                {
                    Id = Guid.NewGuid(),
                    Type = nameof(VehicleHistoryType.Tax),
                    Category = nameof(TaxVehicleHistoryCategory.Duty),
                    IsRemindableByDate = true,
                    ExpiresOn = DateTime.UtcNow.AddDays(20),
                    DaysRange = 10,
                    IsRemindableByMilage = true,
                    ExpiresOnMilage  = 1000,
                    MilageRange = 100,
                    Vehicle = new VehicleModel()
                    {
                        Milage = 910
                    }
                },
                new RemindableVehicleHistoryModel()
                {
                    Id = Guid.NewGuid(),
                    Type = nameof(VehicleHistoryType.Repair),
                    Category = nameof(RepairVehicleHistoryCategory.BodyAndChassis),
                    IsRemindableByDate = true,
                    ExpiresOn = DateTime.UtcNow.AddDays(20),
                    DaysRange = 10,
                    Vehicle = new VehicleModel()
                    {
                        Milage = 910
                    }
                }
            };

            _reminderServiceMock
                .Setup(x => x.GetIncomingVehicleHistories())
                .ReturnsAsync(data);

            _userServiceMock
                .Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(new ApplicationUser() { Id = Guid.NewGuid().ToString() });

            _configurationMock
                .Setup(x => x.TryGetValue<string>(It.IsAny<string>()))
                .ReturnsAsync("https://localhost:5001");

            _emailSenderMock
                .Setup(x => x.SendTemplatedEmail(It.IsAny<EmailModel>()))
                .Returns(Task.CompletedTask);

            //Act & Assert
            await _service.EmailUsersWithIncomingVehicleService();
        }
    }
}
