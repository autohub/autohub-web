using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Services.VehicleHistories;
using api.Services.Vehicles;
using api.test.unit.Helpers;
using Moq;
using Xunit;

namespace api.test.unit.Services
{
    public class RepairVehicleHistoryServiceTests : BaseServiceTests
    {
        private readonly RepairVehicleHistoryService _service;
        private readonly Mock<IVehicleService> _vehicleServiceMock;

        public RepairVehicleHistoryServiceTests()
        {
            _vehicleServiceMock = new Mock<IVehicleService>();
            _service = new RepairVehicleHistoryService(
                _data.Object,
                _mapper,
                _vehicleServiceMock.Object
            );
        }

        [Fact]
        public async Task GetRepairVehicleHistory_Returns_DetailsRepairVehicleHistoryResponse()
        {
            //Arrange
            var id = It.IsAny<Guid>();
            var vehicleHistory = new RepairVehicleHistory()
            {
                Id = id
            };
            _data.Setup(x => x.RepairVehicleHistories.All())
                .Returns(new List<RepairVehicleHistory>() { vehicleHistory }
                    .AsQueryable());

            //Act
            var result = await _service.GetRepairVehicleHistoryAsync(id);

            //Assert
            Assert.IsType<RepairVehicleHistoryModel>(result);
        }

        [Fact]
        public async Task GetRepairVehicleHistory_Returns_Null()
        {
            //Arrange
            _data.Setup(x => x.RepairVehicleHistories.All())
                .Returns(new List<RepairVehicleHistory>() { new RepairVehicleHistory() }
                    .AsQueryable());

            //Act
            var result = await _service.GetRepairVehicleHistoryAsync(Guid.NewGuid());

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task UpdateRepairVehicleHistory_Throws()
        {
            //Arrange
            var vehicleHistory = new UpsertRepairVehicleHistoryRequest();

            _data.Setup(x => x.RepairVehicleHistories.All())
                .Returns(new List<RepairVehicleHistory>()
                    .AsQueryable());

            //Act & Assert
            await Assert.ThrowsAnyAsync<Exception>(async () =>
                await _service.UpdateRepairVehicleHistoryAsync(vehicleHistory))
            ;
        }

        [Fact]
        public async Task UpdateRepairVehicleHistory_NotThrows()
        {
            //Arrange
            var id = Guid.NewGuid();
            var repairVehicleHistory = new UpsertRepairVehicleHistoryRequest() { Id = id };
            var vehicleHistory = new RepairVehicleHistory() { Id = id };

            _data.Setup(x => x.RepairVehicleHistories.GetById(It.IsAny<Guid>()))
                .Returns(vehicleHistory);

            _vehicleServiceMock.Setup(x => x.UpdateMilageAsync(It.IsAny<Guid>(), It.IsAny<long>()))
               .Returns(Task.CompletedTask);

            //Act & Assert
            await _service.UpdateRepairVehicleHistoryAsync(repairVehicleHistory);
        }

        [Theory]
        [MemberData(nameof(GetValidUpdateVehicleHistoriesWithExpireInfo))]
        public async Task UpdateRepairVehicleHistory_WithValidRemindableVehicleHistory(
            UpsertRepairVehicleHistoryRequest vehicleHistory)
        {
            //Arrange
            var id = Guid.NewGuid();
            var expectedVehicleHistory = new RepairVehicleHistory() { Id = id };

            _data.Setup(x => x.RepairVehicleHistories.GetById(It.IsAny<Guid>()))
                .Returns(expectedVehicleHistory);

            _vehicleServiceMock.Setup(x => x.UpdateMilageAsync(It.IsAny<Guid>(), It.IsAny<long>()))
               .Returns(Task.CompletedTask);

            //Act & Assert
            await _service.UpdateRepairVehicleHistoryAsync(vehicleHistory);
        }

        [Fact]
        public async Task CreateRepairVehicleHistory_NotThrow()
        {
            //Arrange
            var vehicleHistory = new UpsertRepairVehicleHistoryRequest();

            _data.Setup(x => x.RepairVehicleHistories.All())
                .Returns(new List<RepairVehicleHistory>()
                    .AsQueryable());

            //Act
            var result = await _service.CreateRepairVehicleHistoryAsync(vehicleHistory);

            //Assert
            Assert.True(result == Guid.Empty);
        }

        [Theory]
        [MemberData(nameof(GetValidVehicleHistories))]
        public async Task CreateRepairVehicleHistory_WithValidRemindableVehicleHistory(
            UpsertRepairVehicleHistoryRequest vehicleHistory)
        {
            //Arrange
            _data.Setup(x => x.RepairVehicleHistories.Add(It.IsAny<RepairVehicleHistory>()))
                .Verifiable();

            //Act
            var result = await _service.CreateRepairVehicleHistoryAsync(vehicleHistory);

            //Assert
            Assert.True(result == Guid.Empty);
        }

        [Fact]
        public async Task DeleteRepairVehicleHistory_NotThrow()
        {
            //Arrange
            var vehicleHistory = new UpsertRepairVehicleHistoryRequest();

            _data.Setup(x => x.RepairVehicleHistories.All())
                .Returns(new List<RepairVehicleHistory>()
                    .AsQueryable());

            //Act & Assert
            await _service.DeleteRepairVehicleHistoryAsync(It.IsAny<Guid>());
        }

        public static IEnumerable<object[]> GetValidVehicleHistories
            => VehicleHistoryFactory.GetValidRepairRemindableVehicleHistories();

        public static IEnumerable<object[]> GetValidUpdateVehicleHistoriesWithExpireInfo
            => VehicleHistoryFactory.GetValidUpdateRepairRemindableVehicleHistories();
    }
}
