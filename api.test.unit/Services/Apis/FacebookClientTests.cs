using System;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using api.Infrastructure.Apis.Facebook;
using api.Services.Users;
using Microsoft.Extensions.Options;
using Moq;
using Moq.Protected;
using Newtonsoft.Json;
using Xunit;

namespace api.test.unit.Services.Apis
{
    public class FacebookClientTests : BaseServiceTests
    {
        private readonly Mock<IOptions<AuthClientOptions>> _facebookClientOptionsMock = new Mock<IOptions<AuthClientOptions>>();

        [Fact]
        public async Task GetAppAccessToken_Returns_FacebookAppAccessToken()
        {
            //Arrange
            var facebookClientOptions = new AuthClientOptions()
            {
                FacebookAppId = string.Empty,
                FacebookAppSecret = string.Empty
            };

            var _facebookClientOptionsMock = new Mock<IOptions<AuthClientOptions>>();
            _facebookClientOptionsMock
                .Setup(x => x.Value)
                .Returns(facebookClientOptions);

            dynamic response = new ExpandoObject();
            response.access_token = "|556710851574675|ca30RmZV-HLLpkA-9n88AA7u6mw";
            response.token_type = "bearer";
            var responseContent = JsonConvert.SerializeObject(response);

            var httpClient = GetHttpClientMock(
                HttpStatusCode.OK,
                responseContent
            );
            var client = new FacebookClient(httpClient, _facebookClientOptionsMock.Object);

            //Act
            var result = await client.GetAppAccessToken();

            //Assert
            Assert.True(result.AccessToken != null);
            Assert.True(result.TokenType != null);
        }

        [Fact]
        public async Task GetUserTokenValidation_Returns_FacebookUserAccessTokenValidation()
        {
            //Arrange
            dynamic response = new ExpandoObject();
            response.data = new ExpandoObject();
            response.data.app_id = "556710851574675";
            response.data.type = "User";
            response.data.application = "AutoHub";
            response.data.data_access_expires_at = 1586820367;
            response.data.expires_at = 1579050000;
            response.data.is_valid = true;
            response.data.user_id = 3025402804156318;
            var responseContent = JsonConvert.SerializeObject(response);

            var httpClient = GetHttpClientMock(
                HttpStatusCode.OK,
                responseContent
            );
            var client = new FacebookClient(httpClient, _facebookClientOptionsMock.Object);

            //Act
            FacebookUserAccessTokenValidation result = await client.GetUserTokenValidation(It.IsAny<string>(), It.IsAny<string>());

            //Assert
            Assert.True(result.UserId != null);
            Assert.True(result.DataAccessExpiresAt != 0);
            Assert.True(result.ExpiresAt != 0);
            Assert.True(result.Type != null);
        }

        [Fact]
        public async Task GetUserData_Returns_FacebookUserModel()
        {
            //Arrange
            dynamic response = new ExpandoObject();
            response.id = "556710851574675";
            response.email = "georgi.marokov@gmail.com";
            response.first_name = "Georgi";
            response.last_name = "Marokov";
            response.name = "Georgi Marokov";

            var responseContent = JsonConvert.SerializeObject(response);

            var httpClient = GetHttpClientMock(
                HttpStatusCode.OK,
                responseContent
            );
            var client = new FacebookClient(httpClient, _facebookClientOptionsMock.Object);

            //Act
            FacebookUserModel result = await client.GetUserData(It.IsAny<string>());

            //Assert
            Assert.True(result.Id != null);
            Assert.True(result.Email != null);
            Assert.True(result.FirstName != null);
            Assert.True(result.LastName != null);
            Assert.True(result.Name != null);
        }

        private HttpClient GetHttpClientMock(HttpStatusCode statusCode, string content)
        {
            var handlerMock = new Mock<HttpMessageHandler>(MockBehavior.Strict);

            handlerMock
               .Protected()
               .Setup<Task<HttpResponseMessage>>(
                  "SendAsync",
                  ItExpr.IsAny<HttpRequestMessage>(),
                  ItExpr.IsAny<CancellationToken>()
               )
               .ReturnsAsync(new HttpResponseMessage()
               {
                   StatusCode = statusCode,
                   Content = new StringContent(content),
               })
               .Verifiable();

            return new HttpClient(handlerMock.Object)
            {
                BaseAddress = new Uri("https://test.com/")
            };
        }
    }
}
