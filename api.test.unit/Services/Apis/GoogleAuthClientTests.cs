using System.Threading.Tasks;
using api.Domain.Models.Requests.Auth;
using api.Infrastructure.Apis.Google;
using api.Services.Users;
using Google.Apis.Auth;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;

namespace api.test.unit.Services.Apis
{
    public class GoogleAuthClientTests : BaseServiceTests
    {
        [Fact]
        public async Task GetUserAccount_Returns_NullPayloadObject_WhenNotValid()
        {
            //Arrange
            const string idToken = "eyJhbGciOiJSUzI1NiIsImtpZCI6ImJhZDM5NzU0ZGYzYjI0M2YwNDI4YmU5YzUzNjFkYmE1YjEwZmZjYzAiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJhY2NvdW50cy5nb29nbGUuY29tIiwiYXpwIjoiMTEyMTQ0NjYyOTk0LXVtdmVyZmNlcG44aTlxMjZxcXE3ZW92dWxkajBqM2NhLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwiYXVkIjoiMTEyMTQ0NjYyOTk0LXVtdmVyZmNlcG44aTlxMjZxcXE3ZW92dWxkajBqM2NhLmFwcHMuZ29vZ2xldXNlcmNvbnRlbnQuY29tIiwic3ViIjoiMTAwOTkxNzM4NzQxNTk2MDMxMjYwIiwiZW1haWwiOiJnZW9yZ2kubWFyb2tvdi51cEBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6dHJ1ZSwiYXRfaGFzaCI6IkQ0dUVMODNkZk00YUJDUWRfTTl4N0EiLCJuYW1lIjoiR2VvcmdpIE1hcm9rb3YiLCJwaWN0dXJlIjoiaHR0cHM6Ly9saDMuZ29vZ2xldXNlcmNvbnRlbnQuY29tLy1xc3hOS0xtcDdoMC9BQUFBQUFBQUFBSS9BQUFBQUFBQUFBQS9BQ0hpM3JmRHJNMEE3QU9OdTRNb2RfckNYYVB2NTZZZ1VnL3M5Ni1jL3Bob3RvLmpwZyIsImdpdmVuX25hbWUiOiJHZW9yZ2kiLCJmYW1pbHlfbmFtZSI6Ik1hcm9rb3YiLCJsb2NhbGUiOiJlbiIsImlhdCI6MTU3OTcxMzAyNiwiZXhwIjoxNTc5NzE2NjI2LCJqdGkiOiI2ZjI3NThjMTIzZGZiNjdiYjVlMGVjMTNjY2RjMDA4YWI0MGE5YjcwIn0.Ml8TkExeepXN940ajnmJwTEO13a2v_iX45b_FKrdE-O5M0Yv4FvixIYxuVxbJ8qUn7oocwgIk7w8TFZ8rp_nabiFxao_9Xym-T884IliHE3Z9f43JyEw_5BYUYCl2cavVgi83_s-r1yHXt6qaZqDHOxk0aXprQfQPPZKhG2ysHbkUpiGqou3RiQ2EJ0Cn0_4-dDchQEjUrIsHg0QoGwIOdvda2RpF-_3cRtVVnZM8kfuTQTDdUuuhsce846sd1zykw3-9Z4UML3JJWICUkQ3E1uAGKGVhCDviAHpFgpJ4c0cLT5dK5RfcmLYMdln6mxpazGLlBI8aYBeQx2ElZlM7A";
            var authClientOptions = new AuthClientOptions() { GoogleAppId = string.Empty };
            var authClientOptionsMock = new Mock<IOptions<AuthClientOptions>>();
            authClientOptionsMock
                .Setup(x => x.Value)
                .Returns(authClientOptions);

            var service = new GoogleAuthClient(authClientOptionsMock.Object);
            var request = new GoogleLoginRequest() { IdToken = idToken };

            //Act & Assert
            await Assert.ThrowsAnyAsync<InvalidJwtException>(async () =>
                await service.GetUserAccount(request));
        }
    }
}
