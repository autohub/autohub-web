using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Domain.Entities;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Enums;
using api.Domain.Exceptions;
using api.Domain.Models.VehicleHistory;
using api.Services.Validation;
using api.Services.Validation.Decorators;
using api.Services.Validation.Validators;
using api.Services.VehicleHistories;
using api.Services.Vehicles;
using Moq;
using Xunit;

namespace api.test.unit.Services.Validators
{
    public class FuelingTypeValidatorTests
        : BaseServiceTests
    {
        private readonly FuelingVehicleHistoryService _service;
        private readonly Mock<IVehicleService> _vehicleServiceMock = new Mock<IVehicleService>();

        public FuelingTypeValidatorTests()
        {
            _service = new FuelingVehicleHistoryService(
                _data.Object,
                _mapper,
                _vehicleServiceMock.Object
            );
        }

        [Theory]
        [MemberData(nameof(GetPositiveFuelingTypes))]
        public async Task CreateFuelingVehicleHistory_ValidateFuelingType_Returns_VehicleHistoryId(
           string fuelType, VehicleFuelType vehicleFuelType)
        {
            //Arrange
            var vehicleHistory = new UpsertFuelingVehicleHistoryRequest() { Category = fuelType };
            var validators = new List<IValidator<BaseFuelingVehicleHistoryRequest>>()
            {
                new FuelingTypeValidator(_data.Object)
            };

            var decoratedService = new ValidationFuelingVehicleHistoryServiceDecorator(_service, validators);

            _data.Setup(x => x.FuelingVehicleHistories.All())
                .Returns(new List<FuelingVehicleHistory>() { new FuelingVehicleHistory() { Id = Guid.NewGuid() } }
                    .AsQueryable());

            _data.Setup(x => x.Vehicles.GetById(It.IsAny<Guid>()))
                .Returns(new Vehicle() { FuelType = vehicleFuelType });

            //Act
            var result = await decoratedService.CreateFuelingVehicleHistoryAsync(vehicleHistory);

            //Assert
            Assert.True(result == Guid.Empty);
        }

        [Theory]
        [MemberData(nameof(GetNegativeFuelingTypes))]
        public async Task CreateFuelingVehicleHistory_ValidateFuelingType_ThrowsDomainException(
           string fuelType, VehicleFuelType vehicleFuelType)
        {
            //Arrange
            var vehicleHistory = new UpsertFuelingVehicleHistoryRequest() { Category = fuelType };
            var validators = new List<IValidator<BaseFuelingVehicleHistoryRequest>>()
            {
                new FuelingTypeValidator(_data.Object)
            };

            var decoratedService = new ValidationFuelingVehicleHistoryServiceDecorator(_service, validators);

            _data.Setup(x => x.Vehicles.GetById(It.IsAny<Guid>()))
                .Returns(new Vehicle() { FuelType = vehicleFuelType });

            //Act & Assert
            await Assert.ThrowsAsync<DomainException>(() =>
                decoratedService.CreateFuelingVehicleHistoryAsync(vehicleHistory));
        }

        [Theory]
        [MemberData(nameof(GetPositiveFuelingTypes))]
        public async Task UpdateFuelingVehicleHistory_ValidateFuelingType_NotThrows(
           string fuelType, VehicleFuelType vehicleFuelType)
        {
            //Arrange
            var vehicleHistory = new UpsertFuelingVehicleHistoryRequest() { Category = fuelType };
            var validators = new List<IValidator<BaseFuelingVehicleHistoryRequest>>()
            {
                new FuelingTypeValidator(_data.Object)
            };

            var decoratedService = new ValidationFuelingVehicleHistoryServiceDecorator(_service, validators);

            _data.Setup(x => x.FuelingVehicleHistories.GetById(It.IsAny<Guid?>()))
                .Returns(new FuelingVehicleHistory());

            _data.Setup(x => x.Vehicles.GetById(It.IsAny<Guid>()))
                .Returns(new Vehicle() { FuelType = vehicleFuelType });

            //Act & Assert
            await decoratedService.UpdateFuelingVehicleHistoryAsync(vehicleHistory);
        }

        [Theory]
        [MemberData(nameof(GetNegativeFuelingTypes))]
        public async Task UpdateFuelingVehicleHistory_ValidateFuelingType_ThrowsDomainException(
           string fuelType, VehicleFuelType vehicleFuelType)
        {
            //Arrange
            var vehicleHistory = new UpsertFuelingVehicleHistoryRequest() { Category = fuelType };
            var validators = new List<IValidator<BaseFuelingVehicleHistoryRequest>>()
            {
                new FuelingTypeValidator(_data.Object)
            };

            var decoratedService = new ValidationFuelingVehicleHistoryServiceDecorator(_service, validators);

            _data.Setup(x => x.Vehicles.GetById(It.IsAny<Guid>()))
                .Returns(new Vehicle() { FuelType = vehicleFuelType });

            //Act & Assert
            await Assert.ThrowsAsync<DomainException>(() =>
                decoratedService.UpdateFuelingVehicleHistoryAsync(vehicleHistory));
        }

        public static IEnumerable<object[]> GetNegativeFuelingTypes =>
           new List<object[]>
           {
                new object[] { "Gas", VehicleFuelType.MethaneGasoline },
                new object[] { "Gasoline", VehicleFuelType.Diesel },
                new object[] { "Methane", VehicleFuelType.GasGasoline },
                new object[] { "Diesel", VehicleFuelType.Ethanol },
                new object[] { "Ethanol", VehicleFuelType.Gasoline },
           };

        public static IEnumerable<object[]> GetPositiveFuelingTypes =>
           new List<object[]>
           {
                new object[] { "Gas", VehicleFuelType.GasGasoline },
                new object[] { "Gasoline", VehicleFuelType.GasGasoline },
                new object[] { "Gasoline", VehicleFuelType.Gasoline },
                new object[] { "Methane", VehicleFuelType.MethaneGasoline },
                new object[] { "Gasoline", VehicleFuelType.MethaneGasoline },
                new object[] { "Diesel", VehicleFuelType.Diesel },
                new object[] { "Ethanol", VehicleFuelType.Ethanol },
           };
    }
}
