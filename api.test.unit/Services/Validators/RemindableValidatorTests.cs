using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using api.Domain.Entities;
using api.Domain.Exceptions;
using api.Domain.Models.Requests.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Services.Validation;
using api.Services.Validation.Decorators;
using api.Services.Validation.Validators;
using api.Services.VehicleHistories;
using api.test.unit.Fixtures;
using api.test.unit.Helpers;
using Moq;
using Xunit;

namespace api.test.unit.Services.Validators
{
    public class RemindableValidatorTests
        : BaseServiceTests, IClassFixture<TestFixture>
    {
        private readonly Mock<ITaxVehicleHistoryService> _serviceMock;
        private readonly TestFixture _fixture;

        public RemindableValidatorTests(TestFixture fixture)
        {
            _serviceMock = new Mock<ITaxVehicleHistoryService>();
            _fixture = fixture;
        }

        [Theory]
        [MemberData(nameof(GetNotApplicableVehicleHistories))]
        [MemberData(nameof(GetValidVehicleHistories))]
        public async Task CreateTaxVehicleHistory_RejectNotRemindable_NotThrows(
            UpsertTaxVehicleHistoryRequest vehicleHistory)
        {
            //Arrange
            var validators = new List<IValidator<BaseRemindableVehicleHistoryRequest>>()
            {
                new RemindableValidator<BaseRemindableVehicleHistoryRequest>(_data.Object, _fixture.Localizer)
            };
            _serviceMock
                .Setup(x => x.CreateTaxVehicleHistoryAsync(It.IsAny<UpsertTaxVehicleHistoryRequest>()))
                .ReturnsAsync(Guid.NewGuid());

            var decoratedService = new ValidationTaxVehicleHistoryServiceDecorator(_serviceMock.Object, validators);

            _data.Setup(x => x.Vehicles.GetById(It.IsAny<Guid>()))
                .Returns(new Vehicle() { Milage = 10 });

            //Act & Assert
            await decoratedService.CreateTaxVehicleHistoryAsync(vehicleHistory);
        }

        [Theory]
        [MemberData(nameof(GetNegativeVehicleHistories))]
        public async Task CreateTaxVehicleHistory_ValidateRemindable_ThrowsDomainException(
            UpsertTaxVehicleHistoryRequest vehicleHistory)
        {
            //Arrange
            var validators = new List<IValidator<BaseRemindableVehicleHistoryRequest>>()
            {
                new RemindableValidator<BaseRemindableVehicleHistoryRequest>(_data.Object, _fixture.Localizer)
            };

            var decoratedService = new ValidationTaxVehicleHistoryServiceDecorator(_serviceMock.Object, validators);

            _data.Setup(x => x.Vehicles.GetById(It.IsAny<Guid>()))
                .Returns(new Vehicle() { Milage = 10 });

            //Act & Assert
            await Assert.ThrowsAsync<DomainException>(async () =>
                await decoratedService.CreateTaxVehicleHistoryAsync(vehicleHistory));
        }

        public static IEnumerable<object[]> GetValidVehicleHistories =>
            VehicleHistoryFactory.GetValidTaxRemindableVehicleHistories();

        public static IEnumerable<object[]> GetNotApplicableVehicleHistories =>
            VehicleHistoryFactory.GetNotApplicableTaxRemindableVehicleHistories();

        public static IEnumerable<object[]> GetNegativeVehicleHistories =>
            VehicleHistoryFactory.GetNegativeTaxRemindableVehicleHistories();
    }
}
