using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using api.Services.Configs;
using Microsoft.Extensions.Configuration;
using Xunit;

namespace api.test.unit.Services
{
    public class ConfigurationManagerTests : BaseServiceTests
    {
        private readonly IConfigurationManager _configurationManager;

        public ConfigurationManagerTests()
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(
                    path: Directory.GetCurrentDirectory() + "/../../../../api/appsettings.json",
                    optional: false,
                    reloadOnChange: true)
                .Build();
            _configurationManager = new ConfigurationManager(configuration);
        }

        [Theory]
        [MemberData(nameof(ConfigData))]
        public async Task TryGetValue_Returns_Value(string config)
        {
            //Act
            var result = await _configurationManager.TryGetValue<string>(config);

            //Assert
            Assert.NotNull(result);
        }

        public static IEnumerable<object[]> ConfigData =>
           new List<object[]>
           {
                new object[] { "connectionStrings:defaultConnection" },
                new object[] { "authentication:facebookAppId" },
                new object[] { "authentication:facebookAppSecret" },
                new object[] { "authentication:googleAppId" },
                new object[] { "frontEndUrl" },
                new object[] { "serverBindingUrl" },
                new object[] { "webClientPath" },
                new object[] { "jwt:key" },
                new object[] { "jwt:issuer" },
                new object[] { "email:smtpConfig" },
                new object[] { "email:enableSSL" },
                new object[] { "email:emailFromName" },
                new object[] { "email:emailFromAddress" },
                new object[] { "sentryDsn" },
           };
    }
}
