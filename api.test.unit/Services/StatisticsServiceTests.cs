using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Domain.Entities;
using Xunit;
using api.Services.Analytics;
using api.Domain.Entities.VehicleHistory;
using GenFu;
using System;
using api.Domain.Enums;

namespace api.test.unit.Services
{
    public class StatisticsServiceTests
    {
        private readonly StatisticsService _service;

        public StatisticsServiceTests()
        {
            _service = new StatisticsService();
        }

        [Fact]
        public async Task GetTotalVehicleHistoriesCount_Returns_CountOfAllVehicleHistories()
        {
            //Arrange
            const int vehiclesCount = 10;
            const int vehicleHistoriesPerVehicle = 25;

            A.Configure<Vehicle>()
                .Fill(p => p.TaxVehicleHistories, () => A.ListOf<TaxVehicleHistory>(vehicleHistoriesPerVehicle))
                .Fill(p => p.RepairVehicleHistories, () => A.ListOf<RepairVehicleHistory>(vehicleHistoriesPerVehicle))
                .Fill(p => p.FuelingVehicleHistories, () => A.ListOf<FuelingVehicleHistory>(vehicleHistoriesPerVehicle));

            var vehicles = A.ListOf<Vehicle>(vehiclesCount);

            //Act
            var result = await Task.Run(() =>
                _service.GetTotalVehicleHistoriesCount(vehicles.AsQueryable()));

            //Assert
            Assert.Equal(result, vehiclesCount * 3 * vehicleHistoriesPerVehicle);
        }

        [Fact]
        public async Task GetTotalVehicleHistoriesCost_Returns_TotalCostsOfAllVehicleHistories()
        {
            //Arrange
            const decimal fuelingVehicleHistoryPrice = 10;
            const decimal taxVehicleHistoryPrice = 50;
            const decimal repairVehicleHistoryPrice = 100;
            const int vehiclesCount = 10;
            const int vehicleHistoriesPerVehicle = 5;

            A.Configure<TaxVehicleHistory>()
                .Fill(p => p.TotalPrice, taxVehicleHistoryPrice);
            var taxVehicleHistories = A.ListOf<TaxVehicleHistory>(vehicleHistoriesPerVehicle);

            A.Configure<RepairVehicleHistory>()
                .Fill(p => p.TotalPrice, repairVehicleHistoryPrice);
            var repairVehicleHistories = A.ListOf<RepairVehicleHistory>(vehicleHistoriesPerVehicle);

            A.Configure<FuelingVehicleHistory>()
                .Fill(p => p.TotalPrice, fuelingVehicleHistoryPrice);
            var fuelingVehicleHistories = A.ListOf<FuelingVehicleHistory>(vehicleHistoriesPerVehicle);

            A.Configure<Vehicle>()
                .Fill(p => p.TaxVehicleHistories, () => taxVehicleHistories)
                .Fill(p => p.RepairVehicleHistories, () => repairVehicleHistories)
                .Fill(p => p.FuelingVehicleHistories, () => fuelingVehicleHistories);

            var vehicles = A.ListOf<Vehicle>(vehiclesCount);

            //Act
            var result = await Task.Run(() =>
                _service.GetTotalVehicleHistoriesCost(vehicles.AsQueryable()));

            //Assert
            const decimal totalPrices = (taxVehicleHistoryPrice + fuelingVehicleHistoryPrice + repairVehicleHistoryPrice)
                * vehicleHistoriesPerVehicle * vehiclesCount;
            Assert.Equal(result, totalPrices);
        }

        [Fact]
        public async Task GetTotalDistance_Returns_SumOfAllVehiclesMilage()
        {
            //Arrange
            var vehicles = new List<Vehicle>()
            {
                new Vehicle() { Milage = 100 },
                new Vehicle() { Milage = 550 }
            };

            //Act
            var result = await Task.Run(() =>
                _service.GetTotalDistance(vehicles.AsQueryable())
            );

            //Assert
            Assert.Equal(result, vehicles.Sum(x => x.Milage));
        }

        [Fact]
        public async Task GetTotalFuelingsCostFilteredByDate_Returns_ZeroAsSum()
        {
            //Arrange
            const int vehiclesCount = 10;
            const int vehicleHistoriesPerVehicle = 5;

            A.Configure<FuelingVehicleHistory>()
                .Fill(p => p.CreatedOn, DateTime.Now.AddDays(-10));

            var fuelingVehicleHistories = A.ListOf<FuelingVehicleHistory>(vehicleHistoriesPerVehicle);
            A.Configure<Vehicle>()
                .Fill(p => p.FuelingVehicleHistories, () => A.ListOf<FuelingVehicleHistory>(vehicleHistoriesPerVehicle));
            var vehicles = A.ListOf<Vehicle>(vehiclesCount);

            //Act
            var result = await Task.Run(() =>
                _service.GetTotalFuelingsCost(vehicles.AsQueryable(), DateTime.Now.AddDays(-1), DateTime.Now));

            //Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public async Task GetTotalFuelingsCostFilteredByDate_Returns_SumOfFuelingsCosts()
        {
            //Arrange
            const int totalVehicleHistories = 20;
            const decimal totalPrice = 200;

            var vehicles = new List<Vehicle>();
            for (int i = 1; i <= totalVehicleHistories; i++)
            {
                vehicles.Add(new Vehicle()
                {
                    FuelingVehicleHistories = new List<FuelingVehicleHistory>()
                    {
                        new FuelingVehicleHistory()
                        {
                            CreatedOn = i <= totalVehicleHistories / 2 ? DateTime.Now.AddDays(-10) : DateTime.Now.AddDays(-3),
                            TotalPrice = totalPrice
                        }
                    }
                });
            }

            //Act
            var result = await Task.Run(() =>
                _service.GetTotalFuelingsCost(vehicles.AsQueryable(), DateTime.Now.AddDays(-5), DateTime.Now));

            //Assert
            Assert.Equal(totalPrice * totalVehicleHistories / 2, result);
        }

        [Fact]
        public async Task GetTotalRepairCostFilteredByDate_Returns_ZeroAsSum()
        {
            //Arrange
            const int vehiclesCount = 10;
            const int vehicleHistoriesPerVehicle = 5;

            A.Configure<RepairVehicleHistory>()
                .Fill(p => p.CreatedOn, DateTime.Now.AddDays(-10));

            var fuelingVehicleHistories = A.ListOf<RepairVehicleHistory>(vehicleHistoriesPerVehicle);
            A.Configure<Vehicle>()
                .Fill(p => p.RepairVehicleHistories, () => A.ListOf<RepairVehicleHistory>(vehicleHistoriesPerVehicle));
            var vehicles = A.ListOf<Vehicle>(vehiclesCount);

            //Act
            var result = await Task.Run(() =>
                _service.GetTotalRepairsCost(vehicles.AsQueryable(), DateTime.Now.AddDays(-1), DateTime.Now));

            //Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public async Task GetTotalRepairCostFilteredByDate_Returns_SumOfFuelingsCosts()
        {
            //Arrange
            const int totalVehicleHistories = 20;
            const decimal totalPrice = 200;

            var vehicles = new List<Vehicle>();
            for (int i = 1; i <= totalVehicleHistories; i++)
            {
                vehicles.Add(new Vehicle()
                {
                    RepairVehicleHistories = new List<RepairVehicleHistory>()
                    {
                        new RepairVehicleHistory()
                        {
                            CreatedOn = i <= totalVehicleHistories / 2 ? DateTime.Now.AddDays(-10) : DateTime.Now.AddDays(-3),
                            TotalPrice = totalPrice
                        }
                    }
                });
            }

            //Act
            var result = await Task.Run(() =>
                _service.GetTotalRepairsCost(vehicles.AsQueryable(), DateTime.Now.AddDays(-5), DateTime.Now));

            //Assert
            Assert.Equal(totalPrice * totalVehicleHistories / 2, result);
        }

        [Fact]
        public async Task GetTotalTaxesCostFilteredByDate_Returns_ZeroAsSum()
        {
            //Arrange
            const int vehiclesCount = 10;
            const int vehicleHistoriesPerVehicle = 5;

            A.Configure<TaxVehicleHistory>()
                .Fill(p => p.CreatedOn, DateTime.Now.AddDays(-10));

            var fuelingVehicleHistories = A.ListOf<TaxVehicleHistory>(vehicleHistoriesPerVehicle);
            A.Configure<Vehicle>()
                .Fill(p => p.TaxVehicleHistories, () => A.ListOf<TaxVehicleHistory>(vehicleHistoriesPerVehicle));
            var vehicles = A.ListOf<Vehicle>(vehiclesCount);

            //Act
            var result = await Task.Run(() =>
                _service.GetTotalTaxesCost(vehicles.AsQueryable(), DateTime.Now.AddDays(-1), DateTime.Now));

            //Assert
            Assert.Equal(0, result);
        }

        [Fact]
        public async Task GetTotalTaxesCostFilteredByDate_Returns_SumOfFuelingsCosts()
        {
            //Arrange
            const int totalVehicleHistories = 20;
            const decimal totalPrice = 200;

            var vehicles = new List<Vehicle>();
            for (int i = 1; i <= totalVehicleHistories; i++)
            {
                vehicles.Add(new Vehicle()
                {
                    TaxVehicleHistories = new List<TaxVehicleHistory>()
                    {
                        new TaxVehicleHistory()
                        {
                            CreatedOn = i <= totalVehicleHistories / 2 ? DateTime.Now.AddDays(-10) : DateTime.Now.AddDays(-3),
                            TotalPrice = totalPrice
                        }
                    }
                });
            }

            //Act
            var result = await Task.Run(() =>
                _service.GetTotalTaxesCost(vehicles.AsQueryable(), DateTime.Now.AddDays(-5), DateTime.Now));

            //Assert
            Assert.Equal(totalPrice * totalVehicleHistories / 2, result);
        }

        [Fact]
        public async Task GetAllFuelingConsumptions()
        {
            //Arrange
            const int currentMilage = 1000;
            const int previousMilage = 500;
            const int currentLiters = 30;

            var vehicles = new List<Vehicle>()
            {
                new Vehicle()
                {
                    FuelingVehicleHistories = new List<FuelingVehicleHistory>()
                    {
                        new FuelingVehicleHistory()
                        {
                            CreatedOn = DateTime.Now.AddDays(-2),
                            Category = FuelType.Diesel,
                            IsMissedPrevious = false,
                            IsPartial = false,
                            CurrentMilage = currentMilage,
                            Liters = currentLiters
                        },
                        new FuelingVehicleHistory()
                        {
                            CreatedOn = DateTime.Now.AddDays(-3),
                            Category = FuelType.Diesel,
                            IsMissedPrevious = false,
                            IsPartial = false,
                            CurrentMilage = previousMilage,
                            Liters = 50
                        }
                    }
                }
            };

            //Act
            var result = await Task.Run(() => _service.GetAllFuelingConsumptions(vehicles, DateTime.Now.AddDays(-10), DateTime.Now));

            //Assert
            const decimal consumptionFormula = (decimal)currentLiters / (currentMilage - previousMilage) * 100;
            Assert.NotEmpty(result);
            Assert.Equal(result.FirstOrDefault()?.Consumption, consumptionFormula);
        }
    }
}
