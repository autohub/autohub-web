using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Domain.Entities;
using api.Domain.Entities.VehicleHistory;
using api.Services.VehicleHistories;
using Moq;
using Xunit;

namespace api.test.unit.Services
{
    public class RemindableVehicleHistoryServiceTests : BaseServiceTests
    {
        private readonly RemindableVehicleHistoryService _service;

        public RemindableVehicleHistoryServiceTests()
        {
            _service = new RemindableVehicleHistoryService(_data.Object, _mapper);
        }

        [Fact]
        public async Task GetAll_Returns_RemindableVehicleHistoriesModel()
        {
            //Arrange
            var repairVehicleHistory = new TaxVehicleHistory()
            {
                IsRemindableByDate = false,
                IsRemindableByMilage = true
            };

            var taxVehicleHistory = new RepairVehicleHistory()
            {
                IsRemindableByDate = false,
                IsRemindableByMilage = true
            };

            _data
                .Setup(x => x.TaxVehicleHistories.All())
                .Returns(new List<TaxVehicleHistory>() { repairVehicleHistory }.AsQueryable());

            _data
                .Setup(x => x.RepairVehicleHistories.All())
                .Returns(new List<RepairVehicleHistory>() { taxVehicleHistory }.AsQueryable());

            //Act
            var result = await Task.Run(() => _service.GetAllRemindableVehicleHistories(It.IsAny<Guid>()));

            //Assert
            Assert.NotEmpty(result);
            Assert.True(result.All(x => x.IsRemindableByDate || x.IsRemindableByMilage));
        }

        [Theory]
        [MemberData(nameof(NegativeRemindableVehicleHistoriesData))]
        public async Task GetIncomingVehicleServices_Return_EmptyRemindableVehicleHistoryModelList(
            List<TaxVehicleHistory> taxVehicleHistories, List<RepairVehicleHistory> repairVehicleHistories)
        {
            //Arrange
            _data
                .Setup(x => x.TaxVehicleHistories.All())
                .Returns(taxVehicleHistories.AsQueryable());

            _data
                .Setup(x => x.RepairVehicleHistories.All())
                .Returns(repairVehicleHistories.AsQueryable());

            //Act
            var result = await _service.GetIncomingVehicleHistories();

            //Assert
            Assert.True(!result.Any());
        }

        [Theory]
        [MemberData(nameof(PositiveRemindableVehicleHistoriesData))]
        public async Task GetIncomingVehicleServices_Return_RemindableVehicleHistoryModelList(
            List<TaxVehicleHistory> taxVehicleHistories, List<RepairVehicleHistory> repairVehicleHistories)
        {
            //Arrange
            _data
                .Setup(x => x.TaxVehicleHistories.All())
                .Returns(taxVehicleHistories.AsQueryable());

            _data
                .Setup(x => x.RepairVehicleHistories.All())
                .Returns(repairVehicleHistories.AsQueryable());

            //Act
            var result = await _service.GetIncomingVehicleHistories();

            //Assert
            Assert.True(result.Any());
        }

        public static IEnumerable<object[]> PositiveRemindableVehicleHistoriesData =>
            new List<object[]>
            {
                new object[]
                {
                    new List<TaxVehicleHistory>(),
                    new List<RepairVehicleHistory>()
                    {
                        new RepairVehicleHistory()
                        {
                            IsRemindableByDate = true,
                            ExpiresOn = DateTime.UtcNow.AddDays(20),
                            DaysRange = 10,
                            IsRemindableByMilage = true,
                            ExpiresOnMilage  = 1000,
                            MilageRange = 100,
                            Vehicle = new Vehicle()
                            {
                                Milage = 910
                            }
                        }
                    }
                },
                new object[]
                {
                    new List<TaxVehicleHistory>()
                    {
                        new TaxVehicleHistory()
                        {
                            IsRemindableByDate = true,
                            ExpiresOn = DateTime.UtcNow.AddDays(20),
                            DaysRange = 10,
                        }
                    },
                    new List<RepairVehicleHistory>()
                    {
                        new RepairVehicleHistory()
                        {
                            IsRemindableByMilage = true,
                            ExpiresOnMilage  = 1000,
                            MilageRange = 100,
                            Vehicle = new Vehicle()
                            {
                                Milage = 910
                            }
                        }
                    }
                }
            };

        public static IEnumerable<object[]> NegativeRemindableVehicleHistoriesData =>
            new List<object[]>
            {
                new object[] { new List<TaxVehicleHistory>(), new List<RepairVehicleHistory>() },
                new object[]
                {
                    new List<TaxVehicleHistory>()
                    {
                        new TaxVehicleHistory()
                    },
                    new List<RepairVehicleHistory>()
                    {
                        new RepairVehicleHistory()
                    }
                }
            };
    }
}
