using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Data;
using api.Domain.Entities;
using api.test.unit.Fakes;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;
using GenFu;
using System;
using api.Domain.Models.Account;
using api.Domain.Models;
using api.Domain.Models.Auth;
using api.Common;
using api.Domain.Enums;
using Microsoft.AspNetCore.Http;
using api.test.unit.Fixtures;
using Microsoft.Net.Http.Headers;
using api.Services.Users;
using api.Infrastructure.Email;
using api.Services.Configs;

namespace api.test.unit.Services
{
    public class UserServiceTests : IClassFixture<TestFixture>
    {
        private readonly UserService _service;
        private Mock<DefaultDbContext> _dbContextMock;
        private readonly Mock<IOptions<JwtOptions>> _jwtOptionsMock = new Mock<IOptions<JwtOptions>>();
        private readonly Mock<FakeSignInManager> _signInManagerMock = new Mock<FakeSignInManager>();
        private readonly Mock<FakeUserManager> _userManagerMock = new Mock<FakeUserManager>();
        private readonly Mock<IEmailSender> _emailSenderMock = new Mock<IEmailSender>();
        private readonly Mock<IConfigurationManager> _configurationManagerMock = new Mock<IConfigurationManager>();
        private readonly Mock<ILoggerFactory> _loggerFactoryMock;
        private readonly Mock<IHttpContextAccessor> _httpContextAccessor = new Mock<IHttpContextAccessor>();
        private readonly TestFixture _fixture;

        public UserServiceTests(TestFixture fixture)
        {
            _fixture = fixture;

            //TODO: Remove to Create logger
            var loggerMock = new Mock<FakeLogger<UserService>>();
            _loggerFactoryMock = new Mock<ILoggerFactory>();
            _loggerFactoryMock
                .Setup(x => x.CreateLogger(It.IsAny<string>()))
                .Returns(loggerMock.Object);

            //Mock httpAccessor
            var context = new DefaultHttpContext();
            var fakeAcceptLanguageHeader = new StringWithQualityHeaderValue("bg");
            var type = context.Request.GetTypedHeaders();
            type.AcceptLanguage = new List<StringWithQualityHeaderValue>() { fakeAcceptLanguageHeader };
            _httpContextAccessor.Setup(_ => _.HttpContext).Returns(context);

            //Create db
            CreateDbContext();

            _service = new UserService(
                _dbContextMock.Object,
                _fixture.Localizer,
                _jwtOptionsMock.Object,
                _signInManagerMock.Object,
                _userManagerMock.Object,
                _emailSenderMock.Object,
                _fixture.Mapper,
                _configurationManagerMock.Object,
                _loggerFactoryMock.Object,
                _httpContextAccessor.Object
            );
        }

        [Fact]
        public async Task FindByIdAsync_ThrowsException()
        {
            //Arrange
            _userManagerMock
                .Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                .ThrowsAsync(new ArgumentNullException());

            //Act & Assert
            await Assert.ThrowsAnyAsync<ArgumentNullException>(async () =>
                await _service.FindByIdAsync(It.IsAny<string>())
            );
        }

        [Fact]
        public async Task FindByIdAsync_ReturnsApplicationUserAsync()
        {
            //Arrange
            _userManagerMock
                .Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(new ApplicationUser());

            //Act
            var result = await _service.FindByIdAsync(It.IsAny<string>());

            //Assert
            Assert.IsType<ApplicationUser>(result);
        }

        [Fact]
        public async Task GetUserAccountAsync_ThrowsExceptionAsync()
        {
            //Act & Assert
            await Assert.ThrowsAnyAsync<ArgumentException>(async () =>
                await _service.GetUserAccountAsync(It.IsAny<string>())
            );
        }

        [Fact]
        public async Task GetUserAccountAsync_ReturnsAccountResponseAsync()
        {
            //Arrange
            _userManagerMock
                .Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(new ApplicationUser());

            //Act
            var result = await _service.GetUserAccountAsync(It.IsAny<string>());

            //Assert
            Assert.IsType<AccountResponse>(result);
        }

        [Fact]
        public async Task Register_FailedTask_WhenRegisterFailsAsync()
        {
            //Arrange
            _userManagerMock
                .Setup(x => x.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Failed(new IdentityError()));

            //Act
            var result = await _service.RegisterUser(new RegisterUserRequest()
            {
                Username = RandomGenerator.RandomString(8),
                Password = RandomGenerator.RandomPassword(),
                UserType = nameof(UserType.Personal),
                Name = RandomGenerator.RandomString(10)
            });

            //Assert
            Assert.True(result.Errors.Any());
        }

        [Fact]
        public async Task Register_SucceededTaskAsync()
        {
            //Arrange
            _userManagerMock
                .Setup(x => x.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);

            _emailSenderMock
                .Setup(x => x.SendTemplatedEmail(new EmailModel(_fixture.Localizer)))
                .Returns(Task.CompletedTask);

            _configurationManagerMock
                .Setup(x => x.TryGetValue<string>("frontEndUrl"))
                .ReturnsAsync(new Uri("https://localhost").AbsoluteUri);

            //Act
            var result = await _service.RegisterUser(new RegisterUserRequest()
            {
                Username = RandomGenerator.RandomString(8),
                Password = RandomGenerator.RandomPassword(),
                UserType = nameof(UserType.Personal),
                Name = RandomGenerator.RandomString(10)
            });

            //Assert
            Assert.True(result.Succeeded);
        }

        [Fact]
        public async Task ResetPasswordRequest_ThrowsException_WhenNoUserFoundAsync()
        {
            //Arrange
            _userManagerMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync((ApplicationUser)null);

            //Act & Assert
            await Assert.ThrowsAnyAsync<ArgumentException>(async () =>
                await _service.ResetPasswordRequest(string.Empty)
            );
        }

        [Fact]
        public async Task ResetPassword_ThrowsException_WhenNotEqualPasswords()
        {
            //Arrange
            var requestModel = new ResetPasswordTokenRequest()
            {
                NewPassword = RandomGenerator.RandomPassword(),
                NewPasswordConfirmation = RandomGenerator.RandomPassword()
            };

            //Act & Assert
            await Assert.ThrowsAnyAsync<ArgumentException>(async () =>
                await _service.ResetPassword(requestModel));
        }

        [Fact]
        public async Task ResetPassword_ThrowsException_WhenNoUserFound()
        {
            //Arrange
            var password = RandomGenerator.RandomPassword();
            var requestModel = new ResetPasswordTokenRequest()
            {
                NewPassword = password,
                NewPasswordConfirmation = password
            };

            _dbContextMock
                .Setup(x => x.ApplicationUsers.Find(It.IsAny<string>()))
                .Returns((ApplicationUser)null);

            //Act & Assert
            await Assert.ThrowsAnyAsync<ArgumentException>(async () =>
                await _service.ResetPassword(requestModel)
            );
        }

        [Fact]
        public async Task ResetPassword_ThrowsException_WhenResetPasswordFail()
        {
            //Arrange
            var password = RandomGenerator.RandomPassword();
            var requestModel = new ResetPasswordTokenRequest()
            {
                NewPassword = password,
                NewPasswordConfirmation = password
            };

            _dbContextMock
                .Setup(x => x.ApplicationUsers.Find(It.IsAny<string>()))
                .Returns(new ApplicationUser());

            _userManagerMock
                .Setup(x => x.ResetPasswordAsync(
                    It.IsAny<ApplicationUser>(),
                    It.IsAny<string>(),
                    It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Failed(new IdentityError()));

            //Act & Assert
            await Assert.ThrowsAnyAsync<ArgumentException>(async () =>
                await _service.ResetPassword(requestModel));
        }

        [Fact]
        public async Task ResetPassword_WithValidResetPasswordTokenRequest()
        {
            //Arrange
            var password = RandomGenerator.RandomPassword();
            var requestModel = new ResetPasswordTokenRequest()
            {
                NewPassword = password,
                NewPasswordConfirmation = password
            };

            _userManagerMock
                .Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(new ApplicationUser());

            _userManagerMock
                .Setup(x => x.ResetPasswordAsync(
                    It.IsAny<ApplicationUser>(),
                    It.IsAny<string>(),
                    It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);

            //Act & Assert
            await _service.ResetPassword(requestModel);
        }

        [Fact]
        public async Task UpdateUser_WithUpdateAccountRequest_ThrowsException()
        {
            //Arrange
            _dbContextMock
                .Setup(x => x.ApplicationUsers.Find(It.IsAny<string>()))
                .Returns((ApplicationUser)null);

            //Act & Assert
            await Assert.ThrowsAnyAsync<ArgumentNullException>(async () =>
                await _service.UpdateUser(
                    new Guid().ToString(), new UpdateAccountRequest()
                )
            );
        }

        [Fact]
        public async Task UpdateUser_WithValidUpdateAccountPasswordRequest()
        {
            //Arrange
            _dbContextMock
                .Setup(x => x.ApplicationUsers.Find(It.IsAny<string>()))
                .Returns(new ApplicationUser() { Id = new Guid().ToString() });

            _userManagerMock
                .Setup(x => x.CheckPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(true);

            _userManagerMock
                .Setup(x => x.ChangePasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);

            _emailSenderMock
                .Setup(x => x.SendTemplatedEmail(It.IsAny<EmailModel>()))
                .Returns(Task.CompletedTask);

            //Act & Assert
            await _service.UpdateUser(new Guid().ToString(), new UpdateAccountPasswordRequest());
        }

        [Fact]
        public async Task UpdateUser_WithUpdateAccountPasswordRequest_ThrowsException_WhenNoUserFound()
        {
            //Arrange
            _dbContextMock
                .Setup(x => x.ApplicationUsers.Find(It.IsAny<string>()))
                .Returns((ApplicationUser)null);

            //Act & Assert
            await Assert.ThrowsAnyAsync<ArgumentNullException>(async () =>
                await _service.UpdateUser(
                    new Guid().ToString(), new UpdateAccountPasswordRequest()));
        }

        [Fact]
        public async Task UpdateUser_WithUpdateAccountPasswordRequest_ThrowsException_WhenFalsePassword()
        {
            //Arrange
            _dbContextMock
                .Setup(x => x.ApplicationUsers.Find(It.IsAny<string>()))
                .Returns(new ApplicationUser());

            _userManagerMock
                .Setup(x => x.CheckPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(false);

            //Act & Assert
            await Assert.ThrowsAnyAsync<ArgumentException>(async () =>
                await _service.UpdateUser(
                    new Guid().ToString(), new UpdateAccountPasswordRequest()));
        }

        [Fact]
        public async Task UpdateUser_WithUpdateAccountPasswordRequest_ThrowsException_WhenChangePasswordFail()
        {
            //Arrange
            _dbContextMock
                .Setup(x => x.ApplicationUsers.Find(It.IsAny<string>()))
                .Returns(new ApplicationUser());

            _userManagerMock
                .Setup(x => x.CheckPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(true);

            _userManagerMock
                .Setup(x => x.ChangePasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Failed(new IdentityError()));

            //Act & Assert
            await Assert.ThrowsAnyAsync<AggregateException>(async () =>
                await _service.UpdateUser(
                    new Guid().ToString(), new UpdateAccountPasswordRequest()));
        }

        [Fact]
        public async Task GetOrCreateUser_Returns_FailResult_WhenCreatingNewUser()
        {
            //Arrange
            var user = new ApplicationUser()
            {
                Email = "georgi.marokov@gmail.com",
                UserName = "Georgi Marokov"
            };

            _userManagerMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync((ApplicationUser)null);

            _userManagerMock
                .Setup(x => x.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Failed(new IdentityError()));

            //Act
            var result = await _service.GetOrCreateUser(user);

            //Assert
            Assert.False(result.WasSuccessful);
            Assert.NotEmpty(result.Errors);
        }

        [Fact]
        public async Task GetOrCreateUser_Returns_SuccessResult_WhenExistingUser()
        {
            //Arrange
            var user = new ApplicationUser()
            {
                Email = "georgi.marokov@gmail.com",
                UserName = "Georgi Marokov"
            };

            _userManagerMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync(user);

            //Act
            var result = await _service.GetOrCreateUser(user);

            //Assert
            Assert.True(result.WasSuccessful);
            Assert.NotNull(result.Value);
            Assert.Null(result.Errors);
        }

        [Fact]
        public async Task GetOrCreateUser_Returns_SuccessResult_WhenNonExistingUser()
        {
            //Arrange
            var user = new ApplicationUser()
            {
                Email = "georgi.marokov@gmail.com",
                UserName = "Georgi Marokov"
            };

            _userManagerMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync((ApplicationUser)null);

            _userManagerMock
                .Setup(x => x.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(IdentityResult.Success);

            //Act
            var result = await _service.GetOrCreateUser(user);

            //Assert
            Assert.True(result.WasSuccessful);
            Assert.NotNull(result.Value);
            Assert.Null(result.Errors);
        }

        private void CreateDbContext()
        {
            var persons = GetFakeData().AsQueryable();

            var dbSet = new Mock<DbSet<ApplicationUser>>();
            dbSet.As<IQueryable<ApplicationUser>>().Setup(m => m.Provider).Returns(persons.Provider);
            dbSet.As<IQueryable<ApplicationUser>>().Setup(m => m.Expression).Returns(persons.Expression);
            dbSet.As<IQueryable<ApplicationUser>>().Setup(m => m.ElementType).Returns(persons.ElementType);
            dbSet.As<IQueryable<ApplicationUser>>().Setup(m => m.GetEnumerator()).Returns(persons.GetEnumerator());

            _dbContextMock = new Mock<DefaultDbContext>();
            _dbContextMock.Setup(c => c.ApplicationUsers).Returns(dbSet.Object);
        }

        private IEnumerable<ApplicationUser> GetFakeData()
        {
            var users = A.ListOf<ApplicationUser>(26);
            users.ForEach(x => x.Id = new Guid().ToString());
            return users.Select(_ => _);
        }
    }
}
