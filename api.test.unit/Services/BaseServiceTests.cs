using System.Reflection;
using api.Data.Contracts;
using AutoMapper;
using Moq;

namespace api.test.unit.Services
{
    public abstract class BaseServiceTests
    {
        protected readonly Mock<IApiData> _data;
        protected readonly IMapper _mapper;

        protected BaseServiceTests()
        {
            _data = new Mock<IApiData>();
            var configuration = new MapperConfiguration(cfg =>
                cfg.AddMaps(Assembly.Load("api")));
            _mapper = configuration.CreateMapper();
        }
    }
}
