using System;
using System.Threading.Tasks;
using api.Domain.Entities;
using api.Domain.Models.Requests.Auth;
using api.Infrastructure.Apis.Facebook;
using api.Infrastructure.Apis.Google;
using api.Services;
using api.Services.Users;
using api.test.unit.Fakes;
using Microsoft.AspNetCore.Identity;
using Moq;
using Xunit;
using static Google.Apis.Auth.GoogleJsonWebSignature;

namespace api.test.unit.Services
{
    public class AuthServiceTests : BaseServiceTests
    {
        private readonly AuthService _service;
        private readonly Mock<IUserService> _userServiceMock = new Mock<IUserService>();
        private readonly Mock<IFacebookClient> _facebookClientMock = new Mock<IFacebookClient>();
        private readonly Mock<IGoogleAuthClient> _googleAuthClientMock = new Mock<IGoogleAuthClient>();
        private readonly Mock<FakeUserManager> _userManagerMock = new Mock<FakeUserManager>();

        public AuthServiceTests()
        {
            _service = new AuthService(
                _userServiceMock.Object,
                _facebookClientMock.Object,
                _googleAuthClientMock.Object
            );
        }

        [Fact]
        public async Task LoginOrRegisterWithFacebook_WhenExistingUser_ReturnsUser()
        {
            //Arrange
            _userServiceMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync(new ApplicationUser() { Email = "georgi.marokov@gmail.com", Name = "Georgi Marokov" });

            _facebookClientMock
                .Setup(x => x.GetAppAccessToken())
                .ReturnsAsync(new FacebookAppAccessToken() { AccessToken = string.Empty, TokenType = string.Empty });

            _facebookClientMock
                .Setup(x => x.GetUserTokenValidation(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new FacebookUserAccessTokenValidation() { IsValid = true });

            _facebookClientMock
                .Setup(x => x.GetUserData(It.IsAny<string>()))
                .ReturnsAsync(new FacebookUserModel() { Email = string.Empty, Name = string.Empty });

            _userServiceMock
                .Setup(x => x.GetOrCreateUser(It.IsAny<ApplicationUser>()))
                .ReturnsAsync(Result<ApplicationUser>.Success(new ApplicationUser()
                {
                    Email = "georgi.marokov@gmail.com",
                    Name = "Georgi Marokov"
                }));

            var request = new FacebookLoginRequest()
            {
                AccessToken = "",
                Name = "",
                Email = "",
                Id = "",
                UserID = "",
                ExpiresIn = 0,
                SignedRequest = "",
                DataAccessExpirationTime = 0,
            };

            //Act
            var userResult = await _service.LoginOrRegisterUser(request);

            //Assert
            Assert.True(userResult.WasSuccessful);
            Assert.True(!string.IsNullOrEmpty(userResult.Value.Email));
            Assert.True(!string.IsNullOrEmpty(userResult.Value.Name));
        }

        [Fact]
        public async Task LoginOrRegisterUserWithFacebook_WhenNotExistingUser_ReturnsUser()
        {
            //Arrange
            _userServiceMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync((ApplicationUser)null);

            _userManagerMock
               .Setup(x => x.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
               .ReturnsAsync(IdentityResult.Success);

            _facebookClientMock
                .Setup(x => x.GetAppAccessToken())
                .ReturnsAsync(new FacebookAppAccessToken() { AccessToken = string.Empty, TokenType = string.Empty });

            _facebookClientMock
                .Setup(x => x.GetUserTokenValidation(It.IsAny<string>(), It.IsAny<string>()))
                .ReturnsAsync(new FacebookUserAccessTokenValidation() { IsValid = true });

            _facebookClientMock
                .Setup(x => x.GetUserData(It.IsAny<string>()))
                .ReturnsAsync(new FacebookUserModel() { Email = string.Empty });

            _userServiceMock
                .Setup(x => x.GetOrCreateUser(It.IsAny<ApplicationUser>()))
                .ReturnsAsync(Result<ApplicationUser>.Success(new ApplicationUser()
                {
                    Email = "georgi.marokov@gmail.com",
                    Name = "Georgi Marokov"
                }));

            var request = new FacebookLoginRequest()
            {
                AccessToken = "3ASK91KAS81SNAM1K29AKNVB",
                Name = "Georgi",
                Email = "georgi.marokov@gmail.com",
                Id = "",
                UserID = "",
                ExpiresIn = 0,
                SignedRequest = "",
                DataAccessExpirationTime = 0,
            };

            //Act
            var userResult = await _service.LoginOrRegisterUser(request);

            //Assert
            Assert.True(userResult.WasSuccessful);
            Assert.NotNull(userResult.Value.Email);
            Assert.NotNull(userResult.Value.Name);
        }

        [Fact]
        public async Task LoginOrRegisterUserWithFacebook_ThrowsException()
        {
            //Assert
            _userServiceMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync((ApplicationUser)null);

            _userManagerMock
               .Setup(x => x.CreateAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
               .ThrowsAsync(new SystemException());

            var request = new FacebookLoginRequest()
            {
                AccessToken = "3ASK91KAS81SNAM1K29AKNVB",
                Name = "Georgi",
                Email = "georgi.marokov@gmail.com",
                Id = "",
                UserID = "",
                ExpiresIn = 0,
                SignedRequest = "",
                DataAccessExpirationTime = 0,
            };

            //Act & Assert
            await Assert.ThrowsAnyAsync<SystemException>(async () =>
                await _service.LoginOrRegisterUser(request)
            );
        }

        [Fact]
        public async Task LoginOrRegisterUserWithGoogle_Returns_FailResult_WhenInvalidTokenId()
        {
            //Assert
            var request = new GoogleLoginRequest() { IdToken = Guid.NewGuid().ToString() };
            _googleAuthClientMock
                .Setup(x => x.GetUserAccount(request))
                .ReturnsAsync((Payload)null);

            //Act
            var result = await _service.LoginOrRegisterUser(request);

            //Assert
            Assert.False(result.WasSuccessful);
        }

        [Fact]
        public async Task LoginOrRegisterUserWithGoogle_Returns_FailResult_WhenIdentityErrors()
        {
            //Assert
            const string email = "georgi.marokov@gmail.com";
            const string name = "Georgi Marokov";
            const bool isEmailVerified = true;

            var request = new GoogleLoginRequest() { IdToken = Guid.NewGuid().ToString() };
            var user = new ApplicationUser()
            {
                Email = email,
                Name = name,
                EmailConfirmed = isEmailVerified
            };

            _googleAuthClientMock
                .Setup(x => x.GetUserAccount(request))
                .ReturnsAsync(new Payload()
                {
                    Email = email,
                    EmailVerified = isEmailVerified,
                    Name = name
                });

            _userServiceMock
                .Setup(x => x.GetOrCreateUser(It.IsAny<ApplicationUser>()))
                .ReturnsAsync(Result<ApplicationUser>.Fail(It.IsAny<string>()));

            //Act
            var result = await _service.LoginOrRegisterUser(request);

            //Assert
            Assert.False(result.WasSuccessful);
            Assert.NotEmpty(result.Errors);
        }

        [Fact]
        public async Task LoginOrRegisterUserWithGoogle_Returns_SuccessResult_WithApplicationUser()
        {
            //Assert
            const string email = "georgi.marokov@gmail.com";
            const string name = "Georgi Marokov";
            const bool isEmailVerified = true;

            var request = new GoogleLoginRequest() { IdToken = Guid.NewGuid().ToString() };
            var user = new ApplicationUser()
            {
                Email = email,
                Name = name,
                EmailConfirmed = isEmailVerified
            };

            _googleAuthClientMock
                .Setup(x => x.GetUserAccount(request))
                .ReturnsAsync(new Payload()
                {
                    Email = email,
                    EmailVerified = isEmailVerified,
                    Name = name
                });

            _userServiceMock
                .Setup(x => x.GetOrCreateUser(It.IsAny<ApplicationUser>()))
                .ReturnsAsync(Result<ApplicationUser>.Success(user));

            //Act
            var result = await _service.LoginOrRegisterUser(request);

            //Assert
            Assert.True(result.WasSuccessful);
        }
    }
}
