using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Services.VehicleHistories;
using api.Services.Vehicles;
using Moq;
using Xunit;

namespace api.test.unit.Services
{
    public class FuelingVehicleHistoryServiceTests : BaseServiceTests
    {
        private readonly FuelingVehicleHistoryService _service;
        private readonly Mock<IVehicleService> _vehicleServiceMock = new Mock<IVehicleService>();

        public FuelingVehicleHistoryServiceTests()
        {
            _service = new FuelingVehicleHistoryService(
                _data.Object,
                _mapper,
                _vehicleServiceMock.Object
            );
        }

        [Fact]
        public async Task GetFuelingVehicleHistory_Returns_DetailsFuelingVehicleHistoryResponse()
        {
            //Arrange
            var id = It.IsAny<Guid>();
            var vehicleHistory = new FuelingVehicleHistory() { Id = id };
            _data.Setup(x => x.FuelingVehicleHistories.All())
                .Returns(new List<FuelingVehicleHistory>() { vehicleHistory }
                    .AsQueryable());

            //Act
            var result = await _service.GetFuelingVehicleHistoryAsync(id);

            //Assert
            Assert.IsType<FuelingVehicleHistoryModel>(result);
        }

        [Fact]
        public async Task GetFuelingVehicleHistory_Returns_Null()
        {
            //Arrange
            _data.Setup(x => x.FuelingVehicleHistories.All())
                .Returns(new List<FuelingVehicleHistory>() { new FuelingVehicleHistory() }
                    .AsQueryable());

            //Act
            var result = await _service.GetFuelingVehicleHistoryAsync(Guid.NewGuid());

            //Assert
            Assert.Null(result);
        }

        [Fact]
        public async Task UpdateFuelingVehicleHistory_Throws()
        {
            //Arrange
            var vehicleHistory = new UpsertFuelingVehicleHistoryRequest();

            _data.Setup(x => x.FuelingVehicleHistories.All())
                .Returns(new List<FuelingVehicleHistory>()
                    .AsQueryable());

            //Act & Assert
            await Assert.ThrowsAnyAsync<Exception>(() =>
                _service.UpdateFuelingVehicleHistoryAsync(vehicleHistory));
        }

        [Fact]
        public async Task UpdateFuelingVehicleHistory_NotThrows()
        {
            //Arrange
            var id = Guid.NewGuid();
            var fuelingVehicleHistory = new UpsertFuelingVehicleHistoryRequest() { Id = id };
            var vehicleHistory = new FuelingVehicleHistory() { Id = id };

            _data.Setup(x => x.FuelingVehicleHistories.GetById(It.IsAny<Guid>()))
                .Returns(vehicleHistory);

            _vehicleServiceMock.Setup(x => x.UpdateMilageAsync(It.IsAny<Guid>(), It.IsAny<long>()))
               .Returns(Task.CompletedTask);

            //Act & Assert
            await _service.UpdateFuelingVehicleHistoryAsync(fuelingVehicleHistory);
        }

        [Fact]
        public async Task CreateFuelingVehicleHistory_NotThrow()
        {
            //Arrange
            var vehicleHistory = new UpsertFuelingVehicleHistoryRequest();

            _data.Setup(x => x.FuelingVehicleHistories.All())
                .Returns(new List<FuelingVehicleHistory>()
                    .AsQueryable());

            //Act
            var result = await _service.CreateFuelingVehicleHistoryAsync(vehicleHistory);

            //Assert
            Assert.True(result == Guid.Empty);
        }

        [Fact]
        public async Task DeleteFuelingVehicleHistory_NotThrow()
        {
            //Arrange
            var vehicleHistory = new UpsertFuelingVehicleHistoryRequest();

            _data.Setup(x => x.FuelingVehicleHistories.All())
                .Returns(new List<FuelingVehicleHistory>()
                    .AsQueryable());

            //Act & Assert
            await _service.DeleteFuelingVehicleHistoryAsync(It.IsAny<Guid>());
        }
    }
}
