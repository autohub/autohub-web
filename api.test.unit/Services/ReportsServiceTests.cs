using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Application.Reports.Queries;
using api.Application.Reports.Queries.GraphResponseTypes;
using api.Domain.Entities;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Enums;
using api.Extensions;
using api.Services.Analytics;
using api.Services.Users;
using api.Services.VehicleHistories;
using api.Services.Vehicles;
using DeepEqual.Syntax;
using Moq;
using Xunit;

namespace api.test.unit.Services
{
    public class ReportsServiceTests : BaseServiceTests
    {
        private readonly Mock<IVehicleService> _vehicleService = new Mock<IVehicleService>();
        private readonly Mock<IVehicleHistoryService> _vehicleHistoryService = new Mock<IVehicleHistoryService>();
        private readonly Mock<IUserService> _userService = new Mock<IUserService>();
        private readonly Mock<IStatisticsService> _statisticsService = new Mock<IStatisticsService>();
        private readonly ReportsService _service;

        private readonly string _userId;
        private readonly GetGraphReportQuery _query;

        public ReportsServiceTests()
        {
            _service = new ReportsService(
                _vehicleService.Object,
                _vehicleHistoryService.Object,
                _userService.Object,
                _statisticsService.Object
            );

            _userId = Guid.NewGuid().ToString();

            _userService
                .Setup(x => x.GetCurrentUserId())
                .ReturnsAsync(_userId);

            _query = new GetGraphReportQuery()
            {
                VehicleId = null,
                From = DateTime.MinValue,
                To = DateTime.MaxValue
            };
        }

        [Fact]
        public async Task GetVehicleHistoriesCostsData_Returns_VehicleHistoriesCostsResponse_WithTotalsVehicleHistories()
        {
            //Arrange
            List<Vehicle> vehicles = GetVehicleWithHistoriesTestData();

            _vehicleService
                .Setup(x => x.GetVehiclesWithHistoriesByUserId(_userId))
                .ReturnsAsync(vehicles.AsQueryable());

            var _service = new ReportsService(
                _vehicleService.Object,
                new VehicleHistoryService(_mapper, _data.Object, _vehicleService.Object),
                _userService.Object,
                new StatisticsService());

            //Act
            var result = await _service.GetVehicleHistoriesCostsData(_query) as VehicleHistoriesCosts;

            //Assert
            Assert.True(result.TotalFuelingCosts > 0);
            Assert.True(result.TotalRepairCosts > 0);
            Assert.True(result.TotalTaxesCosts > 0);
            result.ShouldDeepEqual(new VehicleHistoriesCosts()
            {
                TotalFuelingCosts = 300,
                TotalRepairCosts = 900,
                TotalTaxesCosts = 900
            });
        }

        [Fact]
        public async Task GetVehicleHistoriesCostsByMonthData_Returns_VehicleHistoriesByMonthCostsResponse_WithVehicleHistoriesByMonthsData()
        {
            //Arrange
            var expectedvehiclesWithHistoriesTestData = GetVehicleWithHistoriesTestData();
            var expectedVehicleHistoryData = GetExpectedVehicleHistoriesTotalsByMonthData();

            _vehicleService
                .Setup(x => x.GetVehiclesWithHistoriesByUserId(_userId))
                .ReturnsAsync(expectedvehiclesWithHistoriesTestData.AsQueryable());

            var _service = new ReportsService(
                _vehicleService.Object,
                new VehicleHistoryService(_mapper, _data.Object, _vehicleService.Object),
                _userService.Object,
                _statisticsService.Object);

            //Act
            var result = await _service.GetVehicleHistoriesCostsByMonthData(_query) as VehicleHistoriesByMonthCosts;

            //Assert
            Assert.True(result.VehicleHistoriesByMonthsData.Count > 0);
            result.VehicleHistoriesByMonthsData.ShouldDeepEqual(expectedVehicleHistoryData);
        }

        [Fact]
        public async Task GetVehiclesMilageGrowthData_Returns_MilageGrowthResponse_WithMilageByMonthData()
        {
            //Arrange
            var expectedvehiclesWithHistoriesTestData = GetVehicleWithHistoriesTestData();
            var expectedMilageByMonth = GetExpectedMilageTotalsByMonthData();

            _vehicleService
                .Setup(x => x.GetVehiclesWithHistoriesByUserId(_userId))
                .ReturnsAsync(expectedvehiclesWithHistoriesTestData.AsQueryable());

            var _service = new ReportsService(
                _vehicleService.Object,
                new VehicleHistoryService(_mapper, _data.Object, _vehicleService.Object),
                _userService.Object,
                _statisticsService.Object);

            //Act
            var result = await _service.GetVehiclesMilageGrowthData(_query) as MilageGrowth;

            //Assert
            Assert.True(result.MilageByMonthData.Count > 0);
            result.MilageByMonthData.ShouldDeepEqual(expectedMilageByMonth);
        }

        [Fact]
        public async Task GetFuelConsumptionsData_Returns_FuelConsumptionsResponse_WithFuelingsByTypeForMonthData()
        {
            //Arrange
            List<Vehicle> vehicles = GetVehiclesWithFuelingsByTypeData();
            var expectedFuelingsConsumptions = GetExpectedFuelingsByTypeForMonthData();

            _vehicleService
                .Setup(x => x.GetVehiclesWithHistoriesByUserId(_userId))
                .ReturnsAsync(vehicles.AsQueryable());

            var _service = new ReportsService(
               _vehicleService.Object,
               _vehicleHistoryService.Object,
               _userService.Object,
               new StatisticsService());

            //Act
            var result = await _service.GetFuelConsumptionsData(_query) as FuelConsumptions;

            //Assert
            Assert.True(result.FuelingsByTypeForMonthData.Count > 0);
            result.FuelingsByTypeForMonthData.ShouldDeepEqual(expectedFuelingsConsumptions);
        }

        [Fact]
        public async Task GetFuelPricesData_Returns_FuelPricesResponse_WithFuelingPricesByTypeForMonthData()
        {
            //Arrange
            var expectedvehiclesWithHistoriesTestData = GetVehicleWithHistoriesTestData();
            var expectedFuelingPricesByMonth = GetExpectedFuelingPricesByMonthData();

            _vehicleService
                .Setup(x => x.GetVehiclesWithHistoriesByUserId(_userId))
                .ReturnsAsync(expectedvehiclesWithHistoriesTestData.AsQueryable());

            //Act
            var result = await _service.GetFuelPricesData(_query) as FuelPrices;

            //Assert
            Assert.True(result.FuelingPricesByTypeForMonthData.Count > 0);
            result.FuelingPricesByTypeForMonthData.ShouldDeepEqual(expectedFuelingPricesByMonth);
        }

        private List<FuelingsByTypeForMonthData> GetExpectedFuelingsByTypeForMonthData()
        {
            var result = new List<FuelingsByTypeForMonthData>()
            {
                new FuelingsByTypeForMonthData
                {
                    Type = nameof(FuelType.Diesel),
                    ConsumptionsPerMonth = new List<ConsumptionPerMonth>()
                    {
                        new ConsumptionPerMonth { YearMonth = "2010/9", Consumption = 8.11M }
                    }
                },
                new FuelingsByTypeForMonthData
                {
                    Type = nameof(FuelType.Gasoline),
                    ConsumptionsPerMonth = new List<ConsumptionPerMonth>()
                    {
                        new ConsumptionPerMonth { YearMonth = "2011/10", Consumption = 13.33M }
                    }
                }
            };

            return result;
        }

        private List<MilageByMonthData> GetExpectedMilageTotalsByMonthData() =>
            new List<MilageByMonthData>()
            {
                new MilageByMonthData() { YearMonth = "2010/1", TotalMilage = 49},
                new MilageByMonthData() { YearMonth = "2010/8", TotalMilage = 18},
                new MilageByMonthData() { YearMonth = "2010/9", TotalMilage = 2000},
                new MilageByMonthData() { YearMonth = "2012/2", TotalMilage = 24},
                new MilageByMonthData() { YearMonth = "2012/5", TotalMilage = 24},
                new MilageByMonthData() { YearMonth = "2013/3", TotalMilage = 64},
                new MilageByMonthData() { YearMonth = "2014/4", TotalMilage = 44},
                new MilageByMonthData() { YearMonth = "2014/7", TotalMilage = 44}
            };

        private List<VehicleHistoriesByMonthData> GetExpectedVehicleHistoriesTotalsByMonthData()
        {
            return new List<VehicleHistoriesByMonthData>()
            {
                new VehicleHistoriesByMonthData() { TotalPriceTaxes = 300, TotalPriceFueling = 0, TotalPriceRepairs = 100, YearMonth = "2010/1"},
                new VehicleHistoriesByMonthData() { TotalPriceTaxes = 0, TotalPriceFueling = 0, TotalPriceRepairs = 200, YearMonth = "2010/8"},
                new VehicleHistoriesByMonthData() { TotalPriceTaxes = 0, TotalPriceFueling = 300, TotalPriceRepairs = 0, YearMonth = "2010/9"},
                new VehicleHistoriesByMonthData() { TotalPriceTaxes = 200, TotalPriceFueling = 0, TotalPriceRepairs = 0, YearMonth = "2012/2"},
                new VehicleHistoriesByMonthData() { TotalPriceTaxes = 0, TotalPriceFueling = 0, TotalPriceRepairs = 200, YearMonth = "2012/5"},
                new VehicleHistoriesByMonthData() { TotalPriceTaxes = 200, TotalPriceFueling = 0, TotalPriceRepairs = 200, YearMonth = "2013/3"},
                new VehicleHistoriesByMonthData() { TotalPriceTaxes = 200, TotalPriceFueling = 0, TotalPriceRepairs = 0, YearMonth = "2014/4"},
                new VehicleHistoriesByMonthData() { TotalPriceTaxes = 0, TotalPriceFueling = 0, TotalPriceRepairs = 200, YearMonth = "2014/7"},
            };
        }

        private List<FuelingPriceByTypeForMonthData> GetExpectedFuelingPricesByMonthData()
        {
            var result = new List<FuelingPriceByTypeForMonthData>()
            {
                new FuelingPriceByTypeForMonthData
                {
                    Type = nameof(FuelType.Diesel),
                    FuelingPricesPerMonth = new List<FuelingPricePerMonth>()
                    {
                        new FuelingPricePerMonth { Date = "2010-9-10", Price = 2M, Type = nameof(FuelType.Diesel) },
                        new FuelingPricePerMonth { Date = "2010-9-15", Price = 2M, Type = nameof(FuelType.Diesel) },
                        new FuelingPricePerMonth { Date = "2010-9-5", Price = 2M, Type = nameof(FuelType.Diesel) },
                    }
                }
            };
            result[0].FuelingPricesPerMonth = result[0].FuelingPricesPerMonth.OrderBy(x => DateTime.Parse(x.Date)).ToList();

            return result;
        }

        private List<Vehicle> GetVehicleWithHistoriesTestData()
        {
            return new List<Vehicle>()
            {
                new Vehicle()
                {
                    TaxVehicleHistories = new List<TaxVehicleHistory>()
                    {
                        new TaxVehicleHistory() { TotalPrice = 100, CurrentMilage = 20, CreatedOn = new DateTime(2012, 2, 2), Type = VehicleHistoryType.Tax},
                        new TaxVehicleHistory() { TotalPrice = 100, CurrentMilage = 24, CreatedOn = new DateTime(2012, 2, 3), Type = VehicleHistoryType.Tax},
                        new TaxVehicleHistory() { TotalPrice = 100, CurrentMilage = 30, CreatedOn = new DateTime(2013, 3, 3), Type = VehicleHistoryType.Tax},
                        new TaxVehicleHistory() { TotalPrice = 100, CurrentMilage = 34, CreatedOn = new DateTime(2013, 3, 5), Type = VehicleHistoryType.Tax},
                        new TaxVehicleHistory() { TotalPrice = 100, CurrentMilage = 40, CreatedOn = new DateTime(2014, 4, 4), Type = VehicleHistoryType.Tax},
                        new TaxVehicleHistory() { TotalPrice = 100, CurrentMilage = 44, CreatedOn = new DateTime(2014, 4, 25), Type = VehicleHistoryType.Tax},
                        new TaxVehicleHistory() { TotalPrice = 100, CurrentMilage = 10, CreatedOn = new DateTime(2010, 1, 1), Type = VehicleHistoryType.Tax},
                        new TaxVehicleHistory() { TotalPrice = 100, CurrentMilage = 19, CreatedOn = new DateTime(2010, 1, 5), Type = VehicleHistoryType.Tax},
                        new TaxVehicleHistory() { TotalPrice = 100, CurrentMilage = 18, CreatedOn = new DateTime(2010, 1, 10), Type = VehicleHistoryType.Tax}
                    },
                    RepairVehicleHistories = new List<RepairVehicleHistory>()
                    {
                        new RepairVehicleHistory() { TotalPrice = 100, CurrentMilage = 20, CreatedOn = new DateTime(2012, 5, 2), Type = VehicleHistoryType.Repair },
                        new RepairVehicleHistory() { TotalPrice = 100, CurrentMilage = 24, CreatedOn = new DateTime(2012, 5, 3), Type = VehicleHistoryType.Repair },
                        new RepairVehicleHistory() { TotalPrice = 100, CurrentMilage = 30, CreatedOn = new DateTime(2013, 3, 3), Type = VehicleHistoryType.Repair },
                        new RepairVehicleHistory() { TotalPrice = 100, CurrentMilage = 64, CreatedOn = new DateTime(2013, 3, 5), Type = VehicleHistoryType.Repair },
                        new RepairVehicleHistory() { TotalPrice = 100, CurrentMilage = 40, CreatedOn = new DateTime(2014, 7, 4), Type = VehicleHistoryType.Repair },
                        new RepairVehicleHistory() { TotalPrice = 100, CurrentMilage = 44, CreatedOn = new DateTime(2014, 7, 25), Type = VehicleHistoryType.Repair },
                        new RepairVehicleHistory() { TotalPrice = 100, CurrentMilage = 10, CreatedOn = new DateTime(2010, 8, 1), Type = VehicleHistoryType.Repair },
                        new RepairVehicleHistory() { TotalPrice = 100, CurrentMilage = 49, CreatedOn = new DateTime(2010, 1, 15), Type = VehicleHistoryType.Repair },
                        new RepairVehicleHistory() { TotalPrice = 100, CurrentMilage = 18, CreatedOn = new DateTime(2010, 8, 10), Type = VehicleHistoryType.Repair }
                    },
                    FuelingVehicleHistories = new List<FuelingVehicleHistory>()
                    {
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 2000, CreatedOn = new DateTime(2010, 9, 15), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = false, Liters = 40, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 1500, CreatedOn = new DateTime(2010, 9, 10), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = false, Liters = 40, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 1000, CreatedOn = new DateTime(2010, 9, 5), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = false, Liters = 20, PricePerLiter = 2 }
                    }
                }
            };
        }

        private static List<Vehicle> GetVehiclesWithFuelingsByTypeData()
        {
            return new List<Vehicle>()
            {
                new Vehicle
                {
                    FuelingVehicleHistories = new List<FuelingVehicleHistory>()
                    {
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 2600, CreatedOn = new DateTime(2010, 9, 30), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = true, Liters = 40, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 2400, CreatedOn = new DateTime(2010, 9, 29), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = false, Liters = 40, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 2000, CreatedOn = new DateTime(2010, 9, 15), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = false, Liters = 40, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 1500, CreatedOn = new DateTime(2010, 9, 10), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = false, Liters = 40, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 1000, CreatedOn = new DateTime(2010, 9, 5), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = false, Liters = 20, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 2000, CreatedOn = new DateTime(2010, 9, 1), Type = VehicleHistoryType.Fueling, Category = FuelType.Gasoline, IsPartial = false, Liters = 40, PricePerLiter = 2 },
                    }
                },
                new Vehicle
                {
                    FuelingVehicleHistories = new List<FuelingVehicleHistory>()
                    {
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 2700, CreatedOn = new DateTime(2010, 9, 30), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = true, Liters = 40, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 2600, CreatedOn = new DateTime(2010, 9, 29), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = false, Liters = 40, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 2000, CreatedOn = new DateTime(2010, 9, 15), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = false, Liters = 40, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 1500, CreatedOn = new DateTime(2010, 9, 10), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = false, Liters = 40, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 1000, CreatedOn = new DateTime(2010, 9, 5), Type = VehicleHistoryType.Fueling, Category = FuelType.Diesel, IsPartial = false, Liters = 20, PricePerLiter = 2 },
                        new FuelingVehicleHistory() { TotalPrice = 100, CurrentMilage = 2000, CreatedOn = new DateTime(2010, 9, 1), Type = VehicleHistoryType.Fueling, Category = FuelType.Gasoline, IsPartial = false, Liters = 40, PricePerLiter = 2 },
                    }
                },
                new Vehicle
                {
                    FuelingVehicleHistories = new List<FuelingVehicleHistory>()
                    {
                        new FuelingVehicleHistory { CurrentMilage = 100300, IsPartial = false, Liters = 40, PricePerLiter = It.IsAny<int>(), CreatedOn = new DateTime(2011, 10, 1),  TotalPrice = It.IsAny<decimal>(), Type = VehicleHistoryType.Fueling, Category = FuelType.Gasoline },
                        new FuelingVehicleHistory { CurrentMilage = 100600, IsPartial = false, Liters = 40, PricePerLiter = It.IsAny<int>(), CreatedOn = new DateTime(2011, 10, 10),  TotalPrice = It.IsAny<decimal>(), Type = VehicleHistoryType.Fueling, Category = FuelType.Gasoline },
                        new FuelingVehicleHistory { CurrentMilage = 100900, IsPartial = false, Liters = 40, PricePerLiter = It.IsAny<int>(), CreatedOn = new DateTime(2011, 10, 20),  TotalPrice = It.IsAny<decimal>(), Type = VehicleHistoryType.Fueling, Category = FuelType.Gasoline }
                    }
                }
            };
        }
    }
}
