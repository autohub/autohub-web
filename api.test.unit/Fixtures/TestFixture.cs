using System.Reflection;
using api.test.unit.Fakes;
using AutoMapper;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Moq;

namespace api.test.unit.Fixtures
{
    public class TestFixture
    {
        public IMapper Mapper { get; }
        public IStringLocalizer<Resources> Localizer { get; }

        public TestFixture()
        {
            Localizer = BuildLocalizer();
            Mapper = BuildIMapper();
        }

        public ILogger<T> GetLogger<T>() => new Mock<FakeLogger<T>>().Object;

        private IStringLocalizer<Resources> BuildLocalizer()
        {
            var options = Options.Create(new LocalizationOptions { ResourcesPath = "Resources" });
            var factory = new ResourceManagerStringLocalizerFactory(options, NullLoggerFactory.Instance);

            return new StringLocalizer<Resources>(factory);
        }

        private IMapper BuildIMapper()
        {
            var configuration = new MapperConfiguration(cfg =>
                cfg.AddMaps(Assembly.Load("api")));

            return configuration.CreateMapper();
        }
    }
}
