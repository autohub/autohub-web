using System.Threading;
using System.Threading.Tasks;
using api.Application.Account.Commands;
using api.Domain.Entities;
using api.Domain.Enums;
using api.Services.Users;
using Moq;
using Xunit;

namespace api.test.unit.Application.Account.Commands
{
    public class SetLanguageCommandHandlerTests
    {
        private readonly Mock<IUserService> _userService;

        public SetLanguageCommandHandlerTests()
        {
            _userService = new Mock<IUserService>();
        }

        [Fact]
        public async Task HandleSetLanguageCommand_Returns_SuccessfulResult()
        {
            //Arrange
            var command = new SetLanguageCommand
            {
                Language = nameof(SupportedLanguages.en)
            };

            _userService.Setup(x => x.GetCurrentUser())
                .ReturnsAsync(new ApplicationUser());

            _userService.Setup(x => x.UpdateUser(It.IsAny<ApplicationUser>()))
                .Returns(Task.CompletedTask);

            var sut = new SetLanguageCommandHandler(_userService.Object);

            // //Act
            var result = await sut.Handle(command, CancellationToken.None);

            // //Assert
            Assert.True(result.WasSuccessful);
        }
    }
}
