using System.Threading;
using System.Threading.Tasks;
using api.Application.Statistics.Queries;
using api.Services.Analytics;
using api.Services.Users;
using api.Services.Vehicles;
using api.test.unit.Fixtures;
using AutoMapper;
using Moq;
using Shouldly;
using Xunit;

namespace api.test.unit.Application.Statistics.Queries
{
    public class GetStatisticsQueryHandlerTests : IClassFixture<TestFixture>
    {
        private readonly Mock<IStatisticsService> _statisticsService;
        private readonly Mock<IVehicleService> _vehicleService;
        private readonly Mock<IUserService> _userService;
        private readonly IMapper _mapper;

        public GetStatisticsQueryHandlerTests(TestFixture fixture)
        {
            _statisticsService = new Mock<IStatisticsService>();
            _vehicleService = new Mock<IVehicleService>();
            _userService = new Mock<IUserService>();
            _mapper = fixture.Mapper;
        }

        [Fact]
        public async Task HandleGetStatisticsQuery_Returns_StatisticsResponse()
        {
            //Arrange
            var sut = new GetStatisticsQueryHandler(
                _vehicleService.Object,
                _userService.Object,
                _statisticsService.Object,
                _mapper);

            //Act
            var result = await sut.Handle(new GetStatisticsQuery(), CancellationToken.None);

            //Assert
            result.ShouldBeOfType<StatisticsResponse>();
            result.Vehicles.Count.ShouldBe(0);
            result.TotalFuelingVehicleHistories.ShouldBe(0);
            result.TotalRepairVehicleHistories.ShouldBe(0);
            result.TotalTaxVehicleHistories.ShouldBe(0);
            result.TotalVehicleHistories.ShouldBe(0);
            result.TotalVehicleHistoriesCost.ShouldBe(0);
            result.TotalVehiclesDistance.ShouldBe(0);
        }
    }
}
