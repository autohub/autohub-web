using System.Threading;
using System.Threading.Tasks;
using api.Application.Reports.Queries;
using Xunit;
using Shouldly;
using api.Domain.Enums;
using Moq;
using api.Application.Reports.Queries.GraphResponseTypes;
using System.Collections.Generic;
using api.Services.Analytics;

namespace api.test.unit.Application.Reports.Queries
{
    public class GetGraphReportQueryHandlerTests
    {
        private readonly Mock<IReportsService> _reportsService;

        public GetGraphReportQueryHandlerTests()
        {
            _reportsService = new Mock<IReportsService>();
        }

        [Fact]
        public async Task HandleGetGraphReportQuery_Returns_VehicleHistoriesCostsResponse()
        {
            //Arrange
            var query = new GetGraphReportQuery
            {
                GraphType = nameof(GraphReportTypes.TotalVehicleHistoriesCosts)
            };

            _reportsService.Setup(x => x.GetVehicleHistoriesCostsData(query))
                .ReturnsAsync(new VehicleHistoriesCosts());

            var sut = new GetGraphReportQueryHandler(_reportsService.Object);

            //Act
            var result = await sut.Handle(query, CancellationToken.None) as VehicleHistoriesCosts;

            //Assert
            result.ShouldBeOfType<VehicleHistoriesCosts>();
            result.TotalFuelingCosts.ShouldBe(0);
            result.TotalRepairCosts.ShouldBe(0);
            result.TotalTaxesCosts.ShouldBe(0);
        }

        [Fact]
        public async Task HandleGraphReportQuery_Returns_VehicleHistoriesCostsByMonthResponse()
        {
            //Arrange
            var query = new GetGraphReportQuery
            {
                GraphType = nameof(GraphReportTypes.TotalVehicleHistoriesCostsByMonth)
            };

            _reportsService.Setup(x => x.GetVehicleHistoriesCostsByMonthData(query))
                .ReturnsAsync(new VehicleHistoriesByMonthCosts());

            var sut = new GetGraphReportQueryHandler(_reportsService.Object);

            //Act
            var result = await sut.Handle(query, CancellationToken.None) as VehicleHistoriesByMonthCosts;

            //Assert
            result.ShouldBeOfType<VehicleHistoriesByMonthCosts>();
            result.VehicleHistoriesByMonthsData.Count.ShouldBe(0);
        }

        [Fact]
        public async Task HandleGraphReportQuery_Returns_MilageGrowthResponse()
        {
            //Arrange
            var query = new GetGraphReportQuery
            {
                GraphType = nameof(GraphReportTypes.MilageGrowth)
            };

            _reportsService.Setup(x => x.GetVehiclesMilageGrowthData(query))
                .ReturnsAsync(new MilageGrowth()
                {
                    MilageByMonthData = new List<MilageByMonthData>
                    {
                        new MilageByMonthData()
                        {
                            YearMonth = "2020/05",
                            TotalMilage = 1000
                        }
                    }
                });

            var sut = new GetGraphReportQueryHandler(_reportsService.Object);

            //Act
            var result = await sut.Handle(query, CancellationToken.None) as MilageGrowth;

            //Assert
            result.ShouldBeOfType<MilageGrowth>();
            result.MilageByMonthData.Count.ShouldBe(1);
        }

        [Fact]
        public async Task HandleGraphReportQuery_Returns_FuelConsumptionsResponse()
        {
            //Arrange
            var query = new GetGraphReportQuery
            {
                GraphType = nameof(GraphReportTypes.FuelConsumptions)
            };

            _reportsService.Setup(x => x.GetFuelConsumptionsData(query))
                .ReturnsAsync(new FuelConsumptions());

            var sut = new GetGraphReportQueryHandler(_reportsService.Object);

            //Act
            var result = await sut.Handle(query, CancellationToken.None) as FuelConsumptions;

            //Assert
            result.ShouldBeOfType<FuelConsumptions>();
            result.FuelingsByTypeForMonthData.Count.ShouldBe(0);
        }

        [Fact]
        public async Task HandleGraphReportQuery_Returns_FuelPricesResponse()
        {
            //Arrange
            var query = new GetGraphReportQuery
            {
                GraphType = nameof(GraphReportTypes.FuelPrices)
            };

            _reportsService.Setup(x => x.GetFuelPricesData(query))
                .ReturnsAsync(new FuelPrices());

            var sut = new GetGraphReportQueryHandler(_reportsService.Object);

            //Act
            var result = await sut.Handle(query, CancellationToken.None) as FuelPrices;

            //Assert
            result.ShouldBeOfType<FuelPrices>();
            result.FuelingPricesByTypeForMonthData.Count.ShouldBe(0);
        }
    }
}
