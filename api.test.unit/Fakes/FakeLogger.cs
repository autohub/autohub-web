using System;
using Microsoft.Extensions.Logging;

namespace api.test.unit.Fakes
{
    public class FakeLogger<T> : ILogger<T>
    {
        public void LogWarning(string msg, params object[] args)
        {
        }

        public void LogError(string msg)
        {
        }

        public IDisposable BeginScope<TState>(TState state)
        {
            throw new NotImplementedException();
        }

        public bool IsEnabled(LogLevel logLevel)
        {
            return true;
        }

        public void Log<TState>(LogLevel logLevel, EventId eventId, TState state, Exception exception, Func<TState, Exception, string> formatter)
        {
        }
    }
}
