﻿using System;
using System.Threading.Tasks;
using DeepEqual.Syntax;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using api.Domain.Models.Auth;
using api.Common;
using api.Domain.Entities;
using api.Domain.Models.Responses.Auth;
using api.Controllers.Public;
using api.test.unit.Fixtures;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Localization;
using api.Services.Users;

namespace api.test.unit.Controllers.Public
{
    public class AuthControllerTests : IClassFixture<TestFixture>
    {
        private readonly AuthController _controller;
        private readonly Mock<IUserService> _userServiceMock;
        private readonly ILogger<AuthController> _logger;
        private readonly IStringLocalizer<Resources> _localizer;

        public AuthControllerTests(TestFixture fixture)
        {
            _logger = fixture.GetLogger<AuthController>();
            _localizer = fixture.Localizer;
            _userServiceMock = new Mock<IUserService>();

            _controller = new AuthController(
                _userServiceMock.Object,
                _logger,
                _localizer);
        }

        [Fact]
        public async Task Post_Login_BadRequestObjectResult_WhenNoSuchUser()
        {
            _userServiceMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync((ApplicationUser)null);

            object expectedObjectException = new { general = ErrorMessages.LoginWrongEmailOrPassword };

            // Act
            var result = await _controller.Login(
                new LoginUserRequest
                {
                    Username = "pavel",
                    Password = "pafkata"
                }) as BadRequestObjectResult;

            //Assert
            Assert.True(result.StatusCode == 400);
            Assert.IsType<BadRequestObjectResult>(result);
            result.Value.ShouldDeepEqual(expectedObjectException);
        }

        [Fact]
        public async Task Post_Login_BadRequestObjectResult_WhenPasswordIsNotCorrect()
        {
            _userServiceMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync(new ApplicationUser() { Name = "Pavel" });

            _userServiceMock
                .Setup(x => x.IsValidPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(false);

            //Arrange
            object expectedObjectException = new
            {
                general = ErrorMessages.LoginWrongEmailOrPassword
            };

            //Act
            var result = await _controller.Login(
                new LoginUserRequest
                {
                    Username = "pavel",
                    Password = "pafkata"
                }) as BadRequestObjectResult;

            //Assert
            Assert.True(result.StatusCode == 400);
            Assert.IsType<BadRequestObjectResult>(result);
            result.Value.ShouldDeepEqual(expectedObjectException);
        }

        [Fact]
        public async Task Post_Login_BadRequestObjectResult_WhenEmailIsNotConfirmed()
        {
            // Arrange
            _userServiceMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync(new ApplicationUser() { Name = "Pavel" });

            _userServiceMock
                .Setup(x => x.IsValidPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(true);

            _userServiceMock
                .Setup(x => x.CanSignInAsync(It.IsAny<ApplicationUser>()))
                .ReturnsAsync(false);

            var expectedObjectException = new { general = ErrorMessages.LoginNotConfirmedEmail };

            // Act
            var result = await _controller.Login(
                new LoginUserRequest
                {
                    Username = "pavel",
                    Password = "pafkata"
                }) as BadRequestObjectResult;

            // Assert
            Assert.True(result.StatusCode == 400);
            Assert.IsType<BadRequestObjectResult>(result);
            result.Value.ShouldDeepEqual(expectedObjectException);
        }

        [Fact]
        public async Task Post_Login_OkObjectResult_WithUserTokenAndInfo()
        {
            // Arrange
            _userServiceMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .ReturnsAsync(new ApplicationUser()
                {
                    Id = "4da986s1d65da",
                    Name = "Pavel",
                    Email = "aaa@abv.bg"
                });

            _userServiceMock
                .Setup(x => x.IsValidPasswordAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .ReturnsAsync(true);

            _userServiceMock
                .Setup(x => x.CanSignInAsync(It.IsAny<ApplicationUser>()))
                .ReturnsAsync(true);

            _userServiceMock
                .Setup(x => x.GenerateLoginToken(It.IsAny<ApplicationUser>()))
                .Returns(It.IsAny<string>());

            var expectedObject = new
            {
                Token = "",
                Username = "Pavel",
                UserEmail = "aaa@abv.bg"
            };

            // Act
            var result = await _controller.Login(
                new LoginUserRequest
                {
                    Username = "pavel",
                    Password = "pafkata"
                }) as OkObjectResult;

            // Assert
            Assert.True(result.StatusCode == 200);
            Assert.IsType<OkObjectResult>(result);
            Assert.IsType<LoginUserResponse>(result.Value);
            result.Value.WithDeepEqual(expectedObject).IgnoreDestinationProperty(x => x.Token).Assert();
        }

        [Fact]
        public async Task Post_Register_BadRequestObjectResult_WhenIdentityErrorIsRaised()
        {
            //Arrange
            _userServiceMock
                .Setup(x => x.RegisterUser(It.IsAny<RegisterUserRequest>()))
                .ReturnsAsync(IdentityResult.Failed(new IdentityError()));

            var user = new RegisterUserRequest
            {
                Username = "pavel",
                Name = "pavel",
                UserType = "Personal"
            };

            // Act
            var result = await _controller.Register(user) as BadRequestObjectResult;

            // Asserts
            Assert.True(result.StatusCode == 400);
            Assert.IsType<BadRequestObjectResult>(result);
        }

        [Fact]
        public async Task Post_Register_OkResult()
        {
            // Arrange
            _userServiceMock
                .Setup(x => x.RegisterUser(It.IsAny<RegisterUserRequest>()))
                .ReturnsAsync(IdentityResult.Success);

            var user = new RegisterUserRequest
            {
                Username = "pavel",
                Name = "pavel",
                UserType = "Personal"
            };

            // Act
            var result = await _controller.Register(user) as OkResult;

            // Assert
            Assert.True(result.StatusCode == 200);
            Assert.IsType<OkResult>(result);
        }

        [Fact]
        public async Task Post_ResetPasswordRequest_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            _userServiceMock
                .Setup(x => x.ResetPasswordRequest(It.IsAny<string>()))
                .Throws(new SystemException());

            var expectedObject = new { general = ErrorMessages.UnexpectedError };

            // Act
            var result = await _controller.ResetPasswordRequest(new ResetPasswordRequest())
                as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
            result.Value.ShouldDeepEqual(expectedObject);
        }

        [Fact]
        public async Task Post_ResetPasswordRequest_BadRequestObjectResult_WhenArgumentExceptionIsThrown()
        {
            // Arrange
            _userServiceMock
                .Setup(x => x.ResetPasswordRequest(It.IsAny<string>()))
                .Throws(new ArgumentException());

            // Act
            var result = await _controller.ResetPasswordRequest(new ResetPasswordRequest())
                as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Post_ResetPasswordRequest_OkResult()
        {
            // Arrange
            _userServiceMock
                .Setup(us => us.ResetPasswordRequest(It.IsAny<string>()))
                .Returns(Task.FromResult(true));

            var resetPasswordRequest = new ResetPasswordRequest() { Email = "email" };

            // Act
            var result = await _controller.ResetPasswordRequest(resetPasswordRequest) as OkResult;

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.True(result.StatusCode == 200);
        }

        [Fact]
        public async Task Post_ResetPassword_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            _userServiceMock
                .Setup(x => x.ResetPassword(It.IsAny<ResetPasswordTokenRequest>()))
                .Throws(new SystemException());

            var expectedObject = new { general = ErrorMessages.UnexpectedError };

            // Act
            var result = await _controller.ResetPassword(new ResetPasswordTokenRequest())
                as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
            result.Value.ShouldDeepEqual(expectedObject);
        }


        [Fact]
        public async Task Post_ResetPassword_OkResult()
        {
            // Arrange
            _userServiceMock
                .Setup(us => us.ResetPassword(It.IsAny<ResetPasswordTokenRequest>()))
                .Returns(Task.FromResult(true));

            var resetPasswordRequest = new ResetPasswordTokenRequest();

            // Act
            var result = await _controller.ResetPassword(resetPasswordRequest) as OkResult;

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.True(result.StatusCode == 200);
        }

        [Fact]
        public async Task Get_Confirm_RedirectResult_WhenNoUserFound()
        {
            // Arrange
            _userServiceMock
                .Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                .ReturnsAsync((ApplicationUser)null);

            // Act
            var result = await _controller.Confirm(It.IsAny<string>(), It.IsAny<string>())
                as RedirectResult;

            // Assert
            Assert.IsType<RedirectResult>(result);
            Assert.True(result.Url == "/error/email-confirm");
        }

        [Fact]
        public async Task Get_Confirm_RedirectResult_WhenConfirmFails()
        {
            // Arrange
            _userServiceMock
                .Setup(x => x.ConfirmEmailAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Failed(new IdentityError())));

            // Act
            var result = await _controller.Confirm(It.IsAny<string>(), It.IsAny<string>())
                as RedirectResult;

            // Assert
            Assert.IsType<RedirectResult>(result);
            Assert.True(result.Url == "/error/email-confirm");
        }

        [Fact]
        public async Task Get_Confirm_RedirectResult_WhenConfirmSucceeded()
        {
            // Arrange
            _userServiceMock
                .Setup(x => x.ConfirmEmailAsync(It.IsAny<ApplicationUser>(), It.IsAny<string>()))
                .Returns(Task.FromResult(IdentityResult.Success));

            _userServiceMock
                .Setup(x => x.FindByIdAsync(It.IsAny<string>()))
                .ReturnsAsync(new ApplicationUser());

            // Act
            var result = await _controller.Confirm(It.IsAny<string>(), It.IsAny<string>())
                as RedirectResult;

            // Assert
            Assert.IsType<RedirectResult>(result);
            Assert.True(result.Url == "/login/?confirmed=1");
        }
    }
}
