using System;
using System.Threading.Tasks;
using api.Controllers.Public;
using api.Domain.Entities;
using api.Domain.Models.Requests.Auth;
using api.Domain.Models.Responses.Auth;
using api.Common;
using api.test.unit.Fixtures;
using DeepEqual.Syntax;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using api.Services.Users;
using api.Services;

namespace api.test.unit.Controllers.Public
{
    public class ExternalAuthControllerTests : IClassFixture<TestFixture>
    {
        private readonly ExternalAuthController _controller;
        private readonly Mock<IUserService> _userServiceMock = new Mock<IUserService>();
        private readonly Mock<IAuthService> _authServiceMock = new Mock<IAuthService>();

        public ExternalAuthControllerTests(TestFixture fixture)
        {
            _controller = new ExternalAuthController(
                _userServiceMock.Object,
                _authServiceMock.Object,
                fixture.Localizer);
        }

        [Fact]
        public async Task Post_FacebookLogin_Returns_OkObjectResult_WithUserInfo_WhenSuccessResult()
        {
            //Arrange
            const string email = "georgi.marokov@gmail.com";
            const string username = "Georgi Marokov";
            const string fbId = "3025402804156318";

            _authServiceMock
               .Setup(x => x.LoginOrRegisterUser(It.IsAny<FacebookLoginRequest>()))
               .ReturnsAsync(Result<ApplicationUser>.Success(new ApplicationUser() { Email = email, Name = username }));

            var request = new FacebookLoginRequest
            {
                Name = username,
                Email = email,
                Id = fbId,
                AccessToken = "EAAH6U1iMZB5MBAIjijmoWZBieIYblIRBakLYXZCZChPJJU6fptPZC4OIsVHTjY7Hq7nLDM9D2DVnwzkC1s5Tav7wange6zgATxE2oHzE8DYINRtar0aOz3EpvCHMo1hRj0ZCfulWCQppqjEuA903ZAaVShMtaYUbo2vp41eFB038OeKpvFW0aNl84eRWsG8ajZCyWpIFuURZCqAZDZD",
                UserID = "3025402804156318",
                ExpiresIn = 6671,
                SignedRequest = "E5FW293ajJgKF35zk42MyuxPF1gPmQJIQqmbmfw3igk.eyJ1c2VyX2lkIjoiMzAyNTQwMjgwNDE1NjMxOCIsImNvZGUiOiJBUUJUQTVGYjVpbmoxdGZkWFRFX2ZvcUg1dHhkUlB3VURxcUE2NG5EU0tvZWRRN2RmbWhWNzY4dTV0TjRyM0xJVWJENmdOTllaYTRqUGpTUXk4elc3VXN0NG1zMmtmUF8zelljeGNvSkI5M1pXLUJtVUtzcjhhaVJiajhYOFoyc2tGeE5kZVhad0xMbGQzMF9mSnl1VlFQOElXSEQ4T0hkWTI5NHJOcmk4WXdiZUJxbkNUdkpPaWZIeTNKR0JyZUpMNmowUkZZeTRuTnRVTUhUdTFCU0lDUnlEUzRFWWdGQUsyczZJSWs0T3lkVjA4Y0Z6cmxWeXk1QlJTSldYRGdZSWtrUm84bTFGUm5helBuMjhROXdSUXg0RVJYSmNBd3NOcVQzdlFhSmVCWGpaak1pRWk2ZUlEcHVmNXpHN3o5d2EtWnA0eXF6NVFHVjl4QW1raFVGRW5SRyIsImFsZ29yaXRobSI6IkhNQUMtU0hBMjU2IiwiaXNzdWVkX2F0IjoxNTc4NTIxMzI5fQ",
                DataAccessExpirationTime = 1586297329,
            };

            var expectedObject = new
            {
                Token = "",
                Username = username,
                UserEmail = email
            };

            //Act
            var result = await _controller.FacebookLogin(request) as OkObjectResult;

            // Assert
            Assert.True(result.StatusCode == 200);
            Assert.IsType<OkObjectResult>(result);
            Assert.IsType<LoginUserResponse>(result.Value);
            result.Value.WithDeepEqual(expectedObject).IgnoreDestinationProperty(x => x.Token).Assert();
        }

        [Fact]
        public async Task Post_FacebookLogin_Returns_BadRequestObjectResult_WhenFailedResult()
        {
            //Arrange
            _authServiceMock
                .Setup(x => x.LoginOrRegisterUser(It.IsAny<FacebookLoginRequest>()))
                .ReturnsAsync(Result<ApplicationUser>.Fail(ErrorMessages.UnexpectedError));

            object expectedObjectException = new { general = ErrorMessages.UnexpectedError };

            // Act
            var result = await _controller.FacebookLogin(
                new FacebookLoginRequest
                {
                    AccessToken = "",
                    Name = "",
                    Email = "",
                    Id = "",
                    UserID = "",
                    ExpiresIn = 0,
                    SignedRequest = "",
                    DataAccessExpirationTime = 0,
                }) as BadRequestObjectResult;

            //Assert
            Assert.True(result.StatusCode == 400);
            Assert.IsType<BadRequestObjectResult>(result);
            result.Value.ShouldDeepEqual(expectedObjectException);
        }

        [Fact]
        public async Task Post_FacebookLogin_Returns_BadRequestObjectResult_WhenExceptionThrown()
        {
            //Arrange
            _authServiceMock
                .Setup(x => x.LoginOrRegisterUser(It.IsAny<FacebookLoginRequest>()))
                .ThrowsAsync(new SystemException());

            object expectedObjectException = new { general = ErrorMessages.UnexpectedError };

            // Act
            var result = await _controller.FacebookLogin(
                new FacebookLoginRequest
                {
                    AccessToken = "",
                    Name = "",
                    Email = "",
                    Id = "",
                    UserID = "",
                    ExpiresIn = 0,
                    SignedRequest = "",
                    DataAccessExpirationTime = 0,
                }) as BadRequestObjectResult;

            //Assert
            Assert.True(result.StatusCode == 400);
            Assert.IsType<BadRequestObjectResult>(result);
            result.Value.ShouldDeepEqual(expectedObjectException);
        }

        [Fact]
        public async Task Post_GoogleLogin_Returns_BadRequestObjectResult_WhenExceptionThrown()
        {
            //Arrange
            var request = new GoogleLoginRequest
            {
                IdToken = Guid.NewGuid().ToString()
            };

            _authServiceMock
                .Setup(x => x.LoginOrRegisterUser(request))
                .ThrowsAsync(new SystemException());

            object expectedObjectException = new { general = ErrorMessages.UnexpectedError };

            // Act
            var result = await _controller.GoogleLogin(request) as BadRequestObjectResult;

            //Assert
            Assert.True(result.StatusCode == 400);
            Assert.IsType<BadRequestObjectResult>(result);
            result.Value.ShouldDeepEqual(expectedObjectException);
        }

        [Fact]
        public async Task Post_GoogleLogin_Returns_BadRequestObjectResult_WhenFailedResult()
        {
            //Arrange
            var request = new GoogleLoginRequest
            {
                IdToken = Guid.NewGuid().ToString()
            };

            _authServiceMock
                .Setup(x => x.LoginOrRegisterUser(request))
                .ReturnsAsync(Result<ApplicationUser>.Fail(ErrorMessages.UnexpectedError));

            object expectedObjectException = new { general = ErrorMessages.UnexpectedError };

            // Act
            var result = await _controller.GoogleLogin(request) as BadRequestObjectResult;

            //Assert
            Assert.True(result.StatusCode == 400);
            Assert.IsType<BadRequestObjectResult>(result);
            result.Value.ShouldDeepEqual(expectedObjectException);
        }

        [Fact]
        public async Task Post_GoogleLogin_Returns_OkObjectResult_WithUserInfo()
        {
            //Arrange
            const string email = "georgi.marokov@gmail.com";
            const string name = "Georgi Marokov";

            var response = Result<ApplicationUser>.Success(new ApplicationUser()
            {
                Email = email,
                Name = name
            });

            _authServiceMock
               .Setup(x => x.LoginOrRegisterUser(It.IsAny<GoogleLoginRequest>()))
               .ReturnsAsync(response);

            var request = new GoogleLoginRequest
            {
                IdToken = "EAAH6U1iMZB5MBAIjijmoWZBieIYblIRBakLYXZCZChPJJU6fptPZC4OIsVHTjY7Hq7nLDM9D2DVnwzkC1s5Tav7wange6zgATxE2oHzE8DYINRtar0aOz3EpvCHMo1hRj0ZCfulWCQppqjEuA903ZAaVShMtaYUbo2vp41eFB038OeKpvFW0aNl84eRWsG8ajZCyWpIFuURZCqAZDZD",
            };

            var expectedObject = new
            {
                Token = "",
                Username = name,
                UserEmail = email
            };

            //Act
            var result = await _controller.GoogleLogin(request) as OkObjectResult;

            // Assert
            Assert.True(result.StatusCode == 200);
            Assert.IsType<OkObjectResult>(result);
            Assert.IsType<LoginUserResponse>(result.Value);
            result.Value.WithDeepEqual(expectedObject).IgnoreDestinationProperty(x => x.Token).Assert();
        }
    }
}
