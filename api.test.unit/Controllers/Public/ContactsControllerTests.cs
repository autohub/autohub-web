using System;
using System.Threading.Tasks;
using api.Controllers;
using api.Domain.Models;
using api.Common;
using api.test.unit.Fakes;
using DeepEqual.Syntax;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Abstractions;
using Microsoft.Extensions.Options;
using Moq;
using Xunit;
using api.Infrastructure.Email;

namespace api.test.unit.Controllers.Public
{
    public class ContactsControllerTests
    {
        private readonly ContactsController _controller;
        private readonly Mock<IEmailSender> _emailSenderMock = new Mock<IEmailSender>();
        private readonly Mock<ILoggerFactory> _loggerFactoryMock;

        public ContactsControllerTests()
        {
            var options = Options.Create(new LocalizationOptions { ResourcesPath = "Resources" });
            var factory = new ResourceManagerStringLocalizerFactory(options, NullLoggerFactory.Instance);
            var localizer = new StringLocalizer<Resources>(factory);

            var loggerMock = new Mock<FakeLogger<ContactsController>>();
            _loggerFactoryMock = new Mock<ILoggerFactory>();
            _loggerFactoryMock
                .Setup(x => x.CreateLogger(It.IsAny<string>()))
                .Returns(loggerMock.Object);

            _controller = new ContactsController(
                _emailSenderMock.Object,
                _loggerFactoryMock.Object,
                localizer);
        }

        [Fact]
        public async Task Post_BadRequestObjectResult_WhenExceptionIsThrownAsync()
        {
            //Arrange
            _emailSenderMock
                .Setup(x => x.SendEmailAsync(It.IsAny<EmailModel>()))
                .ThrowsAsync(new Exception());

            var expected = new { general = ErrorMessages.UnexpectedError };

            //Act
            var result = await _controller.PostAsync(new ContactRequest()) as BadRequestObjectResult;

            //Assert
            Assert.True(result.StatusCode == 400);
            Assert.IsType<BadRequestObjectResult>(result);
            result.Value.ShouldDeepEqual(expected);
        }

        [Fact]
        public async Task Post_OkResult()
        {
            //Arrange
            _emailSenderMock
                .Setup(x => x.SendEmailAsync(It.IsAny<EmailModel>()))
                .Returns(Task.CompletedTask);

            //Act
            var result = await _controller.PostAsync(new ContactRequest()) as OkResult;

            //Assert
            Assert.True(result.StatusCode == 200);
            Assert.IsType<OkResult>(result);
        }
    }
}
