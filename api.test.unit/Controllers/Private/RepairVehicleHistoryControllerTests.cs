using System;
using System.Threading.Tasks;
using api.Domain.Models.VehicleHistory;
using DeepEqual.Syntax;
using Microsoft.AspNetCore.Mvc;
using api.Controllers.Private;
using Moq;
using Xunit;
using api.Domain.Exceptions;
using api.test.unit.Fixtures;
using api.Services.VehicleHistories;

namespace api.test.unit.Controllers.Private
{
    public class RepairVehicleHistoryControllerTests : BasePrivateTestController, IClassFixture<TestFixture>
    {
        private readonly RepairVehicleHistoriesController _controller;
        private readonly Mock<IRepairVehicleHistoryService> _mockRepairVehicleHistoryService;

        public RepairVehicleHistoryControllerTests(TestFixture fixture)
        {
            _mockRepairVehicleHistoryService = new Mock<IRepairVehicleHistoryService>();

            _controller = new RepairVehicleHistoriesController(
                _mockRepairVehicleHistoryService.Object,
                VehicleHistoryServiceMock.Object,
                VehicleServiceMock.Object,
                UserServiceMock.Object,
                fixture.Localizer);

            SetupController(ref _controller);
        }

        [Fact]
        public async Task Get_ObjectResult()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            // Act
            var result = await _controller.GetAsync(new Guid());

            // // Assert
            Assert.IsType<ObjectResult>(result);
        }

        [Fact]
        public async Task Get_UnauthorizedResult_WhenNotOwnedVehicleHistory()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            // Act
            var result = await _controller.GetAsync(new Guid()) as UnauthorizedResult;

            // // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Get_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _mockRepairVehicleHistoryService
                .Setup(x => x.GetRepairVehicleHistoryAsync(It.IsAny<Guid>()))
                .Throws(new SystemException());

            // Act
            var result = await _controller.GetAsync(new Guid()) as BadRequestObjectResult;

            // // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public void Post_ShouldThrowException()
        {
            // Act & Assert
            Assert.ThrowsAnyAsync<Exception>(() => _controller.PostAsync(new UpsertRepairVehicleHistoryRequest()));
        }

        [Fact]
        public async Task Post_UnauthorizedResult_WhenNotOwnedVehicle()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            var model = new UpsertRepairVehicleHistoryRequest();

            // Act
            var result = await _controller.PostAsync(model) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Post_CreatedAtRouteResult_WithModel()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _mockRepairVehicleHistoryService
                .Setup(x => x.CreateRepairVehicleHistoryAsync(It.IsAny<UpsertRepairVehicleHistoryRequest>()))
                .ReturnsAsync(new Guid());

            var model = new UpsertRepairVehicleHistoryRequest
            {
                Id = new Guid()
            };

            var expectedObject = new CreatedAtRouteResult(
                "GetRepairVehicleHistory",
                new { id = model.Id },
                model);

            // Act
            var result = await _controller.PostAsync(model) as CreatedAtRouteResult;

            // Assert
            Assert.IsType<CreatedAtRouteResult>(result);
            result.ShouldDeepEqual(expectedObject);
            Assert.True(result.StatusCode == 201);
        }

        [Fact]
        public async Task Post_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _mockRepairVehicleHistoryService
                .Setup(x => x.CreateRepairVehicleHistoryAsync(It.IsAny<UpsertRepairVehicleHistoryRequest>()))
                .Throws(new SystemException());

            var model = new UpsertRepairVehicleHistoryRequest();

            // Act
            var result = await _controller.PostAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Post_BadRequestObjectResult_WhenDomainExceptionIsThrown()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _mockRepairVehicleHistoryService
                .Setup(x => x.CreateRepairVehicleHistoryAsync(It.IsAny<UpsertRepairVehicleHistoryRequest>()))
                .Throws(new DomainException());

            var model = new UpsertRepairVehicleHistoryRequest();

            // Act
            var result = await _controller.PostAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Put_UnauthorizedResult_WhenNotOwnedVehicleHistory()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            var model = new UpsertRepairVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Put_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _mockRepairVehicleHistoryService
                .Setup(x => x.UpdateRepairVehicleHistoryAsync(It.IsAny<UpsertRepairVehicleHistoryRequest>()))
                .Throws(new SystemException());

            var model = new UpsertRepairVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Put_BadRequestObjectResult_WhenDomainExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _mockRepairVehicleHistoryService
                .Setup(x => x.UpdateRepairVehicleHistoryAsync(It.IsAny<UpsertRepairVehicleHistoryRequest>()))
                .Throws(new DomainException());

            var model = new UpsertRepairVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Put_AcceptedResult()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _mockRepairVehicleHistoryService
                .Setup(x => x.UpdateRepairVehicleHistoryAsync(It.IsAny<UpsertRepairVehicleHistoryRequest>()))
                .Returns(Task.FromResult(true));

            var model = new UpsertRepairVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as AcceptedResult;

            // Assert
            Assert.IsType<AcceptedResult>(result);
            Assert.True(result.StatusCode == 202);
        }

        [Fact]
        public async Task Delete_OkResult()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _mockRepairVehicleHistoryService
                .Setup(x => x.DeleteRepairVehicleHistoryAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(true));

            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id) as OkResult;

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.True(result.StatusCode == 200);
        }

        [Fact]
        public async Task Delete_UnauthorizedResult()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Delete_BadResultObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _mockRepairVehicleHistoryService
                .Setup(x => x.DeleteRepairVehicleHistoryAsync(It.IsAny<Guid>()))
                .Throws(new SystemException());

            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
