﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Controllers.Private;
using api.Domain.Exceptions;
using api.Domain.Models.Responses.Vehicle;
using api.Domain.Models.Vehicle;
using api.Domain.Models.VehicleHistory;
using api.test.unit.Fixtures;
using DeepEqual.Syntax;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace api.test.unit.Controllers.Private
{
    public class VehiclesControllerTests : BasePrivateTestController, IClassFixture<TestFixture>
    {
        private readonly VehiclesController _controller;

        public VehiclesControllerTests(TestFixture fixture)
        {
            _controller = new VehiclesController(
                VehicleServiceMock.Object,
                UserServiceMock.Object,
                fixture.Localizer);

            SetupController(ref _controller);
        }

        [Fact]
        public async Task Get_ObjectResult_WithListOfVehicles()
        {
            // Arrange
            var returnObject = new List<ListVehicleResponse>
            {
                new ListVehicleResponse()
            };
            var obj = returnObject.ToList();

            VehicleServiceMock
                .Setup(x => x.GetVehiclesOwnedByMe(It.IsAny<string>()))
                .Returns(Task.FromResult(obj as IEnumerable<ListVehicleResponse>));

            // Act
            var result = await _controller.GetAsync() as ObjectResult;

            // Assert
            var vehiclesList = result.Value as List<ListVehicleResponse>;
            Assert.True(vehiclesList.Count > 0);
            Assert.True(vehiclesList.All(x => x.RemindableVehicleHistories.Count == 0));
        }

        [Fact]
        public async Task Get_ObjectResult_WithEmptyList()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.GetVehiclesOwnedByMe(It.IsAny<string>()))
                .Returns(Task.FromResult(new List<ListVehicleResponse>() as IEnumerable<ListVehicleResponse>));

            // Act
            var result = await _controller.GetAsync() as ObjectResult;

            // Assert
            var vehiclesList = result.Value as List<ListVehicleResponse>;
            Assert.False(vehiclesList.Count > 0);
        }

        [Fact]
        public async Task Get_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.GetVehiclesOwnedByMe(It.IsAny<string>()))
                .Throws(new SystemException());

            // Act
            var result = await _controller.GetAsync() as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Get_WithGuidSignature_ObjectResult()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleServiceMock
                .Setup(x => x.GetVehicleAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(
                    new DetailsVehicleResponse()
                    {
                        RemindableVehicleHistories = new List<BaseRemindableVehicleHistoryModel>()
                    })
                );

            // Act
            var result = await _controller.GetAsync(new Guid()) as ObjectResult;

            // Assert
            Assert.IsType<ObjectResult>(result);
            var resultDetails = result.Value as DetailsVehicleResponse;
            Assert.True(resultDetails.RemindableVehicleHistories.Count == 0);
        }

        [Fact]
        public async Task Get_WitHGuidSignature_UnauthorizedResult_WhenNotOwnedVehicle()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            VehicleServiceMock
                .Setup(x => x.GetVehicleAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(new DetailsVehicleResponse()));

            // Act
            var result = await _controller.GetAsync(new Guid()) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Get_WithGuidSignature_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleServiceMock
                .Setup(x => x.GetVehicleAsync(It.IsAny<Guid>()))
                .Throws(new SystemException());

            // Act
            var result = await _controller.GetAsync(new Guid()) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Post_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleServiceMock
                .Setup(x => x.CreateVehicleAsync(It.IsAny<CreateVehicleRequest>()))
                .Throws(new SystemException());

            var model = new CreateVehicleRequest();

            // Act
            var result = await _controller.PostAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Post_BadRequestObjectResult_WhenDomainExceptionIsThrown()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleServiceMock
                .Setup(x => x.CreateVehicleAsync(It.IsAny<CreateVehicleRequest>()))
                .Throws(new DomainException());

            var model = new CreateVehicleRequest();

            // Act
            var result = await _controller.PostAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Post_CreatedAtRouteResult()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            VehicleServiceMock
                .Setup(x => x.GetVehicleAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(new DetailsVehicleResponse()));

            VehicleServiceMock
                .Setup(x => x.CreateVehicleAsync(It.IsAny<CreateVehicleRequest>()))
                .ReturnsAsync(new Guid());

            var model = new CreateVehicleRequest();

            // Act
            var result = await _controller.PostAsync(model) as CreatedAtRouteResult;

            // Assert
            Assert.IsType<CreatedAtRouteResult>(result);
            Assert.True(result.StatusCode == 201);
        }

        [Fact]
        public async Task Post_CreatedAtRouteResult_WithModel()
        {
            // Arrange
            var model = new CreateVehicleRequest
            {
                Id = new Guid()
            };

            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            VehicleServiceMock
                .Setup(x => x.GetVehicleAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(new DetailsVehicleResponse()));

            VehicleServiceMock
                .Setup(x => x.CreateVehicleAsync(It.IsAny<CreateVehicleRequest>()))
                .ReturnsAsync(model.Id.Value);

            var expectedObject = new CreatedAtRouteResult(
                "GetVehicle", new { id = model.Id }, model);

            // Act
            var result = await _controller.PostAsync(model) as CreatedAtRouteResult;

            // Assert
            result.ShouldDeepEqual(expectedObject);
            Assert.IsType<CreatedAtRouteResult>(result);
            Assert.True(result.StatusCode == 201);
        }

        [Fact]
        public async Task Put_UnauthorizedResult_WhenNotOwnedVehicle()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            var model = new UpdateVehicleRequest();

            // Act
            var result = await _controller.PutAsync(model) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Put_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleServiceMock
                .Setup(x => x.UpdateVehicleAsync(It.IsAny<UpdateVehicleRequest>()))
                .Throws(new SystemException());

            var model = new UpdateVehicleRequest();

            // Act
            var result = await _controller.PutAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Put_BadRequestObjectResult_WhenDomainExceptionIsThrown()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleServiceMock
                .Setup(x => x.UpdateVehicleAsync(It.IsAny<UpdateVehicleRequest>()))
                .Throws(new DomainException());

            var model = new UpdateVehicleRequest();

            // Act
            var result = await _controller.PutAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Put_AcceptedResult()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleServiceMock
                .Setup(x => x.UpdateVehicleAsync(It.IsAny<UpdateVehicleRequest>()))
                .Returns(Task.FromResult(true));

            var model = new UpdateVehicleRequest();

            // Act
            var result = await _controller.PutAsync(model) as AcceptedResult;

            // Assert
            Assert.IsType<AcceptedResult>(result);
            Assert.True(result.StatusCode == 202);
        }

        [Fact]
        public async Task Delete_OkResult()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleServiceMock
                .Setup(x => x.DeleteVehicleAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(true));

            var model = new UpdateVehicleRequest();
            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id) as OkResult;

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.True(result.StatusCode == 200);
        }

        [Fact]
        public async Task Delete_UnauthorizedResult_WhenNotOwnedVehicle()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            var model = new UpdateVehicleRequest();
            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Delete_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleServiceMock
                .Setup(x => x.DeleteVehicleAsync(It.IsAny<Guid>()))
                .Throws(new SystemException());

            var model = new UpdateVehicleRequest();
            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id);

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
        }
    }
}
