using System;
using System.Security.Principal;
using System.Threading.Tasks;
using api.Domain.Entities;
using api.Services.Analytics;
using api.Services.Users;
using api.Services.VehicleHistories;
using api.Services.Vehicles;
using api.test.unit.Fakes;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;

namespace api.test.unit.Controllers.Private
{
    public class BasePrivateTestController
    {
        public BasePrivateTestController()
        {
            SetupUserService();
            SetupLogger();
        }

        private void SetupUserService()
        {
            UserServiceMock = new Mock<IUserService>();
            UserServiceMock
                .Setup(x => x.FindByEmailAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(new ApplicationUser
                {
                    Id = new Guid().ToString()
                }));
        }

        //Can be done when setuping the controller and become generic
        private void SetupLogger()
        {
            var loggerMock = new Mock<FakeLogger<BasePrivateTestController>>();
            LoggerFactoryMock = new Mock<ILoggerFactory>();
            LoggerFactoryMock
                .Setup(x => x.CreateLogger(It.IsAny<string>()))
                .Returns(loggerMock.Object);
        }

        protected Mock<IMediator> MediatorMock { get; } = new Mock<IMediator>();
        protected Mock<ILoggerFactory> LoggerFactoryMock { get; private set; }
        protected Mock<IUserService> UserServiceMock { get; private set; }
        protected Mock<IVehicleService> VehicleServiceMock { get; } = new Mock<IVehicleService>();

        protected Mock<IVehicleHistoryService> VehicleHistoryServiceMock { get; } = new Mock<IVehicleHistoryService>();

        protected Mock<IStatisticsService> StatisticsServiceMock { get; } =
            new Mock<IStatisticsService>();

        public virtual T SetupController<T>(ref T controller) where T : Controller
        {
            controller.ControllerContext = new ControllerContext
            {
                HttpContext = new DefaultHttpContext()
            };
            controller.ControllerContext.HttpContext.User = new GenericPrincipal(
                new GenericIdentity("User"), null);

            return controller;
        }
    }
}