using System;
using System.Threading.Tasks;
using api.Controllers.Private;
using api.Domain.Exceptions;
using api.Domain.Models.VehicleHistory;
using api.Services.VehicleHistories;
using api.test.unit.Fixtures;
using DeepEqual.Syntax;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace api.test.unit.Controllers.Private
{
    public class FuelingVehicleHistoryControllerTests : BasePrivateTestController, IClassFixture<TestFixture>
    {
        private readonly FuelingVehicleHistoriesController _controller;
        private readonly Mock<IFuelingVehicleHistoryService> _fuelingVehicleHistoryService = new Mock<IFuelingVehicleHistoryService>();

        public FuelingVehicleHistoryControllerTests(TestFixture fixture)
        {
            _controller = new FuelingVehicleHistoriesController(
                _fuelingVehicleHistoryService.Object,
                VehicleServiceMock.Object,
                VehicleHistoryServiceMock.Object,
                UserServiceMock.Object,
                fixture.Localizer);

            SetupController(ref _controller);
        }

        [Fact]
        public async Task Get_ObjectResult()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            // Act
            var result = await _controller.GetAsync(new Guid());

            // // Assert
            Assert.IsType<ObjectResult>(result);
        }

        [Fact]
        public async Task Get_UnauthorizedResult_WhenNotOwnedVehicleHistory()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            // Act
            var result = await _controller.GetAsync(new Guid()) as UnauthorizedResult;

            // // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Get_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);
            _fuelingVehicleHistoryService
                .Setup(x => x.GetFuelingVehicleHistoryAsync(It.IsAny<Guid>()))
                .Throws(new SystemException());

            // Act
            var result = await _controller.GetAsync(new Guid()) as BadRequestObjectResult;

            // // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Post_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            //Arrage
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);
            _fuelingVehicleHistoryService
                .Setup(x => x.CreateFuelingVehicleHistoryAsync(It.IsAny<UpsertFuelingVehicleHistoryRequest>()))
                .Throws(new SystemException());
            var model = new UpsertFuelingVehicleHistoryRequest();

            //Act
            var result = await _controller.PostAsync(model) as BadRequestObjectResult;

            //Then
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Post_BadRequestObjectResult_WhenDomainExceptionIsThrown()
        {
            //Arrage
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);
            _fuelingVehicleHistoryService
                .Setup(x => x.CreateFuelingVehicleHistoryAsync(It.IsAny<UpsertFuelingVehicleHistoryRequest>()))
                .Throws(new DomainException());
            var model = new UpsertFuelingVehicleHistoryRequest();

            //Act
            var result = await _controller.PostAsync(model) as BadRequestObjectResult;

            //Then
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Post_UnauthorizedResult_WhenNotOwnedVehicle()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);
            var model = new UpsertFuelingVehicleHistoryRequest();

            // Act
            var result = await _controller.PostAsync(model) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Post_CreatedAtRouteResult_WithModel()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _fuelingVehicleHistoryService
                .Setup(x => x.CreateFuelingVehicleHistoryAsync(It.IsAny<UpsertFuelingVehicleHistoryRequest>()))
                .ReturnsAsync(new Guid());

            var model = new UpsertFuelingVehicleHistoryRequest
            {
                Id = new Guid()
            };

            var expectedObject = new CreatedAtRouteResult(
                "GetFuelingVehicleHistory",
                new { id = model.Id },
                model);

            // Act
            var result = await _controller.PostAsync(model) as CreatedAtRouteResult;

            // Assert
            Assert.IsType<CreatedAtRouteResult>(result);
            result.ShouldDeepEqual(expectedObject);
            Assert.True(result.StatusCode == 201);
        }

        [Fact]
        public async Task Put_UnauthorizedResult_WhenNotOwnedVehicleHistory()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            var model = new UpsertFuelingVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Put_AcceptedResult()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _fuelingVehicleHistoryService
                .Setup(x => x.UpdateFuelingVehicleHistoryAsync(It.IsAny<UpsertFuelingVehicleHistoryRequest>()))
                .Returns(Task.FromResult(true));

            var model = new UpsertFuelingVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as AcceptedResult;

            // Assert
            Assert.IsType<AcceptedResult>(result);
            Assert.True(result.StatusCode == 202);
        }

        [Fact]
        public async Task Put_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _fuelingVehicleHistoryService
                .Setup(x => x.UpdateFuelingVehicleHistoryAsync(It.IsAny<UpsertFuelingVehicleHistoryRequest>()))
                .Throws(new SystemException());

            var model = new UpsertFuelingVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Put_BadRequestObjectResult_WhenDomainExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _fuelingVehicleHistoryService
                .Setup(x => x.UpdateFuelingVehicleHistoryAsync(It.IsAny<UpsertFuelingVehicleHistoryRequest>()))
                .Throws(new DomainException());

            var model = new UpsertFuelingVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Delete_OkResult()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _fuelingVehicleHistoryService
                .Setup(x => x.DeleteFuelingVehicleHistoryAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(true));

            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id) as OkResult;

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.True(result.StatusCode == 200);
        }

        [Fact]
        public async Task Delete_UnauthorizedResult_WhenNotOwnedVehicleHistory()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Delete_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _fuelingVehicleHistoryService
                .Setup(x => x.DeleteFuelingVehicleHistoryAsync(It.IsAny<Guid>()))
                .Throws(new SystemException());

            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }
    }
}
