using System;
using System.Threading.Tasks;
using api.Controllers.Private;
using api.Domain.Exceptions;
using api.Domain.Models.Requests.VehicleHistory;
using api.Services.VehicleHistories;
using api.test.unit.Fixtures;
using DeepEqual.Syntax;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace api.test.unit.Controllers.Private
{
    public class TaxVehicleHistoryControllerTests : BasePrivateTestController, IClassFixture<TestFixture>
    {
        private readonly TaxVehicleHistoriesController _controller;
        private readonly Mock<ITaxVehicleHistoryService> _taxVehicleHistoryService;

        public TaxVehicleHistoryControllerTests(TestFixture fixture)
        {
            _taxVehicleHistoryService = new Mock<ITaxVehicleHistoryService>();
            _controller = new TaxVehicleHistoriesController(
                _taxVehicleHistoryService.Object,
                VehicleHistoryServiceMock.Object,
                VehicleServiceMock.Object,
                UserServiceMock.Object,
                fixture.Localizer);

            SetupController(ref _controller);
        }

        [Fact]
        public async Task Get_ObjectResult()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            // Act
            var result = await _controller.GetAsync(new Guid());

            // // Assert
            Assert.IsType<ObjectResult>(result);
        }

        [Fact]
        public async Task Get_UnauthorizedResult_WhenNotOwnedVehicleHistory()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            // Act
            var result = await _controller.GetAsync(new Guid()) as UnauthorizedResult;

            // // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Get_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _taxVehicleHistoryService
                .Setup(x => x.GetTaxVehicleHistoryAsync(It.IsAny<Guid>()))
                .Throws(new SystemException());

            // Act
            var result = await _controller.GetAsync(new Guid()) as BadRequestObjectResult;

            // // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Post_BadRequestObjectResult_WhenDomainExceptionIsThrown()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _taxVehicleHistoryService
                .Setup(x => x.CreateTaxVehicleHistoryAsync(It.IsAny<UpsertTaxVehicleHistoryRequest>()))
                .Throws(new DomainException());

            var model = new UpsertTaxVehicleHistoryRequest();

            // Act
            var result = await _controller.PostAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Post_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _taxVehicleHistoryService
                .Setup(x => x.CreateTaxVehicleHistoryAsync(It.IsAny<UpsertTaxVehicleHistoryRequest>()))
                .Throws(new Exception());

            var model = new UpsertTaxVehicleHistoryRequest();

            // Act
            var result = await _controller.PostAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Post_CreatedAtRouteResult_WithModel()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _taxVehicleHistoryService
                .Setup(x => x.CreateTaxVehicleHistoryAsync(It.IsAny<UpsertTaxVehicleHistoryRequest>()))
                .ReturnsAsync(new Guid());

            var model = new UpsertTaxVehicleHistoryRequest
            {
                Id = new Guid(),
                IsRemindableByDate = true,
                DaysRange = 30,
                ExpiresOn = DateTime.Now,
                IsRemindableByMilage = true,
                ExpiresOnMilage = 131,
                MilageRange = 5000
            };

            var expectedObject = new CreatedAtRouteResult("GetTaxVehicleHistory", new { id = model.Id }, model);

            // Act
            var result = await _controller.PostAsync(model) as CreatedAtRouteResult;

            // Assert
            Assert.IsType<CreatedAtRouteResult>(result);
            result.ShouldDeepEqual(expectedObject);
            Assert.True(result.StatusCode == 201);
        }

        [Fact]
        public async Task Post_UnauthorizedResult_WhenNotOwnedVehicle()
        {
            // Arrange
            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            var model = new UpsertTaxVehicleHistoryRequest();

            // Act
            var result = await _controller.PostAsync(model) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Put_UnauthorizedResult_WhenNotOwnedVehicleHistory()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            var model = new UpsertTaxVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Put_AcceptedResult()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _taxVehicleHistoryService
                .Setup(x => x.UpdateTaxVehicleHistoryAsync(It.IsAny<UpsertTaxVehicleHistoryRequest>()))
                .Returns(Task.FromResult(true));

            var model = new UpsertTaxVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as AcceptedResult;

            // Assert
            Assert.IsType<AcceptedResult>(result);
            Assert.True(result.StatusCode == 202);
        }

        [Fact]
        public async Task Put_BadRequestObjectResult_WhenDomainExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _taxVehicleHistoryService
                .Setup(x => x.UpdateTaxVehicleHistoryAsync(It.IsAny<UpsertTaxVehicleHistoryRequest>()))
                .Throws(new DomainException());

            var model = new UpsertTaxVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Put_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _taxVehicleHistoryService
                .Setup(x => x.UpdateTaxVehicleHistoryAsync(It.IsAny<UpsertTaxVehicleHistoryRequest>()))
                .Throws(new SystemException());

            var model = new UpsertTaxVehicleHistoryRequest() { Id = Guid.NewGuid() };

            // Act
            var result = await _controller.PutAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Delete_OkResult()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _taxVehicleHistoryService
                .Setup(x => x.DeleteTaxVehicleHistoryAsync(It.IsAny<Guid>()))
                .Returns(Task.FromResult(true));

            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id) as OkResult;

            // Assert
            Assert.IsType<OkResult>(result);
            Assert.True(result.StatusCode == 200);
        }

        [Fact]
        public async Task Delete_UnauthorizedResult_WhenNotOwnedVehicleHistory()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(false);

            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id) as UnauthorizedResult;

            // Assert
            Assert.IsType<UnauthorizedResult>(result);
            Assert.True(result.StatusCode == 401);
        }

        [Fact]
        public async Task Delete_BadRequestObjectResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            VehicleHistoryServiceMock
                .Setup(x => x.IsVehicleHistoryOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            _taxVehicleHistoryService
                .Setup(x => x.DeleteTaxVehicleHistoryAsync(It.IsAny<Guid>()))
                .Throws(new SystemException());

            var id = new Guid();

            // Act
            var result = await _controller.DeleteAsync(id) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }
    }
}
