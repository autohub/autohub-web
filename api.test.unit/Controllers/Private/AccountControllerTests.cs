using System;
using System.Threading.Tasks;
using api.Domain.Models.Account;
using api.Domain.Enums;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;
using api.Controllers.Private;
using api.Application.Account.Commands;
using System.Collections.Generic;
using api.test.unit.Fixtures;
using api.Services;

namespace api.test.unit.Controllers.Private
{
    public class AccountControllerTests : BasePrivateTestController, IClassFixture<TestFixture>
    {
        private readonly AccountController _controller;

        public AccountControllerTests(TestFixture fixture)
        {
            _controller = new AccountController(
                MediatorMock.Object,
                fixture.GetLogger<AccountController>(),
                UserServiceMock.Object,
                fixture.Localizer);

            SetupController(ref _controller);
        }

        [Fact]
        public async Task Get_ObjectResult()
        {
            // Arrange
            UserServiceMock
                .Setup(x => x.GetUserAccountAsync(It.IsAny<string>()))
                .Returns(Task.FromResult(new AccountResponse()));

            // Act
            var accountModel = new AccountResponse();
            var result = await _controller.GetAsync();

            // Assert
            Assert.IsType<ObjectResult>(result);
        }

        [Fact]
        public async Task Get_BadRequestResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            UserServiceMock
                .Setup(x => x.GetUserAccountAsync(It.IsAny<string>()))
                .Throws(new SystemException());

            // Act
            var accountModel = new AccountResponse();
            var result = await _controller.GetAsync() as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Put_BadRequestResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            UserServiceMock
                .Setup(x => x.UpdateUser(It.IsAny<string>(), It.IsAny<UpdateAccountRequest>()))
                .Returns(Task.FromException(new SystemException()));

            var model = new UpdateAccountRequest()
            {
                Name = "MaName",
                UserType = UserType.Personal
            };

            // Act
            var result = await _controller.PutAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task Put_AcceptedResult()
        {
            // Arrange
            UserServiceMock
                .Setup(x => x.UpdateUser(It.IsAny<string>(), It.IsAny<UpdateAccountRequest>()))
                .Returns(Task.CompletedTask);

            var model = new UpdateAccountRequest()
            {
                Name = "morethan50charsmorethan50charsmorethan50charsmorethan50charsmorethan50charsmorethan50chars",
                UserType = UserType.Personal
            };

            // Act
            var result = await _controller.PutAsync(model) as AcceptedResult;

            // Assert
            Assert.IsType<AcceptedResult>(result);
            Assert.True(result.StatusCode == 202);
        }

        [Fact]
        public async Task PutPassword_BadRequestResult_WhenSystemExceptionIsThrown()
        {
            // Arrange
            UserServiceMock
                .Setup(x => x.UpdateUser(It.IsAny<string>(), It.IsAny<UpdateAccountPasswordRequest>()))
                .Returns(Task.FromException(new SystemException()));

            var model = new UpdateAccountPasswordRequest()
            {
                OldPassword = "MaName",
                NewPassword = "",
                NewPasswordConfirmation = ""
            };

            // Act
            var result = await _controller.PutPasswordAsync(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }

        [Fact]
        public async Task PutPassword_AcceptedResult()
        {
            // Arrange
            UserServiceMock
                .Setup(x => x.UpdateUser(It.IsAny<string>(), It.IsAny<UpdateAccountPasswordRequest>()))
                .Returns(Task.CompletedTask);

            var model = new UpdateAccountPasswordRequest()
            {
                OldPassword = "testing",
                NewPassword = "mynewpassword",
                NewPasswordConfirmation = "newpassword"
            };

            // Act
            var result = await _controller.PutPasswordAsync(model) as AcceptedResult;

            // Assert
            Assert.IsType<AcceptedResult>(result);
            Assert.True(result.StatusCode == 202);
        }

        [Fact]
        public async Task SetLanguage_AcceptedResult()
        {
            // Arrange
            var model = new SetLanguageCommand() { Language = nameof(SupportedLanguages.en) };
            MediatorMock.Setup(x => x.Send(model, default))
                .ReturnsAsync(Result<bool>.Success(It.IsAny<bool>()));

            // Act
            var result = await _controller.SetLanguage(model) as AcceptedResult;

            // Assert
            Assert.IsType<AcceptedResult>(result);
            Assert.True(result.StatusCode == 202);
        }

        [Fact]
        public async Task SetLanguage_BadRequestObjectResult()
        {
            // Arrange
            var model = new SetLanguageCommand() { Language = nameof(SupportedLanguages.en) };
            MediatorMock.Setup(x => x.Send(model, default))
                .ReturnsAsync(Result<bool>.Fail(new List<string>() { "error1" }));

            // Act
            var result = await _controller.SetLanguage(model) as BadRequestObjectResult;

            // Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }
    }
}
