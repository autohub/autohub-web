using System.Threading;
using System.Threading.Tasks;
using api.Application.Statistics.Queries;
using api.Controllers.Private;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace api.test.unit.Controllers.Private
{
    public class StatisticsControllerTests : BasePrivateTestController
    {
        private readonly StatisticsController _controller;
        private readonly Mock<IMediator> _mediator = new Mock<IMediator>();

        public StatisticsControllerTests()
        {
            _controller = new StatisticsController(_mediator.Object);

            SetupController(ref _controller);
        }

        [Fact]
        public async Task Get_Returns_OkObjectResult_WithStatisticsResponse()
        {
            //Arrange
            _mediator
                .Setup(m => m.Send(It.IsAny<GetStatisticsQuery>(), CancellationToken.None))
                .ReturnsAsync(new StatisticsResponse())
                .Verifiable("Request was not sent.");

            //Act
            var result = await _controller.GetAsync() as OkObjectResult;

            //Assert
            _mediator.Verify(x => x.Send(It.IsAny<GetStatisticsQuery>(), It.IsAny<CancellationToken>()), Times.Once());
            Assert.IsType<OkObjectResult>(result);
            Assert.True((result.Value as StatisticsResponse)?.Vehicles.Count == 0);
        }
    }
}
