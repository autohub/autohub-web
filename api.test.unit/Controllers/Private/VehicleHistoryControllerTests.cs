using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Controllers.Private;
using api.Domain.Models.Requests.VehicleHistory;
using api.Domain.Models.Responses.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.test.unit.Fixtures;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace api.test.unit.Controllers.Private
{
    public class VehicleHistoryControllerTests : BasePrivateTestController, IClassFixture<TestFixture>
    {
        private readonly VehicleHistoriesController _controller;

        public VehicleHistoryControllerTests(TestFixture fixture)
        {
            _controller = new VehicleHistoriesController(
                VehicleHistoryServiceMock.Object,
                UserServiceMock.Object,
                fixture.Localizer);

            SetupController(ref _controller);
        }

        [Fact]
        public async Task Get_ObjectResult_WithListOfVehicleHistories()
        {
            // Arrange
            var expectedList = new PagedListVehicleHistoriesResponse()
            {
                VehicleHistories = new List<GenericVehicleHistory>()
                {
                    new GenericVehicleHistory()
                },
                PagesCount = 4
            };

            var pagedModel = new PagedListVehicleHistoriesRequest()
            {
                Page = 1,
                PerPage = 10
            };

            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleHistoryServiceMock
                .Setup(x => x.GetVehicleHistories(It.IsAny<string>(), pagedModel))
                .Returns(Task.FromResult(expectedList));

            // Act
            var result = await _controller
                .GetAsync(pagedModel) as ObjectResult;

            // Assert
            var vehicleHistoryListResponse = result.Value as PagedListVehicleHistoriesResponse;
            Assert.True(vehicleHistoryListResponse.VehicleHistories.Any());
        }

        [Fact]
        public async Task Get_ObjectResult_WithEmptyList()
        {
            //Arrange
            var expectedList = new PagedListVehicleHistoriesResponse()
            {
                VehicleHistories = new List<GenericVehicleHistory>(),
                PagesCount = 4
            };

            var pagedModel = new PagedListVehicleHistoriesRequest()
            {
                Page = 1,
                PerPage = 10
            };

            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleHistoryServiceMock
                .Setup(x => x.GetVehicleHistories(It.IsAny<string>(), pagedModel))
                .Returns(Task.FromResult(expectedList));

            //Act
            var result = await _controller
                .GetAsync(pagedModel) as ObjectResult;

            //Assert
            var vehicleHistoryListResponse = result.Value as PagedListVehicleHistoriesResponse;
            Assert.False(vehicleHistoryListResponse.VehicleHistories.Any());
        }

        [Fact]
        public async Task Get_BadRequestObjectResult_WhenSystemExceptionsIsThrown()
        {
            //Arrange
            var expectedList = new PagedListVehicleHistoriesResponse()
            {
                VehicleHistories = new List<GenericVehicleHistory>()
                {
                    new GenericVehicleHistory()
                },
                PagesCount = 4
            };

            var pagedModel = new PagedListVehicleHistoriesRequest()
            {
                Page = 1,
                PerPage = 10
            };

            VehicleServiceMock
                .Setup(x => x.IsVehicleOwnedBy(It.IsAny<Guid>(), It.IsAny<string>()))
                .Returns(true);

            VehicleHistoryServiceMock
                .Setup(x => x.GetVehicleHistories(It.IsAny<string>(), pagedModel))
                .Throws(new SystemException());

            //Act
            var result = await _controller
                .GetAsync( pagedModel) as BadRequestObjectResult;

            //Assert
            Assert.IsType<BadRequestObjectResult>(result);
            Assert.True(result.StatusCode == 400);
        }
    }
}
