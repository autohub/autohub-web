using System.Threading;
using System.Threading.Tasks;
using api.Application.Reports.Queries;
using api.Controllers.Private;
using api.Domain.Enums;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace api.test.unit.Controllers.Private
{
    public class ReportsControllerTests : BasePrivateTestController
    {
        private readonly ReportsController _controller;
        private readonly Mock<IMediator> _mediator = new Mock<IMediator>();

        public ReportsControllerTests()
        {
            _controller = new ReportsController(_mediator.Object);

            SetupController(ref _controller);
        }

        [Fact]
        public async Task Get_Returns_OkObjectResult_WithVehicleHistoriesCostsResponse()
        {
            //Arrange
            var query = new GetGraphReportQuery()
            {
                GraphType = nameof(GraphReportTypes.TotalVehicleHistoriesCosts)
            };

            _mediator
                .Setup(m => m.Send(It.IsAny<GetGraphReportQuery>(), CancellationToken.None))
                .ReturnsAsync(new VehicleHistoriesCosts())
                .Verifiable("Request was not sent.");

            //Act
            var result = await _controller.PostAsync(query) as OkObjectResult;

            //Assert
            _mediator.Verify(x => x.Send(It.IsAny<GetGraphReportQuery>(), It.IsAny<CancellationToken>()), Times.Once());
            Assert.IsType<OkObjectResult>(result);
            Assert.True((result.Value as VehicleHistoriesCosts)?.TotalFuelingCosts == 0);
            Assert.True((result.Value as VehicleHistoriesCosts)?.TotalRepairCosts == 0);
            Assert.True((result.Value as VehicleHistoriesCosts)?.TotalTaxesCosts == 0);
        }

        [Fact]
        public async Task Get_Returns_OkObjectResult_WithVehicleHistoriesCostsByMonthResponse()
        {
            //Arrange
            var query = new GetGraphReportQuery()
            {
                GraphType = nameof(GraphReportTypes.TotalVehicleHistoriesCostsByMonth)
            };

            _mediator
                .Setup(m => m.Send(It.IsAny<GetGraphReportQuery>(), CancellationToken.None))
                .ReturnsAsync(new VehicleHistoriesByMonthCosts())
                .Verifiable("Request was not sent.");

            //Act
            var result = await _controller.PostAsync(query) as OkObjectResult;

            //Assert
            _mediator.Verify(x => x.Send(It.IsAny<GetGraphReportQuery>(), It.IsAny<CancellationToken>()), Times.Once());
            Assert.IsType<OkObjectResult>(result);
            Assert.True((result.Value as VehicleHistoriesByMonthCosts)?.VehicleHistoriesByMonthsData.Count == 0);
        }

        [Fact]
        public async Task Get_Returns_OkObjectResult_WithMilageGrowthResponse()
        {
            //Arrange
            var query = new GetGraphReportQuery()
            {
                GraphType = nameof(GraphReportTypes.MilageGrowth)
            };

            _mediator
                .Setup(m => m.Send(It.IsAny<GetGraphReportQuery>(), CancellationToken.None))
                .ReturnsAsync(new MilageGrowth())
                .Verifiable("Request was not sent.");

            //Act
            var result = await _controller.PostAsync(query) as OkObjectResult;

            //Assert
            _mediator.Verify(x => x.Send(It.IsAny<GetGraphReportQuery>(), It.IsAny<CancellationToken>()), Times.Once());
            Assert.IsType<OkObjectResult>(result);
            Assert.True((result.Value as MilageGrowth)?.MilageByMonthData.Count == 0);
        }

        [Fact]
        public async Task Get_Returns_OkObjectResult_WithFuelConsumptionsResponse()
        {
            //Arrange
            var query = new GetGraphReportQuery()
            {
                GraphType = nameof(GraphReportTypes.FuelConsumptions)
            };

            _mediator
                .Setup(m => m.Send(It.IsAny<GetGraphReportQuery>(), CancellationToken.None))
                .ReturnsAsync(new FuelConsumptions())
                .Verifiable("Request was not sent.");

            //Act
            var result = await _controller.PostAsync(query) as OkObjectResult;

            //Assert
            _mediator.Verify(x => x.Send(It.IsAny<GetGraphReportQuery>(), It.IsAny<CancellationToken>()), Times.Once());
            Assert.IsType<OkObjectResult>(result);
            Assert.True((result.Value as FuelConsumptions)?.FuelingsByTypeForMonthData.Count == 0);
        }

        [Fact]
        public async Task Get_Returns_OkObjectResult_WithFuelPricesResponse()
        {
            //Arrange
            var query = new GetGraphReportQuery()
            {
                GraphType = nameof(GraphReportTypes.FuelPrices)
            };

            _mediator
                .Setup(m => m.Send(It.IsAny<GetGraphReportQuery>(), CancellationToken.None))
                .ReturnsAsync(new FuelPrices())
                .Verifiable("Request was not sent.");

            //Act
            var result = await _controller.PostAsync(query) as OkObjectResult;

            //Assert
            _mediator.Verify(x => x.Send(It.IsAny<GetGraphReportQuery>(), It.IsAny<CancellationToken>()), Times.Once());
            Assert.IsType<OkObjectResult>(result);
            Assert.True((result.Value as FuelPrices)?.FuelingPricesByTypeForMonthData.Count == 0);
        }
    }
}
