using System;
using System.Collections.Generic;
using api.Domain.Models.Requests.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using Newtonsoft.Json;

namespace api.test.unit.Helpers
{
    public static class VehicleHistoryFactory
    {
        public static DateTime date = DateTime.Parse("2020-01-25T22:00:00.000Z");

        public static IEnumerable<object[]> GetValidRepairRemindableVehicleHistories()
        {
            IEnumerable<object[]> items = GetValidRemindableVehicleHistories;
            return ConvertTo<UpsertRepairVehicleHistoryRequest>(items);
        }

        public static IEnumerable<object[]> GetValidUpdateRepairRemindableVehicleHistories()
        {
            IEnumerable<object[]> items = GetValidUpdateRemindableVehicleHistories;
            return ConvertTo<UpsertRepairVehicleHistoryRequest>(items);
        }

        public static IEnumerable<object[]> GetValidTaxRemindableVehicleHistories()
        {
            IEnumerable<object[]> items = GetValidRemindableVehicleHistories;
            return ConvertTo<UpsertTaxVehicleHistoryRequest>(items);
        }

        public static IEnumerable<object[]> GetValidUpdateTaxRemindableVehicleHistories()
        {
            IEnumerable<object[]> items = GetValidUpdateRemindableVehicleHistories;
            return ConvertTo<UpsertTaxVehicleHistoryRequest>(items);
        }

        public static IEnumerable<object[]> GetNotApplicableTaxRemindableVehicleHistories()
        {
            IEnumerable<object[]> items = GetNotApplicableRemindableVehicleHistories;
            return ConvertTo<UpsertTaxVehicleHistoryRequest>(items);
        }

        public static IEnumerable<object[]> GetNegativeTaxRemindableVehicleHistories()
        {
            IEnumerable<object[]> items = GetNegativeRemindableVehicleHistories;
            return ConvertTo<UpsertTaxVehicleHistoryRequest>(items);
        }

        private static IEnumerable<object[]> ConvertTo<T>(IEnumerable<object[]> list)
        {
            foreach (var item in list)
            {
                for (int i = 0; i < item.Length; i++)
                {
                    var serializedParent = JsonConvert.SerializeObject(item[i]);
                    item[i] = JsonConvert.DeserializeObject<T>(serializedParent);
                }
            }

            return list;
        }

        public static IEnumerable<object[]> GetValidRemindableVehicleHistories =>
            new List<object[]>
            {
                new object[] { new BaseRemindableVehicleHistoryRequest() { IsRemindableByDate = true, IsRemindableByMilage = false, DaysRange = 10, ExpiresOn = date } },
                new object[] { new BaseRemindableVehicleHistoryRequest() { IsRemindableByMilage = true, IsRemindableByDate = false, MilageRange = 15, ExpiresOnMilage = 300 } },
                new object[] {
                    new BaseRemindableVehicleHistoryRequest()
                    {
                        IsRemindableByMilage = true,
                        MilageRange = 10,
                        ExpiresOnMilage = 300,
                        IsRemindableByDate = true,
                        DaysRange = 10,
                        ExpiresOn = date
                    }
                },
            };

        public static IEnumerable<object[]> GetValidUpdateRemindableVehicleHistories =>
           new List<object[]>
           {
                new object[] { new BaseRemindableVehicleHistoryRequest() { Id = Guid.NewGuid(), IsRemindableByDate = true, DaysRange = 10, ExpiresOn = date } },
                new object[] { new BaseRemindableVehicleHistoryRequest() { Id = Guid.NewGuid(), IsRemindableByMilage = true, MilageRange = 15, ExpiresOnMilage = 300 } },
                new object[] {
                    new BaseRemindableVehicleHistoryRequest()
                    {
                        Id = Guid.NewGuid(),
                        IsRemindableByMilage = true,
                        MilageRange = 10,
                        ExpiresOnMilage = 300,
                        IsRemindableByDate = true,
                        DaysRange = 10,
                        ExpiresOn = date
                    }
                },
           };

        public static IEnumerable<object[]> GetNotApplicableRemindableVehicleHistories =>
            new List<object[]>
            {
                new object[] { new BaseRemindableVehicleHistoryRequest() { IsRemindableByDate = false, IsRemindableByMilage = false, DaysRange = 10, ExpiresOn = date } },
                new object[] { new BaseRemindableVehicleHistoryRequest() { IsRemindableByMilage = false, IsRemindableByDate = false, MilageRange = 10, ExpiresOnMilage = 300 } },
                new object[] {
                    new BaseRemindableVehicleHistoryRequest()
                    {
                        IsRemindableByMilage = false,
                        MilageRange = 10,
                        ExpiresOnMilage = 300,
                        IsRemindableByDate = false,
                        DaysRange = 0,
                        ExpiresOn = date
                    }
                },
            };

        public static IEnumerable<object[]> GetNegativeRemindableVehicleHistories =>
            new List<object[]>
            {
                new object[] { new BaseRemindableVehicleHistoryRequest() { IsRemindableByDate = true, IsRemindableByMilage = false, DaysRange = 0, ExpiresOn = date } },
                new object[] { new BaseRemindableVehicleHistoryRequest() { IsRemindableByDate = true, IsRemindableByMilage = false, DaysRange = 10, ExpiresOn = (DateTime?)null } },
                new object[] { new BaseRemindableVehicleHistoryRequest() { IsRemindableByMilage = true, IsRemindableByDate = false, MilageRange = 0, ExpiresOnMilage = 300 } },
                new object[] { new BaseRemindableVehicleHistoryRequest() { IsRemindableByMilage = true, IsRemindableByDate = false, MilageRange = 10, ExpiresOnMilage = 0 } },
                new object[] { new BaseRemindableVehicleHistoryRequest() { IsRemindableByMilage = true, IsRemindableByDate = false, MilageRange = 10, ExpiresOnMilage = 9 } },
                new object[] {
                    new BaseRemindableVehicleHistoryRequest()
                    {
                        IsRemindableByMilage = true,
                        MilageRange = 10,
                        ExpiresOnMilage = 300,
                        IsRemindableByDate = true,
                        DaysRange = 0,
                        ExpiresOn = date
                    }
                },
                new object[] {
                    new BaseRemindableVehicleHistoryRequest()
                    {
                        IsRemindableByMilage = true,
                        MilageRange = 0,
                        ExpiresOnMilage = 300,
                        IsRemindableByDate = true,
                        DaysRange = 10,
                        ExpiresOn = date
                    }
                },
            };
    }
}
