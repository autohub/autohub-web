process.env.NODE_ENV = "test";
module.exports = {
    testEnvironment: "node",
    preset: "ts-jest",
    transform: {
        "^.+\\.(ts|tsx)$": "ts-jest",
    },
    testMatch: ["**/*.test.ts"],
    collectCoverage: true,
    coveragePathIgnorePatterns: [
        "/node_modules/",
        "/test/"
    ],
    coverageDirectory: "client-react.test/coverage",
    moduleFileExtensions: ["ts", "js"],
    reporters: ['default',  ['jest-sonar', {
        outputDirectory: 'client-react.test/reports',
        outputName: 'jest-sonar-report.xml',
        reportedFilePath: 'absolute'
    }]],
};
