using api.Services;
using MediatR;

namespace api.Application.Account.Commands
{
    public class SetLanguageCommand : IRequest<Result<bool>>
    {
        public string Language { get; set; }
    }
}