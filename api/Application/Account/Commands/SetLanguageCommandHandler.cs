using System.Threading;
using System.Threading.Tasks;
using api.Common;
using MediatR;
using api.Services.Users;
using api.Services;

namespace api.Application.Account.Commands
{
    public class SetLanguageCommandHandler : IRequestHandler<SetLanguageCommand, Result<bool>>
    {
        private readonly IUserService _userService;

        public SetLanguageCommandHandler(IUserService userService)
        {
            _userService = userService;
        }

        public async Task<Result<bool>> Handle(SetLanguageCommand request, CancellationToken cancellationToken)
        {
            var user = await _userService.GetCurrentUser();
            if (user == null) return Result<bool>.Fail(ErrorMessages.UserDoesNotExists);
            user.Language = request.Language;
            await _userService.UpdateUser(user);

            return Result<bool>.Success(true);
        }
    }
}
