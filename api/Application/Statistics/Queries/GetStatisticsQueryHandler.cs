using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using api.Domain.Models.Vehicles;
using api.Services.Analytics;
using api.Services.Users;
using api.Services.Vehicles;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using MediatR;

namespace api.Application.Statistics.Queries
{
    public class GetStatisticsQuery : IRequest<StatisticsResponse> { }

    public class GetStatisticsQueryHandler : IRequestHandler<GetStatisticsQuery, StatisticsResponse>
    {
        private readonly IVehicleService _vehicleService;
        private readonly IUserService _userService;
        private readonly IStatisticsService _statisticsService;
        private readonly IMapper _mapper;

        public GetStatisticsQueryHandler(
            IVehicleService vehicleService,
            IUserService userService,
            IStatisticsService statisticsService,
            IMapper mapper)
        {
            _vehicleService = vehicleService;
            _userService = userService;
            _statisticsService = statisticsService;
            _mapper = mapper;
        }

        public async Task<StatisticsResponse> Handle(GetStatisticsQuery request, CancellationToken cancellationToken)
        {
            var userId = await _userService.GetCurrentUserId();
            var vehicles = await _vehicleService.GetVehiclesWithHistoriesByUserId(userId);

            return new StatisticsResponse()
            {
                Vehicles = vehicles.ProjectTo<VehicleModel>(_mapper.ConfigurationProvider).ToList(),
                TotalVehiclesDistance = _statisticsService.GetTotalDistance(vehicles),
                TotalVehicleHistories = _statisticsService.GetTotalVehicleHistoriesCount(vehicles),
                TotalVehicleHistoriesCost = _statisticsService.GetTotalVehicleHistoriesCost(vehicles),
                TotalTaxVehicleHistories = _statisticsService.GetTotalTaxesCost(vehicles),
                TotalFuelingVehicleHistories = _statisticsService.GetTotalFuelingsCost(vehicles),
                TotalRepairVehicleHistories = _statisticsService.GetTotalRepairsCost(vehicles),
            };
        }
    }
}
