using System.Collections.Generic;
using api.Domain.Models.Vehicles;
namespace api.Application.Statistics.Queries
{
    public class StatisticsResponse
    {
        public List<VehicleModel> Vehicles { get; set; } = new List<VehicleModel>();

        public long TotalVehiclesDistance { get; set; }

        public int TotalVehicleHistories { get; set; }

        public decimal TotalVehicleHistoriesCost { get; set; }

        public decimal TotalFuelingVehicleHistories { get; set; }

        public decimal TotalRepairVehicleHistories { get; set; }

        public decimal TotalTaxVehicleHistories { get; set; }
    }
}
