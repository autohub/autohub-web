using System.Collections.Generic;
using api.Application.Reports.Queries.GraphResponseTypes;

namespace api.Application.Reports.Queries
{
    public class GraphReportResponse { }

    public class VehicleHistoriesCosts : GraphReportResponse
    {
        public decimal TotalTaxesCosts { get; set; }

        public decimal TotalRepairCosts { get; set; }

        public decimal TotalFuelingCosts { get; set; }
    }

    public class MilageGrowth : GraphReportResponse
    {
        public List<MilageByMonthData> MilageByMonthData { get; set; } = new List<MilageByMonthData>();
    }

    public class VehicleHistoriesByMonthCosts : GraphReportResponse
    {
        public List<VehicleHistoriesByMonthData> VehicleHistoriesByMonthsData { get; set; } = new List<VehicleHistoriesByMonthData>();
    }

    public class FuelConsumptions : GraphReportResponse
    {
        public List<FuelingsByTypeForMonthData> FuelingsByTypeForMonthData { get; set; } = new List<FuelingsByTypeForMonthData>();
    }

    public class FuelPrices : GraphReportResponse
    {
        public List<FuelingPriceByTypeForMonthData> FuelingPricesByTypeForMonthData { get; set; } = new List<FuelingPriceByTypeForMonthData>();
    }
}
