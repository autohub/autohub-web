using System;
using System.ComponentModel.DataAnnotations;
using MediatR;

namespace api.Application.Reports.Queries
{
    public class GetGraphReportQuery : IRequest<GraphReportResponse>
    {
        [Required]
        public string GraphType { get; set; }

        [Required]
        public DateTime From { get; set; }

        [Required]
        public DateTime To { get; set; }

        public string VehicleId { get; set; }
    }
}