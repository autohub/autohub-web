using System.Collections.Generic;

namespace api.Application.Reports.Queries.GraphResponseTypes
{
    public class FuelingPriceByTypeForMonthData
    {
        public string Type { get; set; }

        public List<FuelingPricePerMonth> FuelingPricesPerMonth { get; set; }
    }

    public class FuelingPricePerMonth
    {
        public string Type { get; set; }

        public string Date { get; set; }

        public decimal Price { get; set; }
    }
}
