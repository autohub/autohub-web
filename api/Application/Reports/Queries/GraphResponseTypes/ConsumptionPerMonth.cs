namespace api.Application.Reports.Queries.GraphResponseTypes
{
    public class ConsumptionPerMonth
    {
        public string YearMonth { get; set; }

        public decimal Consumption { get; set; }
    }
}
