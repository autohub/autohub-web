namespace api.Application.Reports.Queries.GraphResponseTypes
{
    public class VehicleHistoriesByMonthData
    {
        public string YearMonth { get; set; }

        public decimal TotalPriceTaxes { get; set; }

        public decimal TotalPriceRepairs { get; set; }

        public decimal TotalPriceFueling { get; set; }
    }
}
