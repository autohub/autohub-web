using System;
using api.Domain.Enums;

namespace api.Application.Reports.Queries.GraphResponseTypes
{
    public class FuelingConsumptionItem
    {
        public decimal Consumption { get; set; }

        public FuelType Category { get; set; }

        public DateTime Date { get; set; }
    }
}
