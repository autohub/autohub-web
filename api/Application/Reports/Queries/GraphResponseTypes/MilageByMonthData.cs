namespace api.Application.Reports.Queries.GraphResponseTypes
{
    public class MilageByMonthData
    {
        public string YearMonth { get; set; }

        public long TotalMilage { get; set; }
    }
}
