using System.Collections.Generic;

namespace api.Application.Reports.Queries.GraphResponseTypes
{
    public class FuelingsByTypeForMonthData
    {
        public string Type { get; set; }

        public List<ConsumptionPerMonth> ConsumptionsPerMonth { get; set; }
    }
}
