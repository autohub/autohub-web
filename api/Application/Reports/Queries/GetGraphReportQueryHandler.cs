using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using api.Domain.Enums;
using api.Domain.Exceptions;
using api.Common;

using MediatR;
using api.Services.Analytics;

namespace api.Application.Reports.Queries
{
    public class GetGraphReportQueryHandler : IRequestHandler<GetGraphReportQuery, GraphReportResponse>
    {
        private readonly IReportsService _reportsService;

        public GetGraphReportQueryHandler(IReportsService reportsService)
        {
            _reportsService = reportsService;
        }

        public async Task<GraphReportResponse> Handle(GetGraphReportQuery request, CancellationToken cancellationToken)
        {
            if (!Enum.TryParse(request.GraphType, true, out GraphReportTypes graphType))
                throw new DomainException(ErrorMessages.InvalidType);

            var actions = new Dictionary<GraphReportTypes, Func<GetGraphReportQuery, Task<GraphReportResponse>>>()
            {
                { GraphReportTypes.MilageGrowth, _reportsService.GetVehiclesMilageGrowthData },
                { GraphReportTypes.TotalVehicleHistoriesCosts, _reportsService.GetVehicleHistoriesCostsData },
                { GraphReportTypes.TotalVehicleHistoriesCostsByMonth, _reportsService.GetVehicleHistoriesCostsByMonthData },
                { GraphReportTypes.FuelConsumptions, _reportsService.GetFuelConsumptionsData },
                { GraphReportTypes.FuelPrices, _reportsService.GetFuelPricesData }
            };

            return await actions[graphType].Invoke(request);
        }
    }
}
