using System.IO;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using api.Data.Contracts;
using Sentry;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using System.Diagnostics.CodeAnalysis;

namespace api
{
    [ExcludeFromCodeCoverage]
    public static class Program
    {
        public static void Main(string[] args)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

			var host = CreateHostBuilder(config, args).Build();

            IWebHostEnvironment env;
            using (var scope = host.Services.CreateScope())
            {
                var dbInitializer = scope.ServiceProvider.GetRequiredService<IDefaultDbContextInitializer>();
                env = scope.ServiceProvider.GetRequiredService<IWebHostEnvironment>();
                dbInitializer.Migrate();
                if (env.IsDevelopment())
                {
                    dbInitializer.Seed().GetAwaiter().GetResult();
                }
            }

            if (env.IsProduction())
            {
                using (SentrySdk.Init(config["sentryDsn"]))
                {
                    host.Run();
                }
            }
            else
            {
                host.Run();
            }
        }

        public static IHostBuilder CreateHostBuilder(IConfigurationRoot config, string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureLogging(logging =>
                {
                    logging.ClearProviders();
                    // Log to console (stdout) - in production stdout will be written to /var/log/{{app_name}}.out.log
                    logging.AddConsole();
                    logging.AddDebug();
                })
                .ConfigureWebHostDefaults(webBuilder =>
				{
					webBuilder
						.UseUrls(config["serverBindingUrl"])
						.UseStartup<Startup>();
				});
    }
}
