namespace api.Domain.Models.Account
{
    public class AccountResponse
    {
        public string Id { get; set;}
        public string Name { get; set;}
        public string UserType { get; set;}
    }
}