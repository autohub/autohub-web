namespace api.Domain.Models.Responses.Auth
{
    public class LoginUserResponse
    {
        public string Token { get; set; }
        public string Username { get; set; }
        public string UserEmail { get; set; }
    }
}