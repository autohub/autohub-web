using System.Collections.Generic;
using api.Domain.Models.VehicleHistory;
using api.Domain.Models.Vehicles;

namespace api.Domain.Models.Responses.VehicleHistory
{
    public class DetailsTaxVehicleHistoryResponse
    {
        public TaxVehicleHistoryModel VehicleHistory { get; set; }
        public IEnumerable<VehicleModel> Vehicles { get; set; }
    }
}
