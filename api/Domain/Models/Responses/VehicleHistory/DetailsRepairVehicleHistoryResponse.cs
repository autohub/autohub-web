using System.Collections.Generic;
using api.Domain.Models.VehicleHistory;
using api.Domain.Models.Vehicles;

namespace api.Domain.Models.Responses.VehicleHistory
{
    public class DetailsRepairVehicleHistoryResponse
    {
        public RepairVehicleHistoryModel VehicleHistory { get; set; }
        public IEnumerable<VehicleModel> Vehicles { get; set; }
    }
}