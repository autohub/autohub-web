using System.Collections.Generic;
using api.Domain.Models.VehicleHistory;
using api.Domain.Models.Vehicles;

namespace api.Domain.Models.Responses.VehicleHistory
{
    public class PagedListVehicleHistoriesResponse
    {
        public IEnumerable<VehicleModel> Vehicles { get; set; }
        public IEnumerable<GenericVehicleHistory> VehicleHistories { get; set; }
        public int PagesCount { get; set; }
    }
}