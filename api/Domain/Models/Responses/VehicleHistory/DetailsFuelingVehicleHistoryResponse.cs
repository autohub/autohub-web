using System.Collections.Generic;
using api.Domain.Models.VehicleHistory;
using api.Domain.Models.Vehicles;

namespace api.Domain.Models.Responses.VehicleHistory
{
    public class DetailsFuelingVehicleHistoryResponse
    {
        public IEnumerable<VehicleModel> Vehicles { get; set; }
        public FuelingVehicleHistoryModel VehicleHistory { get; set; }
    }
}