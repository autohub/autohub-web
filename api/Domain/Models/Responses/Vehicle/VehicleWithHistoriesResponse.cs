using System.Collections.Generic;
using api.Domain.Models.Responses.VehicleHistory;
using api.Domain.Models.VehicleHistory;

namespace api.Domain.Models.Responses.Vehicle
{
    public class VehicleWithHistoriesResponse : BaseVehicleResponse
    {
        public Dictionary<string, long> MilageStatisticData { get; set; }
        public List<VehicleHistoryStatisticData> VehicleHistoriesStatisticData { get; set; }
        public List<DetailsTaxVehicleHistoryResponse> TaxVehicleHistories { get; set; }
        public List<DetailsRepairVehicleHistoryResponse> RepairVehicleHistories { get; set; }
        public List<FuelingVehicleHistoryModel> FuelingVehicleHistories { get; set; }
        public List<GenericVehicleHistory> VehicleHistories { get; set; }
    }

    public class VehicleHistoryStatisticData
    {
        public string YearMonth { get; set; }
        public decimal TotalPriceTax { get; set; }
        public decimal TotalPriceRepair { get; set; }
        public decimal TotalPriceFueling { get; set; }
    }
}