using System;
using System.Collections.Generic;
using api.Domain.Models.VehicleHistory;

namespace api.Domain.Models.Responses.Vehicle
{
    public class DetailsVehicleResponse : BaseVehicleResponse
    {
        public DateTime? ProductionDate { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
        public decimal? LastFuelConsumption { get; set; }
        public decimal? AverageFuelConsumption { get; set; }
        public List<BaseRemindableVehicleHistoryModel> RemindableVehicleHistories { get; set; }
    }
}
