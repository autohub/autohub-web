using System.Collections.Generic;
using api.Domain.Models.VehicleHistory;

namespace api.Domain.Models.Responses.Vehicle
{
    public class ListVehicleResponse : BaseVehicleResponse
    {
        public int TotalVehicleHistories { get; set; }

        public List<BaseRemindableVehicleHistoryModel> RemindableVehicleHistories { get; set; } = 
            new List<BaseRemindableVehicleHistoryModel>();
    }
}
