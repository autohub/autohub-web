using System;

namespace api.Domain.Models.VehicleHistory
{
    public class BaseVehicleHistoryModel
    {
        public Guid Id { get; set; }
        public Guid VehicleId { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public long CurrentMilage { get; set; }
        public decimal TotalPrice { get; set; }
        public string Note { get; set; }
        public DateTime CreatedOn { get; set; }
        public DateTime? UpdatedOn { get; set; }
    }
}