namespace api.Domain.Models.VehicleHistory
{
    public class FuelingVehicleHistoryModel : BaseVehicleHistoryModel
    {
        public decimal Liters { get; set; }
        public decimal PricePerLiter { get; set; }
        public bool IsPartial { get; set; }
        public bool IsMissedPrevious { get; set; }
    }
}
