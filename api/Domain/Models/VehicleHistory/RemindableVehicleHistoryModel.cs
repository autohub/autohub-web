using System;

namespace api.Domain.Models.VehicleHistory
{
    public class RemindableVehicleHistoryModel : BaseRemindableVehicleHistoryModel
    {
        public long? ExpirationMilageRemaining { get; set; }
        public int? ExpirationDaysRemaining { get; set; }
        public DateTime? OccurredOn { get; set; }
    }
}