using System;
using api.Domain.Models.Vehicles;

namespace api.Domain.Models.VehicleHistory
{
    public class BaseRemindableVehicleHistoryModel : BaseVehicleHistoryModel
    {
        public bool IsRemindableByDate { get; set; }
        public DateTime? ExpiresOn { get; set; }
        public uint? DaysRange { get; set; }
        public bool IsRemindableByMilage { get; set; }
        public long? ExpiresOnMilage { get; set; }
        public uint? MilageRange { get; set; }
        public long? CurrentVehicleMilage { get; set; }
        public DateTime? CompletedOn { get; set; }
        public VehicleModel Vehicle { get; set; }
    }
}