using System;

namespace api.Domain.Models.Vehicles
{
    public class VehicleModel
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string UserId { get; set; }
        public string Make { get; set; }
        public string Model { get; set; }
        public long? Milage { get; set; }
        public int? EngineSize { get; set; }
        public string FuelType { get; set; }
        public int? HorsePower { get; set; }
    }
}