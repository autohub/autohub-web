using System;
using System.ComponentModel.DataAnnotations;

namespace api.Domain.Models.Requests.VehicleHistory
{
    public class PagedListVehicleHistoriesRequest
    {
        public Guid? VehicleId { get; set; }

        public string Type { get; set; }

        public string Category { get; set; }

        public int Page { get; set; } = 1;

        [Range(ModelConstants.MinItemsPerPage, ModelConstants.MaxItemsPerPage)]
        public int PerPage { get; set; } = ModelConstants.MinItemsPerPage;
    }
}
