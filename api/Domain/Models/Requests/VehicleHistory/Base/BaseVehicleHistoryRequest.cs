using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using api.Common;

namespace api.Domain.Models.VehicleHistory
{
    public abstract class BaseVehicleHistoryRequest
    {
        public Guid? Id { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [DisplayName("Vehicle ID")]
        public Guid VehicleId { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxStringLength, ErrorMessage = ErrorMessages.IncorrectField)]
        [DisplayName("Vehicle history type")]
        public string Type { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [DisplayName("Category")]
        public string Category { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [Range(1, ModelConstants.MaxMilage, ErrorMessage = ErrorMessages.IncorrectField)]
        [DisplayName("Milage")]
        public long CurrentMilage { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [Range(1, ModelConstants.MaxTotalPrice, ErrorMessage = ErrorMessages.IncorrectField)]
        [DisplayName("Price")]
        public decimal TotalPrice { get; set; }

        [StringLength(ModelConstants.MaxNoteLength, ErrorMessage = ErrorMessages.IncorrectField)]
        [DisplayName("Note")]
        public string Note { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        public DateTime CreatedOn { get; set; }
    }
}