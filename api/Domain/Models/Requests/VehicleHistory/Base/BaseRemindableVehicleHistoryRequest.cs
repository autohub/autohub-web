using System;

namespace api.Domain.Models.VehicleHistory
{
    public class BaseRemindableVehicleHistoryRequest : BaseVehicleHistoryRequest
    {
        public bool IsRemindableByDate { get; set; }

        public DateTime? ExpiresOn { get; set; }

        public uint? DaysRange { get; set; }

        public bool IsRemindableByMilage { get; set; }

        public long? ExpiresOnMilage { get; set; }

        public uint? MilageRange { get; set; }
    }
}
