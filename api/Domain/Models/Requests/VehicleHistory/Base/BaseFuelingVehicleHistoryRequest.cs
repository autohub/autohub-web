using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using api.Common;

namespace api.Domain.Models.VehicleHistory
{
    public abstract class BaseFuelingVehicleHistoryRequest : BaseVehicleHistoryRequest
    {
        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [Range(1, ModelConstants.MaxLiters, ErrorMessage = ErrorMessages.IncorrectField)]
        [DisplayName("Liters")]
        public decimal Liters { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [Range(1, ModelConstants.MaxPricePerLiter, ErrorMessage = ErrorMessages.IncorrectField)]
        [DisplayName("Price per liter")]
        public decimal PricePerLiter { get; set; }

        public bool IsPartial { get; set; }

        public bool IsMissedPrevious { get; set; }
    }
}