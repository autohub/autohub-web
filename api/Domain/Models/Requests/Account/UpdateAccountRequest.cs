using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using api.Domain.Enums;
using api.Common;

namespace api.Domain.Models.Account
{
    public class UpdateAccountRequest
    {
        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxStringLength, ErrorMessage = ErrorMessages.StringInvalidRange, MinimumLength = ModelConstants.MinStringLength)]
        [DisplayName("Name")]
        public string Name { get; set;}

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [DisplayName("User type")]
        public UserType UserType { get; set;}
    }
}