using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using api.Common;

namespace api.Domain.Models.Account
{
    public class UpdateAccountPasswordRequest
    {
        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxStringLength, ErrorMessage = ErrorMessages.StringInvalidRange, MinimumLength = ModelConstants.MinStringLength)]
        [DisplayName("Old password")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxStringLength, ErrorMessage = ErrorMessages.StringInvalidRange, MinimumLength = ModelConstants.MinStringLength)]
        [DataType(DataType.Password)]
        [DisplayName("New password")]
        public string NewPassword { get; set;}

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [Compare("NewPassword", ErrorMessage = ErrorMessages.PasswordsNotMatch)]
        [DataType(DataType.Password)]
        [DisplayName("Confirm password")]
        public string NewPasswordConfirmation { get; set;}
    }
}