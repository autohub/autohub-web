using System;
using System.ComponentModel.DataAnnotations;

namespace api.Domain.Models.Vehicle
{
    public class UpdateVehicleRequest : BaseVehicleRequest
    {
        [Required]
        public Guid Id { get; set; }

        public DateTime? CreatedOn { get; set; }
    }
}
