using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using api.Common;

namespace api.Domain.Models.Vehicle
{
    public abstract class BaseVehicleRequest
    {
        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxStringLength)]
        [DisplayName("Make")]
        public string Make { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxStringLength)]
        [DisplayName("Model")]
        public string Model { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [Range(1, long.MaxValue, ErrorMessage = ErrorMessages.IncorrectField)]
        [DisplayName("Milage")]
        public long Milage { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [DisplayName("Fuel type")]
        public string FuelType { get; set; }

        [DataType(DataType.Date)]
        [DisplayName("Manufactured date")]
        public DateTime? ProductionDate { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = ErrorMessages.IncorrectField)]
        [DisplayName("Engine size")]
        public int? EngineSize { get; set; }

        [Range(1, int.MaxValue, ErrorMessage = ErrorMessages.IncorrectField)]
        [DisplayName("Power")]
        public int? HorsePower { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public string UserId { get; set; }
    }
}