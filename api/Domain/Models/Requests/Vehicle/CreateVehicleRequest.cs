using System;

namespace api.Domain.Models.Vehicle
{
    public class CreateVehicleRequest : BaseVehicleRequest
    {
        public Guid? Id { get; set; }

        public DateTime CreatedOn { get; set; }
    }
}
