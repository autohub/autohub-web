namespace api.Domain.Models.Requests.Auth
{
    public class GoogleLoginRequest
    {
        public string IdToken { get; set; }
    }
}