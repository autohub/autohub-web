namespace api.Domain.Models.Requests.Auth
{
    public class FacebookLoginRequest
    {
        public string Name { get; set; }

        public string Email { get; set; }

        public string Id { get; set; }

        public string AccessToken { get; set; }

        public string UserID { get; set; }

        public int ExpiresIn { get; set; }

        public string SignedRequest { get; set; }

        public int DataAccessExpirationTime { get; set; }
    }
}
