using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using api.Common;

namespace api.Domain.Models.Auth
{
    public class ResetPasswordTokenRequest
    {
        [Required]
        public string UserId { get; set; }

        [Required]
        public string Token { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxStringLength, ErrorMessage = ErrorMessages.StringInvalidRange, MinimumLength = ModelConstants.MinStringLength)]
        [DataType(DataType.Password)]
        [DisplayName("Password")]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [Compare("NewPassword", ErrorMessage = ErrorMessages.PasswordsNotMatch)]
        [DataType(DataType.Password)]
        [DisplayName("Confirm password")]
        public string NewPasswordConfirmation { get; set; }
    }
}