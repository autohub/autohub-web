using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using api.Common;

namespace api.Domain.Models.Auth
{
    public class RegisterUserRequest
    {
        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [EmailAddress(ErrorMessage = ErrorMessages.IncorrectField)]
        [StringLength(ModelConstants.MaxStringLength, ErrorMessage = ErrorMessages.StringInvalidRange, MinimumLength = ModelConstants.MinStringLength)]
        [DisplayName("Email")]
        public string Username { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxStringLength, ErrorMessage = ErrorMessages.StringInvalidRange, MinimumLength = ModelConstants.MinStringLength)]
        [DisplayName("Name")]
        public string Name { get;set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxStringLength, ErrorMessage = ErrorMessages.StringInvalidRange, MinimumLength = ModelConstants.MinStringLength)]
        [DisplayName("Password")]
        public string Password { get; set; }

        public string UserType { get; set; }
    }
}
