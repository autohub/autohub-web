namespace api.Domain.Models.Auth
{
    public class ConfirmEmailRequest
    {
        public string User_id { get; set; }

        public string Token { get; set; }
    }
}
