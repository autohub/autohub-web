using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using api.Common;

namespace api.Domain.Models.Auth
{
    public class ResetPasswordRequest
    {
        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [EmailAddress(ErrorMessage = ErrorMessages.IncorrectField)]
        [DisplayName("Email")]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
    }
}