using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using api.Common;

namespace api.Domain.Models.Auth
{
    public class LoginUserRequest
    {
        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [DisplayName("Email")]
        public string Username { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [DisplayName("Password")]
        public string Password { get; set; }
    }
}