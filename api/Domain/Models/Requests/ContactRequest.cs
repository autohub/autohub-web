using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using api.Common;

namespace api.Domain.Models
{
    public class ContactRequest
    {
        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxStringLength, ErrorMessage = ErrorMessages.StringInvalidRange, MinimumLength = ModelConstants.MinStringLength)]
        [EmailAddress(ErrorMessage = ErrorMessages.IncorrectField)]
        [DataType(DataType.EmailAddress)]
        [DisplayName("Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxStringLength, ErrorMessage = ErrorMessages.StringInvalidRange, MinimumLength = ModelConstants.MinStringLength)]
        [DisplayName("Name")]
        public string Name { get; set; }

        [Required(ErrorMessage = ErrorMessages.RequiredField)]
        [StringLength(ModelConstants.MaxNoteLength, ErrorMessage = ErrorMessages.StringInvalidRange, MinimumLength = ModelConstants.MinStringLength)]
        [DisplayName("Message")]
        public string Message { get; set; }
    }
}