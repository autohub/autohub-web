using Microsoft.Extensions.Localization;

namespace api.Domain.Models
{
    public class EmailModel
    {
        public EmailModel(IStringLocalizer<Resources> localizer)
        {
            FooterMessage = localizer["We wish you milage without problems!"];
            ContactMessage = localizer["Contacts form"];
            PostScriptum = localizer["This email was send from AutoHub Bulgaria - your vehicle history management tool"];
            ContactMessagePrefix = localizer["Help us improve the service"];
            ContactMessageCompliment = localizer["Your opinion is important"];
        }

        public string UserEmail { get; set; }
        public string EmailTitle { get; set; }
        public string TextContent { get; set; }
        public string HtmlContent { get; set; }
        public string ActionButtonUrl { get; set; }
        public string ActionButtonText { get; set; }
        public string FooterMessage { get; set; }
        public string ContactMessage { get; set; }
        public string ContactMessagePrefix { get; set; }
        public string ContactMessageCompliment { get; set; }
        public string PostScriptum { get; set; }
        public string SiteUrl { get; set; }
        public string ViewName { get; set; } = "Default";
    }
}