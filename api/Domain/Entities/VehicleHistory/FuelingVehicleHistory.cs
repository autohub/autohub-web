using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using api.Domain.Enums;

namespace api.Domain.Entities.VehicleHistory
{
    public class FuelingVehicleHistory : BaseVehicleHistory
    {
        [Required]
        [MaxLength(50)]
        public FuelType Category { get; set; }

        [Required]
        public decimal Liters { get; set; }

        [Required]
        public decimal PricePerLiter { get; set; }

        public bool IsPartial { get; set; }

        public bool IsMissedPrevious { get; set; }
    }
}



