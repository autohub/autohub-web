using System;

namespace api.Domain.Entities.VehicleHistory
{
    public abstract class BaseRemindableVehicleHistory : BaseVehicleHistory
    {
        public bool IsRemindableByDate { get; set; }

        public DateTime? ExpiresOn { get; set; }

        public uint? DaysRange { get; set; }

        public bool IsRemindableByMilage { get; set; }

        public long? ExpiresOnMilage { get; set; }

        public uint? MilageRange { get; set; }

        public DateTime? OccurredOn { get; set; }

        public DateTime? CompletedOn { get; set; }
    }
}
