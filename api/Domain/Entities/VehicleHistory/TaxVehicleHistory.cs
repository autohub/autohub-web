using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using api.Domain.Enums;

namespace api.Domain.Entities.VehicleHistory
{
    public class TaxVehicleHistory : BaseRemindableVehicleHistory
    {
        [Required]
        [MaxLength(50)]
        public TaxVehicleHistoryCategory Category { get; set; }
    }
}












