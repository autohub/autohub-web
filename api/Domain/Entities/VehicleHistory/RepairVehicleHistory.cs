using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using api.Domain.Enums;

namespace api.Domain.Entities.VehicleHistory
{
    public class RepairVehicleHistory : BaseRemindableVehicleHistory
    {
        [Required]
        [MaxLength(50)]
        public RepairVehicleHistoryCategory Category { get; set; }
    }
}



