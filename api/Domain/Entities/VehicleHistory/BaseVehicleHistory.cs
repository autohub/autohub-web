using System;
using System.ComponentModel.DataAnnotations;
using api.Domain.Enums;

namespace api.Domain.Entities.VehicleHistory
{
    public abstract class BaseVehicleHistory
    {
        [Key]
        public Guid Id { get; set; }

        public VehicleHistoryType Type { get; set; }

        [Range(0, long.MaxValue)]
        public long CurrentMilage { get; set; }

        public decimal TotalPrice { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public string Note { get; set; }

        public Guid VehicleId { get; set; }

        public Vehicle Vehicle { get; set; }

        public string UserId { get; set; }

        public ApplicationUser User { get; set; }
    }
}



