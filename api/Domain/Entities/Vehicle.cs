using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Enums;

namespace api.Domain.Entities
{
    public class Vehicle
    {
        [Key]
        public Guid Id { get; set; }

        public VehicleCategory VehicleCategory { get; set; }

        [Required]
        [MaxLength(50)]
        public string Make { get; set; }

        [Required]
        [MaxLength(50)]
        public string Model { get; set; }

        [Required]
        [Range(0, long.MaxValue)]
        public long Milage { get; set; }

        public DateTime? ProductionDate { get; set;}

        [Range(0, int.MaxValue)]
        public int? EngineSize { get; set; }

        public VehicleFuelType FuelType { get; set; }

        [Range(0, int.MaxValue)]
        public int? HorsePower { get; set; }

        public DateTime CreatedOn { get; set; }

        public DateTime? UpdatedOn { get; set; }

        public string VIN { get; set; }

        public string Description { get; set; }

        [Required]
        public string UserId { get; set; }

        public ApplicationUser User { get; set; }

        public virtual ICollection<FuelingVehicleHistory> FuelingVehicleHistories { get; set; } 
            = new HashSet<FuelingVehicleHistory>();
        public virtual ICollection<RepairVehicleHistory> RepairVehicleHistories { get; set; } 
            = new HashSet<RepairVehicleHistory>();
        public virtual ICollection<TaxVehicleHistory> TaxVehicleHistories { get; set; } 
            = new HashSet<TaxVehicleHistory>();
    }
}



