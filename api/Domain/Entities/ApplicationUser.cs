using System.Collections.Generic;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Enums;
using Microsoft.AspNetCore.Identity;

namespace api.Domain.Entities
{
    public class ApplicationUser: IdentityUser
    {
        public string Name { get; set; }

        public UserType UserType { get; set; }

        public string Language { get; set; } = nameof(SupportedLanguages.en);

        public virtual ICollection<FuelingVehicleHistory> FuelingVehicleHistories  { get; set; } = new HashSet<FuelingVehicleHistory>();

        public virtual ICollection<Vehicle> Vehicles { get; set; } = new HashSet<Vehicle>();
    }
}
