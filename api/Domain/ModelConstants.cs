namespace api.Domain
{
    public static class ModelConstants
    {
        public const int MaxStringLength = 50;
        public const int MinStringLength = 3;
        public const int PasswordMaxLength = 6;

        public const int MinItemsPerPage = 10;
        public const int MaxItemsPerPage = 100;
        public const int MaxMilage = 100000000;
        public const int MaxTotalPrice = 100000;
        public const int MaxLiters = 1000;
        public const int MaxNoteLength = 1000;
        public const int MaxPricePerLiter = 100;
    }
}