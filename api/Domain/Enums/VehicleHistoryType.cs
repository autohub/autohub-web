namespace api.Domain.Enums
{
    public enum VehicleHistoryType
    {
        Repair,
        Fueling,
        Tax,
    }
}