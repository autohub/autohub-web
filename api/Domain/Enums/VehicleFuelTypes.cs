namespace api.Domain.Enums
{
    public enum VehicleFuelType
    {
        Gasoline,
        Diesel,
        GasGasoline,
        MethaneGasoline,
        Ethanol,
        Electricity,
    }
}