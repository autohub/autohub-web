namespace api.Domain.Enums
{
    using System.ComponentModel;

    public enum RepairVehicleHistoryCategory
    {
        [Description("Exhaust system")]
        ExhaustSystem,

        [Description("Transmission system")]
        TransmissionSystem,

        [Description("Cooling system")]
        CoolingSystem,

        [Description("Breaking system")]
        BreakingSystem,

        [Description("Steering system")]
        SteeringSystem,

        [Description("Fueling system")]
        FuelingSystem,

        [Description("Heating system")]
        HeatingSystem,

        [Description("Suspension system")]
        SuspensionSystem,

        [Description("Air conditioner system")]
        AirConditionerSystem,

        [Description("Clutch")]
        Clutch,

        [Description("Fluids")]
        Fluids,

        [Description("Oil change")]
        OilChange,

        [Description("Body and chassis")]
        BodyAndChassis,

        [Description("Tyres and rims")]
        TyresAndRims,

        [Description("Engine")]
        Engine,

        [Description("Straps")]
        Straps,

        [Description("Wipers")]
        Wipers,

        [Description("Lights")]
        Lights,

        [Description("Glasses and mirrors")]
        GlassesAndMirrors,

        [Description("Candles")]
        Candles,

        [Description("Expenses for labor")]
        ExpensesForLabor,

        [Description("Battery")]
        Battery,

        [Description("Fuel filter")]
        FuelFilter,

        [Description("Inner filter")]
        InnerFilter,

        [Description("Air filter")]
        AirFilter,

        [Description("Breaking pads")]
        BreakingPads,

        [Description("Inspection")]
        Inspection
    }
}