namespace api.Domain.Enums
{
    public enum FuelType
    {
        Gasoline,
        Diesel,
        Gas,
        Methane,
        Ethanol,
    }
}