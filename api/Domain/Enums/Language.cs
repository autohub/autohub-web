namespace api.Domain.Enums
{
    public enum SupportedLanguages
    {
        en,
        bg
    }
}
