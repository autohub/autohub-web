namespace api.Domain.Enums
{
    public enum GraphReportTypes
    {
        MilageGrowth,
        TotalVehicleHistoriesCosts,
        TotalVehicleHistoriesCostsByMonth,
        FuelPrices,
        FuelConsumptions
    }
}