namespace api.Domain.Enums
{
    using System.ComponentModel;

    public enum TaxVehicleHistoryCategory
    {
        [Description("Fine")]
        Fine,

        [Description("Duty")]
        Duty,

        [Description("Parking")]
        Parking,

        [Description("Payment")]
        Payment,

        [Description("Road tax")]
        RoadTax,

        [Description("Registration")]
        Registration,

        [Description("Additional insurance")]
        AdditionalInsurance,

        [Description("Insurance")]
        Insurance,

        [Description("Annually technical check")]
        TechnicalCheck,

        [Description("Vignette")]
        Vignette
    }
}