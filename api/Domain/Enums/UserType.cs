namespace api.Domain.Enums
{
    public enum UserType
    {
        Personal,
        Business
    }
}