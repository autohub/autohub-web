namespace api.Domain.Enums
{
    public enum VehicleCategory
    {
        Car,
        IndustrialMachines
    }
}