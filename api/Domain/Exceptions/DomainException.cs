using System;

namespace api.Domain.Exceptions
{
    public class DomainException : Exception
    {
        private readonly string _message;

        public DomainException()
        {
        }

        public DomainException(string message)
            : base(message)
        {
            _message = message;
        }

        public override string Message
        {
            get
            {
                return _message;
            }
        }
    }
}
