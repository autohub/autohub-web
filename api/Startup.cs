using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using AutoMapper;
using Hangfire;
using Hangfire.PostgreSql;
using Microsoft.AspNetCore.Mvc;
using api.Data;
using api.Domain.Entities;
using api.Services.ContainerContracts;
using api.Domain.Models.VehicleHistory;
using MediatR;
using System.Reflection;
using System.Globalization;
using Microsoft.AspNetCore.Localization;
using api.Domain;
using api.Infrastructure.Apis.Facebook;
using api.Infrastructure.Email;
using api.Infrastructure.Hangfire;
using api.Services.Users;
using api.Services.Validation;
using api.Services.Validation.Validators;
using api.Services.Validation.Decorators;
using api.Services.VehicleHistories;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using HealthChecks.UI.Client;
using System.Diagnostics.CodeAnalysis;
using api.Middleware;

namespace api
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public IHostEnvironment CurrentEnvironment { get; protected set; }
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("defaultConnection");
            services
                .AddDbContext<DefaultDbContext>(options => options.UseNpgsql(connectionString))
                .AddHttpContextAccessor()
                .AddIdentity()
                .AddAuthentication(Configuration)
                .AddLocalizationLocal()
                .AddMediatR(Assembly.GetExecutingAssembly())
                .AddAutoMapper(typeof(Startup).Assembly)
                .AddApplication()
                .AddConfigs(Configuration)
                .AddHttpClients()
                .AddHangfire(x => x.UsePostgreSqlStorage(connectionString))
                .AddControllersWithViews()
                .AddNewtonsoftJson()
                .AddDataAnnotationsLocalization(o => o.DataAnnotationLocalizerProvider = (type, factory) => factory.Create(typeof(Resources)))
                .SetCompatibilityVersion(CompatibilityVersion.Version_3_0);

            services.AddSpaStaticFiles(configuration: options => options.RootPath = "wwwroot");

            services
                .AddHealthChecks()
                .AddNpgSql(connectionString)
                .AddDbContextCheck<DefaultDbContext>()
                .AddHangfire(conf => conf.MaximumJobsFailed = 10)
                .AddDiskStorageHealthCheck(s => s.AddDrive("/", 1024))
                .AddProcessAllocatedMemoryHealthCheck(512);
        }

        public void Configure(IApplicationBuilder app, IHostEnvironment env)
        {
            // If not requesting /api*, rewrite to / so SPA app will be returned
            app.UseSpaFallback(new SpaFallbackOptions()
            {
                ApiPathPrefix = "/api",
                RewritePath = "/"
            });

            app.UseDefaultFiles();
            app.UseStaticFiles();

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                // Read and use headers coming from reverse proxy: X-Forwarded-For X-Forwarded-Proto
                // This is particularly important so that HttpContet.Request.Scheme will be correct behind a SSL terminating proxy
                ForwardedHeaders = ForwardedHeaders.XForwardedFor |
                ForwardedHeaders.XForwardedProto
            });

            app.UseRouting();
            app.UseAuthentication();
            app.UseAuthorization();
            app.UseRequestLocalization();
            app.UseHangfire();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHealthChecks("/api/health", new HealthCheckOptions()
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
            });

            if (env.IsDevelopment() || env.IsEnvironment("Debug"))
                app.UseDevPipe();
        }
    }

    public static class IApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseHangfire(this IApplicationBuilder app)
        {
            app.UseHangfireServer();
            app.UseHangfireDashboard();

            RecurringJob.AddOrUpdate<ICronService>(x =>
               x.EmailUsersWithIncomingVehicleService(), Cron.Daily());

            return app;
        }

        public static IApplicationBuilder UseDevPipe(this IApplicationBuilder app)
        {
            app.UseSpa(spa =>
            {
                // Configure Webpack Middleware
                spa.UseProxyToSpaDevelopmentServer("http://localhost:8080/");
            });

            app.UseDeveloperExceptionPage();
            app.UseDatabaseErrorPage();
            app.UseHttpsRedirection(); // Only for dev because of nginx reverse proxy for live

            return app;
        }
    }

    public static class IServiceCollectionExtensions
    {
        public static IServiceCollection AddConfigs(this IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<EmailSenderOptions>(configuration.GetSection("email"))
                .Configure<JwtOptions>(configuration.GetSection("jwt"))
                .Configure<AuthClientOptions>(configuration.GetSection("authentication"));

            return services;
        }

        public static IServiceCollection AddIdentity(this IServiceCollection services)
        {
            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddEntityFrameworkStores<DefaultDbContext>()
                .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(options =>
            {
                options.SignIn.RequireConfirmedEmail = true;
                options.SignIn.RequireConfirmedPhoneNumber = false;
                options.Password.RequireDigit = false;
                options.Password.RequiredLength = ModelConstants.PasswordMaxLength;
                options.Password.RequireLowercase = true;
                options.Password.RequireNonAlphanumeric = false;
                options.Password.RequireUppercase = false;
            });

            return services;
        }

        public static IServiceCollection AddHttpClients(this IServiceCollection services)
        {
            services.AddHttpClient<IFacebookClient, FacebookClient>();
            return services;
        }

        public static IServiceCollection AddAuthentication(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            })
            .AddJwtBearer(config =>
            {
                config.RequireHttpsMetadata = false;
                config.SaveToken = true;

                config.TokenValidationParameters = new TokenValidationParameters()
                {
                    ValidIssuer = configuration["jwt:issuer"],
                    ValidAudience = configuration["jwt:issuer"],
                    IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["jwt:key"]))
                };
            });

            return services;
        }

        public static IServiceCollection AddApplication(this IServiceCollection services)
        {
            services.Scan(scan => scan
              .FromCallingAssembly()
              .AddClasses(classes => classes.AssignableTo<ITransientService>())
                  .AsMatchingInterface()
                  .WithTransientLifetime()
            )
            .Decorate<IFuelingVehicleHistoryService, ValidationFuelingVehicleHistoryServiceDecorator>()
            .Decorate<ITaxVehicleHistoryService, ValidationTaxVehicleHistoryServiceDecorator>()
            .Decorate<IRepairVehicleHistoryService, ValidationRepairVehicleHistoryServiceDecorator>()
            .AddTransient(typeof(IValidator<BaseFuelingVehicleHistoryRequest>), typeof(FuelingTypeValidator))
            .AddTransient(typeof(IValidator<BaseRemindableVehicleHistoryRequest>), typeof(RemindableValidator<BaseRemindableVehicleHistoryRequest>));

            return services;
        }

        public static IServiceCollection AddLocalizationLocal(this IServiceCollection services)
        {
            services.AddLocalization(options => options.ResourcesPath = "Resources");
            services.Configure<RequestLocalizationOptions>(options =>
            {
                var supportedCultures = new[]
                {
                    new CultureInfo("en"),
                    new CultureInfo("bg")
                };

                options.DefaultRequestCulture = new RequestCulture(culture: "en", uiCulture: "en");
                options.SupportedCultures = supportedCultures;
                options.SupportedUICultures = supportedCultures;
                options.RequestCultureProviders.Insert(0, new AcceptLanguageHeaderRequestCultureProvider());
            });

            return services;
        }
    }
}
