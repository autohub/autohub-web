namespace api.Services.Users
{
    public class AuthClientOptions
    {
        public string FacebookAppId { get; set; }

        public string FacebookAppSecret { get; set; }

        public string GoogleAppId { get; set; }
    }
}
