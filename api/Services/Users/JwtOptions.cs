namespace api.Services.Users
{
    public class JwtOptions
    {
        public string Key { get; set; }
        public string Issuer { get; set; }
    }
}
