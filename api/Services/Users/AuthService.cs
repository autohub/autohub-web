using System.Threading.Tasks;
using api.Domain.Entities;
using api.Domain.Enums;
using api.Domain.Models.Requests.Auth;
using api.Common;
using static Google.Apis.Auth.GoogleJsonWebSignature;
using api.Infrastructure.Apis.Facebook;
using api.Infrastructure.Apis.Google;

namespace api.Services.Users
{
    public class AuthService : IAuthService
    {
        private readonly IUserService _userService;
        private readonly IFacebookClient _facebookClient;
        private readonly IGoogleAuthClient _googleAuthClient;

        public AuthService(
            IUserService userService,
            IFacebookClient facebookClient,
            IGoogleAuthClient googleAuthClient)
        {
            _userService = userService;
            _facebookClient = facebookClient;
            _googleAuthClient = googleAuthClient;
        }

        public async Task<Result<ApplicationUser>> LoginOrRegisterUser(FacebookLoginRequest request)
        {
            var appAccessToken = await _facebookClient.GetAppAccessToken();
            var userAccessTokenValidation = await _facebookClient.GetUserTokenValidation(request.AccessToken, appAccessToken.AccessToken);
            if (!userAccessTokenValidation.IsValid)
                return Result<ApplicationUser>.Fail(ErrorMessages.IncorrectField);

            var facebookUser = await _facebookClient.GetUserData(request.AccessToken);

            var userTask = await _userService.GetOrCreateUser(MapUser(facebookUser));
            if (!userTask.WasSuccessful)
                return Result<ApplicationUser>.Fail(userTask.Errors);

            return Result<ApplicationUser>.Success(userTask.Value);
        }

        public async Task<Result<ApplicationUser>> LoginOrRegisterUser(GoogleLoginRequest request)
        {
            var googleUser = await _googleAuthClient.GetUserAccount(request);
            if (googleUser == null)
                return Result<ApplicationUser>.Fail(ErrorMessages.IncorrectField);

            var userTask = await _userService.GetOrCreateUser(MapUser(googleUser));
            if (!userTask.WasSuccessful)
                return Result<ApplicationUser>.Fail(userTask.Errors);

            return Result<ApplicationUser>.Success(userTask.Value);
        }

        private ApplicationUser MapUser(FacebookUserModel model) => new ApplicationUser
        {
            UserName = model.Email,
            Name = model.Name,
            Email = model.Email,
            EmailConfirmed = true,
            UserType = UserType.Personal
        };

        private ApplicationUser MapUser(Payload model) => new ApplicationUser
        {
            UserName = model.Email,
            EmailConfirmed = model.EmailVerified,
            Name = model.Name,
            Email = model.Email,
            UserType = UserType.Personal
        };
    }
}
