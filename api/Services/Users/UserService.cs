
using System.Linq;
using System;
using AutoMapper;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using api.Domain.Models.Auth;
using System.Web;
using api.Common;
using System.Text;
using api.Domain.Models.Account;
using api.Domain.Enums;
using api.Domain.Models;
using api.Data;
using api.Domain.Entities;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Collections.Generic;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Localization;
using api.Infrastructure.Email;
using api.Services.Configs;

namespace api.Services.Users
{
    public class UserService : IUserService
    {
        private const int ONE_WEEK_IN_MINUTES = 10080;

        private readonly DefaultDbContext _dbContext;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IStringLocalizer<Resources> _localizer;
        private readonly JwtOptions _jwtOptions;
        private readonly IEmailSender _emailSender;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly IConfigurationManager _configurationManager;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(
            DefaultDbContext dbContext,
            IStringLocalizer<Resources> localizer,
            IOptions<JwtOptions> jwtOptions,
            SignInManager<ApplicationUser> signInManager,
            UserManager<ApplicationUser> userManager,
            IEmailSender emailSender,
            IMapper mapper,
            IConfigurationManager configurationManager,
            ILoggerFactory loggerFactory,
            IHttpContextAccessor httpContextAccessor)
        {
            _dbContext = dbContext;
            _localizer = localizer;
            _jwtOptions = jwtOptions.Value;
            _emailSender = emailSender;
            _signInManager = signInManager;
            _userManager = userManager;
            _logger = loggerFactory.CreateLogger<UserService>();
            _mapper = mapper;
            _configurationManager = configurationManager;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<ApplicationUser> FindByEmailAsync(string email) =>
            await _userManager.FindByEmailAsync(email);

        public async Task<bool> IsValidPasswordAsync(ApplicationUser user, string password) =>
            await _userManager.CheckPasswordAsync(user, password);

        public async Task<bool> CanSignInAsync(ApplicationUser user) =>
            await _signInManager.CanSignInAsync(user);

        public async Task<IdentityResult> ConfirmEmailAsync(ApplicationUser user, string token) =>
            await _userManager.ConfirmEmailAsync(user, token);

        public string GenerateLoginToken(ApplicationUser user)
        {
            var claims = new Claim[]
            {
                new Claim(ClaimTypes.Name, user.Email),
                new Claim(JwtRegisteredClaimNames.Sub, user.Email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };
            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtOptions.Key)); // Exception is thrown when key is not 128bit
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Issuer,
                claims: claims,
                expires: DateTime.Now.AddMinutes(ONE_WEEK_IN_MINUTES),
                signingCredentials: creds);

            var tokenStringified = new JwtSecurityTokenHandler().WriteToken(token);

            return tokenStringified;
        }

        public async Task<ApplicationUser> FindByIdAsync(string id) =>
            await _userManager.FindByIdAsync(id);

        public async Task<AccountResponse> GetUserAccountAsync(string id)
        {
            var user = await _userManager.FindByIdAsync(id);
            if (user == null) throw new ArgumentException("No user found.");

            return _mapper.Map<AccountResponse>(user);
        }

        public async Task<IdentityResult> RegisterUser(RegisterUserRequest model)
        {
            ApplicationUser user = CreateNewUser(model);
            var result = await _userManager.CreateAsync(user, model.Password);

            if (result.Succeeded && !user.EmailConfirmed)
                await SendUserConfirmRegisterEmail(user);

            _logger.LogInformation($"New user registered (id: {user.Id})");

            return result;
        }

        public async Task ResetPasswordRequest(string userEmail)
        {
            var user = await _userManager.FindByEmailAsync(userEmail);
            if (user == null || !await _signInManager.CanSignInAsync(user))
                throw new ArgumentException(_localizer[ErrorMessages.UserDoesNotExists].Value);

            await SendPasswordResetEmail(user);
        }

        public async Task ResetPassword(ResetPasswordTokenRequest resetPasswordModel)
        {
            if (resetPasswordModel.NewPassword != resetPasswordModel.NewPasswordConfirmation)
                throw new ArgumentException(_localizer["Passwords are not equal!"]);

            var user = await _userManager.FindByIdAsync(resetPasswordModel.UserId);
            if (user == null)
                throw new ArgumentException("User does not exist!");

            var passwordChangeResult = await _userManager.ResetPasswordAsync(user, resetPasswordModel.Token, resetPasswordModel.NewPassword);

            if (passwordChangeResult.Errors.Any())
            {
                var result = new StringBuilder();
                foreach (var error in passwordChangeResult.Errors)
                {
                    result.AppendFormat(error.Description + ". ");
                }

                throw new ArgumentException(result.ToString());
            }
        }

        public async Task UpdateUser(string id, UpdateAccountRequest model)
        {
            var user = _dbContext.ApplicationUsers.Find(id);
            if (user == null) throw new ArgumentNullException();

            _mapper.Map(model, user);
            _dbContext.Update(user);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateUser(ApplicationUser user)
        {
            _dbContext.Update(user);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateUser(string id, UpdateAccountPasswordRequest model)
        {
            var user = _dbContext.ApplicationUsers.Find(id);
            if (user == null) throw new ArgumentNullException();

            var isValidPass = _userManager.CheckPasswordAsync(user, model.OldPassword).Result;
            if (!isValidPass) throw new ArgumentException("Password doesnt match");

            var result = await _userManager.ChangePasswordAsync(user, model.OldPassword, model.NewPassword);
            if (!result.Succeeded)
                throw new AggregateException(ErrorMessages.UnexpectedError, ConvertIdentityErrors(result.Errors));

            await _emailSender.SendTemplatedEmail(new EmailModel(_localizer)
            {
                UserEmail = user.Email,
                EmailTitle = _localizer["Password has been changed"],
                TextContent = _localizer["Howdy! The password on your account in AutoHub has been changed."]
            });
        }

        public async Task<Result<ApplicationUser>> GetOrCreateUser(ApplicationUser user)
        {
            var existingUser = await FindByEmailAsync(user.Email);
            if (existingUser == null)
            {
                existingUser = user;
                user.Language = GetLang();
                IdentityResult result = await _userManager.CreateAsync(user, Convert.ToBase64String(Guid.NewGuid().ToByteArray()).Substring(0, 8));
                if (!result.Succeeded)
                    return Result<ApplicationUser>.Fail(result.Errors.Select(x => x.Description));
            }

            return Result<ApplicationUser>.Success(user);
        }

        public async Task<string> GetCurrentUserId()
        {
            var name = _httpContextAccessor.HttpContext.User.Identity.Name;
            var user = await FindByEmailAsync(name);
            return user.Id;
        }

        public async Task<ApplicationUser> GetCurrentUser()
        {
            var name = _httpContextAccessor.HttpContext.User.Identity.Name;
            var user = await FindByEmailAsync(name);

            return user;
        }

        private IEnumerable<ArgumentException> ConvertIdentityErrors(IEnumerable<IdentityError> errors)
        {
            var exceptions = new List<ArgumentException>();
            exceptions.AddRange(errors
                .Where(x => !string.IsNullOrEmpty(x.Description))
                .Select(x => new ArgumentException(x.Description))
            );

            return exceptions;
        }

        private ApplicationUser CreateNewUser(RegisterUserRequest model) => new ApplicationUser
        {
            UserName = model.Username,
            Name = model.Name,
            Email = model.Username,
            Language = GetLang(),
            UserType = (UserType)Enum.Parse(typeof(UserType), model.UserType)
        };

        private string GetLang()
        {
            var headerLang = _httpContextAccessor.HttpContext.Request.GetTypedHeaders().AcceptLanguage.FirstOrDefault()?.Value.Value;

            return string.IsNullOrEmpty(headerLang)
                ? nameof(SupportedLanguages.en)
                : headerLang;
        }

        private async Task SendUserConfirmRegisterEmail(ApplicationUser user)
        {
            var token = HttpUtility.UrlEncode(
                await _userManager.GenerateEmailConfirmationTokenAsync(user));
            const string path = "api/auth/confirm";
            var query = $"userId={user.Id}&token={token}";
            var uri = await BuildUriAsync(path, query);

            await _emailSender.SendTemplatedEmail(new EmailModel(_localizer)
            {
                UserEmail = user.Email,
                EmailTitle = _localizer["Welcome to your virtual vehicle history book"],
                ActionButtonText = _localizer["Confirm your profile"],
                ActionButtonUrl = uri,
                TextContent = BuildRegisterUserEmailText(uri)
            });
        }

        private async Task SendPasswordResetEmail(ApplicationUser user)
        {
            var token = HttpUtility.UrlEncode(
                await _userManager.GeneratePasswordResetTokenAsync(user));
            var path = $"account/{user.Id}/password/reset";
            var query = $"token={token}";
            var uri = await BuildUriAsync(path, query);

            await _emailSender.SendTemplatedEmail(
                new EmailModel(_localizer)
                {
                    EmailTitle = _localizer["Request for password change"],
                    UserEmail = user.Email,
                    ActionButtonText = _localizer["New password"],
                    ActionButtonUrl = uri,
                    TextContent = BuildResetPasswordEmailText(uri)
                });
        }

        private string BuildRegisterUserEmailText(string uri)
        {
            var text = new StringBuilder(_localizer["Howdy! Welcome to AutoHub! "]);
            text.Append(_localizer["Please, confirm you email by opening "]);
            text.Append($"<a href=\"{uri}\">{_localizer["this link."]}</a>. ");

            return text.ToString();
        }

        private string BuildResetPasswordEmailText(string uri)
        {
            var text = new StringBuilder(_localizer["Howdy! We received request for your password request. "]);
            text.Append(_localizer["If that's the case, please follow the button above or "]);
            text.Append($"<a href=\"{uri}\">{_localizer["this link."]}</a>. ");
            text.Append(_localizer["In other case, ignore this email."]);

            return text.ToString();
        }

        private async Task<string> BuildUriAsync(string path, string query)
        {
            var uri = new Uri(await _configurationManager.TryGetValue<string>("frontEndUrl"));
            var uriBuilder = new UriBuilder
            {
                Scheme = uri.Scheme,
                Host = uri.Host,
                Port = uri.Port,
                Path = path,
                Query = query
            };

            return uriBuilder.Uri.AbsoluteUri;
        }
    }
}
