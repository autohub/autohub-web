using System.Threading.Tasks;
using api.Domain.Entities;
using api.Domain.Models.Requests.Auth;
using api.Services.ContainerContracts;

namespace api.Services.Users
{
    public interface IAuthService : ITransientService
    {
        Task<Result<ApplicationUser>> LoginOrRegisterUser(FacebookLoginRequest request);
        Task<Result<ApplicationUser>> LoginOrRegisterUser(GoogleLoginRequest request);
    }
}