using System.Threading.Tasks;
using api.Domain.Entities;
using api.Domain.Models.Account;
using api.Domain.Models.Auth;
using api.Services.ContainerContracts;
using Microsoft.AspNetCore.Identity;

namespace api.Services.Users
{
    public interface IUserService : ITransientService
    {
        Task<ApplicationUser> FindByEmailAsync(string email);
        Task<ApplicationUser> FindByIdAsync(string id);
        Task<Result<ApplicationUser>> GetOrCreateUser(ApplicationUser user);
        Task<IdentityResult> RegisterUser(RegisterUserRequest model);
        Task<IdentityResult> ConfirmEmailAsync(ApplicationUser user, string token);
        Task<bool> IsValidPasswordAsync(ApplicationUser user, string password);
        Task<bool> CanSignInAsync(ApplicationUser user);
        Task<AccountResponse> GetUserAccountAsync(string id);
        Task UpdateUser(string id, UpdateAccountRequest model);
        Task UpdateUser(string id, UpdateAccountPasswordRequest model);
        Task UpdateUser(ApplicationUser user);
        Task ResetPasswordRequest(string userEmail);
        Task ResetPassword(ResetPasswordTokenRequest resetPasswordModel);
        string GenerateLoginToken(ApplicationUser user);
        Task<string> GetCurrentUserId();
        Task<ApplicationUser> GetCurrentUser();
    }
}