using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;

namespace api.Services.Configs
{
    public class ConfigurationManager : IConfigurationManager
    {
        private readonly IConfiguration _configuration;

        public ConfigurationManager(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public Task<T> TryGetValue<T>(string value) =>
            Task.Run(() => _configuration.GetValue<T>(value));
    }
}
