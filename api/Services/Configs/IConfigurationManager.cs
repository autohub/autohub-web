using System.Threading.Tasks;
using api.Services.ContainerContracts;

namespace api.Services.Configs
{
    public interface IConfigurationManager : ITransientService
    {
        Task<T> TryGetValue<T>(string value);
    }
}