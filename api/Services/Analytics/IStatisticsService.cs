using System;
using System.Collections.Generic;
using System.Linq;
using api.Application.Reports.Queries.GraphResponseTypes;
using api.Domain.Entities;
using api.Services.ContainerContracts;

namespace api.Services.Analytics
{
    public interface IStatisticsService : ITransientService
    {
        int GetTotalVehicleHistoriesCount(IQueryable<Vehicle> vehicles);
        decimal GetTotalVehicleHistoriesCost(IQueryable<Vehicle> vehicles);
        long GetTotalDistance(IQueryable<Vehicle> vehicles);
        decimal GetTotalFuelingsCost(IQueryable<Vehicle> vehicles);
        decimal GetTotalFuelingsCost(IQueryable<Vehicle> vehicles, DateTime from, DateTime to);
        decimal GetTotalTaxesCost(IQueryable<Vehicle> vehicles);
        decimal GetTotalTaxesCost(IQueryable<Vehicle> vehicles, DateTime from, DateTime to);
        decimal GetTotalRepairsCost(IQueryable<Vehicle> vehicles);
        decimal GetTotalRepairsCost(IQueryable<Vehicle> vehicles, DateTime from, DateTime to);
        List<FuelingConsumptionItem> GetAllFuelingConsumptions(List<Vehicle> vehicles, DateTime from, DateTime to);
    }
}