using System;
using System.Collections.Generic;
using System.Linq;
using api.Application.Reports.Queries.GraphResponseTypes;
using api.Domain.Entities;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Enums;

namespace api.Services.Analytics
{
    public class StatisticsService : IStatisticsService
    {
        public int GetTotalVehicleHistoriesCount(IQueryable<Vehicle> vehicles) =>
            vehicles.Sum(x =>
               x.TaxVehicleHistories.Count
               + x.FuelingVehicleHistories.Count
               + x.RepairVehicleHistories.Count);

        public decimal GetTotalVehicleHistoriesCost(IQueryable<Vehicle> vehicles) =>
            GetTotalFuelingsCost(vehicles)
            + GetTotalTaxesCost(vehicles)
            + GetTotalRepairsCost(vehicles);

        public long GetTotalDistance(IQueryable<Vehicle> vehicles) =>
            vehicles.Sum(v => v.Milage);

        public decimal GetTotalFuelingsCost(IQueryable<Vehicle> vehicles) =>
            vehicles.Sum(v => v.FuelingVehicleHistories.Sum(e => e.TotalPrice));

        public decimal GetTotalFuelingsCost(IQueryable<Vehicle> vehicles, DateTime from, DateTime to) =>
            vehicles.Sum(v => v.FuelingVehicleHistories
                .Where(vh => vh.CreatedOn > from && vh.CreatedOn < to)
                .Sum(vh => vh.TotalPrice));

        public decimal GetTotalTaxesCost(IQueryable<Vehicle> vehicles) =>
            vehicles.Sum(v => v.TaxVehicleHistories.Sum(e => e.TotalPrice));

        public decimal GetTotalTaxesCost(IQueryable<Vehicle> vehicles, DateTime from, DateTime to) =>
            vehicles.Sum(v => v.TaxVehicleHistories
                .Where(vh => vh.CreatedOn > from && vh.CreatedOn < to)
                .Sum(e => e.TotalPrice));

        public decimal GetTotalRepairsCost(IQueryable<Vehicle> vehicles) =>
            vehicles.Sum(v => v.RepairVehicleHistories.Sum(e => e.TotalPrice));

        public decimal GetTotalRepairsCost(IQueryable<Vehicle> vehicles, DateTime from, DateTime to) =>
            vehicles.Sum(v => v.RepairVehicleHistories
                .Where(vh => vh.CreatedOn > from && vh.CreatedOn < to)
                .Sum(e => e.TotalPrice));

        public List<FuelingConsumptionItem> GetAllFuelingConsumptions(List<Vehicle> vehicles, DateTime from, DateTime to)
        {
            var consumptionItems = new List<FuelingConsumptionItem>();

            foreach (var vehicle in vehicles)
            {
                var fuelingVehicleHistories = vehicle.FuelingVehicleHistories
                    .Where(vh => vh.CreatedOn > from && vh.CreatedOn < to)
                    .OrderBy(x => x.CreatedOn)
                    .ToList();

                for (int i = 1; i < fuelingVehicleHistories.Count; i++)
                {
                    var current = fuelingVehicleHistories[i];
                    var previous = fuelingVehicleHistories[i - 1];

                    if (HasValidConsumptionRule(current, previous))
                    {
                        var consumption = CalculateConsumption(current, previous);

                        consumptionItems.Add(new FuelingConsumptionItem
                        {
                            Consumption = consumption,
                            Category = current.Category,
                            Date = current.CreatedOn
                        });
                    }
                }
            }

            return consumptionItems;
        }

        private static decimal CalculateConsumption(FuelingVehicleHistory current, FuelingVehicleHistory previous)
        {
            switch (current.Category)
            {
                case FuelType.Diesel:
                case FuelType.Gasoline:
                case FuelType.Ethanol:
                    var kmDiff = current.CurrentMilage - previous.CurrentMilage;
                    if (kmDiff <= 0) return 0;
                    var consumption = (current.Liters / kmDiff) * 100;
                    return decimal.Round(consumption, 2, MidpointRounding.AwayFromZero);
                default:
                    return 0;
            }
        }

        private bool HasValidConsumptionRule(FuelingVehicleHistory current, FuelingVehicleHistory previous) =>
            !current.IsPartial
            && !previous.IsPartial
            && !current.IsMissedPrevious
            && current.Category == previous.Category;
    }
}
