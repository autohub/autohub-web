using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Application.Reports.Queries;
using api.Application.Reports.Queries.GraphResponseTypes;
using api.Domain.Entities;
using api.Domain.Enums;
using api.Extensions;
using api.Services.Users;
using api.Services.VehicleHistories;
using api.Services.Vehicles;

namespace api.Services.Analytics
{
    public class ReportsService : IReportsService
    {
        private readonly IVehicleService _vehicleService;
        private readonly IVehicleHistoryService _vehicleHistoryService;
        private readonly IUserService _userService;
        private readonly IStatisticsService _statisticsService;

        public ReportsService(
            IVehicleService vehicleService,
            IVehicleHistoryService vehicleHistoryService,
            IUserService userService,
            IStatisticsService statisticsService)
        {
            _vehicleService = vehicleService;
            _vehicleHistoryService = vehicleHistoryService;
            _userService = userService;
            _statisticsService = statisticsService;
        }

        public async Task<GraphReportResponse> GetVehicleHistoriesCostsData(GetGraphReportQuery request)
        {
            var vehicles = await GetFilteredVehicles(request.VehicleId);

            return new VehicleHistoriesCosts()
            {
                TotalFuelingCosts = _statisticsService.GetTotalFuelingsCost(vehicles, request.From, request.To),
                TotalRepairCosts = _statisticsService.GetTotalRepairsCost(vehicles, request.From, request.To),
                TotalTaxesCosts = _statisticsService.GetTotalTaxesCost(vehicles, request.From, request.To)
            };
        }

        public async Task<GraphReportResponse> GetVehicleHistoriesCostsByMonthData(GetGraphReportQuery request)
        {
            var vehicles = await GetFilteredVehicles(request.VehicleId);

            return new VehicleHistoriesByMonthCosts()
            {
                VehicleHistoriesByMonthsData = AggregateVehicleHistoriesCostsPerMonthByDate(vehicles.ToList(), request.From, request.To)
            };
        }

        public async Task<GraphReportResponse> GetVehiclesMilageGrowthData(GetGraphReportQuery request)
        {
            var vehicles = await GetFilteredVehicles(request.VehicleId);

            return new MilageGrowth()
            {
                MilageByMonthData = AggregateMilageByDate(vehicles.ToList(), request.From, request.To)
            };
        }

        public async Task<GraphReportResponse> GetFuelConsumptionsData(GetGraphReportQuery request)
        {
            var vehicles = await GetFilteredVehicles(request.VehicleId);

            return new FuelConsumptions()
            {
                FuelingsByTypeForMonthData = AggregateFuelingConsumptionsByDate(vehicles.ToList(), request.From, request.To)
            };
        }

        public async Task<GraphReportResponse> GetFuelPricesData(GetGraphReportQuery request)
        {
            var vehicles = await GetFilteredVehicles(request.VehicleId);

            return new FuelPrices()
            {
                FuelingPricesByTypeForMonthData = AggregateFuelingPricesByDate(vehicles.ToList(), request.From, request.To)
            };
        }

        private async Task<IQueryable<Vehicle>> GetFilteredVehicles(string vehicleId)
        {
            var userId = await _userService.GetCurrentUserId();
            var vehicles = (await _vehicleService.GetVehiclesWithHistoriesByUserId(userId))
               .WhereIf(!string.IsNullOrEmpty(vehicleId), v => v.Id == Guid.Parse(vehicleId));

            return vehicles;
        }

        private List<FuelingPriceByTypeForMonthData> AggregateFuelingPricesByDate(List<Vehicle> vehicles, DateTime from, DateTime to)
        {
            var totalFuelingConsumptionsPerMonth = new List<FuelingPriceByTypeForMonthData>();
            var allFuelingPrices = vehicles.SelectMany(x =>
                x.FuelingVehicleHistories
                    .Where(vh => vh.CreatedOn > from && vh.CreatedOn < to)
                    .OrderBy(fh => fh.CreatedOn)
                    .Select(y =>
                        new FuelingPricePerMonth
                        {
                            Price = y.PricePerLiter,
                            Date = $"{y.CreatedOn.Year}-{y.CreatedOn.Month}-{y.CreatedOn.Day}",
                            Type = y.Category.ToString()
                        })
            ).ToList();

            foreach (var typeGroup in allFuelingPrices.GroupBy(x => x.Type))
            {
                totalFuelingConsumptionsPerMonth.Add(new FuelingPriceByTypeForMonthData
                {
                    Type = typeGroup.Key,
                    FuelingPricesPerMonth = typeGroup.ToList()
                });
            }

            return totalFuelingConsumptionsPerMonth;
        }

        private List<FuelingsByTypeForMonthData> AggregateFuelingConsumptionsByDate(List<Vehicle> vehicles, DateTime from, DateTime to)
        {
            var totalFuelingConsumptionsPerMonth = new List<FuelingsByTypeForMonthData>();
            var allFuelingConsumptionsItems = _statisticsService.GetAllFuelingConsumptions(vehicles, from, to);

            foreach (var typeGroup in allFuelingConsumptionsItems.GroupBy(x => x.Category))
            {
                foreach (var dateGroup in typeGroup.GroupBy(x => ToYearMonthString(x.Date)))
                {
                    var averageConsumption = decimal.Round(
                        dateGroup.Sum(x => x.Consumption) / dateGroup.Count(), 2, MidpointRounding.AwayFromZero);

                    var item = new ConsumptionPerMonth
                    {
                        YearMonth = dateGroup.Key,
                        Consumption = averageConsumption
                    };

                    if (totalFuelingConsumptionsPerMonth.Any(x => x.Type == typeGroup.Key.ToString()))
                    {
                        var currentCategory = totalFuelingConsumptionsPerMonth.FirstOrDefault(x => x.Type == typeGroup.Key.ToString());
                        currentCategory.ConsumptionsPerMonth.Add(item);
                    }
                    else
                    {
                        totalFuelingConsumptionsPerMonth.Add(new FuelingsByTypeForMonthData
                        {
                            Type = typeGroup.Key.ToString(),
                            ConsumptionsPerMonth = new List<ConsumptionPerMonth>() { item }
                        });
                    }
                }
            }

            totalFuelingConsumptionsPerMonth.All(x =>
            {
                var vh = (FuelType)Enum.Parse(typeof(FuelType), x.Type);
                x.Type = vh.ToString();
                return true;
            });

            return totalFuelingConsumptionsPerMonth;
        }

        private List<MilageByMonthData> AggregateMilageByDate(List<Vehicle> vehicles, DateTime from, DateTime to)
        {
            Dictionary<string, long> overallMilageStatisticsData = GroupVehicleHistoriesByMonth(vehicles, from, to);

            var result = new List<MilageByMonthData>();

            foreach (var item in overallMilageStatisticsData)
            {
                result.Add(new MilageByMonthData()
                {
                    YearMonth = item.Key,
                    TotalMilage = item.Value
                });
            }

            return result;
        }

        private Dictionary<string, long> GroupVehicleHistoriesByMonth(List<Vehicle> vehicles, DateTime from, DateTime to)
        {
            var overallMilageStatisticsData = new Dictionary<string, long>();
            foreach (var vehicle in vehicles)
            {
                var vehicleHistories = _vehicleHistoryService.CombineVehicleHistories(vehicles)
                    .Where(vh => vh.CreatedOn > from && vh.CreatedOn < to);

                var vhMonthlyGrouped = vehicleHistories
                    .GroupBy(x => ToYearMonthString(x.CreatedOn))
                    .OrderBy(x => x.Key);

                foreach (var group in vhMonthlyGrouped)
                {
                    var groupSorted = group.OrderBy(x => x.CurrentMilage);
                    if (overallMilageStatisticsData.ContainsKey(group.Key))
                        overallMilageStatisticsData[group.Key] += groupSorted.Last().CurrentMilage;
                    else
                        overallMilageStatisticsData.Add(group.Key, groupSorted.Last().CurrentMilage);
                }
            }

            return overallMilageStatisticsData;
        }

        private List<VehicleHistoriesByMonthData> AggregateVehicleHistoriesCostsPerMonthByDate(List<Vehicle> vehicles, DateTime from, DateTime to)
        {
            var dataList = new List<VehicleHistoriesByMonthData>();
            var vehicleHistories = _vehicleHistoryService.CombineVehicleHistories(vehicles)
                .Where(vh => vh.CreatedOn > from && vh.CreatedOn < to);

            var vehicleHistoriesMonthlyGrouped = vehicleHistories
                    .GroupBy(x => ToYearMonthString(x.CreatedOn))
                    .OrderBy(x => x.Key);

            foreach (var group in vehicleHistoriesMonthlyGrouped)
            {
                var monthlyData = new VehicleHistoriesByMonthData
                {
                    YearMonth = group.Key,
                    TotalPriceRepairs = group.Where(x => x.Type.Equals("Repair")).Sum(x => x.TotalPrice),
                    TotalPriceFueling = group.Where(x => x.Type.Equals("Fueling")).Sum(x => x.TotalPrice),
                    TotalPriceTaxes = group.Where(x => x.Type.Equals("Tax")).Sum(x => x.TotalPrice)
                };

                dataList.Add(monthlyData);
            }

            return dataList;
        }

        private readonly Func<DateTime, string> ToYearMonthString = x => x.Date.Year + "/" + x.Date.Month;
    }
}
