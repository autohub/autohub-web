using System.Threading.Tasks;
using api.Application.Reports.Queries;
using api.Services.ContainerContracts;

namespace api.Services.Analytics
{
    public interface IReportsService : ITransientService
    {
        Task<GraphReportResponse> GetVehicleHistoriesCostsData(GetGraphReportQuery request);
        Task<GraphReportResponse> GetVehicleHistoriesCostsByMonthData(GetGraphReportQuery request);
        Task<GraphReportResponse> GetVehiclesMilageGrowthData(GetGraphReportQuery request);
        Task<GraphReportResponse> GetFuelConsumptionsData(GetGraphReportQuery request);
        Task<GraphReportResponse> GetFuelPricesData(GetGraphReportQuery request);
    }
}