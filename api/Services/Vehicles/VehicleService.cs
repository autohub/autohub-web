
using System.Linq;
using System.Collections.Generic;
using AutoMapper.QueryableExtensions;
using api.Domain.Models.Vehicle;
using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using api.Data;
using api.Domain.Entities;
using api.Domain.Models.Responses.Vehicle;
using api.Data.Contracts;
using api.Common;
using api.Domain.Models.Vehicles;
using api.Domain.Exceptions;
using api.Extensions;
using api.Domain.Enums;
using api.Services.Users;
using api.Services.VehicleHistories;
using api.Services.Analytics;

namespace api.Services.Vehicles
{
    public class VehicleService : IVehicleService
    {
        private readonly IApiData _data;
        private readonly DefaultDbContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IRemindableVehicleHistoryService _reminderService;
        private readonly IStatisticsService _statisticsService;
        private readonly IUserService _userService;

        public VehicleService(
            IApiData data,
            DefaultDbContext dbContext,
            IMapper mapper,
            IRemindableVehicleHistoryService reminderService,
            IStatisticsService statisticsService,
            IUserService userService)
        {
            _data = data;
            _dbContext = dbContext;
            _mapper = mapper;
            _reminderService = reminderService;
            _statisticsService = statisticsService;
            _userService = userService;
        }

        public async Task<IEnumerable<ListVehicleResponse>> GetVehiclesOwnedByMe(string userId)
        {
            var vehicles = await _dbContext.Vehicles
                .Where(v => v.UserId == userId)
                .ProjectTo<ListVehicleResponse>(_mapper.ConfigurationProvider)
                .ToListAsync();

            vehicles.ForEach(x =>
                x.RemindableVehicleHistories = _reminderService.GetAllRemindableVehicleHistories(x.Id).ToList());

            return vehicles;
        }

        public async Task<IEnumerable<VehicleModel>> GetAllByUserId(string userId) =>
            await _data.Vehicles.All()
                .AsNoTracking()
                .Where(v => v.UserId == userId)
                .ProjectTo<VehicleModel>(_mapper.ConfigurationProvider)
                .ToListAsync();

        public async Task<DetailsVehicleResponse> GetVehicleAsync(Guid id)
        {
            var userId = await _userService.GetCurrentUserId();
            var vehicle = await GetVehicleWithHistoriesByUserId(id, userId);
            var consumptions = _statisticsService.GetAllFuelingConsumptions(new List<Vehicle>() { vehicle }, DateTime.MinValue, DateTime.UtcNow)
                .AsQueryable()
                .WhereIf(vehicle.FuelType == VehicleFuelType.GasGasoline, f => f.Category == FuelType.Gas || f.Category == FuelType.Gasoline)
                .WhereIf(vehicle.FuelType == VehicleFuelType.MethaneGasoline, f => f.Category == FuelType.Methane || f.Category == FuelType.Gasoline)
                .WhereIf(vehicle.FuelType == VehicleFuelType.Diesel, f => f.Category == FuelType.Diesel)
                .WhereIf(vehicle.FuelType == VehicleFuelType.Ethanol, f => f.Category == FuelType.Ethanol)
                .OrderByDescending(x => x.Date);

            var vResponse = _mapper.Map<DetailsVehicleResponse>(vehicle);
            vResponse.RemindableVehicleHistories = _reminderService.GetAllRemindableVehicleHistories(id).ToList();
            if (consumptions.Any())
            {
                vResponse.LastFuelConsumption = consumptions.FirstOrDefault().Consumption;
                vResponse.AverageFuelConsumption = consumptions.Sum(x => x.Consumption) / consumptions.Count();
            }

            return vResponse;
        }

        public async Task<Guid> CreateVehicleAsync(CreateVehicleRequest model)
        {
            model.CreatedOn = DateTime.UtcNow;
            var vehicle = _mapper.Map<CreateVehicleRequest, Vehicle>(model);
            _dbContext.Vehicles.Add(vehicle);

            await _dbContext.SaveChangesAsync();
            return vehicle.Id;
        }

        public async Task UpdateVehicleAsync(UpdateVehicleRequest model)
        {
            var vehicle = _dbContext.Vehicles.Find(model.Id);
            if (model.Milage < vehicle.Milage) throw new DomainException(ErrorMessages.MilageDecrease);

            model.UpdatedOn = DateTime.UtcNow;
            _mapper.Map(model, vehicle);
            _dbContext.Update(vehicle);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateMilageAsync(Guid vehicleId, long milage)
        {
            var vehicle = _data.Vehicles.GetById(vehicleId);
            if (milage > vehicle.Milage)
            {
                vehicle.Milage = milage;
                await Task.Run(() => _data.Vehicles.Update(vehicle));
            }
        }

        public async Task DeleteVehicleAsync(Guid id)
        {
            Vehicle vehicle = new Vehicle() { Id = id };
            _dbContext.Vehicles.Remove(vehicle);

            await _dbContext.SaveChangesAsync();
        }

        public bool IsVehicleOwnedBy(Guid vehicleId, string userId)
        {
            var vehicle = _dbContext
                .Vehicles
                .AsNoTracking()
                .Where(v => v.UserId == userId && v.Id == vehicleId)
                .SingleOrDefault();

            return vehicle != null;
        }

        public async Task<IQueryable<Vehicle>> GetVehiclesWithHistoriesByUserId(string userId) =>
            await Task.FromResult(_data.Vehicles.All()
                .AsNoTracking()
                .Where(v => v.UserId == userId)
                .Include(x => x.TaxVehicleHistories)
                .Include(x => x.FuelingVehicleHistories)
                .Include(x => x.RepairVehicleHistories)
                .AsQueryable());

        private async Task<Vehicle> GetVehicleWithHistoriesByUserId(Guid id, string userId) =>
             await _data.Vehicles.All()
                .AsNoTracking()
                .Where(v => v.UserId == userId && v.Id == id)
                .Include(x => x.TaxVehicleHistories)
                .Include(x => x.FuelingVehicleHistories)
                .Include(x => x.RepairVehicleHistories)
                .FirstOrDefaultAsync();
    }
}
