using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Domain.Entities;
using api.Domain.Models.Responses.Vehicle;
using api.Domain.Models.Vehicle;
using api.Domain.Models.Vehicles;
using api.Services.ContainerContracts;

namespace api.Services.Vehicles
{
    public interface IVehicleService : ITransientService
    {
        Task<IEnumerable<VehicleModel>> GetAllByUserId(string userId);
        Task<IEnumerable<ListVehicleResponse>> GetVehiclesOwnedByMe(string userId);
        Task<IQueryable<Vehicle>> GetVehiclesWithHistoriesByUserId(string userId);
        Task<DetailsVehicleResponse> GetVehicleAsync(Guid id);
        Task<Guid> CreateVehicleAsync(CreateVehicleRequest model);
        Task UpdateVehicleAsync(UpdateVehicleRequest model);
        Task UpdateMilageAsync(Guid vehicleId, long milage);
        Task DeleteVehicleAsync(Guid id);

        bool IsVehicleOwnedBy(Guid vehicleId, string userId);
    }
}