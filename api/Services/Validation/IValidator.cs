namespace api.Services.Validation
{
    public interface IValidator<T>
    {
        void Validate(T model);
    }
}