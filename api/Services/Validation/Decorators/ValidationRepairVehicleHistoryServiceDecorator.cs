using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using api.Domain.Models.VehicleHistory;

using api.Services.VehicleHistories;

namespace api.Services.Validation.Decorators
{
    public class ValidationRepairVehicleHistoryServiceDecorator : IRepairVehicleHistoryService
    {
        public readonly IEnumerable<IValidator<BaseRemindableVehicleHistoryRequest>> _validators;
        private readonly IRepairVehicleHistoryService _service;
        private readonly CompositeValidator<BaseRemindableVehicleHistoryRequest> _validator;

        public ValidationRepairVehicleHistoryServiceDecorator(
            IRepairVehicleHistoryService service,
            IEnumerable<IValidator<BaseRemindableVehicleHistoryRequest>> validators)
        {
            _service = service;
            _validators = validators;
            _validator = new CompositeValidator<BaseRemindableVehicleHistoryRequest>(_validators);
        }

        public async Task<Guid> CreateRepairVehicleHistoryAsync(UpsertRepairVehicleHistoryRequest model)
        {
            _validator.Validate(model);

            return await _service.CreateRepairVehicleHistoryAsync(model);
        }

        public Task UpdateRepairVehicleHistoryAsync(UpsertRepairVehicleHistoryRequest model)
        {
            _validator.Validate(model);

            return _service.UpdateRepairVehicleHistoryAsync(model);
        }

        public Task DeleteRepairVehicleHistoryAsync(Guid id) =>
            _service.DeleteRepairVehicleHistoryAsync(id);

        public Task<RepairVehicleHistoryModel> GetRepairVehicleHistoryAsync(Guid id) =>
            _service.GetRepairVehicleHistoryAsync(id);
    }
}
