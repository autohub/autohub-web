using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using api.Domain.Models.Requests.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Services.VehicleHistories;

namespace api.Services.Validation.Decorators
{
    public class ValidationTaxVehicleHistoryServiceDecorator : ITaxVehicleHistoryService
    {
        public readonly IEnumerable<IValidator<BaseRemindableVehicleHistoryRequest>> _validators;
        private readonly ITaxVehicleHistoryService _service;
        private readonly CompositeValidator<BaseRemindableVehicleHistoryRequest> _validator;

        public ValidationTaxVehicleHistoryServiceDecorator(
            ITaxVehicleHistoryService service,
            IEnumerable<IValidator<BaseRemindableVehicleHistoryRequest>> validators)
        {
            _service = service;
            _validators = validators;
            _validator = new CompositeValidator<BaseRemindableVehicleHistoryRequest>(_validators);
        }

        public async Task<Guid> CreateTaxVehicleHistoryAsync(UpsertTaxVehicleHistoryRequest model)
        {
            _validator.Validate(model);

            return await _service.CreateTaxVehicleHistoryAsync(model);
        }

        public Task UpdateTaxVehicleHistoryAsync(UpsertTaxVehicleHistoryRequest model)
        {
            _validator.Validate(model);

            return _service.UpdateTaxVehicleHistoryAsync(model);
        }

        public Task DeleteTaxVehicleHistoryAsync(Guid id) =>
            _service.DeleteTaxVehicleHistoryAsync(id);

        public Task<TaxVehicleHistoryModel> GetTaxVehicleHistoryAsync(Guid id) =>
            _service.GetTaxVehicleHistoryAsync(id);
    }
}
