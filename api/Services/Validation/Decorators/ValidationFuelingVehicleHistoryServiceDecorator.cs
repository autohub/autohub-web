using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using api.Domain.Models.VehicleHistory;
using api.Services.VehicleHistories;

namespace api.Services.Validation.Decorators
{
    public class ValidationFuelingVehicleHistoryServiceDecorator : IFuelingVehicleHistoryService
    {
        public readonly IEnumerable<IValidator<BaseFuelingVehicleHistoryRequest>> _validators;
        private readonly IFuelingVehicleHistoryService _service;
        private readonly CompositeValidator<BaseFuelingVehicleHistoryRequest> _validator;

        public ValidationFuelingVehicleHistoryServiceDecorator(
            IFuelingVehicleHistoryService service,
            IEnumerable<IValidator<BaseFuelingVehicleHistoryRequest>> validators)
        {
            _service = service;
            _validators = validators;
            _validator = new CompositeValidator<BaseFuelingVehicleHistoryRequest>(_validators);
        }

        public async Task<Guid> CreateFuelingVehicleHistoryAsync(UpsertFuelingVehicleHistoryRequest model)
        {
            _validator.Validate(model);

            return await _service.CreateFuelingVehicleHistoryAsync(model);
        }

        public Task UpdateFuelingVehicleHistoryAsync(UpsertFuelingVehicleHistoryRequest model)
        {
            _validator.Validate(model);

            return _service.UpdateFuelingVehicleHistoryAsync(model);
        }

        public Task DeleteFuelingVehicleHistoryAsync(Guid id) =>
            _service.DeleteFuelingVehicleHistoryAsync(id);

        public Task<FuelingVehicleHistoryModel> GetFuelingVehicleHistoryAsync(Guid id) =>
            _service.GetFuelingVehicleHistoryAsync(id);
    }
}
