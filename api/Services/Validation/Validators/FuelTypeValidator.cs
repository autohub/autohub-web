using System;
using api.Data.Contracts;
using api.Domain.Enums;
using api.Domain.Exceptions;
using api.Domain.Models.VehicleHistory;
using api.Common;

namespace api.Services.Validation.Validators
{
    public class FuelingTypeValidator : IValidator<BaseFuelingVehicleHistoryRequest>
    {
        public readonly IApiData _data;

        public FuelingTypeValidator(IApiData data)
        {
            _data = data;
        }

        public void Validate(BaseFuelingVehicleHistoryRequest model)
        {
            var vehicleFuelType = _data.Vehicles.GetById(model.VehicleId)?.FuelType;
            Enum.TryParse(model.Category, out FuelType requestFuelType);

            if (!IsValidFuelingType(vehicleFuelType.Value, requestFuelType))
                throw new DomainException(ErrorMessages.FuelTypeNotTheSame);
        }

        private bool IsValidFuelingType(VehicleFuelType vehicleFuelType, FuelType requestFuelType)
        {
            var isValid = false;

            switch (requestFuelType)
            {
                case FuelType.Gas:
                    if (vehicleFuelType == VehicleFuelType.GasGasoline) isValid = true; break;
                case FuelType.Methane:
                    if (vehicleFuelType == VehicleFuelType.MethaneGasoline) isValid = true; break;
                case FuelType.Gasoline:
                    if (vehicleFuelType == VehicleFuelType.Gasoline
                        || vehicleFuelType == VehicleFuelType.GasGasoline
                        || vehicleFuelType == VehicleFuelType.MethaneGasoline)
                            isValid = true; break;
                case FuelType.Diesel:
                    if (vehicleFuelType == VehicleFuelType.Diesel) isValid = true; break;
                case FuelType.Ethanol:
                    if (vehicleFuelType == VehicleFuelType.Ethanol) isValid = true; break;
                default: throw new DomainException(ErrorMessages.FuelTypeNotTheSame);
            }

            return isValid;
        }
    }
}
