using System;
using System.Collections.Generic;
using api.Data.Contracts;
using api.Domain.Exceptions;
using api.Domain.Models.VehicleHistory;
using api.Common;
using Microsoft.Extensions.Localization;

namespace api.Services.Validation.Validators
{
    public class RemindableValidator<T> : IValidator<BaseRemindableVehicleHistoryRequest>
    {
        private readonly IApiData _data;
        private readonly IStringLocalizer<Resources> _localizer;

        public RemindableValidator(IApiData data, IStringLocalizer<Resources> localizer)
        {
            _data = data;
            _localizer = localizer;
        }

        public void Validate(BaseRemindableVehicleHistoryRequest model)
        {
            var actions = new List<Action<BaseRemindableVehicleHistoryRequest>>
            {
                TryValidateByDateReminder,
                TryValidateByMilageReminder,
            };

            actions.ForEach(x => x.Invoke(model));
        }

        private void TryValidateByMilageReminder(BaseRemindableVehicleHistoryRequest request)
        {
            if (request.IsRemindableByMilage && !IsValidByMilageReminder(request))
                throw new DomainException(_localizer[ErrorMessages.NotValidByMilageReminder]);
        }

        private void TryValidateByDateReminder(BaseRemindableVehicleHistoryRequest request)
        {
            if (request.IsRemindableByDate && !IsValidByDateReminder(request))
                throw new DomainException(_localizer[ErrorMessages.NotValidByDateReminder]);
        }

        private bool IsValidByDateReminder(BaseRemindableVehicleHistoryRequest request) =>
            request.IsRemindableByDate
                && request.DaysRange.HasValue
                && request.DaysRange.Value > 0
                && request.ExpiresOn.HasValue;

        private bool IsValidByMilageReminder(BaseRemindableVehicleHistoryRequest request)
        {
            var vehicle = _data.Vehicles.GetById(request.VehicleId);
            return request.IsRemindableByMilage
                && request.MilageRange.HasValue
                && request.MilageRange.Value > 0
                && request.ExpiresOnMilage > vehicle.Milage;
        }
    }
}
