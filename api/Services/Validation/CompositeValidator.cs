using System.Collections.Generic;

namespace api.Services.Validation
{
    public class CompositeValidator<T> : IValidator<T>
    {
        private readonly IEnumerable<IValidator<T>> _validators;

        public CompositeValidator(IEnumerable<IValidator<T>> validators)
        {
            _validators = validators;
        }

        public void Validate(T model)
        {
            foreach (var validator in _validators)
                validator.Validate(model);
        }
    }
}
