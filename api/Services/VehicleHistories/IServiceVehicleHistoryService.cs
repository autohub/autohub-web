using System;
using System.Threading.Tasks;
using api.Domain.Models.VehicleHistory;
using api.Services.ContainerContracts;

namespace api.Services.VehicleHistories
{
    public interface IRepairVehicleHistoryService : ITransientService
    {
        Task<RepairVehicleHistoryModel> GetRepairVehicleHistoryAsync(Guid id);
        Task<Guid> CreateRepairVehicleHistoryAsync(UpsertRepairVehicleHistoryRequest model);
        Task UpdateRepairVehicleHistoryAsync(UpsertRepairVehicleHistoryRequest model);
        Task DeleteRepairVehicleHistoryAsync(Guid id);
    }
}