using System;
using System.Linq;
using System.Threading.Tasks;
using api.Data.Contracts;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Services.Vehicles;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace api.Services.VehicleHistories
{
    public class FuelingVehicleHistoryService : IFuelingVehicleHistoryService
    {
        private readonly IApiData _data;
        private readonly IMapper _mapper;
        private readonly IVehicleService _vehicleService;

        public FuelingVehicleHistoryService(IApiData data, IMapper mapper, IVehicleService vehicleService)
        {
            _data = data;
            _mapper = mapper;
            _vehicleService = vehicleService;
        }

        public async Task<FuelingVehicleHistoryModel> GetFuelingVehicleHistoryAsync(Guid id) =>
            await Task.FromResult(_data.FuelingVehicleHistories
                .All()
                .Where(v => v.Id == id)
                .ProjectTo<FuelingVehicleHistoryModel>(_mapper.ConfigurationProvider)
                .SingleOrDefault());

        public async Task<Guid> CreateFuelingVehicleHistoryAsync(UpsertFuelingVehicleHistoryRequest model)
        {
            var vehicleHistory = _mapper.Map<UpsertFuelingVehicleHistoryRequest, FuelingVehicleHistory>(model);
            _data.FuelingVehicleHistories.Add(vehicleHistory);

            await _vehicleService.UpdateMilageAsync(model.VehicleId, model.CurrentMilage);
            await _data.SaveChangesAsync();

            return vehicleHistory.Id;
        }

        public async Task UpdateFuelingVehicleHistoryAsync(UpsertFuelingVehicleHistoryRequest model)
        {
            var vehicleHistory = _data.FuelingVehicleHistories.GetById(model.Id);
            if(vehicleHistory == null) throw new ArgumentNullException();

            _mapper.Map(model, vehicleHistory);
            vehicleHistory.UpdatedOn = DateTime.UtcNow;
            _data.FuelingVehicleHistories.Update(vehicleHistory);

            await _vehicleService.UpdateMilageAsync(model.VehicleId, model.CurrentMilage);
            await _data.SaveChangesAsync();
        }

        public async Task DeleteFuelingVehicleHistoryAsync(Guid id)
        {
            _data.FuelingVehicleHistories.Delete(id);
            await _data.SaveChangesAsync();
        }
    }
}
