using System;
using System.Linq;
using System.Threading.Tasks;
using api.Data.Contracts;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Models.Requests.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Services.Vehicles;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace api.Services.VehicleHistories
{
    public class TaxVehicleHistoryService : ITaxVehicleHistoryService
    {
        private readonly IApiData _data;
        private readonly IMapper _mapper;
        private readonly IVehicleService _vehicleService;

        public TaxVehicleHistoryService(
            IApiData data,
            IMapper mapper,
            IVehicleService vehicleService)
        {
            _mapper = mapper;
            _data = data;
            _vehicleService = vehicleService;
        }

        public async Task<TaxVehicleHistoryModel> GetTaxVehicleHistoryAsync(Guid id) =>
            await Task.FromResult(_data.TaxVehicleHistories
                .All()
                .Where(v => v.Id == id)
                .ProjectTo<TaxVehicleHistoryModel>(_mapper.ConfigurationProvider)
                .SingleOrDefault());

        public async Task<Guid> CreateTaxVehicleHistoryAsync(UpsertTaxVehicleHistoryRequest model)
        {
            var vehicleHistory = _mapper.Map<UpsertTaxVehicleHistoryRequest, TaxVehicleHistory>(model);
            _data.TaxVehicleHistories.Add(vehicleHistory);
            model.Id = vehicleHistory.Id;

            await _vehicleService.UpdateMilageAsync(model.VehicleId, model.CurrentMilage);
            await _data.SaveChangesAsync();

            return vehicleHistory.Id;
        }

        public async Task UpdateTaxVehicleHistoryAsync(UpsertTaxVehicleHistoryRequest model)
        {
            var vehicleHistory = _data.TaxVehicleHistories.GetById(model.Id);
            if (vehicleHistory == null) throw new ArgumentNullException();

            _mapper.Map(model, vehicleHistory);
            vehicleHistory.UpdatedOn = DateTime.UtcNow;
            _data.TaxVehicleHistories.Update(vehicleHistory);

            await _vehicleService.UpdateMilageAsync(model.VehicleId, model.CurrentMilage);
            await _data.SaveChangesAsync();
        }

        public async Task DeleteTaxVehicleHistoryAsync(Guid id)
        {
            _data.TaxVehicleHistories.Delete(id);
            await _data.SaveChangesAsync();
        }
    }
}
