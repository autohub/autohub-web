
using System.Linq;
using AutoMapper.QueryableExtensions;
using api.Domain.Models.VehicleHistory;
using System;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using api.Data.Contracts;
using api.Domain.Models.Requests.VehicleHistory;
using api.Domain.Models.Responses.VehicleHistory;
using api.Extensions;
using System.Collections.Generic;
using api.Domain.Entities;
using api.Services.Vehicles;

namespace api.Services.VehicleHistories
{
    public class VehicleHistoryService : IVehicleHistoryService
    {
        private readonly IMapper _mapper;
        private readonly IApiData _data;
        private readonly IVehicleService _vehicleService;

        public VehicleHistoryService(
            IMapper mapper,
            IApiData data,
            IVehicleService vehicleService)
        {
            _mapper = mapper;
            _data = data;
            _vehicleService = vehicleService;
        }

        //TODO: Unit test it
        public async Task<PagedListVehicleHistoriesResponse> GetVehicleHistories(
            string userId, PagedListVehicleHistoriesRequest model)
        {
            var vhs = await _data.FuelingVehicleHistories.All()
                .AsNoTracking()
                .Include(vh => vh.Vehicle)
                .Where(vh => vh.Vehicle.UserId == userId)
                .ProjectTo<GenericVehicleHistory>(_mapper.ConfigurationProvider)
                .ToListAsync();

            vhs.AddRange(await _data.RepairVehicleHistories.All()
                .AsNoTracking()
                .Include(vh => vh.Vehicle)
                .Where(vh => vh.Vehicle.UserId == userId)
                .ProjectTo<GenericVehicleHistory>(_mapper.ConfigurationProvider)
                .ToListAsync());

           vhs.AddRange(await _data.TaxVehicleHistories.All()
                .AsNoTracking()
                .Include(vh => vh.Vehicle)
                .Where(vh => vh.Vehicle.UserId == userId)
                .ProjectTo<GenericVehicleHistory>(_mapper.ConfigurationProvider)
                .ToListAsync());

            var filteredVhs = vhs.AsQueryable()
                .WhereIf(model.VehicleId.HasValue, vh => vh.VehicleId == model.VehicleId.Value)
                .WhereIf(!string.IsNullOrEmpty(model.Type), vh => vh.Type.Equals(model.Type))
                .WhereIf(!string.IsNullOrEmpty(model.Category), vh => vh.Category.Equals(model.Category))
                .OrderByDescending(vh => vh.CreatedOn);

            var pagedVhs = filteredVhs
                .Skip((model.Page - 1) * model.PerPage)
                .Take(model.PerPage)
                .ToList();

            var pagesCount = Convert.ToInt32(
                Math.Ceiling(filteredVhs.Count() / (decimal)model.PerPage));

            return new PagedListVehicleHistoriesResponse()
            {
                Vehicles = await _vehicleService.GetAllByUserId(userId),
                VehicleHistories = pagedVhs,
                PagesCount = pagesCount
            };
        }

        //TODO: UNIT test it! Remove dbcxontext dep
        public bool IsVehicleHistoryOwnedBy(Guid id, string userId)
        {
            var vehicleHistoryFueling = _data.FuelingVehicleHistories.All()
                .AsNoTracking()
                .SingleOrDefault(v => v.UserId == userId && v.Id == id);

            if (vehicleHistoryFueling == null)
            {
                var vehicleHistoryService = _data.RepairVehicleHistories.All()
                    .AsNoTracking()
                    .SingleOrDefault(v => v.UserId == userId && v.Id == id);

                if (vehicleHistoryService == null)
                {
                    var vehicleHistoryTax = _data.TaxVehicleHistories.All()
                        .AsNoTracking()
                        .SingleOrDefault(v => v.UserId == userId && v.Id == id);

                    return vehicleHistoryTax != null;
                }
                else
                {
                    return true;
                }
            }
            else
            {
                return true;
            }
        }

        public async Task UpdateRenew(Guid id)
        {
            var vhTax = _data.TaxVehicleHistories.GetById(id);
            if (vhTax != null)
            {
                vhTax.CompletedOn = DateTime.UtcNow;
                _data.TaxVehicleHistories.Update(vhTax);
            }
            else if (vhTax == null)
            {
                var vhRepair = _data.RepairVehicleHistories.GetById(id);
                vhRepair.CompletedOn = DateTime.UtcNow;
                _data.RepairVehicleHistories.Update(vhRepair);
            }
            await _data.SaveChangesAsync();
        }

        public IEnumerable<GenericVehicleHistory> CombineVehicleHistories(IEnumerable<Vehicle> vehicles)
        {
            var vehicleHistoriesGrouped = vehicles.SelectMany(x =>
                x.TaxVehicleHistories
                   .AsQueryable()
                   .ProjectTo<GenericVehicleHistory>(_mapper.ConfigurationProvider)
                   .ToList()
                )
                .ToList();

            vehicleHistoriesGrouped
                .AddRange(vehicles.SelectMany(x =>
                    x.FuelingVehicleHistories
                    .AsQueryable()
                    .ProjectTo<GenericVehicleHistory>(_mapper.ConfigurationProvider)
                .ToList()));

            vehicleHistoriesGrouped
                .AddRange(vehicles.SelectMany(x =>
                    x.RepairVehicleHistories
                    .AsQueryable()
                    .ProjectTo<GenericVehicleHistory>(_mapper.ConfigurationProvider)
                .ToList()));

            return vehicleHistoriesGrouped;
        }
    }
}
