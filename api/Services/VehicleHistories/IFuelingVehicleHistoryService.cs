using System;
using System.Threading.Tasks;
using api.Domain.Models.VehicleHistory;
using api.Services.ContainerContracts;

namespace api.Services.VehicleHistories
{
    public interface IFuelingVehicleHistoryService : ITransientService
    {
        Task<FuelingVehicleHistoryModel> GetFuelingVehicleHistoryAsync(Guid id);
        Task<Guid> CreateFuelingVehicleHistoryAsync(UpsertFuelingVehicleHistoryRequest model);
        Task UpdateFuelingVehicleHistoryAsync(UpsertFuelingVehicleHistoryRequest model);
        Task DeleteFuelingVehicleHistoryAsync(Guid id);
    }
}