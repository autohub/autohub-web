using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using api.Domain.Models.VehicleHistory;
using api.Services.ContainerContracts;

namespace api.Services.VehicleHistories
{
    public interface IRemindableVehicleHistoryService : ITransientService
    {
        IEnumerable<BaseRemindableVehicleHistoryModel> GetAllRemindableVehicleHistories(Guid vehicleId);
        Task<IEnumerable<RemindableVehicleHistoryModel>> GetIncomingVehicleHistories();
        Task UpdateOccurred(IEnumerable<RemindableVehicleHistoryModel> vehicleHistories);
    }
}