using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using api.Domain.Entities;
using api.Domain.Models.Requests.VehicleHistory;
using api.Domain.Models.Responses.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Services.ContainerContracts;

namespace api.Services.VehicleHistories
{
    public interface IVehicleHistoryService : ITransientService
    {
        Task<PagedListVehicleHistoriesResponse> GetVehicleHistories(
            string userId, PagedListVehicleHistoriesRequest model);

        bool IsVehicleHistoryOwnedBy(Guid id, string userId);

        Task UpdateRenew(Guid id);

        IEnumerable<GenericVehicleHistory> CombineVehicleHistories(IEnumerable<Vehicle> vehicles);
    }
}
