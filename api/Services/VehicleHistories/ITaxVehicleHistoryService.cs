using System;
using System.Threading.Tasks;
using api.Domain.Models.Requests.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Services.ContainerContracts;

namespace api.Services.VehicleHistories
{
    public interface ITaxVehicleHistoryService : ITransientService
    {
        Task<TaxVehicleHistoryModel> GetTaxVehicleHistoryAsync(Guid id);
        Task<Guid> CreateTaxVehicleHistoryAsync(UpsertTaxVehicleHistoryRequest model);
        Task UpdateTaxVehicleHistoryAsync(UpsertTaxVehicleHistoryRequest model);
        Task DeleteTaxVehicleHistoryAsync(Guid id);
    }
}