using System;
using System.Linq;
using System.Threading.Tasks;
using api.Data.Contracts;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Services.Vehicles;
using AutoMapper;
using AutoMapper.QueryableExtensions;

namespace api.Services.VehicleHistories
{
    public class RepairVehicleHistoryService : IRepairVehicleHistoryService
    {
        private readonly IApiData _data;
        private readonly IMapper _mapper;
        private readonly IVehicleService _vehicleService;

        public RepairVehicleHistoryService(
            IApiData data,
            IMapper mapper,
            IVehicleService vehicleService)
        {
            _data = data;
            _mapper = mapper;
            _vehicleService = vehicleService;
        }

        public async Task<RepairVehicleHistoryModel> GetRepairVehicleHistoryAsync(Guid id) =>
            await Task.FromResult(_data.RepairVehicleHistories
                .All()
                .Where(v => v.Id == id)
                .ProjectTo<RepairVehicleHistoryModel>(_mapper.ConfigurationProvider)
                .SingleOrDefault());

        public async Task<Guid> CreateRepairVehicleHistoryAsync(UpsertRepairVehicleHistoryRequest model)
        {
            var vehicleHistory = _mapper.Map<UpsertRepairVehicleHistoryRequest, RepairVehicleHistory>(model);
            _data.RepairVehicleHistories.Add(vehicleHistory);
            model.Id = vehicleHistory.Id;

            await _vehicleService.UpdateMilageAsync(model.VehicleId, model.CurrentMilage);
            await _data.SaveChangesAsync();

            return vehicleHistory.Id;
        }

        public async Task UpdateRepairVehicleHistoryAsync(UpsertRepairVehicleHistoryRequest model)
        {
            var vehicleHistory = _data.RepairVehicleHistories.GetById(model.Id);
            if (vehicleHistory == null) throw new ArgumentNullException();

            _mapper.Map(model, vehicleHistory);
            vehicleHistory.UpdatedOn = DateTime.UtcNow;

            _data.RepairVehicleHistories.Update(vehicleHistory);
            await _vehicleService.UpdateMilageAsync(model.VehicleId, model.CurrentMilage);

            await _data.SaveChangesAsync();
        }

        public async Task DeleteRepairVehicleHistoryAsync(Guid id)
        {
            _data.RepairVehicleHistories.Delete(id);
            await _data.SaveChangesAsync();
        }
    }
}
