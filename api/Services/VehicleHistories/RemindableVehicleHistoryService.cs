using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using api.Data.Contracts;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Microsoft.EntityFrameworkCore;

namespace api.Services.VehicleHistories
{
    public class RemindableVehicleHistoryService : IRemindableVehicleHistoryService
    {
        private readonly IApiData _data;
        private readonly IMapper _mapper;

        public RemindableVehicleHistoryService(IApiData data, IMapper mapper)
        {
            _data = data;
            _mapper = mapper;
        }

        public IEnumerable<BaseRemindableVehicleHistoryModel> GetAllRemindableVehicleHistories(Guid vehicleId)
        {
            var taxVehicleHistories = _data.TaxVehicleHistories.All()
                .Where(x => x.VehicleId == vehicleId)
                .Where(x => x.IsRemindableByDate || x.IsRemindableByMilage)
                .AsNoTracking()
                .ProjectTo<BaseRemindableVehicleHistoryModel>(_mapper.ConfigurationProvider)
                .ToList();

            taxVehicleHistories.AddRange(_data.RepairVehicleHistories.All()
                .Where(x => x.VehicleId == vehicleId)
                .Where(x => x.IsRemindableByDate || x.IsRemindableByMilage)
                .AsNoTracking()
                .ProjectTo<BaseRemindableVehicleHistoryModel>(_mapper.ConfigurationProvider)
                .ToList());

            return taxVehicleHistories;
        }

        public async Task<IEnumerable<RemindableVehicleHistoryModel>> GetIncomingVehicleHistories()
        {
            var taxVehicleHistories = _data.TaxVehicleHistories.All()
                .AsNoTracking()
                .Include(vh => vh.Vehicle)
                .Where(vh => !vh.OccurredOn.HasValue && !vh.CompletedOn.HasValue && (vh.IsRemindableByDate || vh.IsRemindableByMilage))
                .ToList();

            var repairVehicleHistories = _data.RepairVehicleHistories.All()
                .AsNoTracking()
                .Include(vh => vh.Vehicle)
                .Where(vh => !vh.OccurredOn.HasValue && !vh.CompletedOn.HasValue && (vh.IsRemindableByDate || vh.IsRemindableByMilage))
                .ToList();

            var vehicleHistories = taxVehicleHistories
                .AsQueryable()
                .Where(vh => IsExpiring(vh))
                .ProjectTo<RemindableVehicleHistoryModel>(_mapper.ConfigurationProvider)
                .ToList();

            vehicleHistories.AddRange(repairVehicleHistories
                .AsQueryable()
                .Where(vh => IsExpiring(vh))
                .ProjectTo<RemindableVehicleHistoryModel>(_mapper.ConfigurationProvider)
                .ToList());

            await Task.Run(() => vehicleHistories.ForEach(MapExpirationRemaining));

            return vehicleHistories;
        }

        public async Task UpdateOccurred(IEnumerable<RemindableVehicleHistoryModel> vehicleHistories)
        {
            vehicleHistories.ToList().ForEach(UpdateOccurredVehicleHistories);
            await _data.SaveChangesAsync();
        }

        private void UpdateOccurredVehicleHistories(RemindableVehicleHistoryModel model)
        {
            if (model.Type == "Tax")
            {
                var vh = _data.TaxVehicleHistories.GetById(model.Id);
                vh.OccurredOn = DateTime.UtcNow;
            }
            else if (model.Type == "Repair")
            {
                var vh = _data.RepairVehicleHistories.GetById(model.Id);
                vh.OccurredOn = DateTime.UtcNow;
            }
        }

        private readonly static Action<RemindableVehicleHistoryModel> MapExpirationRemaining = x =>
        {
            if (x.IsRemindableByDate)
            {
                var diff = x.ExpiresOn.Value.ToUniversalTime() - DateTime.UtcNow;
                x.ExpirationDaysRemaining = diff.Days;
            }
            if (x.IsRemindableByMilage)
            {
                var diff = x.ExpiresOnMilage - x.Vehicle.Milage;
                x.ExpirationMilageRemaining = diff.Value;
            }
        };

        readonly static Func<BaseRemindableVehicleHistory, bool> IsExpiring = x => IsRemindingByDate(x) || IsRemindingByMilage(x);
        // !IsExpiredByDate(x) && !IsExpiredByMilage(x) && (IsRemindingByDate(x) || IsRemindingByMilage(x));

        readonly static Func<RemindableVehicleHistoryModel, bool> IsExpiredByDate = x =>
        {
            if (!x.IsRemindableByDate) return false;
            var diff = CalcDaysDiff(x.ExpiresOn.Value);
            return diff < 0;
        };

        private readonly static Func<BaseRemindableVehicleHistory, bool> IsExpiredByMilage = x =>
            x.IsRemindableByMilage && x.Vehicle.Milage >= x.ExpiresOnMilage.Value;

        private readonly static Func<BaseRemindableVehicleHistory, bool> IsRemindingByMilage = x =>
            x.IsRemindableByMilage
            && x.ExpiresOnMilage.HasValue
            && x.MilageRange.HasValue
            && IsInMilageRange(x);
        // && !IsExpiredByMilage(x);

        private readonly static Func<BaseRemindableVehicleHistory, bool> IsRemindingByDate = x =>
            x.IsRemindableByDate
            && x.ExpiresOn.HasValue
            // && x.ExpiresOn.Value.ToUniversalTime() > DateTime.UtcNow
            && IsInDateRange(x);
        // && !IsExpiredByDate(x);

        private readonly static Func<BaseRemindableVehicleHistory, bool> IsInDateRange = x =>
        {
            var diff = CalcDaysDiff(x.ExpiresOn.Value);
            return x.DaysRange.Value - diff >= -1;
        };

        private readonly static Func<BaseRemindableVehicleHistory, bool> IsInMilageRange = x =>
            x.ExpiresOnMilage.Value - x.Vehicle.Milage < x.MilageRange.Value;

        private static int CalcDaysDiff(DateTime expiresOn) =>
            (expiresOn.ToUniversalTime() - DateTime.UtcNow).Days;
    }
}
