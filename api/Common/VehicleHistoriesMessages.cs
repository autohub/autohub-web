namespace api.Common
{
    public static class VehicleHistoriesMessages
    {
        public readonly static string ExpireVehicleHistoryEmailTitle = "Reminder for your vehicle history";
        public readonly static string ExpireVehicleHistoryEmailButton = "See details";
        public readonly static string ExpireVehicleHistoryEmailBody = "This is a reminder for your vehicle history of type {0} for vehicle {1} {2}, which";
        public readonly static string BeforeExpireVehicleHistoryByDate = " expires after {0} day/s. ";
        public readonly static string AfterExpireVehicleHistoryByDate = " has expired before {0} day/s. ";
        public readonly static string TodayExpireVehicleHistoryByDate= " expires today ";
        public readonly static string BeforeExpireVehicleHistoryByMilage = " expires after {0} km. ";
        public readonly static string AfterExpireVehicleHistoryByMilage = " has expired before {0} km. ";
    }
}
