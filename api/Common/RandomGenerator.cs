using System;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace api.Common
{
    [ExcludeFromCodeCoverage]
    public static class RandomGenerator
    {
        public static int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        public static string RandomString(int size)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor((26 * random.NextDouble()) + 65)));
                builder.Append(ch);
            }

            return builder.ToString();
        }

        public static string RandomPassword()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(RandomString(4));
            builder.Append(RandomNumber(1000, 9999));
            builder.Append(RandomString(2));

            return builder.ToString();
        }
    }
}
