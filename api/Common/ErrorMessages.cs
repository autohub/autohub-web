namespace api.Common
{
    public static class ErrorMessages
    {
        public const string RequiredField = "{0} field is required. ";
        public const string IncorrectField = "{0} field is incorrect. ";

        public const string UnexpectedError = "Unexpected error. Please try again later. ";
        public const string UserDoesNotExists = "Are you sure this is your email? ";
        public const string UserWrongPassword = "Wrong current password! ";
        public const string PasswordsNotMatch = "Your passwords doesn't match. ";

        public const string LoginWrongEmailOrPassword = "Wrong email or password. ";
        public const string LoginNotConfirmedEmail = "You have to confirm your email in order to login. ";

        public const string FuelTypeNotTheSame = "Fuel type doesn't match with that of the vehicle. ";
        public const string MilageDecrease = "Milage can't be lower than the previous. ";

        public const string StringInvalidRange = "{0} field is not in the range of {2} to {1} symbols. ";

        public const string NotValidByDateReminder = "Invalid data for date reminder. Check again. ";
        public const string NotValidByMilageReminder = "Invalid data for milage reminder. Check again. ";
        public const string InvalidType = "Invalid type";
    }
}
