using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using api.Domain.Enums;
using api.Domain.Entities;
using api.Domain.Entities.VehicleHistory;

namespace api.Data
{
    public class DefaultDbContext : IdentityDbContext<ApplicationUser>
    {
        public DefaultDbContext() { }

        public DefaultDbContext(DbContextOptions<DefaultDbContext> options)
            : base(options) { }

        public virtual DbSet<ApplicationUser> ApplicationUsers { get; set; }
        public virtual DbSet<Vehicle> Vehicles { get; set; }
        public virtual DbSet<FuelingVehicleHistory> FuelingVehicleHistories { get; set; }
        public virtual DbSet<RepairVehicleHistory> RepairVehicleHistories { get; set; }
        public virtual DbSet<TaxVehicleHistory> TaxVehicleHistories { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder
                .Entity<ApplicationUser>()
                .Property(e => e.UserType)
                .HasConversion(new EnumToStringConverter<UserType>());
            builder
                .Entity<ApplicationUser>()
                .Property(e => e.Language);

            builder
                .Entity<Vehicle>()
                .HasOne(v => v.User)
                .WithMany(u => u.Vehicles)
                .HasForeignKey(v => v.UserId);
            builder
                .Entity<Vehicle>()
                .Property(v => v.VehicleCategory)
                .HasConversion(new EnumToStringConverter<VehicleCategory>());
            builder
                .Entity<Vehicle>()
                .Property(v => v.FuelType)
                .HasConversion(new EnumToStringConverter<VehicleFuelType>());

            builder
                .Entity<FuelingVehicleHistory>()
                .HasOne(vs => vs.Vehicle)
                .WithMany(v => v.FuelingVehicleHistories)
                .HasForeignKey(v => v.VehicleId);
            builder
                .Entity<FuelingVehicleHistory>()
                .Property(v => v.Category)
                .HasConversion(new EnumToStringConverter<FuelType>());
            builder
                .Entity<FuelingVehicleHistory>()
                .Property(v => v.Type)
                .HasConversion(new EnumToStringConverter<VehicleHistoryType>());

            builder
                .Entity<RepairVehicleHistory>()
                .HasOne(vs => vs.Vehicle)
                .WithMany(v => v.RepairVehicleHistories)
                .HasForeignKey(v => v.VehicleId);
            builder
                .Entity<RepairVehicleHistory>()
                .Property(v => v.Category)
                .HasConversion(new EnumToStringConverter<RepairVehicleHistoryCategory>());
            builder
                .Entity<RepairVehicleHistory>()
                .Property(v => v.Type)
                .HasConversion(new EnumToStringConverter<VehicleHistoryType>());

            builder
                .Entity<TaxVehicleHistory>()
                .HasOne(vs => vs.Vehicle)
                .WithMany(v => v.TaxVehicleHistories)
                .HasForeignKey(v => v.VehicleId);
            builder
                .Entity<TaxVehicleHistory>()
                .Property(v => v.Category)
                .HasConversion(new EnumToStringConverter<TaxVehicleHistoryCategory>());
            builder
                .Entity<TaxVehicleHistory>()
                .Property(v => v.Type)
                .HasConversion(new EnumToStringConverter<VehicleHistoryType>());
        }
    }
}
