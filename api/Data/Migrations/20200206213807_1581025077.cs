﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class _1581025077 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsRemindable",
                table: "ServiceVehicleHistories");

            migrationBuilder.DropColumn(
                name: "StartsOn",
                table: "ServiceVehicleHistories");

            migrationBuilder.DropColumn(
                name: "IsRemindable",
                table: "ExpenseVehicleHistories");

            migrationBuilder.DropColumn(
                name: "StartsOn",
                table: "ExpenseVehicleHistories");

            migrationBuilder.AddColumn<Guid>(
                name: "ReminderId",
                table: "ServiceVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ReminderId",
                table: "ExpenseVehicleHistories",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "VehicleHistoryReminders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    IsRemindableByDate = table.Column<bool>(nullable: true),
                    ExpiresOn = table.Column<DateTime>(nullable: true),
                    DaysRange = table.Column<long>(nullable: true),
                    IsRemindableByMilage = table.Column<bool>(nullable: true),
                    ExpiresOnMilage = table.Column<long>(nullable: true),
                    MilageRange = table.Column<long>(nullable: true),
                    ExpenseVehicleHistoryId = table.Column<Guid>(nullable: true),
                    ServiceVehicleHistoryId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleHistoryReminders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleHistoryReminders_ExpenseVehicleHistories_ExpenseVehi~",
                        column: x => x.ExpenseVehicleHistoryId,
                        principalTable: "ExpenseVehicleHistories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_VehicleHistoryReminders_ServiceVehicleHistories_ServiceVehi~",
                        column: x => x.ServiceVehicleHistoryId,
                        principalTable: "ServiceVehicleHistories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHistoryReminders_ExpenseVehicleHistoryId",
                table: "VehicleHistoryReminders",
                column: "ExpenseVehicleHistoryId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHistoryReminders_ServiceVehicleHistoryId",
                table: "VehicleHistoryReminders",
                column: "ServiceVehicleHistoryId",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleHistoryReminders");

            migrationBuilder.DropColumn(
                name: "ReminderId",
                table: "ServiceVehicleHistories");

            migrationBuilder.DropColumn(
                name: "ReminderId",
                table: "ExpenseVehicleHistories");

            migrationBuilder.AddColumn<bool>(
                name: "IsRemindable",
                table: "ServiceVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartsOn",
                table: "ServiceVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsRemindable",
                table: "ExpenseVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "StartsOn",
                table: "ExpenseVehicleHistories",
                nullable: true);
        }
    }
}
