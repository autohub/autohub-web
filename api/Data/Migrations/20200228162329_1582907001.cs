﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class _1582907001 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<bool>(
                name: "IsRemindableByMilage",
                table: "ServiceVehicleHistories",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsRemindableByDate",
                table: "ServiceVehicleHistories",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CompletedOn",
                table: "ServiceVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "OccurredOn",
                table: "ServiceVehicleHistories",
                nullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsRemindableByMilage",
                table: "ExpenseVehicleHistories",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AlterColumn<bool>(
                name: "IsRemindableByDate",
                table: "ExpenseVehicleHistories",
                nullable: false,
                oldClrType: typeof(bool),
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CompletedOn",
                table: "ExpenseVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "OccurredOn",
                table: "ExpenseVehicleHistories",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "CompletedOn",
                table: "ServiceVehicleHistories");

            migrationBuilder.DropColumn(
                name: "OccurredOn",
                table: "ServiceVehicleHistories");

            migrationBuilder.DropColumn(
                name: "CompletedOn",
                table: "ExpenseVehicleHistories");

            migrationBuilder.DropColumn(
                name: "OccurredOn",
                table: "ExpenseVehicleHistories");

            migrationBuilder.AlterColumn<bool>(
                name: "IsRemindableByMilage",
                table: "ServiceVehicleHistories",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsRemindableByDate",
                table: "ServiceVehicleHistories",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsRemindableByMilage",
                table: "ExpenseVehicleHistories",
                nullable: true,
                oldClrType: typeof(bool));

            migrationBuilder.AlterColumn<bool>(
                name: "IsRemindableByDate",
                table: "ExpenseVehicleHistories",
                nullable: true,
                oldClrType: typeof(bool));
        }
    }
}
