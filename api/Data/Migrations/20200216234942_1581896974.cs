﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class _1581896974 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleHistoryReminders_ExpenseVehicleHistories_ExpenseVehi~",
                table: "VehicleHistoryReminders");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleHistoryReminders_ServiceVehicleHistories_ServiceVehi~",
                table: "VehicleHistoryReminders");

            migrationBuilder.DropIndex(
                name: "IX_VehicleHistoryReminders_ExpenseVehicleHistoryId",
                table: "VehicleHistoryReminders");

            migrationBuilder.DropIndex(
                name: "IX_VehicleHistoryReminders_ServiceVehicleHistoryId",
                table: "VehicleHistoryReminders");

            migrationBuilder.DropColumn(
                name: "ReminderId",
                table: "ServiceVehicleHistories");

            migrationBuilder.DropColumn(
                name: "ReminderId",
                table: "ExpenseVehicleHistories");

            migrationBuilder.AddColumn<long>(
                name: "DaysRange",
                table: "ServiceVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ExpiresOnMilage",
                table: "ServiceVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsRemindableByDate",
                table: "ServiceVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsRemindableByMilage",
                table: "ServiceVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MilageRange",
                table: "ServiceVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "DaysRange",
                table: "ExpenseVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "ExpiresOnMilage",
                table: "ExpenseVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsRemindableByDate",
                table: "ExpenseVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsRemindableByMilage",
                table: "ExpenseVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<long>(
                name: "MilageRange",
                table: "ExpenseVehicleHistories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHistoryReminders_ExpenseVehicleHistoryId",
                table: "VehicleHistoryReminders",
                column: "ExpenseVehicleHistoryId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHistoryReminders_ServiceVehicleHistoryId",
                table: "VehicleHistoryReminders",
                column: "ServiceVehicleHistoryId");

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleHistoryReminders_ExpenseVehicleHistories_ExpenseVehi~",
                table: "VehicleHistoryReminders",
                column: "ExpenseVehicleHistoryId",
                principalTable: "ExpenseVehicleHistories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleHistoryReminders_ServiceVehicleHistories_ServiceVehi~",
                table: "VehicleHistoryReminders",
                column: "ServiceVehicleHistoryId",
                principalTable: "ServiceVehicleHistories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_VehicleHistoryReminders_ExpenseVehicleHistories_ExpenseVehi~",
                table: "VehicleHistoryReminders");

            migrationBuilder.DropForeignKey(
                name: "FK_VehicleHistoryReminders_ServiceVehicleHistories_ServiceVehi~",
                table: "VehicleHistoryReminders");

            migrationBuilder.DropIndex(
                name: "IX_VehicleHistoryReminders_ExpenseVehicleHistoryId",
                table: "VehicleHistoryReminders");

            migrationBuilder.DropIndex(
                name: "IX_VehicleHistoryReminders_ServiceVehicleHistoryId",
                table: "VehicleHistoryReminders");

            migrationBuilder.DropColumn(
                name: "DaysRange",
                table: "ServiceVehicleHistories");

            migrationBuilder.DropColumn(
                name: "ExpiresOnMilage",
                table: "ServiceVehicleHistories");

            migrationBuilder.DropColumn(
                name: "IsRemindableByDate",
                table: "ServiceVehicleHistories");

            migrationBuilder.DropColumn(
                name: "IsRemindableByMilage",
                table: "ServiceVehicleHistories");

            migrationBuilder.DropColumn(
                name: "MilageRange",
                table: "ServiceVehicleHistories");

            migrationBuilder.DropColumn(
                name: "DaysRange",
                table: "ExpenseVehicleHistories");

            migrationBuilder.DropColumn(
                name: "ExpiresOnMilage",
                table: "ExpenseVehicleHistories");

            migrationBuilder.DropColumn(
                name: "IsRemindableByDate",
                table: "ExpenseVehicleHistories");

            migrationBuilder.DropColumn(
                name: "IsRemindableByMilage",
                table: "ExpenseVehicleHistories");

            migrationBuilder.DropColumn(
                name: "MilageRange",
                table: "ExpenseVehicleHistories");

            migrationBuilder.AddColumn<Guid>(
                name: "ReminderId",
                table: "ServiceVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<Guid>(
                name: "ReminderId",
                table: "ExpenseVehicleHistories",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHistoryReminders_ExpenseVehicleHistoryId",
                table: "VehicleHistoryReminders",
                column: "ExpenseVehicleHistoryId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHistoryReminders_ServiceVehicleHistoryId",
                table: "VehicleHistoryReminders",
                column: "ServiceVehicleHistoryId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleHistoryReminders_ExpenseVehicleHistories_ExpenseVehi~",
                table: "VehicleHistoryReminders",
                column: "ExpenseVehicleHistoryId",
                principalTable: "ExpenseVehicleHistories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_VehicleHistoryReminders_ServiceVehicleHistories_ServiceVehi~",
                table: "VehicleHistoryReminders",
                column: "ServiceVehicleHistoryId",
                principalTable: "ServiceVehicleHistories",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
