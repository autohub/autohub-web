﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class _1584635225 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ServiceVehicleHistories");

            migrationBuilder.CreateTable(
                name: "RepairVehicleHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    CurrentMilage = table.Column<long>(nullable: false),
                    TotalPrice = table.Column<decimal>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    VehicleId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    IsRemindableByDate = table.Column<bool>(nullable: false),
                    ExpiresOn = table.Column<DateTime>(nullable: true),
                    DaysRange = table.Column<long>(nullable: true),
                    IsRemindableByMilage = table.Column<bool>(nullable: false),
                    ExpiresOnMilage = table.Column<long>(nullable: true),
                    MilageRange = table.Column<long>(nullable: true),
                    OccurredOn = table.Column<DateTime>(nullable: true),
                    CompletedOn = table.Column<DateTime>(nullable: true),
                    Category = table.Column<string>(maxLength: 50, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RepairVehicleHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RepairVehicleHistories_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_RepairVehicleHistories_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_RepairVehicleHistories_UserId",
                table: "RepairVehicleHistories",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_RepairVehicleHistories_VehicleId",
                table: "RepairVehicleHistories",
                column: "VehicleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "RepairVehicleHistories");

            migrationBuilder.CreateTable(
                name: "ServiceVehicleHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Category = table.Column<string>(maxLength: 50, nullable: false),
                    CompletedOn = table.Column<DateTime>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CurrentMilage = table.Column<long>(nullable: false),
                    DaysRange = table.Column<long>(nullable: true),
                    ExpiresOn = table.Column<DateTime>(nullable: true),
                    ExpiresOnMilage = table.Column<long>(nullable: true),
                    IsRemindableByDate = table.Column<bool>(nullable: false),
                    IsRemindableByMilage = table.Column<bool>(nullable: false),
                    MilageRange = table.Column<long>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    OccurredOn = table.Column<DateTime>(nullable: true),
                    TotalPrice = table.Column<decimal>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    VehicleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ServiceVehicleHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ServiceVehicleHistories_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ServiceVehicleHistories_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ServiceVehicleHistories_UserId",
                table: "ServiceVehicleHistories",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ServiceVehicleHistories_VehicleId",
                table: "ServiceVehicleHistories",
                column: "VehicleId");
        }
    }
}
