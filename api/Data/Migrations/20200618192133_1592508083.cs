﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class _1592508083 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Reason",
                table: "TaxVehicleHistories");

            migrationBuilder.DropColumn(
                name: "Reason",
                table: "FuelingVehicleHistories");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Reason",
                table: "TaxVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Reason",
                table: "FuelingVehicleHistories",
                nullable: true);
        }
    }
}
