﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class _1584622684 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "VehicleHistoryReminders");

            migrationBuilder.DropTable(
                name: "ExpenseVehicleHistories");

            migrationBuilder.CreateTable(
                name: "TaxVehicleHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    CurrentMilage = table.Column<long>(nullable: false),
                    TotalPrice = table.Column<decimal>(nullable: false),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    VehicleId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<string>(nullable: true),
                    IsRemindableByDate = table.Column<bool>(nullable: false),
                    ExpiresOn = table.Column<DateTime>(nullable: true),
                    DaysRange = table.Column<long>(nullable: true),
                    IsRemindableByMilage = table.Column<bool>(nullable: false),
                    ExpiresOnMilage = table.Column<long>(nullable: true),
                    MilageRange = table.Column<long>(nullable: true),
                    OccurredOn = table.Column<DateTime>(nullable: true),
                    CompletedOn = table.Column<DateTime>(nullable: true),
                    Category = table.Column<string>(maxLength: 50, nullable: false),
                    Reason = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TaxVehicleHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TaxVehicleHistories_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TaxVehicleHistories_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TaxVehicleHistories_UserId",
                table: "TaxVehicleHistories",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_TaxVehicleHistories_VehicleId",
                table: "TaxVehicleHistories",
                column: "VehicleId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TaxVehicleHistories");

            migrationBuilder.CreateTable(
                name: "ExpenseVehicleHistories",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Category = table.Column<string>(maxLength: 50, nullable: false),
                    CompletedOn = table.Column<DateTime>(nullable: true),
                    CreatedOn = table.Column<DateTime>(nullable: false),
                    CurrentMilage = table.Column<long>(nullable: false),
                    DaysRange = table.Column<long>(nullable: true),
                    ExpiresOn = table.Column<DateTime>(nullable: true),
                    ExpiresOnMilage = table.Column<long>(nullable: true),
                    IsRemindableByDate = table.Column<bool>(nullable: false),
                    IsRemindableByMilage = table.Column<bool>(nullable: false),
                    MilageRange = table.Column<long>(nullable: true),
                    Note = table.Column<string>(nullable: true),
                    OccurredOn = table.Column<DateTime>(nullable: true),
                    Reason = table.Column<string>(nullable: true),
                    TotalPrice = table.Column<decimal>(nullable: false),
                    Type = table.Column<string>(nullable: false),
                    UpdatedOn = table.Column<DateTime>(nullable: true),
                    UserId = table.Column<string>(nullable: true),
                    VehicleId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ExpenseVehicleHistories", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ExpenseVehicleHistories_AspNetUsers_UserId",
                        column: x => x.UserId,
                        principalTable: "AspNetUsers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_ExpenseVehicleHistories_Vehicles_VehicleId",
                        column: x => x.VehicleId,
                        principalTable: "Vehicles",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "VehicleHistoryReminders",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    DaysRange = table.Column<long>(nullable: true),
                    ExpenseVehicleHistoryId = table.Column<Guid>(nullable: true),
                    ExpiresOn = table.Column<DateTime>(nullable: true),
                    ExpiresOnMilage = table.Column<long>(nullable: true),
                    IsRemindableByDate = table.Column<bool>(nullable: true),
                    IsRemindableByMilage = table.Column<bool>(nullable: true),
                    MilageRange = table.Column<long>(nullable: true),
                    ServiceVehicleHistoryId = table.Column<Guid>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_VehicleHistoryReminders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_VehicleHistoryReminders_ExpenseVehicleHistories_ExpenseVehi~",
                        column: x => x.ExpenseVehicleHistoryId,
                        principalTable: "ExpenseVehicleHistories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_VehicleHistoryReminders_ServiceVehicleHistories_ServiceVehi~",
                        column: x => x.ServiceVehicleHistoryId,
                        principalTable: "ServiceVehicleHistories",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ExpenseVehicleHistories_UserId",
                table: "ExpenseVehicleHistories",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_ExpenseVehicleHistories_VehicleId",
                table: "ExpenseVehicleHistories",
                column: "VehicleId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHistoryReminders_ExpenseVehicleHistoryId",
                table: "VehicleHistoryReminders",
                column: "ExpenseVehicleHistoryId");

            migrationBuilder.CreateIndex(
                name: "IX_VehicleHistoryReminders_ServiceVehicleHistoryId",
                table: "VehicleHistoryReminders",
                column: "ServiceVehicleHistoryId");
        }
    }
}
