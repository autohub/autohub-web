﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class _1570227979 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsFullTank",
                table: "FuelingVehicleHistories");

            migrationBuilder.DropColumn(
                name: "IsMissedPrevious",
                table: "FuelingVehicleHistories");

            migrationBuilder.AlterColumn<long>(
                name: "Milage",
                table: "Vehicles",
                nullable: false,
                oldClrType: typeof(long),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOn",
                table: "ServiceVehicleHistories",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOn",
                table: "FuelingVehicleHistories",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsPartial",
                table: "FuelingVehicleHistories",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOn",
                table: "ExpenseVehicleHistories",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "IsPartial",
                table: "FuelingVehicleHistories");

            migrationBuilder.AlterColumn<long>(
                name: "Milage",
                table: "Vehicles",
                nullable: true,
                oldClrType: typeof(long));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOn",
                table: "ServiceVehicleHistories",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOn",
                table: "FuelingVehicleHistories",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AddColumn<bool>(
                name: "IsFullTank",
                table: "FuelingVehicleHistories",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "IsMissedPrevious",
                table: "FuelingVehicleHistories",
                nullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "CreatedOn",
                table: "ExpenseVehicleHistories",
                nullable: true,
                oldClrType: typeof(DateTime));
        }
    }
}
