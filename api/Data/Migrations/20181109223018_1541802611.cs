﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace api.Migrations
{
    public partial class _1541802611 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Descrription",
                table: "Vehicles",
                newName: "Description");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ProductionDate",
                table: "Vehicles",
                nullable: true,
                oldClrType: typeof(DateTime));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Description",
                table: "Vehicles",
                newName: "Descrription");

            migrationBuilder.AlterColumn<DateTime>(
                name: "ProductionDate",
                table: "Vehicles",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldNullable: true);
        }
    }
}
