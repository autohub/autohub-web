using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using api.Domain.Enums;
using api.Domain.Entities;
using api.Data.Contracts;

namespace api.Data
{
    public class DefaultDbContextInitializer : IDefaultDbContextInitializer
    {
        private readonly DefaultDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public DefaultDbContextInitializer(
            DefaultDbContext context,
            UserManager<ApplicationUser> userManager)
        {
            _userManager = userManager;
            _context = context;
        }

        public bool EnsureCreated() => _context.Database.EnsureCreated();

        public void Migrate() => _context.Database.Migrate();

        public async Task Seed() => await SeedUsers();

        private async Task SeedUsers()
        {
            const string email = "webmaster@autohub.bg";
            var user = await _userManager.FindByEmailAsync(email);
            if (user == null)
            {
                user = new ApplicationUser
                {
                    UserName = email,
                    Email = email,
                    EmailConfirmed = true,
                    Name = "Webmaster",
                    Language = nameof(SupportedLanguages.en),
                    UserType = UserType.Personal
                };
                await _userManager.CreateAsync(user, "querty123$!");
            }
        }
    }
}
