using System;
using System.Linq;
using api.Data.Contracts;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace api.Data
{
    public class GenericRepository <T> : IRepository<T> where T : class
    {
        public GenericRepository(DefaultDbContext context)
        {
            Context = context ?? throw new ArgumentException("An instance of DbContext is required to use this repository.", "context");
            DbSet = Context.Set<T>();
        }

        protected DbSet<T> DbSet { get; set; }

        protected DefaultDbContext Context { get; set; }

        public virtual IQueryable<T> All() => DbSet.AsQueryable();

        public virtual T GetById(object id) => DbSet.Find(id);

        public virtual void Add(T entity)
        {
            EntityEntry entry = Context.Entry(entity);
            if (entry.State != EntityState.Detached)
                entry.State = EntityState.Added;
            else
                DbSet.Add(entity);
        }

        public virtual void Update(T entity)
        {
            EntityEntry entry = Context.Entry(entity);
            if (entry.State == EntityState.Detached)
                DbSet.Attach(entity);

            entry.State = EntityState.Modified;
        }

        public virtual void Delete(T entity)
        {
            EntityEntry entry = Context.Entry(entity);
            if (entry.State != EntityState.Deleted)
            {
                entry.State = EntityState.Deleted;
            }
            else
            {
                DbSet.Attach(entity);
                DbSet.Remove(entity);
            }
        }

        public virtual void Delete(object id)
        {
            var entity = GetById(id);

            if (entity != null)
                Delete(entity);
        }

        public virtual void Detach(T entity)
        {
            EntityEntry entry = Context.Entry(entity);

            entry.State = EntityState.Detached;
        }
    }
}