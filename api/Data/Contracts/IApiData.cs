using System.Threading.Tasks;
using api.Domain.Entities;
using api.Domain.Entities.VehicleHistory;
using api.Services.ContainerContracts;

namespace api.Data.Contracts
{
    public interface IApiData : ITransientService
    {
        DefaultDbContext Context { get; }

        IRepository<ApplicationUser> Users { get; }

        IRepository<Vehicle> Vehicles { get; }

        IRepository<TaxVehicleHistory> TaxVehicleHistories { get; }

        IRepository<RepairVehicleHistory> RepairVehicleHistories { get; }

        IRepository<FuelingVehicleHistory> FuelingVehicleHistories { get; }

        void Dispose();

        Task<int> SaveChangesAsync();
    }
}