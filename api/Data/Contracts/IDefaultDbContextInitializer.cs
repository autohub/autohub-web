using System.Threading.Tasks;
using api.Services.ContainerContracts;

namespace api.Data.Contracts
{
    public interface IDefaultDbContextInitializer : ITransientService
    {
        bool EnsureCreated();
        void Migrate();
        Task Seed();
    }
}