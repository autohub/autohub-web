using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using api.Data.Contracts;
using api.Domain.Entities;
using api.Domain.Entities.VehicleHistory;

namespace api.Data
{
    public class ApiData : IApiData
    {
        private readonly Dictionary<Type, object> repositories = new Dictionary<Type, object>();

        public ApiData(DefaultDbContext context)
        {
            Context = context;
        }

        public DefaultDbContext Context { get; }

        public IRepository<ApplicationUser> Users => GetRepository<ApplicationUser>();

        public IRepository<Vehicle> Vehicles => GetRepository<Vehicle>();

        public IRepository<TaxVehicleHistory> TaxVehicleHistories => 
            GetRepository<TaxVehicleHistory>();

        public IRepository<RepairVehicleHistory> RepairVehicleHistories => 
            GetRepository<RepairVehicleHistory>();

        public IRepository<FuelingVehicleHistory> FuelingVehicleHistories => 
            GetRepository<FuelingVehicleHistory>();

        public void Dispose() => Dispose();

        public async Task<int> SaveChangesAsync() => await Context.SaveChangesAsync();

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                Context?.Dispose();
        }

        private IRepository<T> GetRepository<T>() where T : class
        {
            if (!repositories.ContainsKey(typeof(T)))
            {
                var type = typeof(GenericRepository<T>);

                repositories.Add(typeof(T), Activator.CreateInstance(type, Context));
            }

            return (IRepository<T>)repositories[typeof(T)];
        }
    }
}