using System.Diagnostics.CodeAnalysis;

namespace api
{
    /// <summary>
    /// Empty class representing reusable resources files for localization
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class Resources
    {
    }
}
