using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using api.Domain.Models.Vehicle;
using System;
using api.Common;
using api.Domain.Exceptions;
using Microsoft.Extensions.Localization;
using api.Services.Users;
using api.Services.Vehicles;

namespace api.Controllers.Private
{
    public class VehiclesController : BasePrivateController
    {
        private readonly IVehicleService _vehicleService;
        private readonly IStringLocalizer<Resources> _localizer;

        public VehiclesController(
            IVehicleService vehicleService,
            IUserService userService,
            IStringLocalizer<Resources> localizer)
            : base(userService)
        {
            _vehicleService = vehicleService;
            _localizer = localizer;
        }

        // GET api/vehicles?page=1&perPage=30
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            try
            {
                return new ObjectResult(
                    await _vehicleService.GetVehiclesOwnedByMe(await UserId));
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }
        }

        // GET api/vehicles/720e118d-0daf-480f-8a8f-3bb9a62018ee
        [HttpGet("{id}", Name = "GetVehicle")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            if (!_vehicleService.IsVehicleOwnedBy(id, await UserId))
                return Unauthorized();

            ObjectResult result;
            try
            {
                result = new ObjectResult(await _vehicleService.GetVehicleAsync(id));
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return result;
        }

        // POST api/vehicles
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]CreateVehicleRequest model)
        {
            try
            {
                model.UserId = await UserId;
                model.Id = await _vehicleService.CreateVehicleAsync(model);
            }
            catch (DomainException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return CreatedAtRoute(
                routeName: "GetVehicle",
                routeValues: new { id = model.Id },
                value: model
            );
        }

        // PUT api/vehicles/
        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody]UpdateVehicleRequest model)
        {
            if (!_vehicleService.IsVehicleOwnedBy(model.Id, await UserId))
                return Unauthorized();

            try
            {
                model.UserId = await UserId;
                await _vehicleService.UpdateVehicleAsync(model);
            }
            catch (DomainException e)
            {
                return BadRequest(new { general = e.Message });
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return Accepted();
        }

        // DELETE api/vehicle/720e118d-0daf-480f-8a8f-3bb9a62018ee
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            if (!_vehicleService.IsVehicleOwnedBy(id, await UserId))
                return Unauthorized();

            try
            {
                await _vehicleService.DeleteVehicleAsync(id);
            }
            catch(Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return Ok();
        }
    }
}
