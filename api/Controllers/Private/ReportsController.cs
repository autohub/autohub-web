using System.Threading.Tasks;
using api.Application.Reports.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers.Private
{
    public class ReportsController : BaseNewPrivateController
    {
        private readonly IMediator _mediator;

        public ReportsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        // POST api/reports
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]GetGraphReportQuery query)
        {
            var report = await _mediator.Send(query);
            return Ok(report);
        }
    }
}
