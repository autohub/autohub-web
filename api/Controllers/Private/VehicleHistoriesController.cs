using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using api.Common;
using api.Domain.Models.Requests.VehicleHistory;
using Microsoft.Extensions.Localization;
using api.Services.Users;
using api.Services.VehicleHistories;

namespace api.Controllers.Private
{
    [Route("api/vehicles/history")]
    public class VehicleHistoriesController : BasePrivateController
    {
        private readonly IVehicleHistoryService _vehicleHistoryService;
        private readonly IStringLocalizer<Resources> _localizer;

        public VehicleHistoriesController(
            IVehicleHistoryService vehicleHistoryService,
            IUserService userService,
            IStringLocalizer<Resources> localizer)
                : base(userService)
        {
            _vehicleHistoryService = vehicleHistoryService;
            _localizer = localizer;
        }

        //GET api/vehicles/history?vehicleId=720e118d-0daf-480f-8a8f-3bb9a62018ee&page=1&perPage=10
        [HttpGet]
        public async Task<IActionResult> GetAsync([FromQuery]PagedListVehicleHistoriesRequest model)
        {
            try
            {
                return new ObjectResult(
                    await _vehicleHistoryService.GetVehicleHistories(await UserId, model));
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }
        }

        //PUT api/vehicles/history/20e118d-0daf-480f-8a8f-3bb9a62018ee/renew
        [HttpPut]
        [Route("{id}/renew")]
        public async Task<IActionResult> UpdateRenew(Guid id)
        {
            var userId = await UserId;
            if (!_vehicleHistoryService.IsVehicleHistoryOwnedBy(id, userId))
                return Unauthorized();

            await _vehicleHistoryService.UpdateRenew(id);
            return Ok();
        }
    }
}
