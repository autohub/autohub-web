using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using api.Domain.Models.VehicleHistory;
using System;
using api.Common;
using api.Domain.Models.Responses.VehicleHistory;
using api.Domain.Exceptions;
using Microsoft.Extensions.Localization;
using api.Services.Users;
using api.Services.VehicleHistories;
using api.Services.Vehicles;

namespace api.Controllers.Private
{
    [Route("api/vehicles/history/fueling")]
    public class FuelingVehicleHistoriesController : BasePrivateController
    {
        private readonly IFuelingVehicleHistoryService _fuelingVehicleHistoryService;
        private readonly IVehicleService _vehicleService;
        private readonly IVehicleHistoryService _vehicleHistoryService;
        private readonly IStringLocalizer<Resources> _localizer;

        public FuelingVehicleHistoriesController(
            IFuelingVehicleHistoryService fuelingVehicleHistoryService,
            IVehicleService vehicleService,
            IVehicleHistoryService vehicleHistoryService,
            IUserService userService,
            IStringLocalizer<Resources> localizer)
                : base(userService)
        {
            _fuelingVehicleHistoryService = fuelingVehicleHistoryService;
            _vehicleService = vehicleService;
            _vehicleHistoryService = vehicleHistoryService;
            _localizer = localizer;
        }

        // GET api/vehicles/history/fueling/720e118d-0daf-480f-8a8f-3bb9a62018ee
        [HttpGet("{id}", Name = "GetFuelingVehicleHistory")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var userId = await UserId;
            if (!_vehicleHistoryService.IsVehicleHistoryOwnedBy(id, userId))
                return Unauthorized();

            ObjectResult result;

            try
            {
                result = new ObjectResult(new DetailsFuelingVehicleHistoryResponse()
                {
                    VehicleHistory =  await _fuelingVehicleHistoryService.GetFuelingVehicleHistoryAsync(id),
                    Vehicles = await _vehicleService.GetAllByUserId(userId)
                });
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return result;
        }

        // POST api/vehicles/history/fueling
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]UpsertFuelingVehicleHistoryRequest model)
        {
            if (!_vehicleService.IsVehicleOwnedBy(model.VehicleId, await UserId))
                return Unauthorized();

            try
            {
                model.UserId = UserId.Result;
                model.Id = await _fuelingVehicleHistoryService.CreateFuelingVehicleHistoryAsync(model);
            }
            catch (DomainException e)
            {
                return BadRequest(new { general = e.Message });
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return CreatedAtRoute(
                routeName: "GetFuelingVehicleHistory",
                routeValues: new { id = model.Id },
                value: model
            );
        }

        // PUT api/vehicle/history/fueling
        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody]UpsertFuelingVehicleHistoryRequest model)
        {
            if (!_vehicleHistoryService.IsVehicleHistoryOwnedBy(model.Id.Value, await UserId))
                return Unauthorized();

            try
            {
                await _fuelingVehicleHistoryService.UpdateFuelingVehicleHistoryAsync(model);
            }
            catch (DomainException e)
            {
                return BadRequest(new { general = e.Message });
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return Accepted();
        }

        // DELETE api/vehicles/history/fueling/720e118d-0daf-480f-8a8f-3bb9a62018ee
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            if (!_vehicleHistoryService.IsVehicleHistoryOwnedBy(id, await UserId))
                return Unauthorized();

            try
            {
                await _fuelingVehicleHistoryService.DeleteFuelingVehicleHistoryAsync(id);
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return Ok();
        }
    }
}
