using System.Threading.Tasks;
using api.Application.Statistics.Queries;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers.Private
{
    public class StatisticsController : BaseNewPrivateController
    {
        private readonly IMediator _mediator;

        public StatisticsController(IMediator mediator)
        {
            _mediator = mediator;
        }

        //GET api/statistics/
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            var stats = await _mediator.Send(new GetStatisticsQuery());
            return Ok(stats);
        }
    }
}
