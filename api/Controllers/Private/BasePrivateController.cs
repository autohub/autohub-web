using System;
using System.Threading.Tasks;
using api.Services.Users;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers.Private
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public abstract class BaseNewPrivateController : Controller
    {
    }

    public abstract class BasePrivateController : BaseNewPrivateController
    {
        protected readonly IUserService userService;

        public BasePrivateController(IUserService userService)
        {
            this.userService = userService;
        }

        protected Task<string> UserId => GetUserIdAsync();

        [Obsolete("Use userservice instead")]
        private async Task<string> GetUserIdAsync()
        {
            var name = HttpContext.User.Identity.Name;
            var user = await userService.FindByEmailAsync(name);
            return user.Id;
        }
    }
}