using System;
using System.Threading.Tasks;
using api.Application.Account.Commands;
using api.Domain.Models.Account;
using api.Common;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using Microsoft.Extensions.Logging;
using api.Services.Users;

namespace api.Controllers.Private
{
    public class AccountController : BasePrivateController
    {
        private readonly IUserService _userService;
        private readonly ILogger _logger;
        private readonly IMediator _mediator;
        private readonly IStringLocalizer<Resources> _localizer;

        public AccountController(IMediator mediator, ILogger<AccountController> logger, IUserService userService, IStringLocalizer<Resources> localizer)
            : base(userService)
        {
            _mediator = mediator;
            _userService = userService;
            _logger = logger;
            _localizer = localizer;
        }

        // GET api/account/
        [HttpGet]
        public async Task<IActionResult> GetAsync()
        {
            ObjectResult result;

            try
            {
                result = new ObjectResult(await _userService.GetUserAccountAsync(await UserId));
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return result;
        }

        // PUT api/account/
        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody] UpdateAccountRequest model)
        {
            try
            {
                await _userService.UpdateUser(await UserId, model);
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return Accepted();
        }

        // PUT api/account/password
        [HttpPut]
        [Route("password")]
        public async Task<IActionResult> PutPasswordAsync([FromBody] UpdateAccountPasswordRequest model)
        {
            try
            {
                await _userService.UpdateUser(await UserId, model);
            }
            catch (ArgumentException)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UserWrongPassword].Value });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return Accepted();
        }

        // PUT api/account/language
        [HttpPut]
        [Route("language")]
        public async Task<IActionResult> SetLanguage([FromBody] SetLanguageCommand command)
        {
            var result = await _mediator.Send(command);

            if (result.WasSuccessful)
                return Accepted();
            else
                return BadRequest(string.Join(", ", result.Errors));
        }
    }
}
