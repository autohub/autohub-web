using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using api.Domain.Models.VehicleHistory;
using System;
using api.Common;
using api.Domain.Models.Responses.VehicleHistory;
using api.Domain.Exceptions;
using Microsoft.Extensions.Localization;
using api.Services.Users;
using api.Services.VehicleHistories;
using api.Services.Vehicles;

namespace api.Controllers.Private
{
    [Route("api/vehicles/history/repair")]
    public class RepairVehicleHistoriesController : BasePrivateController
    {
        private readonly IRepairVehicleHistoryService _repairVehicleHistoryService;
        private readonly IVehicleHistoryService _vehicleHistoryService;
        private readonly IVehicleService _vehicleService;
        private readonly IStringLocalizer<Resources> _localizer;

        public RepairVehicleHistoriesController(
            IRepairVehicleHistoryService repairVehicleHistoryService,
            IVehicleHistoryService vehicleHistoryService,
            IVehicleService vehicleService,
            IUserService userService,
            IStringLocalizer<Resources> localizer)
                : base(userService)
        {
            _vehicleHistoryService = vehicleHistoryService;
            _vehicleService = vehicleService;
            _repairVehicleHistoryService = repairVehicleHistoryService;
            _localizer = localizer;
        }

        // GET api/vehicles/history/repair/720e118d-0daf-480f-8a8f-3bb9a62018ee
        [HttpGet("{id}", Name = "GetRepairVehicleHistory")]
        public async Task<IActionResult> GetAsync(Guid id)
        {
            var userId = await UserId;
            if (!_vehicleHistoryService.IsVehicleHistoryOwnedBy(id, userId))
                return Unauthorized();

            ObjectResult result;
            try
            {
                result = new ObjectResult(new DetailsRepairVehicleHistoryResponse()
                {
                    VehicleHistory = await _repairVehicleHistoryService.GetRepairVehicleHistoryAsync(id),
                    Vehicles = await _vehicleService.GetAllByUserId(userId)
                });
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return result;
        }

        // POST api/vehicles/history/repair
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]UpsertRepairVehicleHistoryRequest model)
        {
            if (!_vehicleService.IsVehicleOwnedBy(model.VehicleId, await UserId))
                return Unauthorized();

            try
            {
                model.UserId = await UserId;
                model.Id = await _repairVehicleHistoryService.CreateRepairVehicleHistoryAsync(model);
            }
            catch (DomainException e)
            {
                return BadRequest(new { general = e.Message });
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return CreatedAtRoute(
                routeName: "GetRepairVehicleHistory",
                routeValues: new { id = model.Id },
                value: model
            );
        }

        // PUT api/vehicle/history/repair
        [HttpPut]
        public async Task<IActionResult> PutAsync([FromBody]UpsertRepairVehicleHistoryRequest model)
        {
            if (!_vehicleHistoryService.IsVehicleHistoryOwnedBy(model.Id.Value, await UserId))
                return Unauthorized();

            try
            {
                await _repairVehicleHistoryService.UpdateRepairVehicleHistoryAsync(model);
            }
            catch (DomainException e)
            {
                return BadRequest(new { general = e.Message });
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return Accepted();
        }

        // DELETE api/vehicles/history/repair/720e118d-0daf-480f-8a8f-3bb9a62018ee
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteAsync(Guid id)
        {
            if (!_vehicleHistoryService.IsVehicleHistoryOwnedBy(id, await UserId))
                return Unauthorized();

            try
            {
                await _repairVehicleHistoryService.DeleteRepairVehicleHistoryAsync(id);
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            return Ok();
        }
    }
}
