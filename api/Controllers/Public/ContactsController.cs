using System.Threading.Tasks;
using api.Common;
using api.Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Localization;
using api.Infrastructure.Email;

namespace api.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ContactsController : Controller
    {
        private readonly IEmailSender _emailSender;
        private readonly ILogger _logger;
        private readonly IStringLocalizer<Resources> _localizer;

        public ContactsController(IEmailSender emailSender, ILoggerFactory loggerFactory, IStringLocalizer<Resources> localizer)
        {
            _emailSender = emailSender;
            _localizer = localizer;
            _logger = loggerFactory.CreateLogger<ContactsController>();
        }

        // POST api/contacts/
        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> PostAsync([FromBody]ContactRequest model)
        {
            try
            {
                await _emailSender.SendEmailAsync(new EmailModel(_localizer)
                {
                    UserEmail = "georgi.marokov@gmail.com",
                    EmailTitle = $"AutoHub Message from {model.Name}",
                    TextContent = $"From: {model.Email}. Message: {model.Message}"
                });
            }
            catch (System.Exception)
            {
                _logger.LogInformation($"Failed Sent email form contacts form.");
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            _logger.LogInformation("Sent email from contacts form.");

            return Ok();
        }
    }
}
