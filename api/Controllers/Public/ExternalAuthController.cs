using System.Threading.Tasks;
using api.Domain.Models.Requests.Auth;
using api.Domain.Models.Responses.Auth;
using api.Common;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Localization;
using api.Services.Users;
using System;

namespace api.Controllers.Public
{
    public class ExternalAuthController : BasePublicController
    {
        private readonly IUserService _userService;
        private readonly IAuthService _authService;
        private readonly IStringLocalizer<Resources> _localizer;

        public ExternalAuthController(IUserService userService, IAuthService authService, IStringLocalizer<Resources> localizer)
        {
            _userService = userService;
            _authService = authService;
            _localizer = localizer;
        }

        // POST api/externalauth/facebook
        [HttpPost("facebook")]
        public async Task<IActionResult> FacebookLogin([FromBody]FacebookLoginRequest model)
        {
            try
            {
                var userResult = await _authService.LoginOrRegisterUser(model);
                if (!userResult.WasSuccessful)
                    return BadRequest(new { general = string.Join(". ", userResult.Errors) });

                var token = _userService.GenerateLoginToken(userResult.Value);

                return Ok(new LoginUserResponse
                {
                    Token = token,
                    Username = userResult.Value.Name,
                    UserEmail = userResult.Value.Email
                });
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }
        }

        // POST api/externalauth/google
        [HttpPost("google")]
        public async Task<IActionResult> GoogleLogin([FromBody]GoogleLoginRequest model)
        {
            try
            {
                var userResult = await _authService.LoginOrRegisterUser(model);
                if (!userResult.WasSuccessful)
                    return BadRequest(new { general = string.Join(". ", userResult.Errors) });

                var token = _userService.GenerateLoginToken(userResult.Value);
                return Ok(new LoginUserResponse
                {
                    Token = token,
                    Username = userResult.Value.Name,
                    UserEmail = userResult.Value.Email
                });
            }
            catch (Exception)
            {
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }
        }
    }
}
