using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using api.Domain.Models.Auth;
using api.Common;
using api.Domain.Models.Responses.Auth;
using Microsoft.Extensions.Localization;
using api.Services.Users;

namespace api.Controllers.Public
{
    public class AuthController : BasePublicController
    {
        private readonly IUserService _userService;
        private readonly IStringLocalizer<Resources> _localizer;
        private readonly ILogger _logger;

        public AuthController(
            IUserService userService,
            ILogger<AuthController> logger,
            IStringLocalizer<Resources> localizer)
        {
            _userService = userService;
            _logger = logger;
            _localizer = localizer;
        }

        // POST api/auth/login
        [HttpPost("login")]
        public async Task<IActionResult> Login([FromBody]LoginUserRequest model)
        {
            var user = await _userService.FindByEmailAsync(model.Username);
            if (user == null || !await _userService.IsValidPasswordAsync(user, model.Password))
                return BadRequest(new { general = _localizer[ErrorMessages.LoginWrongEmailOrPassword].Value });

            if (!await _userService.CanSignInAsync(user))
                return BadRequest(new { general = _localizer[ErrorMessages.LoginNotConfirmedEmail].Value });

            var token = _userService.GenerateLoginToken(user);
            _logger.LogInformation($"User logged in (id: {user.Id})");

            return Ok(new LoginUserResponse
            {
                Token = token,
                Username = user.Name,
                UserEmail = user.Email
            });
        }

        // POST api/auth/register
        [HttpPost("register")]
        public async Task<IActionResult> Register([FromBody]RegisterUserRequest model)
        {
            try
            {
                var result = await _userService.RegisterUser(model);

                if (!result.Succeeded)
                    return BadRequest(new { general = result.Errors.Select(x => x.Description) });
                else
                    return Ok();
            }
            catch (Exception e)
            {
                _logger.LogCritical($"Exception caught: (e: {e.Message} trace: {e.StackTrace} more: {e.Source})");
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }
        }

        // GET api/auth/confirm?userId=1992310&token=234af010skka0123
        [HttpGet("confirm", Name = "ConfirmEmail")]
        public async Task<IActionResult> Confirm(string userId, string token)
        {
            var user = await _userService.FindByIdAsync(userId);
            if (user == null)
                return Redirect("/error/email-confirm");

            var confirmResult = await _userService.ConfirmEmailAsync(user, token);
            if (confirmResult.Succeeded)
            {
                return Redirect("/login/?confirmed=1");
            }
            else
            {
                return Redirect("/error/email-confirm");
            }
        }

        // POST api/auth/password/request
        [HttpPost("password/request")]
        public async Task<IActionResult> ResetPasswordRequest([FromBody]ResetPasswordRequest model)
        {
            try
            {
                await _userService.ResetPasswordRequest(model.Email);
            }
            catch (ArgumentException ar)
            {
                _logger.LogWarning(ar.Message);
                return BadRequest(new { general = ar.Message });
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            _logger.LogInformation("User requested password change.");
            return Ok();
        }

        // POST api/auth/password/reset
        [HttpPost("password/reset")]
        public async Task<IActionResult> ResetPassword([FromBody]ResetPasswordTokenRequest model)
        {
            try
            {
                await _userService.ResetPassword(model);
            }
            catch (Exception e)
            {
                _logger.LogError(e.Message);
                return BadRequest(new { general = _localizer[ErrorMessages.UnexpectedError].Value });
            }

            _logger.LogInformation("User changed password.");
            return Ok();
        }
    }
}
