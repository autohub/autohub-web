using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace api.Controllers.Public
{
    [AllowAnonymous]
    [ApiController]
    [Route("api/[controller]")]
    public abstract class BasePublicController : Controller
    {
    }
}