using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using api.Domain.Models;
using Microsoft.Extensions.Configuration;
using api.Infrastructure.Razor;

namespace api.Infrastructure.Email
{
    public class EmailSender : IEmailSender
    {
        private readonly IConfiguration _configuration;

        public EmailSender(IConfiguration configuration, IOptions<EmailSenderOptions> optionsAccessor)
        {
            _configuration = configuration;
            Options = optionsAccessor.Value;
        }

        public EmailSenderOptions Options { get; }

        public async Task SendEmailAsync(EmailModel model) =>
            await SendMessage(BuildMailMessage(model));

        public async Task SendTemplatedEmail<T>(T model) where T : EmailModel
        {
            model.SiteUrl = _configuration.GetValue<string>("frontEndUrl");
            model.HtmlContent = await TemplateEngine.RunCompile($"{model.ViewName}.cshtml", model);

            await SendMessage(BuildMailMessage(model));
        }

        private MailMessage BuildMailMessage(EmailModel model)
        {
            MailMessage mailMessage = new MailMessage
            {
                From = new MailAddress(Options.EmailFromAddress, Options.EmailFromName)
            };
            mailMessage.To.Add(new MailAddress(model.UserEmail));
            mailMessage.Body = model.TextContent;
            mailMessage.BodyEncoding = Encoding.UTF8;
            mailMessage.Subject = model.EmailTitle;
            mailMessage.SubjectEncoding = Encoding.UTF8;

            if (!string.IsNullOrEmpty(model.HtmlContent))
            {
                AlternateView htmlView = AlternateView.CreateAlternateViewFromString(model.HtmlContent);
                htmlView.ContentType = new System.Net.Mime.ContentType("text/html");
                mailMessage.AlternateViews.Add(htmlView);
            }

            return mailMessage;
        }

        private async Task SendMessage(MailMessage message)
        {
            using SmtpClient client = new SmtpClient(Options.Host, Options.Port);
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(Options.Username, Options.Password);
            client.EnableSsl = Options.EnableSSL;
            await client.SendMailAsync(message);
        }
    }
}
