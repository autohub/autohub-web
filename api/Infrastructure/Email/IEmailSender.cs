using System.Threading.Tasks;
using api.Domain.Models;
using api.Services.ContainerContracts;

namespace api.Infrastructure.Email
{
    public interface IEmailSender : ITransientService
    {
        Task SendTemplatedEmail<T>(T model) where T : EmailModel;
        Task SendEmailAsync(EmailModel model);
    }
}
