using System.Threading.Tasks;
using api.Services.ContainerContracts;

namespace api.Infrastructure.Hangfire
{
    public interface ICronService : ITransientService
    {
        Task EmailUsersWithIncomingVehicleService();
    }
}