
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using api.Domain.Models.VehicleHistory;
using System.Text;
using System.Web;
using System;
using api.Domain.Models;
using api.Common;
using Microsoft.Extensions.Localization;
using System.Globalization;
using System.Threading;
using api.Infrastructure.Email;
using api.Services.Users;
using api.Services.VehicleHistories;
using api.Services.Configs;

namespace api.Infrastructure.Hangfire
{
    public class CronService : ICronService
    {
        private readonly IUserService _userService;
        private readonly IEmailSender _emailSender;
        private readonly IRemindableVehicleHistoryService _reminderService;
        private readonly IConfigurationManager _configuration;
        private readonly ILogger<CronService> _logger;
        private readonly IStringLocalizer<Resources> _localizer;

        public CronService(
            IEmailSender emailSender,
            IUserService userService,
            ILogger<CronService> logger,
            IRemindableVehicleHistoryService reminderService,
            IConfigurationManager configuration,
            IStringLocalizer<Resources> localizer)
        {
            _emailSender = emailSender;
            _userService = userService;
            _logger = logger;
            _reminderService = reminderService;
            _configuration = configuration;
            _localizer = localizer;
        }

        public async Task EmailUsersWithIncomingVehicleService()
        {
            var incomingServices = await _reminderService.GetIncomingVehicleHistories();
            if (!incomingServices.Any()) return;

            foreach (var vehicleHistory in incomingServices)
            {
                var model = await BuildEmailModel(vehicleHistory);
                await _emailSender.SendTemplatedEmail(model);
            }

            await _reminderService.UpdateOccurred(incomingServices);

            _logger.LogInformation("Executed task: EmailUsersWithIncomingVehicleService");
        }

        private async Task<EmailModel> BuildEmailModel(RemindableVehicleHistoryModel vehicleHistory)
        {
            var user = await _userService.FindByIdAsync(vehicleHistory.Vehicle.UserId);
            Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(user.Language);
            Thread.CurrentThread.CurrentUICulture = CultureInfo.CreateSpecificCulture(user.Language);

            return new EmailModel(_localizer)
            {
                UserEmail = user.Email,
                EmailTitle = _localizer[VehicleHistoriesMessages.ExpireVehicleHistoryEmailTitle],
                ActionButtonText = _localizer[VehicleHistoriesMessages.ExpireVehicleHistoryEmailButton],
                TextContent = BuildEmailText(vehicleHistory),
                ActionButtonUrl = await BuildActionUri(vehicleHistory)
            };
        }

        private string BuildEmailText(RemindableVehicleHistoryModel vh)
        {
            var emailText = new StringBuilder(string.Format(
                _localizer[VehicleHistoriesMessages.ExpireVehicleHistoryEmailBody],
                _localizer[vh.Category],
                vh.Vehicle.Make,
                vh.Vehicle.Model));

            if (vh.ExpirationDaysRemaining.HasValue)
                emailText.Append(TextForExpiredByDate(vh.ExpirationDaysRemaining.Value));
            else if (vh.ExpirationMilageRemaining.HasValue)
                emailText.Append(TextForExpiredByMilage(vh.ExpirationMilageRemaining.Value));

            return HttpUtility.HtmlEncode(emailText.ToString());
        }

        private string TextForExpiredByMilage(long milage)
        {
            string message = milage > 0
                ? _localizer[VehicleHistoriesMessages.BeforeExpireVehicleHistoryByMilage]
                : _localizer[VehicleHistoriesMessages.AfterExpireVehicleHistoryByMilage];

            return string.Format(message, Math.Abs(milage));
        }

        private string TextForExpiredByDate(int days)
        {
            string message;
            if (days > 0)
                message = _localizer[VehicleHistoriesMessages.BeforeExpireVehicleHistoryByDate];
            else if (days < 0)
                message = _localizer[VehicleHistoriesMessages.AfterExpireVehicleHistoryByDate];
            else
                message = _localizer[VehicleHistoriesMessages.TodayExpireVehicleHistoryByDate];

            return string.Format(message, Math.Abs(days));
        }

        private async Task<string> BuildActionUri(RemindableVehicleHistoryModel vehicleHistory)
        {
            var uri = new Uri(await _configuration.TryGetValue<string>("frontEndUrl"));
            var uriBuilder = new UriBuilder
            {
                Host = uri.Host,
                Port = uri.Port,
                Path = $"/vehicles/history/{vehicleHistory.Type.ToLower()}/view/{vehicleHistory.Id}/"
            };

            return uriBuilder.Uri.AbsoluteUri;
        }
    }
}
