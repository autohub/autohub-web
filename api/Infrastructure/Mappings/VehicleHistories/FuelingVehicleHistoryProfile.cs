using System;
using api.Domain.Entities.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Extensions;
using AutoMapper;

namespace api.Infrastructure.Mappings.VehicleHistories
{
    public class FuelingVehicleHistoryProfile : Profile
    {
        public FuelingVehicleHistoryProfile()
        {
            CreateMap<UpsertFuelingVehicleHistoryRequest, FuelingVehicleHistory>()
                .ForMember(dest => dest.Category, op => op.MapFrom(src => src.Category));

            CreateMap<UpsertFuelingVehicleHistoryRequest, FuelingVehicleHistory>()
                .ForMember(dest => dest.Category, op => op.MapFrom(src => src.Category))
                .ForMember(dest => dest.IsPartial, op => op.MapFrom(src => src.IsPartial))
                .ForMember(dest => dest.IsMissedPrevious, op => op.MapFrom(src => src.IsMissedPrevious))
                .ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault());

            CreateMap<FuelingVehicleHistory, FuelingVehicleHistoryModel>()
                .ForMember(dest => dest.UpdatedOn, opt => opt.MapFrom(src =>
                    src.UpdatedOn.HasValue
                        ? (DateTime?)new DateTime(src.UpdatedOn.Value.Ticks, DateTimeKind.Utc)
                        : null));

            CreateMap<FuelingVehicleHistory, GenericVehicleHistory>()
                .ForMember(dest => dest.CurrentVehicleMilage, op => op.MapFrom(src => src.Vehicle.Milage));
        }
    }
}
