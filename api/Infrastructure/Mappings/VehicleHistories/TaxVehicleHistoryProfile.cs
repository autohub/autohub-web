using api.Domain.Entities.VehicleHistory;
using api.Domain.Models.Requests.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Extensions;
using AutoMapper;

namespace api.Infrastructure.Mappings.VehicleHistories
{
    public class TaxVehicleHistoryProfile : Profile
    {
        public TaxVehicleHistoryProfile()
        {
            CreateMap<TaxVehicleHistory, TaxVehicleHistoryModel>()
                .ForMember(dest => dest.CurrentVehicleMilage, op => op.MapFrom(src => src.Vehicle.Milage));

            CreateMap<TaxVehicleHistory, GenericVehicleHistory>()
                .ForMember(dest => dest.CurrentVehicleMilage, op => op.MapFrom(src => src.Vehicle.Milage));

            CreateMap<TaxVehicleHistory, BaseRemindableVehicleHistoryModel>()
                .ForMember(dest => dest.CurrentVehicleMilage, op => op.MapFrom(src => src.Vehicle.Milage));

            CreateMap<TaxVehicleHistory, RemindableVehicleHistoryModel>();

            CreateMap<UpsertTaxVehicleHistoryRequest, TaxVehicleHistory>()
                .ForMember(dest => dest.Category, op => op.MapFrom(src => src.Category))
                .ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault());
        }
    }
}
