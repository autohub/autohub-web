using api.Domain.Entities.VehicleHistory;
using api.Domain.Models.VehicleHistory;
using api.Extensions;
using AutoMapper;

namespace api.Infrastructure.Mappings.VehicleHistories
{
    public class RepairVehicleHistoryProfile : Profile
    {
        public RepairVehicleHistoryProfile()
        {
            CreateMap<RepairVehicleHistory, RepairVehicleHistoryModel>()
                .ForMember(dest => dest.CurrentVehicleMilage, op => op.MapFrom(src => src.Vehicle.Milage));

            CreateMap<RepairVehicleHistory, GenericVehicleHistory>()
                .ForMember(dest => dest.CurrentVehicleMilage, op => op.MapFrom(src => src.Vehicle.Milage));

            CreateMap<RepairVehicleHistory, BaseRemindableVehicleHistoryModel>()
                .ForMember(dest => dest.CurrentVehicleMilage, op => op.MapFrom(src => src.Vehicle.Milage));

            CreateMap<RepairVehicleHistory, RemindableVehicleHistoryModel>();

            CreateMap<UpsertRepairVehicleHistoryRequest, RepairVehicleHistory>()
                .ForMember(dest => dest.Category, op => op.MapFrom(src => src.Category))
                .ForAllOtherMembers(opt => opt.IgnoreSourceWhenDefault());
        }
    }
}
