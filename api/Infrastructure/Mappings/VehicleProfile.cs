using System;
using System.Linq;
using api.Domain.Entities;
using api.Domain.Models.Responses.Vehicle;
using api.Domain.Models.Vehicle;
using api.Domain.Models.VehicleHistory;
using api.Domain.Models.Vehicles;
using AutoMapper;

namespace api.Infrastructure.Mappings
{
    public class VehicleProfile : Profile
    {
        public VehicleProfile()
        {
            CreateMap<CreateVehicleRequest, Vehicle>();
            CreateMap<UpdateVehicleRequest, Vehicle>();

            CreateMap<Vehicle, ListVehicleResponse>()
                .ForMember(dest => dest.TotalVehicleHistories, op => op.MapFrom(src =>
                    src.TaxVehicleHistories.Count
                    + src.FuelingVehicleHistories.Count
                    + src.RepairVehicleHistories.Count
                ))
                .ForMember(dest => dest.Title, op => op.MapFrom(src => $"{src.Make} {src.Model}"));

            CreateMap<Vehicle, VehicleWithHistoriesResponse>()
                .ForMember(dest => dest.VehicleHistories, opt => opt.MapFrom(src =>
                    src.TaxVehicleHistories
                    .Select(item => new GenericVehicleHistory()
                    {
                        Id = item.Id,
                        CreatedOn = item.CreatedOn,
                        VehicleId = item.VehicleId,
                        CurrentMilage = item.CurrentMilage,
                        TotalPrice = item.TotalPrice,
                        Type = item.Type.ToString()
                    })
                    .ToList()
                    .Concat(
                        src.FuelingVehicleHistories
                        .Select(item => new GenericVehicleHistory()
                        {
                            Id = item.Id,
                            CreatedOn = item.CreatedOn,
                            VehicleId = item.VehicleId,
                            CurrentMilage = item.CurrentMilage,
                            TotalPrice = item.TotalPrice,
                            Type = item.Type.ToString()
                        })
                        .ToList())
                    .Concat(
                        src.RepairVehicleHistories
                        .Select(item => new GenericVehicleHistory()
                        {
                            Id = item.Id,
                            CreatedOn = item.CreatedOn,
                            VehicleId = item.VehicleId,
                            CurrentMilage = item.CurrentMilage,
                            TotalPrice = item.TotalPrice,
                            Type = item.Type.ToString()
                        })
                        .ToList())
                ));

            CreateMap<Vehicle, VehicleModel>()
                .ForMember(dest => dest.Title, op => op.MapFrom(src => $"{src.Make} {src.Model}"));

            CreateMap<Vehicle, DetailsVehicleResponse>()
                .ForMember(dest => dest.Title, op => op.MapFrom(src => $"{src.Make} {src.Model}"))
                .ForMember(dest => dest.FuelType, op => op.MapFrom(src => src.FuelType.ToString()))
                .ForMember(dest => dest.UpdatedOn, opt => opt.MapFrom(src =>
                    src.UpdatedOn.HasValue ? (DateTime?)new DateTime(src.UpdatedOn.Value.Ticks, DateTimeKind.Utc) : null))
                .ForMember(dest => dest.ProductionDate, opt => opt.MapFrom(src =>
                    src.ProductionDate.HasValue ? (DateTime?)new DateTime(src.ProductionDate.Value.Ticks, DateTimeKind.Utc) : null))
                .ForAllMembers(opts => opts.Condition((_, __, srcMember) => srcMember != null));
        }
    }
}
