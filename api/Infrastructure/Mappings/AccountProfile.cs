using api.Domain.Entities;
using api.Domain.Models.Account;
using AutoMapper;

namespace api.Infrastructure.Mappings
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<UpdateAccountRequest, ApplicationUser>();
            CreateMap<ApplicationUser, AccountResponse>();
        }
    }
}