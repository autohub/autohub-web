using System.Threading.Tasks;
using api.Domain.Models.Requests.Auth;
using api.Services.Users;
using Microsoft.Extensions.Options;
using static Google.Apis.Auth.GoogleJsonWebSignature;

namespace api.Infrastructure.Apis.Google
{
    public class GoogleAuthClient : IGoogleAuthClient
    {
        private readonly AuthClientOptions _options;

        public GoogleAuthClient(IOptions<AuthClientOptions> options)
        {
            _options = options.Value;
        }

        public async Task<Payload> GetUserAccount(GoogleLoginRequest model)
        {
            return await ValidateAsync(model.IdToken, new ValidationSettings()
            {
                Audience = new[] { _options.GoogleAppId }
            });
        }
    }
}
