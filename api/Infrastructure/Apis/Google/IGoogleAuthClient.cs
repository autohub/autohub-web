using System.Threading.Tasks;
using api.Domain.Models.Requests.Auth;
using api.Services.ContainerContracts;
using static Google.Apis.Auth.GoogleJsonWebSignature;

namespace api.Infrastructure.Apis.Google
{
    public interface IGoogleAuthClient : ITransientService
    {
        Task<Payload> GetUserAccount(GoogleLoginRequest model);
    }
}
