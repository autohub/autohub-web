using Newtonsoft.Json;

namespace api.Infrastructure.Apis.Facebook
{
    public class FacebookAppAccessToken
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("token_type")]
        public string TokenType { get; set; }
    }
}
