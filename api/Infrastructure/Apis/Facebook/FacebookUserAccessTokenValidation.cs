using Newtonsoft.Json;

namespace api.Infrastructure.Apis.Facebook
{
    public class FacebookUserAccessTokenValidation
    {
        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("data_access_expires_at")]
        public int DataAccessExpiresAt { get; set; }

        [JsonProperty("expires_at")]
        public int ExpiresAt { get; set; }

        [JsonProperty("is_valid")]
        public bool IsValid { get; set; }

        [JsonProperty("user_id")]
        public string UserId { get; set; }
    }
}
