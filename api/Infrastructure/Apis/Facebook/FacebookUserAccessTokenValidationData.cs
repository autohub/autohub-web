using Newtonsoft.Json;

namespace api.Infrastructure.Apis.Facebook
{
    public class FacebookUserAccessTokenValidationData
    {
        [JsonProperty("data")]
        public FacebookUserAccessTokenValidation ValidationData { get; set; }
    }
}
