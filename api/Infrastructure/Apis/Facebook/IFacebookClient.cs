using System.Threading.Tasks;

namespace api.Infrastructure.Apis.Facebook
{
    public interface IFacebookClient
    {
        Task<FacebookAppAccessToken> GetAppAccessToken();
        Task<FacebookUserAccessTokenValidation> GetUserTokenValidation(string userAccessToken, string appAccessToken);
        Task<FacebookUserModel> GetUserData(string accessToken);
    }
}
