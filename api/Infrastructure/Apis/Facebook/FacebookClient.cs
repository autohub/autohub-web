using System;
using System.Net.Http;
using System.Threading.Tasks;
using api.Services.Users;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;

namespace api.Infrastructure.Apis.Facebook
{
    public class FacebookClient : IFacebookClient
    {
        private const string FACEBOOK_API_URL = "https://graph.facebook.com/v2.8";
        private readonly HttpClient _client;
        private readonly AuthClientOptions _options;

        public FacebookClient(
            HttpClient httpClient,
            IOptions<AuthClientOptions> options)
        {
            _options = options.Value;
            httpClient.BaseAddress = new Uri(FACEBOOK_API_URL);
            httpClient.DefaultRequestHeaders.Add("User-Agent", "HttpClientFactory");
            _client = httpClient;
        }

        public async Task<FacebookAppAccessToken> GetAppAccessToken()
        {
            var appAccessTokenResponse = await _client.GetStringAsync(
                $"/oauth/access_token?client_id={_options.FacebookAppId}&client_secret={_options.FacebookAppSecret}&grant_type=client_credentials");

            return JsonConvert.DeserializeObject<FacebookAppAccessToken>(appAccessTokenResponse);
        }

        public async Task<FacebookUserAccessTokenValidation> GetUserTokenValidation(string userAccessToken, string appAccessToken)
        {
            var userAccessTokenValidationResponse = await _client
                .GetStringAsync(
                    $"/debug_token?input_token={userAccessToken}&access_token={appAccessToken}");
            var userAccessTokenValidationData = JsonConvert.DeserializeObject<FacebookUserAccessTokenValidationData>(userAccessTokenValidationResponse);

            return userAccessTokenValidationData.ValidationData;
        }

        public async Task<FacebookUserModel> GetUserData(string accessToken)
        {
            var userInfoResponse = await _client.GetStringAsync(
                $"/me?fields=id,email,first_name,last_name,name,picture&access_token={accessToken}");

            return JsonConvert.DeserializeObject<FacebookUserModel>(userInfoResponse);
        }
    }
}
