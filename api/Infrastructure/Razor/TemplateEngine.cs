using System.IO;
using System.Threading.Tasks;
using RazorLight;

namespace api.Infrastructure.Razor
{
    public static class TemplateEngine
    {
        private static readonly string TEMPLATES_PATH = $"{Directory.GetCurrentDirectory()}/Templates/Email";

        /// <summary>
        /// Generate an HTML document from the specified Razor template and model.
        /// </summary>
        /// <param name="templateName">The name of the Razor template (.cshtml)</param>
        /// <param name="model">The model containing the information to be supplied to the Razor template</param>
        /// <returns>HTML representing the email ready to be sent</returns>
        public static async Task<string> RunCompile(string templateName, object model)
        {
            var engine = new RazorLightEngineBuilder()
              .UseFileSystemProject(TEMPLATES_PATH)
              .UseMemoryCachingProvider()
              .Build();

            return await engine.CompileRenderAsync(templateName, model);
        }
    }
}
